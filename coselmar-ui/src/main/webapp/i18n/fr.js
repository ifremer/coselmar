/*
 * #%L
 * Coselmar :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
var translateFR = {

"application.title"             :   "Plateforme",

"menu.item.users"               :   "Utilisateurs",
"menu.item.users.list"          :   "Liste",
"menu.item.users.add"           :   "Ajouter un utilisateur",
"menu.item.users.add.client"    :   "Ajouter un client",
"menu.item.questions"           :   "Projets",
"menu.item.questions.list"      :   "Liste",
"menu.item.questions.add"       :   "Ajouter un projet",
"menu.item.documents"           :   "Documents",
"menu.item.documents.list"      :   "Liste",
"menu.item.documents.add"       :   "Ajouter un document",
"menu.item.referential"         :   "Référentiel",
"menu.item.admin"               :   "Admin",
"menu.item.links"               :   "Liens utiles",

"menu.button.login"             :   "Connexion",
"menu.button.profile"           :   "Profil",
"menu.button.logout"            :   "Déconnexion",

//Footer

"footer.item.licence"                           :   "License",
"footer.item.report"                            :   "Rapport de bug",
"footer.item.support"                           :   "Support utilisateur",

//Documents part

"document.list.title"                           :   "Liste des documents",
"document.modalSearch.title"                    :   "Recherche de documents",
"document.button.add"                           :   "Ajouter un document",
"document.message.delete"                       :   "Êtes vous sûrs de vouloir supprimer ce document ?",
"document.view.title"                           :   "Détails du document",
"document.create.title"                         :   "Nouveau document",
"document.create.info"                          :   "Vous pouvez ajouter un nouveau document à la base de connaissance. Pour chaque document,\
                                                    des mots clefs sont nécessaires pour facilier la recherche des documents, et vous pouvez\
                                                    précisez la visibilité :\
                                                    <ul>\
                                                      <li>Publique : tous les membres peuvent accéder au document</li>\
                                                      <li>Privé : vous seul pouvez accéder au document</li>\
                                                      <li>Restreint : le document ne sera accessible qu'aux participants d'un projet ou est attaché le document</li>\
                                                    </ul>",
"document.bibliography.title"                   :   "Bibliographie",

"document.metadata.name"                        :   "Titre",
"document.metadata.authors"                     :   "Auteurs",
"document.metadata.privacy"                     :   "Visibilité",
"document.metadata.keywords"                    :   "Mots clefs",
"document.metadata.depositor"                   :   "Déposant",
"document.metadata.depositDate"                 :   "Date de dépôt",
"document.metadata.relatedQuestions"            :   "Projets relatifs",
"document.metadata.view.relatedQuestions"       :   "Projets utilisant ce document",
"document.metadata.type"                        :   "Type",
"document.metadata.type.publication"            :   "Publication",
"document.metadata.type.meetingPublication"     :   "Publication dans des colloques ou séminaires",
"document.metadata.type.periodicalPublication"  :   "Publication dans une revue",
"document.metadata.type.thesis"                 :   "Thèse",
"document.metadata.type.report"                 :   "Rapport",
"document.metadata.type.scientificReport"       :   "Rapport scientifique",
"document.metadata.type.scientificWork"         :   "Ouvrage scientifique",
"document.metadata.type.chapterWork"            :   "Chapitre d'ouvrage",
"document.metadata.type.contractReport"         :   "Rapport de contrat",
"document.metadata.type.expertise"              :   "Expertise/Avis",
"document.metadata.type.poster"                 :   "Poster",
"document.metadata.type.conferenceArticle"      :   "Acte de colloque",
"document.metadata.type.conferenceSummary"      :   "Communication sans actes",
"document.metadata.type.data"                   :   "Données",
"document.metadata.type.other"                  :   "Autre",
"document.metadata.file"                        :   "Fichier",
"document.metadata.mimetype"                    :   "Type Mime",
"document.metadata.externalUrl"                 :   "URL externe",
"document.metadata.copyright"                   :   "Copyright",
"document.metadata.licence"                     :   "License",
"document.metadata.language"                    :   "Langue",
"document.metadata.publicationDate"             :   "Date de publication",
"document.metadata.summary"                     :   "Résumé",
"document.metadata.comment"                     :   "Commentaire",
"document.metadata.fileName"                    :   "Nom du fichier associé",
"document.metadata.externalLink"                :   "Lien externe",
"document.metadata.citation"                    :   "Citation",

"document.metadata.depositBefore"               :   "Déposé avant le",
"document.metadata.depositAfter"                :   "Déposé après le",
"document.metadata.publishBefore"               :   "Publié avant le",
"document.metadata.publishAfter"                :   "Publié après le",

"document.message.requiredName"                 :   "Le nom du document est requis.",
"document.message.requiredType"                 :   "Le type du document est requis.",
"document.message.requiredExternalUrlOrFile"    :   "Un fichier ou un lien externe est requis.",
"document.message.requiredExternalUrl"          :   "Pour ce document, une URL externe est requise.",
"document.message.requiredKeywords"             :   "Au moins un mot clef est requis.",
"document.message.requiredAuthors"              :   "Le ou les auteur(s) du document est requis.",
"document.message.requiredCopyright"            :   "Le copyright du document est requis.",
"document.message.requiredSummary"              :   "Un résumé est requis.",
"document.message.requiredCitation"             :   "La citation est requise.",
"document.privacy.questionRestricted"           :   "Restreint aux participants de projet.",
"document.privacy.restricted.info"              :   "Restreint aux experts indiqués et aux projets incluant le document",
"document.message.created"                      :   "Document créé.",
"document.message.updated"                      :   "Document modifié.",
"document.message.deleted"                      :   "Document supprimé.",

"document.message.info.privacy"                 :   "La visibilité définit qui pourra accéder au document :<ul>\
                                                        <li>Privé : seulement vous et les superviseurs,</li>\
                                                        <li>Publique : tout le monde.</li>\
                                                        <li>Restreint : vous, les experts selectionnés et les membres des projets utilisant le document.</li>\
                                                     </ul>",
"document.message.info.keywords"                :   "Les mots-clefs permettent de classifier le projet et faciliter sa recherche. Chaque mot-clef saisi doit être validé avec le bouton \"Ajouter\".",
"document.message.info.referredInProjects"      :   "Le document est référencé par des projets et ne peut donc être édité.",

"document.button.download"                      :   "Télécharger",
"document.button.openLink"                      :   "Ouvrir le lien",
"document.button.removeFile"                    :   "Supprimer le fichier",
"document.button.generateBibliography"          :   "Générer la bibliographie",

//Questions part

"question.list.title"                           :   "Projets",
"question.add.title"                            :   "Ajouter un projet",
"question.newDocument.title"                    :   "Contribuer avec de nouveaux documents",
"question.modal.parents.title"                  :   "Désigner un projet parent",
"question.modal.hierarchy.title"                :   "Arborescence du projet",

"question.metadata.title"                       :   "Titre",
"question.metadata.type"                        :   "Type",
"question.metadata.type.reunion"                :   "Réunion (séminaire, table ronde, ...)",
"question.metadata.type.expertise"              :   "Expertise",
"question.metadata.type.project"                :   "Projet (montage, brainstorming, ...)",
"question.metadata.privacy"                     :   "Visibilité",
"question.metadata.themes"                      :   "Thèmes",
"question.metadata.submissionDate"              :   "Date de soumission",
"question.metadata.deadline"                    :   "Date limite",
"question.metadata.summary"                     :   "Résumé",
"question.metadata.status"                      :   "Status",
"question.metadata.supervisors"                 :   "Superviseurs",
"question.metadata.experts"                     :   "Experts",
"question.metadata.externalExperts"             :   "Experts extérieurs",
"question.metadata.clients"                     :   "Clients",
"question.metadata.contributors"                :   "Contributeurs",
"question.metadata.relatedDocuments"            :   "Documents associés",
"question.metadata.conclusion"                  :   "Conclusion",
"question.metadata.conclusionDocuments"         :   "Documents de conclusion",
"question.metadata.parents"                     :   "Projets parents",
"question.metadata.parentsPhrase"               :   "Provoqué ou inspiré par",
"question.metadata.childrenPhrase"              :   "A provoqué ou inspiré",
"question.metadata.participants"                :   "Participants",
"question.metadata.links"                       :   "Liens associés",

"question.metadata.hierarchy.display"           :   "Voir l'arborescence",

"question.metadata.submitBefore"                :   "Soumis avant le",
"question.metadata.submitAfter"                 :   "Soumis après le",
"question.metadata.deadlineBefore"              :   "Date limite avant le",
"question.metadata.deadlineAfter"               :   "Date limite après le",

"question.message.requiredTitle"                :   "Le titre est requis, avec au minimum 10 caractères.",
"question.message.requiredType"                 :   "Le type est requis..",
"question.message.requiredSummary"              :   "Un résumé est requis.",
"question.message.requiredThemes"               :   "Au moins un thème est requis.",
"question.message.saved"                        :   "Projet enregistré.",
"question.message.deleted"                      :   "Projet supprimé.",
"question.message.closed"                       :   "Le projet a été fermé.",
"question.message.reopened"                     :   "Le projet a été rouvert.",
"question.message.adjourned"                    :   "Le projet a été ajourné.",
"question.message.newDocuments.added"           :   "Les nouveaux documents ont été ajoutés au projet",

"question.button.add"                           :   "Ajouter un projet",
"question.button.reopen"                        :   "Réouvrir",
"question.button.close"                         :   "Fermer",
"question.button.close.fullText"                :   "Fermer ce projet",
"question.button.adjourn"                       :   "Ajourner",
"question.button.validateChanges"               :   "Valider les modifications",
"question.button.validateNewDocuments"          :   "Valider les nouveaux documents",

"question.message.notAvailable"                 :   "Cet projet n'est pas disponible à la consultation.",
"question.message.notAllowedToCreate"           :   "Vous n'êtes pas autorisé à déposer un projet",
"question.message.delete"                       :   "Êtes vous sûr de vouloir supprimer ce projet ?",
"question.message.closedOn"                     :   "Fermé le ",
"question.message.info.type"                    :   "Le type peut être l'un des trois suivants :<ul>\
                                                        <li>Réunion : séminaire, table ronde, ...</li>\
                                                        <li>Expertise</li>\
                                                        <li>Projet : tout ce qui tourne autour d'un projet comme le montage ou les brainstorming</li>\
                                                     </ul>",
"question.message.info.privacy"                 :   "La visibilité définit qui pourra accéder au contenu du projet :<ul>\
                                                        <li>Privé : seuls les experts assignés,</li>\
                                                        <li>Publique : tous les experts de l'application.</li>\
                                                     </ul>\
                                                     À noter que les clients du projet ont aussi accès à celui-ci.",
"question.message.info.themes"                  :   "Les thèmes permettent de classifier le projet et faciliter sa recherche. Chaque thème saisi doit être validé avec le bouton \"Ajouter\".",
"question.message.info.contributors"            :   "Les contributeurs sont des experts ayant été affectés au projet et qui ne le sont plus.",

// Links

"link.metadata.url"                             :   "Adresse web",
"link.metadata.name"                            :   "Nom",

"link.create.title"                             :   "Ajouter un lien",
"link.update.title"                             :   "Mettre à jour le lien",

//Users part

"user.list.title"                               :   "Liste des utilisateurs",
"user.info.title"                               :   "Données de l'utilisateur",
"user.projects.title"                           :   "Projets de l'utilisateur",
"user.create.title"                             :   "Ajouter un utilisateur",
"user.create.info"                              :   "Créer un nouvel utilisateur, avec l'un des rôles suivants :\
                                                     <ul>\
                                                       <li>Superviseur : Manager qui relaie les projets de clients et qui assigne les experts aux projets</li>\
                                                       <li>Expert : Scientifique, qui peut ajouter des nouveaux documents à la base de connaissance et voir les documents des autres scientifiques (en fonction des permissions)</li>\
                                                       <li>Membre</li>\
                                                       <li>Client</li>\
                                                     </ul>",
"user.create.client.info"                       :   "Créer un nouvel utilisateur de rôle \"Client\"",
"user.password.title"                           :   "Envoi d'un nouveau mot de passe",

"user.metadata.name"                            :   "Nom",
"user.metadata.firstName"                       :   "Prénom",
"user.metadata.mail"                            :   "Contact",
"user.metadata.mail.login"                      :   "Identifiant",
"user.metadata.phone"                           :   "Numéro de téléphone",
"user.metadata.qualification"                   :   "Profession",
"user.metadata.organization"                    :   "Organisation",
"user.metadata.role"                            :   "Rôle",
"user.metadata.status"                          :   "Status",
"user.metadata.status.disable"                  :   "Désactivé",
"user.metadata.status.enable"                   :   "Activé",
"user.metadata.password"                        :   "Mot de passe",
"user.metadata.password.new"                    :   "Nouveau mot de passe",
"user.metadata.role.all"                        :   "Tous",
"user.metadata.role.admin"                      :   "Admin",
"user.metadata.role.supervisor"                 :   "Superviseur",
"user.metadata.role.expert"                     :   "Expert",
"user.metadata.role.member"                     :   "Membre",
"user.metadata.role.client"                     :   "Client",

"user.metadata.projects"                        :   "Liste des projets",
"user.metadata.projects.asSupervisor"           :   "Projets en tant que superviseur",
"user.metadata.projects.asParticipant"          :   "Projets en tant qu'expert",
"user.metadata.projects.asContributor"          :   "Projets en tant que contributeur",
"user.metadata.projects.asClient"               :   "Projets en tant que client",

"user.message.disable"                          :   "Êtes vous sûr de vouloir désactiver cet utilisateur ?",
"user.message.delete"                           :   "Êtes vous sûr de vouloir supprimer cet utilisateur ",
"user.message.requiredFirsName"                 :   "Le prénom est obligatoire.",
"user.message.requiredName"                     :   "Le nom est obligatoire.",
"user.message.invalidMail"                      :   "Entrer une adresse courriel valide.",
"user.message.requiredQualification"            :   "La profession est obligatoire.",
"user.message.requiredPassword"                 :   "Le mot de passe est obligatoire pour valider les modifications.",
"user.label.currentPasswordToValidate"          :   "Entrer votre mot de passe pour valider les modifications",
"user.message.wannaChangePassword"              :   "Je veux changer le mot de passe",
"user.message.tooShortPassword"                 :   "Le nouveau mot de passe doit contenir au moins 6 caractères.",
"user.message.passwordsNoMatch"                 :   "Les deux mots de passe entrés sont différents.",
"user.message.login"                            :   "Connectez-vous :",
"user.message.forgotPassword"                   :   "J'ai oublié mon mot de passe",
"user.message.enterMail"                        :   "Veuillez entrer votre courriel",
"user.message.unknownMail"                      :   "Ce courriel est inconnu.",
"user.message.newPasswordSent"                  :   "Un nouveau mot de passe a été envoyé.",
"user.message.wannaSimpleLogin"                 :   "Je souhaite utiliser un simple identifiant et non un courriel (Attention : aucun mot de passe ne pourra être envoyé.)",
"user.message.invalidLogin"                     :   "L'identifiant est obligatoire.",
"user.message.created"                          :   "Utilisateur créé.",
"user.message.updated"                          :   "Utilisateur modifié.",
"user.message.deleted"                          :   "Utilisateur supprimé.",
"user.message.disable"                          :   "Utilisateur désactivé.",
"user.message.enable"                           :   "Utilisateur réactivé.",
"user.message.mail.alreadyExisting"             :   "Le courriel (ou identifiant) saisi est déjà attribué.",
"user.message.login.fail"                       :   "La connexion a échoué, veuillez réessayer.",
"user.message.noProject"                        :   "Cet utilisateur n'a pas de projet",

"user.button.add"                               :   "Ajouter un utilisateur",
"user.button.add.client"                        :   "Ajouter un client",
"user.button.disable"                           :   "Désactiver",
"user.button.enable"                            :   "Activer",
"user.button.showDisable"                       :   "Montrer les utilisateurs désactivés",
"user.button.newPassword"                       :   "Envoyer",

//Referential part

"referential.search.title"                      :   "Rechercher dans le référentiel",
"referential.search.keywords"                   :   "Mots clefs",
"referential.search.documents"                  :   "Documents",
"referential.search.questions"                  :   "Projets",
"referential.search.singleDocumentFound"        :   "document trouvé.",
"referential.search.multipleDocumentsFound"     :   "documents trouvés.",
"referential.search.singleQuestionFound"        :   "projet trouvé.",
"referential.search.multipleQuestionsFound"     :   "projets trouvés.",
"referential.search.noDocumentFound"            :   "Aucun document trouvé.",
"referential.search.noQuestionFound"            :   "Aucun projet trouvé.",

//Admin part

"admin.tools.title"                             :   "Outils Admin",
"admin.tools.label.luceneRefresh"               :   "Rafraichir l'index de recherche.",
"admin.tools.button.refresh"                    :   "Rafraichir !",
"admin.tools.message.refresh.inprogress"        :   "Mise à jour en cours.",
"admin.tools.message.refresh.done"              :   "L'index de recherche à bien été raffraichi !",
"admin.tools.message.refresh.errors"            :   "Une erreur est survenue durant la mise à jour de l'index de recherche. Veuillez contacter l'administrateur serveur.",
"admin.tools.label.documentsImport"             :   "Importer un Zip de Documents",
"admin.tools.button.documentsimport"            :   "Importer !",
"admin.tools.message.massimport.inprogress"     :   "Import en cours.",
"admin.tools.message.massimport.done"           :   "Les documents ont bien été importés !",
"admin.tools.message.massimport.errors"         :   "Des erreurs sont survenues durant l'import. Veuillez contacter l'administrateur serveur.",
"admin.tools.message.massimport.fail"           :   "Des erreurs sont survenues durant le traitement des fichiers.",
"admin.tools.message.massimport.zipReadFail"    :   "Impossible de lire le fichier zip, peut-être est-il corrompu ?",
"admin.tools.message.massimport.csvFindFail"    :   "Le fichier 'description.csv' est manquant.",
"admin.tools.message.massimport.csvReadFail"    :   "Erreur dans le traitement du fichier 'description.csv'. Veuillez vérifier le format des données.",
"admin.tools.message.massimport.fileReadFail"   :   "Erreur dans le traitement de fichiers contenus dans le zip.",
"admin.tools.message.massimport.missingFiles"   :   "Fichiers non retrouvés dans le zip : ",
"admin.tools.message.massimport.missingAttachments"   :   "Des documents n'ont ni fichier ni URL externe, au moins l'un des deux est obligatoire : ",
"admin.tools.presentation.luceneRefresh"        :   "<p>L'index de recherche permet d'accélérer la fonctionnalité de recherche.</p>\
                                                     <p>Le rafraîchissement de cet index permet d'assurer la bonne synchronisation avec les informations\
                                                        stockées en base de données.</p>",
"admin.tools.presentation.documentsimport"      :   "La fonctionnalité d ajout de documents en masse par import est réservée au profil Admin et est accessible depuis le menu 'Admin'.\
                                                    </p>\
                                                     <p>Il faut constituer un fichier Zip qui contient :\
                                                        <ul>\
                                                        <li>Un fichier CSV 'description.csv' contenant pour chaque lignes les metadata relatives à un fichier. \
                                                            Ce fichier csv est en encodage UFT8, séparateur ';'.</li>\
                                                        <li>l'ensemble des fichiers listés dans le fichier 'description.csv'.</li>\
                                                        </ul>\
                                                        Le nom du fichier Zip est libre.\
                                                     </p>\
                                                     <p>Le fichier description.csv contiendra les colonnes suivantes (* = colonnes obligatoires) : \
                                                        <ul>\
                                                          <li><strong>name*</strong> : titre du document (champ Title)</li>\
                                                          <li><strong>type*</strong> : type de document (champ Type) aux valeurs décrites ci-après</li>\
                                                          <li><strong>keywords*</strong> : mots clefs, séparés par des virgules (champ Keywords)</li>\
                                                          <li><strong>authors</strong> : auteurs, séparés par des virgules (champ Authors)</li>\
                                                          <li><strong>summary*</strong> : résumé (champ Summary). Ne pas utiliser de ; dans le texte, sinon encapsuler tout le texte entre des \" (exemple : \"text ; rest\")</li>\
                                                          <li><strong>license</strong> : type de licence (champ Licence)</li>\
                                                          <li><strong>copyright</strong> : copyright( champ Copyright)</li>\
                                                          <li><strong>publicationDate</strong> : date de publication (champ Publication Date). Format YYYY/MM/DD</li>\
                                                          <li><strong>language</strong> : langue du document, par exemple 'fr' ou 'en' (champ Langue)</li>\
                                                          <li><strong>comment</strong> : commentaire (champ Comment)</li>\
                                                          <li><strong>citation*</strong> : citation (champ Citation)</li>\
                                                          <li><strong>fileName*</strong> : nom du fichier lié (doit être exactement le même que dans le zip ; peut être vide si externalUrl fournie)</li>\
                                                          <li><strong>externalUrl*</strong> : url externe (sans le 'http://' ; peut être vide si le fileName est fourni)</li>\
                                                        </ul>\
                                                        Les différentes valeurs possibles pour le <strong>Type</strong> de document sont :\
                                                        <ul>\
                                                          <li><strong>PERIODICAL_PUBLICATION</strong> (publication dans une revue)</li>\
                                                          <li><strong>MEETING_PUBLICATION</strong> (publication dans des colloques ou séminaires)</li>\
                                                          <li><strong>THESIS</strong> (thèse)</li>\
                                                          <li><strong>REPORT</strong> (rapport)</li>\
                                                          <li><strong>SCIENTIFIC_REPORT</strong> (rapport scientifique)</li>\
                                                          <li><strong>SCIENTIFIC_WORK</strong> (ouvrage scientifique)</li>\
                                                          <li><strong>CHAPTER_WORK</strong> (chapitre d'ouvrage)</li>\
                                                          <li><strong>CONTRACT_REPORT</strong> (rapport de contrat)</li>\
                                                          <li><strong>EXPERTISE</strong> (expertise/avis)</li>\
                                                          <li><strong>POSTER</strong> (poster)</li>\
                                                          <li><strong>CONFERENCE_ARTICLE</strong> (acte de colloque)</li>\
                                                          <li><strong>CONFERENCE_SUMMARY</strong> (communication sans actes)</li>\
                                                          <li><strong>DATA</strong> (données)</li>\
                                                          <li><strong>OTHER</strong> (autre)</li>\
                                                        </ul>\
                                                        À noter que les documents créés seront <strong>visibles pour tous</strong>.\
                                                    </p>",

//Common part

"common.privacy.private"                        :   "Privé",
"common.privacy.public"                         :   "Publique",
"common.privacy.restricted"                     :   "Restreint",

"common.button.validate"                        :   "Valider",
"common.button.delete"                          :   "Supprimer",
"common.button.edit"                            :   "Éditer",
"common.button.add"                             :   "Ajouter",
"common.button.cancel"                          :   "Annuler",
"common.button.close"                           :   "Fermer",
"common.button.search"                          :   "Rechercher",
"common.button.searchField"                     :   "Recherche : ",
"common.button.advanceSearch"                   :   "Recherche avancée",
"common.button.simpleSearch"                    :   "Recherche simple",
"common.button.csvExport"                       :   "Exporter les résultats en CSV",
"common.message.missingMandatoryFields"         :   "Certains champs obligatoires (*) n'ont pas été remplis.",
"common.message.notYetAvailable"                :   "Pas encore disponible.",
"common.message.mandatoryFieldsInfo"            :   "Les champs avec un <strong><big>*</big></strong> sont obligatoires.",
"common.message.mandatoryInputLanguage"         :   "Les champs sont à saisir en anglais.",
"common.message.internalError"                  :   "Erreur durant le traitement. Si le problème persiste, veuillez contacter l'administrateur de l'application.",
"common.search.noResult"                        :   "Aucun résultat.",

"OPEN"                                          :   "Ouvert",
"IN_PROGRESS"                                   :   "En cours",
"CLOSED"                                        :   "Fermé",
"ADJOURNED"                                     :   "Ajourné",
"DELETED"                                       :   "Supprimé",

"home.presentation.title"                       :   "<h1>Bienvenue sur la plateforme de Coselmar</h1>",
"home.presentation.body"                        :   "<p>Cette plateforme de connaissance a pour but de centraliser la connaissance exploitée et produite dans le cadre du projet Coselmar.</p>\
                                                     <p>La connaissance y est structurée autour du concept de projets (projet de recherche, expertise, réunion , séminaire etc.) auxquels sont\
                                                        rattachés des objets (documents, données, publications, etc.) et les personnes impliquées dans ces projets. La plateforme gère aussi les\
                                                        liens entre projets : les antériorités (projets ayant donné naissance à des nouveaux projets) et les éventuelles suites données à ce projet (liste des nouveaux projets issus de ces projets).</p>\
                                                     <p>La base de connaissance peut être interrogée et le résultat des requêtes consultable selon différentes vues qui peuvent être exportées. \
                                                        Ces dernières fonctionnalités sont en cours de développement.</p>\
                                                     <p>L'accès à cette plateforme est sécurisé. Pour obtenir un droit d'accès merci d'en faire la demande au travers de la page contact du site Coselmar (<a href='http://www.coselmar.fr/contact/' target='_blank'>http://www.coselmar.fr/contact/</a>).\
                                                    </p>",
"home.lastProjects"                             :   "Derniers projets",
"home.wordCloud"                                :   "Mots les plus courants sur la plateforme",
"project.wordCloud"                             :   "Mots les plus courants autour du projet",

"error.message.authentication.title"            :   "Échec d'authentification",
"error.message.authentication.body"             :   "Une erreur avec l'authentification est survenue, veuillez vous reconnecter svp.",
"error.message.unauthorized.title"              :   "Non autorisé",
"error.message.unauthorized.body"               :   "Vous n'êtes pas autorisé à consulter cette page.",
"error.message.notFound.title"                  :   "Perdu ?",
"error.message.notFound.body"                   :   "Vous êtes ici, mais il n'y a rien ici.",

"common.message.info.searchKeywords"            :   "Vous pouvez utiliser plusieurs mots-clefs en les séparant par une virgule. La recherche sera faite avec tous les mots-clefs.",

"pagination.results.perPage"                    :   "Nombre de résultats par page",
"pagination.results.page"                       :   "Page",
"pagination.results.of"                         :   "sur",
"pagination.results.results"                    :   "Résultats: ",

"placeholder.mail"                              :   "Courriel",
"placeholder.password"                          :   "Mot de passe",
"placeholder.name"                              :   "Nom",
"placeholder.keywords"                          :   "mot-clef1,mot-clef2,...",
"placeholder.addKeyword"                        :   "Ajoutez un mot-clef",
"placeholder.title"                             :   "Titre",
"placeholder.nameOrFirstname"                   :   "Nom et/ou Prénom",
"placeholder.fullname"                          :   "Prénom Nom",
"placeholder.date"                              :   "jj/mm/aaaa",
"placeholder.website"                           :   "www.siteinternet.com",

"placeholder.document.name"                     :   "Nom du document",
"placeholder.document.copyright"                :   "Porteur du copyright",
"placeholder.document.license"                  :   "Licence du document",
"placeholder.document.language"                 :   "en,fr,...",
"placeholder.document.summary"                  :   "Saisissez le résumé du document, cela aidera à le retrouver",

"placeholder.project.title"                     :   "Nom du projet",
"placeholder.project.summary"                   :   "Ce résumé sera également utilisé pour retrouver le projet",
"placeholder.project.addTag"                    :   "Ajoutez un tag",
"placeholder.project.externalexperts"           :   "Expert1,Expert2,...",

"placeholder.user.firstname"                    :   "Prénom",
"placeholder.user.name"                         :   "Nom",
"placeholder.user.mail"                         :   "example@provider.mail",
"placeholder.user.login"                        :   "Idenfitifiant",
"placeholder.user.phone"                        :   "+33912345678",
"placeholder.user.organization"                 :   "Organisation",
"placeholder.user.qualification"                :   "Profession",
"placeholder.user.currentPassword"              :   "Mot de passe actuel",
"placeholder.user.newPassword"                  :   "Nouveau mot de passe",
"placeholder.user.confirmPassword"              :   "Confirmez le mot de passe",
"placeholder.user.search"                       :   "Recherche d'un utilisateur",

}