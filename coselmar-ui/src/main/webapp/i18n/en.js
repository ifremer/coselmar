/*
 * #%L
 * Coselmar :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
var translateEN = {

"application.title"                             :   "Platform",

"menu.item.users"                               :   "Users",
"menu.item.users.list"                          :   "List",
"menu.item.users.add"                           :   "Add an user",
"menu.item.users.add.client"                    :   "Add a client",
"menu.item.questions"                           :   "Projects",
"menu.item.questions.list"                      :   "List",
"menu.item.questions.add"                       :   "Add a project",
"menu.item.documents"                           :   "Documents",
"menu.item.documents.list"                      :   "List",
"menu.item.documents.add"                       :   "Add a document",
"menu.item.referential"                         :   "Referential Search",
"menu.item.admin"                               :   "Admin",
"menu.item.links"                               :   "Useful links",

"menu.button.login"                             :   "Sign in",
"menu.button.profile"                           :   "Profile",
"menu.button.logout"                            :   "Logout",

//Footer

"footer.item.licence"                           :   "Licence",
"footer.item.report"                            :   "Bug report",
"footer.item.support"                           :   "User support",

//Documents part

"document.list.title"                           :   "All documents",
"document.modalSearch.title"                    :   "Search Documents",
"document.button.add"                           :   "Add a document",
"document.message.delete"                       :   "Do you really want to delete this document ?",
"document.view.title"                           :   "Document's details",
"document.create.title"                         :   "New document",
"document.create.info"                          :   "Here you can add a new document in the repository. For each document,\
                                                    some keywords are needed to make easier search of the document, and you\
                                                    can specify a privacy :\
                                                    <ul>\
                                                      <li>Public : all members can see the document</li>\
                                                      <li>Private : only you can see the document</li>\
                                                      <li>Restricted : you can specify who can see the document specifically</li>\
                                                    </ul>",
"document.bibliography.title"                   :   "Bibliography",

"document.metadata.name"                        :   "Title",
"document.metadata.authors"                     :   "Authors",
"document.metadata.privacy"                     :   "Privacy",
"document.metadata.keywords"                    :   "Keywords",
"document.metadata.depositor"                   :   "Depositor",
"document.metadata.depositDate"                 :   "Deposit Date",
"document.metadata.relatedQuestions"            :   "Related Projects",
"document.metadata.view.relatedQuestions"       :   "Projects with this document",
"document.metadata.type"                        :   "Type",
"document.metadata.type.publication"            :   "Publication",
"document.metadata.type.meetingPublication"     :   "Publication in meeting or symposium",
"document.metadata.type.periodicalPublication"  :   "Publication in periodical",
"document.metadata.type.thesis"                 :   "Thesis",
"document.metadata.type.report"                 :   "Report",
"document.metadata.type.scientificReport"       :   "Scientific report",
"document.metadata.type.scientificWork"         :   "Scientific work",
"document.metadata.type.chapterWork"            :   "Work chapter",
"document.metadata.type.contractReport"         :   "Contract report",
"document.metadata.type.expertise"              :   "Expertise/Opinion",
"document.metadata.type.poster"                 :   "Poster",
"document.metadata.type.conferenceArticle"      :   "Conference article",
"document.metadata.type.conferenceSummary"      :   "Conference summary",
"document.metadata.type.data"                   :   "Data",
"document.metadata.type.other"                  :   "Other",
"document.metadata.file"                        :   "File",
"document.metadata.mimetype"                    :   "Mime Type",
"document.metadata.externalUrl"                 :   "External URL",
"document.metadata.copyright"                   :   "Copyright",
"document.metadata.licence"                     :   "Licence",
"document.metadata.language"                    :   "Language",
"document.metadata.publicationDate"             :   "Publication Date",
"document.metadata.summary"                     :   "Summary",
"document.metadata.comment"                     :   "Comment",
"document.metadata.fileName"                    :   "Document File",
"document.metadata.externalLink"                :   "External Link",
"document.metadata.citation"                    :   "Citation",

"document.metadata.depositBefore"               :   "Deposit before",
"document.metadata.depositAfter"                :   "Deposit after",
"document.metadata.publishBefore"               :   "Publish before",
"document.metadata.publishAfter"                :   "Publish after",

"document.message.requiredName"                 :   "Document name is required.",
"document.message.requiredType"                 :   "Document type is required.",
"document.message.requiredExternalUrlOrFile"    :   "A File or an external URL is required.",
"document.message.requiredExternalUrl"          :   "For this document, an external URL is required.",
"document.message.requiredKeywords"             :   "At least one keyword is required.",
"document.message.requiredAuthors"              :   "Document authors is required.",
"document.message.requiredCopyright"            :   "Document copyright is required.",
"document.message.requiredSummary"              :   "A summary is required.",
"document.message.requiredCitation"             :   "Citation is required.",
"document.privacy.questionRestricted"           :   "Restricted to Project participants",
"document.privacy.restricted.info"              :   "Restricted to selected experts and to projects using document",
"document.message.created"                      :   "Document well created.",
"document.message.updated"                      :   "Document well modified.",
"document.message.deleted"                      :   "Document well deleted.",

"document.message.info.privacy"                 :   "Visibility defined who can access to document :<ul>\
                                                        <li>Public : everyone,</li>\
                                                        <li>Private : only you and supervisor,</li>\
                                                        <li>Restricted : you, the defined experts and members of project using this document</li>\
                                                     </ul>",
"document.message.info.keywords"                :   "Keywords enable to classify document and make search easier. Each input keyword should be validated with button \"Add\".",
"document.message.info.referredInProjects"      :   "Document is referred by projects and so cannot be modified.",

"document.button.download"                      :   "Download",
"document.button.openLink"                      :   "Open link",
"document.button.removeFile"                    :   "Remove file",
"document.button.generateBibliography"          :   "Generate bibliography",

//Questions part

"question.list.title"                           :   "Projects",
"question.add.title"                            :   "Add a Project",
"question.newDocument.title"                    :   "Contribute with new documents",
"question.modal.parents.title"                  :   "Assign parent project",
"question.modal.hierarchy.title"                :   "Tree view of project ",

"question.metadata.title"                       :   "Title",
"question.metadata.type"                        :   "Type",
"question.metadata.type.reunion"                :   "Meeting (seminar, roundtable, ...)",
"question.metadata.type.expertise"              :   "Expertise",
"question.metadata.type.project"                :   "Project (arrangement, brainstorming, ...)",
"question.metadata.privacy"                     :   "Visibility",
"question.metadata.themes"                      :   "Themes",
"question.metadata.submissionDate"              :   "Submission date",
"question.metadata.deadline"                    :   "Deadline",
"question.metadata.summary"                     :   "Summary",
"question.metadata.status"                      :   "Status",
"question.metadata.supervisors"                 :   "Supervisors",
"question.metadata.experts"                     :   "Experts",
"question.metadata.externalExperts"             :   "External experts",
"question.metadata.clients"                     :   "Clients",
"question.metadata.contributors"                :   "Contributors",
"question.metadata.relatedDocuments"            :   "Related documents",
"question.metadata.conclusion"                  :   "Conclusion",
"question.metadata.conclusionDocuments"         :   "Conclusion Documents",
"question.metadata.parents"                     :   "Parent projects",
"question.metadata.parentsPhrase"               :   "Produced or inspired by",
"question.metadata.childrenPhrase"              :   "Has produced or inspired",
"question.metadata.participants"                :   "Participants",
"question.metadata.links"                       :   "Associated Links",

"question.metadata.hierarchy.display"           :   "Show tree view",

"question.metadata.submitBefore"                :   "Submit before",
"question.metadata.submitAfter"                 :   "Submit after",
"question.metadata.deadlineBefore"              :   "Deadline before",
"question.metadata.deadlineAfter"               :   "Deadline after",

"question.message.requiredTitle"                :   "Title is required, minimum 10 characters.",
"question.message.requiredType"                 :   "Type is required.",
"question.message.requiredSummary"              :   "A summary is required.",
"question.message.requiredThemes"               :   "At least one theme is required.",
"question.message.saved"                        :   "Project well saved.",
"question.message.deleted"                      :   "Project well deleted.",
"question.message.closed"                       :   "Project has been closed.",
"question.message.reopened"                     :   "Project has been reopened.",
"question.message.adjourned"                    :   "Project has been adjourned.",
"question.message.newDocuments.added"           :   "New documents has been added.",

"question.button.add"                           :   "Add a project",
"question.button.reopen"                        :   "Reopen",
"question.button.close"                         :   "Close",
"question.button.close.fullText"                :   "Close this project",
"question.button.adjourn"                       :   "Adjourn",
"question.button.validateChanges"               :   "Valide changes",
"question.button.validateNewDocuments"          :   "Valide new documents",

"question.message.notAvailable"                 :   "This project is not available to consultation.",
"question.message.notAllowedToCreate"           :   "You're not allowed to create project",
"question.message.delete"                       :   "Do you really want to delete this project ?",
"question.message.closedOn"                     :   "Closed on ",
"question.message.info.type"                    :   "Type could be of three kinds : <ul>\
                                                        <li>Meeting : seminar, roundtable, short meeting, ...</li>\
                                                        <li>Expertise</li>\
                                                        <li>Project : all around project life such as arrangement or brainstorming</li>\
                                                     </ul>",
"question.message.info.privacy"                 :   "Visibility defined who can access to project content :<ul>\
                                                        <li>Private : only assigned experts,</li>\
                                                        <li>Public : all experts from application.</li>\
                                                     </ul>\
                                                     Please note that all project clients have also access to the project.",
"question.message.info.themes"                  :   "Themes enable to classify project and make search easier. Each input theme should be validated with button \"Add\".",
"question.message.info.contributors"            :   "Contributors are experts who have been experts on the project before.",

// Links

"link.metadata.url"                             :   "URL",
"link.metadata.name"                            :   "Display name",

"link.create.title"                             :   "Add a link",
"link.update.title"                             :   "Update link",

//Users part

"user.list.title"                               :   "All users",
"user.create.title"                             :   "Add an user",
"user.create.info"                              :   "Create new user, with one of following roles :\
                                                     <ul>\
                                                       <li>Supervisor : Kind of big manager, who relays member/client project and assigns expert on projects</li>\
                                                       <li>Expert : Scientist, who can add new document in the repository and see other scientist documents (according file permission)</li>\
                                                       <li>Member</li>\
                                                       <li>Client</li>\
                                                     </ul>",
"user.create.client.info"                       :   "Create new user with Client role",
"user.password.title"                           :   "Send a new password",
"user.info.title"                               :   "User infos",
"user.projects.title"                           :   "User projects",

"user.metadata.name"                            :   "Name",
"user.metadata.firstName"                       :   "First name",
"user.metadata.mail"                            :   "Contact",
"user.metadata.mail.login"                      :   "Login",
"user.metadata.phone"                           :   "Phone number",
"user.metadata.qualification"                   :   "Qualification",
"user.metadata.organization"                    :   "Organization",
"user.metadata.role"                            :   "role",
"user.metadata.status"                          :   "Status",
"user.metadata.status.disable"                  :   "Disable",
"user.metadata.status.enable"                   :   "Active",
"user.metadata.password"                        :   "Password",
"user.metadata.password.new"                    :   "New Password",
"user.metadata.role.all"                        :   "All",
"user.metadata.role.admin"                      :   "Admin",
"user.metadata.role.supervisor"                 :   "Supervisor",
"user.metadata.role.expert"                     :   "Expert",
"user.metadata.role.member"                     :   "Member",
"user.metadata.role.client"                     :   "Client",

"user.metadata.projects"                        :   "Projects list",
"user.metadata.projects.asSupervisor"           :   "Projects as supervisorr",
"user.metadata.projects.asParticipant"          :   "Projects as expert",
"user.metadata.projects.asContributor"          :   "Projects as contributor",
"user.metadata.projects.asClient"               :   "Projects as client",

"user.message.disable"                          :   "Do you really want to disable this user ?",
"user.message.delete"                           :   "Do you really want to delete this user ?",
"user.message.requiredFirsName"                 :   "First name is required.",
"user.message.requiredName"                     :   "Name is required.",
"user.message.invalidMail"                      :   "Enter a valid mail.",
"user.message.requiredQualification"            :   "Qualification is required.",
"user.message.requiredPassword"                 :   "Password is required to validate modification.",
"user.label.currentPasswordToValidate"          :   "Tape your current password to validate the modifications",
"user.message.wannaChangePassword"              :   "I want to change the password",
"user.message.tooShortPassword"                 :   "New password should contain at least 6 characters.",
"user.message.passwordsNoMatch"                 :   "The two new passwords don't match.",
"user.message.login"                            :   "Login :",
"user.message.forgotPassword"                   :   "I forgot my password",
"user.message.enterMail"                        :   "Please enter your email",
"user.message.unknownMail"                      :   "This email does not exist.",
"user.message.newPasswordSent"                  :   "A new password has been sent.",
"user.message.wannaSimpleLogin"                 :   "I want to use a simple login, not a mail (Warning : no password could be sent)",
"user.message.invalidLogin"                     :   "Login is mandatory.",
"user.message.created"                          :   "User well created.",
"user.message.updated"                          :   "User well modified.",
"user.message.deleted"                          :   "User well deleted.",
"user.message.disable"                          :   "User well disable.",
"user.message.enable"                           :   "User well enable.",
"user.message.mail.alreadyExisting"             :   "The provided mail (or login) already exists.",
"user.message.login.fail"                       :   "Fail to login, please try again.",
"user.message.noProject"                        :   "This user has no project",

"user.button.add"                               :   "Add an user",
"user.button.add.client"                        :   "Add a client",
"user.button.disable"                           :   "Disable",
"user.button.enable"                            :   "Enable",
"user.button.showDisable"                       :   "Show disable users",
"user.button.newPassword"                       :   "Send",

//Referential part

"referential.search.title"                      :   "Ask the Referential",
"referential.search.keywords"                   :   "Keywords",
"referential.search.documents"                  :   "Documents",
"referential.search.questions"                  :   "Projects",
"referential.search.singleDocumentFound"        :   "document found.",
"referential.search.multipleDocumentsFound"     :   "documents found.",
"referential.search.singleQuestionFound"        :   "project found.",
"referential.search.multipleQuestionsFound"     :   "projects found.",
"referential.search.noDocumentFound"            :   "No document found.",
"referential.search.noQuestionFound"            :   "No project found.",

//Admin part

"admin.tools.title"                             :   "Admin tools",
"admin.tools.label.luceneRefresh"               :   "Refresh Search Index",
"admin.tools.button.refresh"                    :   "Refresh !",
"admin.tools.message.refresh.inprogress"        :   "Refresh in progress.",
"admin.tools.message.refresh.done"              :   "Search index has been well refreshed !",
"admin.tools.message.refresh.errors"            :   "Error during search index refresh. Please try again. If problem persist, see server administrator.",
"admin.tools.label.documentsImport"             :   "Import Documents Zip file",
"admin.tools.button.documentsimport"            :   "Import !",
"admin.tools.message.massimport.inprogress"     :   "Import in progress.",
"admin.tools.message.massimport.done"           :   "Documents have been well imported !",
"admin.tools.message.massimport.errors"         :   "Error during documents import. Please try again. If problem persist, see server administrator.",
"admin.tools.message.massimport.fail"           :   "Some error occur during import.",
"admin.tools.message.massimport.zipReadFail"    :   "Unable to open zipfile, maybe it is corrupt ?",
"admin.tools.message.massimport.csvFindFail"    :   "Unable to retrieve 'description.csv' file in zip.",
"admin.tools.message.massimport.csvReadFail"    :   "Unable to read 'description.csv' file, please verify file format.",
"admin.tools.message.massimport.fileReadFail"   :   "Unable to read some files from zip.",
"admin.tools.message.massimport.missingFiles"   :   "Files not found from zip : ",
"admin.tools.message.massimport.missingAttachments"   :   "Some documents has no file and no external URL, at least one is mandatory : ",
"admin.tools.presentation.luceneRefresh"        :   "<p>Search Index makes search easier and faster.</p>\
                                                     <p>Refresh it guaranties to be weel synch with information from database.</p>",
"admin.tools.presentation.documentsimport"      :   "<p>Documents Mass Import is a functionality reserved to Admin and available inside 'Admin' menu entry.</p>\
                                                     <p>A Zip file should be made, containing :\
                                                        <ul>\
                                                        <li>a CSV file named 'description.csv' containing on each line metadata related to a file.\
                                                            This file is UTF-8 encoded and using ';' as separator</li>\
                                                        <li>all document files described in 'description.csv'.</li>\
                                                        </ul>\
                                                        Name of the zip file is free.\
                                                     </p>\
                                                     <p>The 'description.csv' file should contain following columns (* = mandatory columns) : \
                                                        <ul>\
                                                          <li><strong>name*</strong> : document title (field Title)</li>\
                                                          <li><strong>type*</strong> : document type (field Type) only with values described in next paragraph</li>\
                                                          <li><strong>keywords*</strong> : keywords, separated with coma (<strong>,</strong>) (field Keywords)</li>\
                                                          <li><strong>authors</strong> : authors, separated with coma (<strong>,</strong>) (field Authors)</li>\
                                                          <li><strong>summary*</strong> : summary (field Summary). If text contains semi-colon ((<strong>;</strong>), born it with double-quote (<strong>\"</strong>, exemple : \"this is my text ; and rest\")</li>\
                                                          <li><strong>license</strong> : license type (field Licence)</li>\
                                                          <li><strong>copyright</strong> : copyright (field Copyright)</li>\
                                                          <li><strong>publicationDate</strong> : publication date (field Publication Date). Format YYYY/MM/DD</li>\
                                                          <li><strong>language</strong> : document language, 'en' or 'fr' for example</li>\
                                                          <li><strong>comment</strong> : comment (field Comment)</li>\
                                                          <li><strong>citation*</strong> : citation (field Citation)</li>\
                                                          <li><strong>fileName*</strong> : linked file name (must be exactly same as in the zip ; could be empty if externalUrl given)</li>\
                                                          <li><strong>externalUrl*</strong> : external url (without 'http://' ; could be empty if fileName given)</li>\
                                                        </ul>\
                                                        Different values allowed for document <strong>Type</strong> are :\
                                                        <ul>\
                                                          <li><strong>PERIODICAL_PUBLICATION</strong> (publication in periodical)</li>\
                                                          <li><strong>MEETING_PUBLICATION</strong> (publication in meeting or symposium)</li>\
                                                          <li><strong>THESIS</strong></li>\
                                                          <li><strong>REPORT</strong></li>\
                                                          <li><strong>SCIENTIFIC_REPORT</strong> (scientific report)</li>\
                                                          <li><strong>SCIENTIFIC_WORK</strong> (scientific work)</li>\
                                                          <li><strong>CHAPTER_WORK</strong> (work chapter)</li>\
                                                          <li><strong>CONTRACT_REPORT</strong> (contract report)</li>\
                                                          <li><strong>EXPERTISE</strong> (expertise/opinion)</li>\
                                                          <li><strong>POSTER</strong></li>\
                                                          <li><strong>CONFERENCE_ARTICLE</strong> (conference article)</li>\
                                                          <li><strong>CONFERENCE_SUMMARY</strong> (conference summary)</li>\
                                                          <li><strong>DATA</strong></li>\
                                                          <li><strong>OTHER</strong></li>\
                                                        </ul>\
                                                        Note that all created documents will be <strong>public</strong>\
                                                    </p>",

//Common part

"common.privacy.private"                        :   "Private",
"common.privacy.public"                         :   "Public",
"common.privacy.restricted"                     :   "Restricted",

"common.button.validate"                        :   "Validate",
"common.button.delete"                          :   "Delete",
"common.button.edit"                            :   "Modify",
"common.button.add"                             :   "Add",
"common.button.cancel"                          :   "Cancel",
"common.button.close"                           :   "Close",
"common.button.search"                          :   "Search",
"common.button.searchField"                     :   "Search : ",
"common.button.advanceSearch"                   :   "Advance search",
"common.button.simpleSearch"                    :   "Simple search",
"common.button.csvExport"                       :   "Export results as CSV",
"common.message.missingMandatoryFields"         :   "Some mandatory field (*) have not been filled.",
"common.message.notYetAvailable"                :   "Not yet available.",
"common.message.mandatoryFieldsInfo"            :   "Field with <strong><big>*</big></strong> are mandatory.",
"common.message.internalError"                  :   "Error during process. If this problem persist, please contact application admin",
"common.search.noResult"                        :   "No result.",

"OPEN"                                          :   "Opened",
"IN_PROGRESS"                                   :   "In progress",
"CLOSED"                                        :   "Closed",
"ADJOURNED"                                     :   "Adjourned",
"DELETED"                                       :   "Deleted",

"home.presentation.title"                       :   "<h1>Welcome to Coselmar Platform</h1>",
"home.presentation.body"                        :   "<p>This knowledge platform is intended to centralize the knowledge produced and exploited in the Coselmar project.</p>\
                                                     <p>Knowledge is structured around the concept of projects (research project, expertise, meeting, seminar etc.) which are \
                                                     attached objects (documents, data, publications, etc.) and those involved in these projects. The platform also manages the \
                                                     links between projects: the prior art (projects spawned new projects) and any action taken on this project (list of new projects from these projects).</p>\
                                                   <p>The knowledge base can be queried and the results of queries consulted a variety of views that can be exported. Recent features are being developed.</p>\
                                                   <p>Access to this platform is secure. To obtain a right of access to thank you for the request through the Coselmar website contact page (<a href='http://www.coselmar.fr/contact/' target='_blank'>http://www.coselmar.fr/contact/</a>).</p>\
                                                    </p>",
"home.lastProjects"                             :   "Last projects",
"home.wordCloud"                                :   "Most used words in the platform",
"project.wordCloud"                             :   "Most used words on the project",

"error.message.authentication.title"            :   "Error with authentication",
"error.message.authentication.body"             :   "Error with authentication, please try to log again.",
"error.message.unauthorized.title"              :   "Not Authorized",
"error.message.unauthorized.body"               :   "You are not authorized to access this page.",
"error.message.notFound.title"                  :   "Nothing Here",
"error.message.notFound.body"                   :   "The page you try to access does not exist.",

"common.message.info.searchKeywords"            :   "You can use several keywords separating them by coma. Search will match all given keywords",

"pagination.results.perPage"                    :   "Number of results per page",
"pagination.results.page"                       :   "Page",
"pagination.results.of"                         :   "of",
"pagination.results.results"                    :   "Results: ",

"placeholder.mail"                              :   "Mail",
"placeholder.password"                          :   "Password",
"placeholder.name"                              :   "Name",
"placeholder.keywords"                          :   "keyword1,keyword2,...",
"placeholder.addKeyword"                        :   "Add a keyword",
"placeholder.title"                             :   "Title",
"placeholder.nameOrFirstname"                   :   "Name and/or firstname",
"placeholder.fullname"                          :   "Fullname",
"placeholder.date"                              :   "dd/mm/yyyy",
"placeholder.website"                           :   "www.website.com",

"placeholder.document.name"                     :   "Document name",
"placeholder.document.copyright"                :   "Copyright owner",
"placeholder.document.license"                  :   "Document License",
"placeholder.document.language"                 :   "en,fr,...",
"placeholder.document.summary"                  :   "Tape the document summary, it will help to retrieve document",

"placeholder.project.title"                     :   "Project name",
"placeholder.project.summary"                   :   "This summary can also be used to retrieve the project",
"placeholder.project.addTag"                    :   "Add a tag",
"placeholder.project.externalexperts"           :   "Expert1,Expert2,...",

"placeholder.user.firstname"                    :   "Firstname",
"placeholder.user.name"                         :   "Name",
"placeholder.user.mail"                         :   "example@provider.mail",
"placeholder.user.login"                        :   "Login",
"placeholder.user.phone"                        :   "+33912345678",
"placeholder.user.organization"                 :   "Organization",
"placeholder.user.qualification"                :   "Qualification",
"placeholder.user.currentPassword"              :   "Current password",
"placeholder.user.newPassword"                  :   "New password",
"placeholder.user.confirmPassword"              :   "Confirm password",
"placeholder.user.search"                       :   "Search an user",

}