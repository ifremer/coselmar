/*
 * #%L
 * Coselmar :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
var coselmarControllers = angular.module('coselmarControllers', ['ui.bootstrap', 'ui.select', 'ui.bootstrap.tooltip', 'pascalprecht.translate', 'tmh.dynamicLocale']);

// Controller when the main page/view loads
coselmarControllers.controller("HomeCtrl", ['$scope', '$http', '$location', '$route', '$routeParams', '$locale', '$translate', 'tmhDynamicLocale', 'userService', 'jwtHelper', 'coselmarConfig', 'notify',
 								function ($scope, $http, $location, $route, $routeParams, $locale, $translate, tmhDynamicLocale, userService, jwtHelper, coselmarConfig, notify) {

	var jwtToken = localStorage.getItem('coselmar-jwt');
	if (jwtToken && !jwtHelper.isTokenExpired(jwtToken)) {
		$scope.context = {currentUser : jwtHelper.decodeToken(jwtToken)};
	}

	//Just get the version from a file
	$http.get('version.txt', {'transformResponse':angular.identity}).success(function (data) {
    	$scope.version = data;
  	});

	$scope.login = function(mail, password) {
		userService.login(mail, password, function(successResult) {
			// Store JsonWebToken to keep authentication
			localStorage.setItem('coselmar-jwt', successResult.jwt);
			$scope.context = { currentUser : jwtHelper.decodeToken(successResult.jwt) };

			// Redirect on initial request page
			if ($location.search()["returnTo"]) {
				$location.path($location.search()["returnTo"]);
				delete returnTo;
			} else {
				// Reload page
				$route.reload();
			}
		}, function(error) {
          // Mail already existing
            notify({
              message: "user.message.login.fail",
              classes: "alert-danger",
              container: "#main-container",
              templateUrl: "views/notificationTemplate.html"
            });

		})
	}

	$scope.logout = function() {
		delete $scope.context.currentUser;
		localStorage.removeItem('coselmar-jwt');
		if ($location.url() == '/') {
		  $route.reload();
		} else {
		  $location.path('/');
		}
	}

    // i18n Management
    $scope.locale = localStorage.getItem('coselmar-locale');

    if (!$scope.locale) {
        // set locale with browser preference
//        $scope.locale = navigator.language || navigator.userLanguage;
//        $scope.locale = $scope.locale.substring(0,2);
        // See #7982: default locale is forced to English
        $scope.locale = "en";

        // get locale supported
        if (coselmarConfig.SUPPORTED_LOCALE.indexOf($scope.locale) == -1) {
          $scope.locale = coselmarConfig.SUPPORTED_LOCALE[0];
        }

    }
    $translate.use($scope.locale);
    tmhDynamicLocale.set($scope.locale);


    $scope.switchLocale = function (locale) {
        // switch locale
        if (locale != $scope.locale) {
            // Store locale in localStorage
			localStorage.setItem('coselmar-locale', locale);
            $scope.locale = locale;
            $translate.use(locale);
            // change $locale
            tmhDynamicLocale.set(locale);
        }
    }

    $scope.$on('dataLoading', function(event, args) {
        $scope.applicationLoading = true;
    });

    $scope.$on('dataLoaded', function(event, args) {
        $scope.applicationLoading = false;
    });

}]);

// Controller when got 401
coselmarControllers.controller("unauthorizedCtrl", ['$scope', '$location', function ($scope, $location) {

	// Just remove the current stored user
	if ($scope.context && $scope.context.currentUser) {
        delete $scope.context.currentUser;
        localStorage.removeItem('coselmar-jwt');
	}

}]);

// Controller for home when user connected, load 5 last projects
coselmarControllers.controller("homeConnectedCtrl", ['$scope', 'questionsService', 'generalService', function ($scope, questionsService, generalService) {

  $scope.closedQuestions = [];
  $scope.topWords = [{text:"coselmar", weight: 10}, {text: "Ifremer", weight: 5}];
  $scope.cloudColors = ["#2f8389", "#349198", "#39A0A8", "#3eaeb6", "#48b8c0"];

  if ($scope.context && $scope.context.currentUser) {

	var searchOptions = {'status' : 'IN_PROGRESS', 'limit' : 5, 'page' : 0};

	questionsService.getQuestions(searchOptions, function(questions) {
		$scope.questions = questions;
	  }, function(fail) {
		$scope.questions = [];
	});

	var closedSearchOptions = {'status' : 'CLOSED', 'limit' : 2, 'page' : 0};

	questionsService.getQuestions(closedSearchOptions, function(questions) {
		$scope.closedQuestions = questions;
	  }, function(fail) {
		$scope.closedQuestions = [];
	});
  } else {

	questionsService.getPublicQuestions(function(questions) {
		$scope.questions = questions;
	  }, function(fail) {
		$scope.questions = [];
	});
  }

  generalService.getTopWords(function(result) {
		$scope.topWords = result.data;
		for (var i=0; i < $scope.topWords.length; i++) {
		    $scope.topWords[i].link = "#/referential?onDocuments&onQuestions&keywords=" + $scope.topWords[i].text;
		}
	  }, function(fail) {
		$scope.topWords = [];
  });


}]);

/////////////////////////////////////////////////
////////////  Documents Part  ///////////////////
/////////////////////////////////////////////////

// Controller for All Documents View
coselmarControllers.controller("DocumentsCtrl", ['$scope', '$route', '$routeParams', '$location', '$uibModal', 'notify', 'documentService', 'errorService',
        function($scope, $route, $routeParams, $location, $uibModal, notify, documentService, errorService){

    $scope.$emit('dataLoading');
	//manage keywords if given
	$scope.example = { fullTextSearch : [], keywords: [], page: 0, limit: 25};

	var keywords = $routeParams.keywords;
	if (Array.isArray(keywords)) {
		$scope.example.fullTextSearch = keywords;
	} else if (keywords) {
		$scope.example.fullTextSearch.push(keywords);
	}

	var advancedSearch = $routeParams.advancedSearch;
	if (advancedSearch) {
	    $scope.advanced = advancedSearch;
	} else {
	    $scope.advanced = false;
	}

	var page = $routeParams.page;
	if (limit) {
	    $scope.example.page = page;
	}

	var limit = $routeParams.limit;
	if (limit) {
	    $scope.example.limit = limit;
	}
	$scope.searchId = btoa(JSON.stringify($scope.example));

    documentService.getPaginatedDocuments($scope.example, function(paginatedDocuments) {
            $scope.paginationData = paginatedDocuments;
        }, errorService.defaultFailOnCall, function() {
            $scope.$broadcast('pageLoaded', $scope.paginationData.currentPage, $scope.paginationData.count)
            $scope.$emit('dataLoaded');
    });

	$scope.deleteDocument = function(documentId){

		// Call service to create a new document
		documentService.deleteDocument(documentId, $scope, function() {
            // Notification
            notify({
              message: "document.message.deleted",
              classes: "alert-warning",
              container: "#main-container",
              templateUrl: "views/notificationTemplate.html"
            });
			// Go back to documents list
			$route.reload();
		}, errorService.defaultFailOnCall);
	};

	$scope.searchDocuments = function(){
		$location.search('keywords', $scope.example.fullTextSearch);
		$location.search('page', $scope.example.page);
		$location.search('limit', $scope.example.limit);
	};

	$scope.advancedSearchDocuments = function() {

        if (angular.isDate($scope.example.depositAfterDate)) {
            $scope.example.depositAfterDate = $scope.example.depositAfterDate.getTime();
        }

        if (angular.isDate($scope.example.depositBeforeDate)) {
            $scope.example.depositBeforeDate = $scope.example.depositBeforeDate.getTime();
        }

        if (angular.isDate($scope.example.publicationAfterDate)) {
            $scope.example.publicationAfterDate = $scope.example.publicationAfterDate.getTime();
        }

        if (angular.isDate($scope.example.publicationBeforeDate)) {
            $scope.example.publicationBeforeDate = $scope.example.publicationBeforeDate.getTime();
        }

	$scope.searchId = btoa(JSON.stringify($scope.example));
        $scope.$emit('dataLoading');
        documentService.getPaginatedDocuments($scope.example, function(paginatedDocuments) {
                $scope.paginationData = paginatedDocuments;
            }, errorService.defaultFailOnCall, function() {
                $scope.$broadcast('pageLoaded', $scope.paginationData.currentPage, $scope.paginationData.count)
                $scope.$emit('dataLoaded');
        });
	};

    $scope.searchMode = function(type) {
      if (type == 'advanced') {
        $location.search('advancedSearch');
        $scope.advanced = true;
      } else {
        $location.search("");
        $scope.advanced = false;
      }
    };

    $scope.$on('loadPage', function(event, pageNumber, pageSize) {
        $scope.example.page = pageNumber;
        $scope.example.limit = pageSize;
        documentService.getPaginatedDocuments($scope.example, function(paginatedDocuments) {
		    $scope.paginationData = paginatedDocuments;
	    }, errorService.defaultFailOnCall, function() {
		    $scope.$broadcast('pageLoaded', $scope.paginationData.currentPage, $scope.paginationData.count);
            $scope.$emit('dataLoaded');
        });
    });

	$scope.modalDisplayBibliography = function () {

		var modalInstance = $uibModal.open({
			templateUrl: 'views/documents/modalBibliography.html',
			controller: 'ModalDocumentsBibliographyCtrl',
			size: 'lg',
			resolve : {
				searchModel : $scope.example
				}
		});

	};

}]);

// Controller for new document View
coselmarControllers.controller("NewDocumentCtrl", ['$scope', '$location', 'notify', 'documentService', 'errorService',
    function($scope, $location, notify, documentService, errorService){

	$scope.document = {'type' : 'PERIODICAL_PUBLICATION', 'privacy': 'PUBLIC', 'keywords': [], 'authorizedUsers' : []};
	$scope.upload = {};
	$scope.existing = {'types' : [], 'keywords' : []};
	$scope.users = [];
    $scope.toAddKeywords = {};

	documentService.findAllTypes(function(results) {
		$scope.existing.types = results;
	});

	// Participants, clients and supervisors management for ui-select
	$scope.usersIndex = [];
	$scope.refreshUsers = function(searchKeyword) {
		var searchKeywords = [];
		if (searchKeyword && searchKeyword.length > 0) {
			searchKeywords.push(searchKeyword);
		}

		documentService.findExperts({'active': true, 'fullTextSearch' : searchKeywords}, function(users) {
			$scope.users = users;
			$scope.usersIndex = [];
			angular.forEach($scope.users, function(user) {
				$scope.usersIndex[user.id] = user;
			});
			bindUsers($scope.document.authorizedUsers, $scope.usersIndex);
		});
	}

	// function to be sure to have same user objects in list
	var bindUsers = function(toDeal, index) {
		if (toDeal) {
			for(var i = 0; i < toDeal.length; i++) {
				var user = toDeal[i];
				if (index[user.id]) {
					toDeal[i] = index[user.id];
				}
			}
		}
	};

	documentService.findAllKeywords(function(results) {
		$scope.existing.keywords = results;
	});

	$scope.createNewDocument = function(){

    // Adapt publicationDate to send a timestamp
    if (angular.isDate($scope.document.publicationDate)) {
        $scope.document.publicationDate = $scope.document.publicationDate.getTime();
    }

    // Adapt external link format if protocol is not set
    if ($scope.document.externalUrl) {
        var urlPattern = /^http[s]?:\/\/.*$/;
        if (!urlPattern.test($scope.document.externalUrl)) {
          $scope.document.externalUrl = 'http://' + $scope.document.externalUrl;
        }
    }

		if ($scope.document.keywords.length == 0) {
		    $scope.keywordsError = true;
        } else {
    		// Call service to create a new document
    		$scope.$emit('dataLoading');

            documentService.saveDocument($scope.document, function(document) {
                    if ($scope.upload.file) {
                        var documentId = document.id;
                        documentService.saveDocumentFile(documentId, $scope.upload.file, function() {
                            // Notification
                            notify({
                              message: "document.message.created",
                              classes: "alert-success",
                              container: "#main-container",
                              templateUrl: "views/notificationTemplate.html"
                            });
                            $location.path("/documents");
                        });
                    } else {
                        // Notification
                        notify({
                          message: "document.message.created",
                          classes: "alert-success",
                          container: "#main-container",
                          templateUrl: "views/notificationTemplate.html"
                        });
                        $location.path("/documents");
                    }
                },
                errorService.defaultFailOnCall,
                function() {
                    $scope.$emit('dataLoaded');
            });

		}

	};

  $scope.cancelDocumentCreation = function(){
    $location.path('/documents');
  }

    $scope.getExistingKeywords = function(search) {
     var newKeywords = $scope.existing.keywords.slice();
      if (search && newKeywords.indexOf(search) === -1) {
        newKeywords.unshift(search);
      }
      return newKeywords;
    }

	$scope.addKeyword = function(keywordList) {
	    if (keywordList) {
	        var keywords = keywordList.split( /,\s*/ );
	        for (var i = 0; i < keywords.length; i++) {
	            var keyword = keywords[i].trim();
                if ($scope.document.keywords.indexOf(keyword) == -1) {
                    $scope.document.keywords.push(keyword);
                }
	        }
		}
        $scope.keywordsError = false;
        $scope.toAddKeywords = {};// reset field
	};

	$scope.removeKeyword = function(keyword) {
		var position = $scope.document.keywords.indexOf(keyword);
		if (keyword && position != -1) {
			$scope.document.keywords.splice(position, 1);
		}

		if ($scope.document.keywords.length == 0) {
            $scope.keywordsError = true;
		}
	};

}]);

// Controller for bibliography View
coselmarControllers.controller("ModalDocumentsBibliographyCtrl", function($scope, $uibModalInstance, searchModel, documentService, errorService){

	$scope.example = searchModel;

	{
        if (angular.isDate($scope.example.depositAfterDate)) {
            $scope.example.depositAfterDate = $scope.example.depositAfterDate.getTime();
        }

        if (angular.isDate($scope.example.depositBeforeDate)) {
            $scope.example.depositBeforeDate = $scope.example.depositBeforeDate.getTime();
        }

        if (angular.isDate($scope.example.publicationAfterDate)) {
            $scope.example.publicationAfterDate = $scope.example.publicationAfterDate.getTime();
        }

        if (angular.isDate($scope.example.publicationBeforeDate)) {
            $scope.example.publicationBeforeDate = $scope.example.publicationBeforeDate.getTime();
        }

        $scope.$emit('dataLoading');
        documentService.getSearchBibliography($scope.example, function(citations) {
                $scope.citations = citations;
            }, errorService.defaultFailOnCall, function() {
                $scope.$emit('dataLoaded');
        });
	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

});

// Controller for single document View
coselmarControllers.controller("DocumentViewCtrl",
 	['$scope', '$route', '$location', '$routeParams', 'coselmarConfig', 'notify', 'documentService', 'errorService',
 	 function($scope, $route, $location, $routeParams, coselmarConfig, notify, documentService, errorService) {

	$scope.container = {baseUrl : coselmarConfig.BASE_URL + "v1"};
	$scope.upload = {};
	$scope.users = [];
    $scope.hasErrors = false;
    $scope.toAddKeywords = {};

	$scope.editSession = $routeParams.edit ? $routeParams.edit : false;

	documentService.getDocument($routeParams.documentId, function(document) {
	    $scope.document = document;
        $scope.canEdit = !(document.relatedQuestions && document.relatedQuestions.length > 0);
        $scope.canEdit = $scope.context.currentUser.role == 'ADMIN' || ($scope.canEdit && ($scope.context.currentUser.role == 'SUPERVISOR' || $scope.context.currentUser.userId == document.ownerId));
        if (document.publicationDate) {
            $scope.document.publicationDate = new Date(document.publicationDate);
        }
        // update scope about current user : if he is participant, more option enable in ui
        if (document.authorizedUsers) {
            $scope.users = document.authorizedUsers;
            bindUsers($scope.document.authorizedUsers, $scope.usersIndex);
        }
	}, errorService.defaultFailOnCall);

	$scope.deleteDocument = function(documentId){

		// Call service to create a new document
		documentService.deleteDocument(documentId, $scope, function() {
                // Notification
                notify({
                  message: "document.message.deleted",
                  classes: "alert-warning",
                  container: "#main-container",
                  templateUrl: "views/notificationTemplate.html"
                });
				// Go back to documents list
				$location.path("/documents");
			}, errorService.defaultFailOnCall);

	};

    // edit part

	//to enter in edit mode from view
	$scope.edit = function() {
	    if ($scope.canEdit) {
		    $location.search("edit");
	    }
	}

	$scope.existing = {'types' : [], 'keywords' : []};

    if ($scope.editSession == true) {
        documentService.findAllTypes(function(results) {
            $scope.existing.types = results;
        });

        documentService.findAllKeywords(function(results) {
            $scope.existing.keywords = results;
        });
    }

    $scope.isFormValid = function() {
        var isValid = $scope.document.name && $scope.document.name.length > 0;
        isValid = isValid && $scope.document.type && $scope.document.type.length > 0;
        isValid = isValid && $scope.document.keywords && $scope.document.keywords.length > 0;
        isValid = isValid && $scope.document.summary && $scope.document.summary.length > 0;
        if (!$scope.document.fileName && $scope.context.currentUser.role != 'ADMIN') {
            isValid = isValid && (($scope.document.externalUrl && $scope.document.externalUrl.length > 0) || $scope.upload.file);
        }
        return isValid;
    };

    $scope.removeFile = function() {
        $scope.document.withFile = false;
        $scope.document.fileName = undefined;
        $scope.document.mimeType = undefined;
    }

	$scope.saveDocument = function(){

		// Call service to save document
        if ($scope.document.keywords.length == 0) {
		    $scope.keywordsError = true;
		    $scope.hasErrors = true;

        } else if ($scope.isFormValid()) {
            $scope.$emit('dataLoading');
		    $scope.hasErrors = false;

            // Adapt publicationDate to send a timestamp
            if (angular.isDate($scope.document.publicationDate)) {
                $scope.document.publicationDate = $scope.document.publicationDate.getTime();
            }

            // Adapt external link format if protocol is not set
            if ($scope.document.externalUrl) {
                var urlPattern = /^http[s]?:\/\/.*$/;
                if (!urlPattern.test($scope.document.externalUrl)) {
                  $scope.document.externalUrl = 'http://' + $scope.document.externalUrl;
                }
            }

            documentService.saveDocument($scope.document, function() {
                    if ($scope.upload.file) {
                        var documentId = $scope.document.id;
                        documentService.saveDocumentFile(documentId, $scope.upload.file, function() {
                            // Notification
                            notify({
                              message: "document.message.updated",
                              classes: "alert-success",
                              container: "#main-container",
                              templateUrl: "views/notificationTemplate.html"
                            });
                            $location.search("");
                        });
                    } else {
                        // Notification
                        notify({
                          message: "document.message.updated",
                          classes: "alert-success",
                          container: "#main-container",
                          templateUrl: "views/notificationTemplate.html"
                        });
                        $location.search("");
                    }
                }, errorService.defaultFailOnCall,
                function() {
                    $scope.$emit('dataLoaded');
                });

		} else {
		    $scope.hasErrors = true;
		}

	};

  $scope.cancelDocumentModification = function(){
    $location.search("");
  }

    $scope.getExistingKeywords = function(search) {
     var newKeywords = $scope.existing.keywords.slice();
      if (search && newKeywords.indexOf(search) === -1) {
        newKeywords.unshift(search);
      }
      return newKeywords;
    }

	$scope.addKeyword = function(keywordList) {
	    if (keywordList) {
	        var keywords = keywordList.split( /,\s*/ );
	        for (var i = 0; i < keywords.length; i++) {
	            var keyword = keywords[i].trim();
                if ($scope.document.keywords.indexOf(keyword) == -1) {
                    $scope.document.keywords.push(keyword);
                }
	        }
		}
        $scope.keywordsError = false;
        $scope.toAddKeywords = {};// reset field
	};

	$scope.removeKeyword = function(keyword) {
		var position = $scope.document.keywords.indexOf(keyword);
		if (keyword && position != -1) {
			$scope.document.keywords.splice(position, 1);
		}

        if ($scope.document.keywords.length == 0) {
            $scope.keywordsError = true;
        }
	};

	// Participants, clients and supervisors management for ui-select
	$scope.usersIndex = [];
	$scope.refreshUsers = function(searchKeyword) {
		var searchKeywords = [];
		if (searchKeyword && searchKeyword.length > 0) {
			searchKeywords.push(searchKeyword);
		}

		documentService.findExperts({'active': true, 'fullTextSearch' : searchKeywords}, function(users) {
			$scope.users = users;
			$scope.usersIndex = [];
			angular.forEach($scope.users, function(user) {
				$scope.usersIndex[user.id] = user;
			});
			bindUsers($scope.document.authorizedUsers, $scope.usersIndex);
		});
	}

	// function to be sure to have same user objects in list
	var bindUsers = function(toDeal, index) {
		if (toDeal) {
			for(var i = 0; i < toDeal.length; i++) {
				var user = toDeal[i];
				if (index[user.id]) {
					toDeal[i] = index[user.id];
				}
			}
		}
	};


} ]);

// Controller for document file download
coselmarControllers.controller("DocumentFileDlCtrl", [ '$scope','documentService', '$routeParams', function($scope, documentService, $routeParams) {
	documentService.getDocumentFile($routeParams.documentId, $scope);
} ]);

coselmarControllers.directive('ngFileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.ngFileModel);
            var modelSetter = model.assign;

            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);


/////////////////////////////////////////////////
//////////////  Users Part  /////////////////////
/////////////////////////////////////////////////

// Controller for All User View
coselmarControllers.controller("UsersCtrl", ['$scope', '$route', '$routeParams', '$location', 'userService', 'errorService', 'notify', function($scope, $route, $routeParams, $location, userService, errorService, notify){

    $scope.$emit('dataLoading');

	//manage keywords if given
	$scope.example = { fullTextSearch : [], active : "true", role : '', page: 0, limit: 25};

	var keywords = $routeParams.keywords;
	if (Array.isArray(keywords)) {
		$scope.example.fullTextSearch = keywords;
	} else if (keywords) {
		$scope.example.fullTextSearch.push(keywords);
	}
	var showDisable = $routeParams.showDisable;
	if (showDisable) {
		$scope.example.showDisable = showDisable;
	} else {
		$scope.example.showDisable = false;
	}

	var advancedSearch = $routeParams.advancedSearch;
	if (advancedSearch) {
	    $scope.advanced = advancedSearch;
	} else {
	    $scope.advanced = false;
	}

	userService.getPaginatedUsers($scope.example, function(paginatedUsers) {
		    $scope.paginationData = paginatedUsers;
		    $scope.$broadcast('pageLoaded', $scope.paginationData.currentPage, paginatedUsers.count)
	    }, errorService.defaultFailOnCall, function() {
            $scope.$emit('dataLoaded');
    });

	$scope.deleteUser = function(userId){

		userService.deleteUser(userId, $scope, function() {
		    // Notification
            notify({
              message: "user.message.delete",
              classes: "alert-warning",
              container: "#main-container",
              templateUrl: "views/notificationTemplate.html"
            });
			// Go back to documents list
			$route.reload();
		});
	};

	$scope.disableUser = function(user){

		user.active = false;
		userService.saveUser(user, function() {
		    // Notification
            notify({
              message: "user.message.disable",
              classes: "alert-warning",
              container: "#main-container",
              templateUrl: "views/notificationTemplate.html"
            });
			// Go back to users list
			$location.path("/users");
		});

	};

	$scope.enableUser = function(user){

		user.active = true;
		userService.saveUser(user, function() {
		    // Notification
            notify({
              message: "user.message.enable",
              classes: "alert-success",
              container: "#main-container",
              templateUrl: "views/notificationTemplate.html"
            });
			// Go back to users list
			$location.path("/users");
		});

	};

	$scope.searchUsers = function(){
		$location.search('keywords', $scope.example.fullTextSearch);
		$location.search('showDisable', $scope.example.showDisable);
	};

	$scope.advancedSearchUsers = function() {
		if ($scope.example.role && $scope.example.role == "ALL") {
		    // Because "ALL" role is just a hack to select no role, remove it from example
			delete $scope.example.role;
		};
		$scope.$emit('dataLoading');


	    userService.getPaginatedUsers($scope.example, function(paginatedUsers) {
		      $scope.paginationData = paginatedUsers;
		      $scope.$broadcast('pageLoaded', $scope.paginationData.currentPage, paginatedUsers.count)
            }, errorService.defaultFailOnCall, function() {
                $scope.$emit('dataLoaded');
            }
        );
	};

    $scope.searchMode = function(type) {
      if (type == 'advanced') {
        $location.search('advancedSearch');
        $scope.advanced = true;
      } else {
        $location.search("");
        $scope.advanced = false;
      }
    };

    $scope.getExportUsersUrl = function() {
      return userService.getExportUsersUrl();
	};

    $scope.getToken = function() {
      return localStorage.getItem('coselmar-jwt');
    };

    $scope.$on('loadPage', function(event, pageNumber, pageSize) {
        $scope.example.page = pageNumber;
        $scope.example.limit = pageSize;
        userService.getPaginatedUsers($scope.example, function(paginatedUsers) {
		    $scope.paginationData = paginatedUsers;
	    }, errorService.defaultFailOnCall, function() {
		    $scope.$broadcast('pageLoaded', $scope.paginationData.currentPage, $scope.paginationData.count);
            $scope.$emit('dataLoaded');
        });
    });

}]);

// Controller for new user View
coselmarControllers.controller("NewUserCtrl", ['$scope', '$route', '$location', 'userService', 'notify', 'errorService',
        function($scope, $route, $location, userService, notify, errorService) {

	if (!$scope.context && $scope.context.currentUser) {
	    errorService.reject401();
	} else if ($scope.context.currentUser.role == 'ADMIN') {
	    $scope.user = {'role' : 'EXPERT'};
	} else if ($scope.context.currentUser.role == 'SUPERVISOR') {
	    $scope.user = {'role' : 'CLIENT'};
	} else {
	    $location.path("403");
	}

	$scope.saveUser = function(isValidForm){

		// Call service to create a new user
		if(isValidForm) {
		    $scope.$emit('dataLoading');
			userService.saveUser($scope.user, function() {
                notify({
                  message: "user.message.created",
                  classes: "alert-success",
                  container: "#main-container",
                  templateUrl: "views/notificationTemplate.html"
                });
				$location.path("/users");

			}, function(error) {
				if (error.status == 409) {
				  // Mail already existing
                    notify({
                      message: "user.message.mail.alreadyExisting",
                      classes: "alert-danger",
                      container: "#main-container",
                      templateUrl: "views/notificationTemplate.html"
                    });
				} else if (error.status == 403) {
				    errorService.reject403();

				} else if (error.status == 401) {
				    errorService.reject401();

				} else {
                    notify({
                      message: "common.message.internalError",
                      classes: "alert-danger",
                      container: "#main-container",
                      templateUrl: "views/notificationTemplate.html"
                    });

				}
			}, function() {
                $scope.$emit('dataLoaded');
            });
		}

	};

  $scope.cancelUserModification = function(){
    $location.path('/users');
  };
}]);

// Controller for single User View & Edit
coselmarControllers.controller("UserViewCtrl",
 	['$scope', '$route', '$location', 'userService', '$routeParams', 'notify', 'errorService',
 	 function($scope, $route, $location, userService, $routeParams, notify, errorService) {

    $scope.$emit('dataLoading');

	$scope.editMode = $routeParams.edit;
	if ($scope.editMode) {
		$scope.editPassword = true;
	    $scope.messages = [];
	}

	userService.getUser($routeParams.userId, function(user) {
	    $scope.user = user;
	    var emailPattern = /^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+((\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)?)+@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\-]*[a-zA-Z0-9])?$/;
	    $scope.simpleLogin = !emailPattern.test(user.mail);
	}, errorService.defaultFailOnCall, function() {
        $scope.$emit('dataLoaded');
    });

    userService.getUserProjects({userId : $routeParams.userId, role : 'SUPERVISOR'}, function(projects) {
        $scope.supervisorProjects = projects;
    });

    userService.getUserProjects({userId : $routeParams.userId, role : 'PARTICIPANT'}, function(projects) {
        $scope.participantProjects = projects;
    });

    userService.getUserProjects({userId : $routeParams.userId, role : 'CONTRIBUTOR'}, function(projects) {
        $scope.contributorProjects = projects;
    });

    userService.getUserProjects({userId : $routeParams.userId, role : 'CLIENT'}, function(projects) {
        $scope.clientProjects = projects;
    });

	$scope.deleteUser = function(userId){

		userService.deleteUser(userId, $scope, function() {
		    // Notification
            notify({
              message: "user.message.deleted",
              classes: "alert-warning",
              container: "#main-container",
              templateUrl: "views/notificationTemplate.html"
            });
			// Go back to users list
			$location.path("/users");

		}, errorService.defaultFailOnCall);

	};

	$scope.disableUser = function(){

		var user = $scope.user;
		user.active = false;
		userService.saveUser(user, function() {
		    // Notification
            notify({
              message: "user.message.disable",
              classes: "alert-warning",
              container: "#main-container",
              templateUrl: "views/notificationTemplate.html"
            });
			// Go back to users list
			$route.reload();

		}, errorService.defaultFailOnCall);

	};

	$scope.enableUser = function(){

		var user = $scope.user;
		user.active = true;
		userService.saveUser(user, function() {
		    // Notification
            notify({
              message: "user.message.enable",
              classes: "alert-success",
              container: "#main-container",
              templateUrl: "views/notificationTemplate.html"
            });
			// Go back to users list
			$route.reload();

		}, errorService.defaultFailOnCall);

	};

	$scope.modifyUser = function(){
		$location.search("edit");
	};

	$scope.saveUser = function(isValidForm){

		// Call service to create a new user
		if(isValidForm) {
		    $scope.$emit('dataLoading');
			userService.saveUser($scope.user,
				function() {
                    // Notification
                    notify({
                      message: "user.message.updated",
                      classes: "alert-success",
                      container: "#main-container",
                      templateUrl: "views/notificationTemplate.html"
                    });
					// On success, back on users list for admin or supervisor
					if ($scope.context.currentUser.role == 'ADMIN' || $scope.context.currentUser.role == 'SUPERVISOR') {
					    $location.path("/users");
					} else {
					    $location.search("");
					}

				},function(error) {
                    if (error.status == 409) {
                      // Mail already existing
                        notify({
                          message: "user.message.mail.alreadyExisting",
                          classes: "alert-danger",
                          container: "#main-container",
                          templateUrl: "views/notificationTemplate.html"
                        });
                    } else if (error.status == 403) {
                        errorService.reject403();

                    } else if (error.status == 401) {
                        errorService.reject401();

                    } else if (error.status == 404) {
                        errorService.reject404();

                    } else {
                        notify({
                          message: "common.message.internalError",
                          classes: "alert-danger",
                          container: "#main-container",
                          templateUrl: "views/notificationTemplate.html"
                        });

                    }
			}, function() {
                    $scope.$emit('dataLoaded');
                });
		}
	};

  $scope.cancelUserModification = function(){
    $location.search("");
  };
} ]);

coselmarControllers.controller("NewPasswordCtrl", ['$scope', '$translate', 'userService', function($scope, $translate, userService){

  $scope.alerts = [];

  $scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };

  $scope.sendPassword = function(){

		// Call service to create a new user
    if($scope.mail) {
      userService.sendNewPassword($scope.mail, function() {
	    $scope.alerts.push({ type : 'success', msg : "user.message.newPasswordSent"});
	    delete $scope.mail;
      }, function(error) {
        if (error.status == 404) {
	      $scope.alerts.push({ type : 'danger', msg : "user.message.unknownMail"});
        } else {
	      $scope.alerts.push({ type : 'danger', msg : "common.message.internalError"});
        }
      });
	} else {
	  $scope.alerts.push({ type : 'danger', msg : "user.message.enterMail"});
    };

  };
}]);

// This directive checks two inputs have identical values.
// This is used for new password check
coselmarControllers.directive('identicalCheck', [function () {
	return {
		require: 'ngModel',
		link: function (scope, elem, attrs, ctrl) {
			var firstWord = '#' + attrs.identicalCheck;
			elem.add(firstWord).on('keyup', function () {
				scope.$apply(function () {
					var areMatching = elem.val()===$(firstWord).val();
					ctrl.$setValidity('identicalmatch', areMatching);
				});
			});
		}
	}
}]);

/////////////////////////////////////////////////
////////////  Questions Part  ///////////////////
/////////////////////////////////////////////////

// Controller for All Question View
coselmarControllers.controller("QuestionsCtrl", ['$scope', '$route', '$routeParams', '$location', 'questionsService', 'notify', 'errorService',
 					function($scope, $route, $routeParams, $location, questionsService, notify, errorService){
    $scope.$emit('dataLoading');

	$scope.searchOptions = { 'privacy' : '', 'status' : '', 'fullTextSearch' : [], page: 0, limit: 25};

	var advancedSearch = $routeParams.advancedSearch;
	if (advancedSearch) {
	    $scope.advanced = advancedSearch;
	} else {
	    $scope.advanced = false;
	}

	questionsService.getPaginatedQuestions($scope.searchOptions, function(paginatedQuestions) {
		// success : just get the questions
		$scope.paginationData = paginatedQuestions;
        $scope.$broadcast('pageLoaded', $scope.paginationData.currentPage, paginatedQuestions.count)
	}, errorService.defaultFailOnCall, function() {
        $scope.$emit('dataLoaded');
    });

	$scope.searchQuestions = function() {
	    $scope.$emit('dataLoading');
	    questionsService.getPaginatedQuestions($scope.searchOptions, function(paginatedQuestions) {
		  $scope.paginationData = paginatedQuestions;
          $scope.$broadcast('pageLoaded', $scope.paginationData.currentPage, paginatedQuestions.count)
		}, errorService.defaultFailOnCall, function() {
            $scope.$emit('dataLoaded');
        });

	};

    $scope.searchMode = function(type) {
      if (type == 'advanced') {
        $location.search('advancedSearch');
        $scope.advanced = true;
      } else {
        $location.search("");
        $scope.advanced = false;
      }
    };

	$scope.advancedSearchQuestions = function() {
	  $scope.$emit('dataLoading');

      //Convert Dates to timestamp
      if (angular.isDate($scope.searchOptions.submissionAfterDate)) {
        $scope.searchOptions.submissionAfterDate = $scope.searchOptions.submissionAfterDate.getTime();
      }

      if (angular.isDate($scope.searchOptions.submissionBeforeDate)) {
        $scope.searchOptions.submissionBeforeDate = $scope.searchOptions.submissionBeforeDate.getTime();
      }

      if (angular.isDate($scope.searchOptions.deadlineAfterDate)) {
        $scope.searchOptions.deadlineAfterDate = $scope.searchOptions.deadlineAfterDate.getTime();
      }

      if (angular.isDate($scope.searchOptions.deadlineBeforeDate)) {
          $scope.searchOptions.deadlineBeforeDate = $scope.searchOptions.deadlineBeforeDate.getTime();
      }

	  questionsService.getPaginatedQuestions($scope.searchOptions, function(paginatedQuestions) {
	    $scope.paginationData = paginatedQuestions;
        $scope.$broadcast('pageLoaded', $scope.paginationData.currentPage, paginatedQuestions.count)
	  }, errorService.defaultFailOnCall, function() {
        $scope.$emit('dataLoaded');
      });

	};

	$scope.deleteQuestion = function(questionId) {

		questionsService.deleteQuestion(questionId, function(questions) {
		    // Notification
            notify({
              message: "question.message.deleted",
              classes: "alert-warning",
              container: "#main-container",
              templateUrl: "views/notificationTemplate.html"
            });
			// success : just reload the questions
			$route.reload();

		}, errorService.defaultFailOnCall);
	};

  $scope.cancelQuestionModification = function(){
    $location.search("");
  };

	$scope.getUserNames = function(users) {

		var names = "";
		for(var i=0; i < users.length; i++) {
			names += users[i].firstName + " " + users[i].name;
      if (i != (users.length -1)) {
        names += ", ";
      }
		}

		return names;
	};

	$scope.getDocumentTitles = function(documents) {

		var titles = "";
		for(var i=0; i < documents.length; i++) {
			titles += documents[i].name;
			if (i != (documents.length)) {
        titles += ", ";
      }
		}

		return titles;
	};

	$scope.getExportQuestionsUrl = function() {
      return questionsService.getExportQuestionsUrl();
	};

    $scope.getToken = function() {
      return localStorage.getItem('coselmar-jwt');
    };

    // Notification de changement dans le module de pagination
    $scope.$on('loadPage', function(event, pageNumber, pageSize) {
        $scope.searchOptions.page = pageNumber;
        $scope.searchOptions.limit = pageSize;
        questionsService.getPaginatedQuestions($scope.searchOptions, function(paginatedQuestions) {
	      $scope.paginationData = paginatedQuestions;
	    }, errorService.defaultFailOnCall, function() {
		    $scope.$broadcast('pageLoaded', $scope.paginationData.currentPage, $scope.paginationData.count);
            $scope.$emit('dataLoaded');
        });
    });

}]);

// Controller for Question View
coselmarControllers.controller("QuestionCtrl", ['$scope', '$route', '$routeParams', '$location', '$uibModal', 'notify', 'questionsService', 'errorService',
 					function($scope, $route, $routeParams, $location, $uibModal, notify, questionsService, errorService){

	$scope.editSession = $routeParams.edit ? $routeParams.edit : false;
	$scope.isCurrentParticipant = false;

	$scope.question = {'privacy' : 'PUBLIC',
						'themes' : [], 'participants' : [], 'externalExperts' : [],
						'clients' : [], 'relatedDocuments': [], 'newRelatedDocuments' : [],
						'links' : []};
	$scope.existing = {'types' : [], 'themes' : []};
	$scope.topWords = [{text:"coselmar", weight: 10}, {text: "Ifremer", weight: 5}];
    $scope.cloudColors = ["#2f8389", "#349198", "#39A0A8", "#3eaeb6", "#48b8c0"];
    $scope.toAddTheme = {};

	// Preload exiting types and themes
	questionsService.findAllTypes(function(results) {
		$scope.existing.types = results;
	});

	questionsService.findAllThemes(function(results) {
		$scope.existing.themes = results;
	});

	//to enter in edit mode from view
	$scope.edit = function() {
		$location.search("edit");
	}

	// Participants, clients and supervisors management for ui-select
	$scope.participantsIndex = {};
	$scope.clientsIndex = {};
	$scope.supervisorsIndex = {};

	$scope.users = { 'participants' : [], 'clients': [], 'supervisors' : []};

	// function to be sure to have same user objects in list
	var bindUsers = function(toDeal, index) {
		if (toDeal) {
			for(var i = 0; i < toDeal.length; i++) {
				var user = toDeal[i];
				if (index[user.id]) {
					toDeal[i] = index[user.id];
				}
			}
		}
	};

	$scope.refreshExperts = function(searchKeyword) {
		var searchKeywords = [];
		if (searchKeyword && searchKeyword.length > 0) {
			searchKeywords.push(searchKeyword);
		}

		questionsService.findUsers({'role': 'EXPERT', 'active': true, 'fullTextSearch' : searchKeywords}, function(users) {
			$scope.users.participants = users;
			$scope.participantsIndex = {};
			angular.forEach($scope.users.participants, function(participant) {
				$scope.participantsIndex[participant.id] = participant;
			});
			bindUsers($scope.question.participants, $scope.participantsIndex);
		});
	}

	$scope.refreshClients = function(searchKeyword) {
		var searchKeywords = [];
		if (searchKeyword && searchKeyword.length > 0) {
			searchKeywords.push(searchKeyword);
		}

		questionsService.findUsers({'active': 'true', 'fullTextSearch' : searchKeywords}, function(users) {
			$scope.users.clients = users;
			$scope.clientsIndex = {};
			angular.forEach($scope.users.clients, function(client) {
				$scope.clientsIndex[client.id] = client;
			});
			bindUsers($scope.question.clients, $scope.clientsIndex);
		});
	}

	$scope.refreshSupervisors = function(searchKeyword) {
		var searchKeywords = [];
		if (searchKeyword && searchKeyword.length > 0) {
			searchKeywords.push(searchKeyword);
		}

		questionsService.findUsers({'role': 'SUPERVISOR', 'active': 'true', 'fullTextSearch' : searchKeywords}, function(users) {
			$scope.users.supervisors = users;
			$scope.supervisorsIndex = {};
			angular.forEach($scope.users.supervisors, function(supervisor) {
				$scope.supervisorsIndex[supervisor.id] = supervisor;
			});
			bindUsers($scope.question.supervisors, $scope.supervisorsIndex);
		});
	}

	// call refresh for init
	if ($scope.editSession) {
		$scope.refreshExperts("");
		$scope.refreshClients("");
		$scope.refreshSupervisors("");
	}

	if ($routeParams.questionId) {
	    $scope.$emit('dataLoading');
		questionsService.getQuestion($routeParams.questionId,
			function(question) {
			// success : just get the questions
			$scope.question = question;

			if(question.deadline) {
				$scope.question.deadline = new Date(question.deadline);
			}

			// update scope about current user : if he is participant, more option enable in ui
			if ($scope.question.participants) {
				for (var i = 0; i < $scope.question.participants.length; i++) {
					if ($scope.question.participants[i].id == $scope.context.currentUser.userId) {
						$scope.isCurrentParticipant = true;
						// Should be able to put new documents in question
						$scope.question.newRelatedDocuments = [];
						break;
					}
				}
			}

			if(!question.relatedDocuments) {
				$scope.question.relatedDocuments = [];
			}

			if (question.participants) {
				$scope.users.participants = question.participants;
				bindUsers($scope.question.participants, $scope.participantsIndex);
			}
			if (question.clients) {
				$scope.users.clients = question.clients;
				bindUsers($scope.question.clients, $scope.clientsIndex);
			}
			if (question.supervisors) {
				$scope.users.supervisors = question.supervisors;
				bindUsers($scope.question.supervisors, $scope.supervisorsIndex);
			}

			if(!question.links) {
				$scope.question.links = [];
			}

		}, errorService.defaultFailOnCall, function() {
            $scope.$emit('dataLoaded');
        });


        questionsService.getTopWords($routeParams.questionId, function(result) {
          $scope.topWords = result.data;
          for (var i=0; i < $scope.topWords.length; i++) {
            $scope.topWords[i].link = "#/referential?onDocuments&onQuestions&keywords=" + $scope.topWords[i].text;
          }
        }, function(fail) {
          $scope.topWords = [];
        });

	};

	//Deletion
	$scope.deleteQuestion = function(){
		questionsService.deleteQuestion($routeParams.questionId, function() {
            notify({
              message: "question.message.deleted",
              classes: "alert-warning",
              container: "#main-container",
              templateUrl: "views/notificationTemplate.html"
            });
			// success : goto questions list
			$location.path('/questions');

		}, errorService.defaultFailOnCall);
	};

	$scope.saveQuestion = function() {

        if (angular.isDate($scope.question.deadline)) {
            $scope.question.deadline = $scope.question.deadline.getTime();
        }

        if (!$scope.question.themes || $scope.question.themes.length == 0) {
            $scope.themesError = true;
        } else {
            $scope.$emit('dataLoading');
            // Call service to create a new question
            questionsService.saveQuestion($scope.question, function() {
                    // Notification
                    notify({
                      message: "question.message.saved",
                      classes: "alert-success",
                      container: "#main-container",
                      templateUrl: "views/notificationTemplate.html"
                    });
                    if ($routeParams.questionId) {
                        $location.search("");
                    } else {
                        $location.path("/questions");
                    }
            }, errorService.defaultFailOnCall, function() {
                $scope.$emit('dataLoaded');
            });
        }
	};

  $scope.cancelQuestionModification = function(){
    $location.search("");
  };
  $scope.cancelQuestionCreation = function(){
    $location.path('/questions');
  };
  $scope.cancelCloseSession = function(){
    $scope.closeSession = false;
  };

	$scope.closeQuestion = function(){
		$scope.question.status = "CLOSED";

        // manage deadline which has been transform to Date for form matching
        if (angular.isDate($scope.question.deadline)) {
            $scope.question.deadline = $scope.question.deadline.getTime();
        }

        $scope.$emit('dataLoading');
		questionsService.saveQuestion($scope.question, function() {
            // Notification
            notify({
              message: "question.message.closed",
              classes: "alert-success",
              container: "#main-container",
              templateUrl: "views/notificationTemplate.html"
            });
			$route.reload();
		},errorService.defaultFailOnCall, function() {
            $scope.$emit('dataLoaded');
        });
	};

	$scope.openCloseQuestionSession = function(){
		if (!$scope.question.closingDocuments) {
			$scope.question.closingDocuments = [];
		}

		$scope.closeSession = true;
	};

	$scope.reopenQuestion = function(){
		$scope.question.status = "IN_PROGRESS";

        // manage deadline which has been transform to Date for form matching
        if (angular.isDate($scope.question.deadline)) {
            $scope.question.deadline = $scope.question.deadline.getTime();
        }

        $scope.$emit('dataLoading');
		questionsService.saveQuestion($scope.question, function() {
            // Notification
            notify({
              message: "question.message.reopened",
              classes: "alert-success",
              container: "#main-container",
              templateUrl: "views/notificationTemplate.html"
            });
			$route.reload();
		},errorService.defaultFailOnCall, function() {
            $scope.$emit('dataLoaded');
        });
	};

	$scope.adjournQuestion = function(){
		$scope.question.status = "ADJOURNED";

        // manage deadline which has been transform to Date for form matching
        if (angular.isDate($scope.question.deadline)) {
            $scope.question.deadline = $scope.question.deadline.getTime();
        }

        $scope.$emit('dataLoading');
		questionsService.saveQuestion($scope.question, function() {
            // Notification
            notify({
              message: "question.message.adjourned",
              classes: "alert-warning",
              container: "#main-container",
              templateUrl: "views/notificationTemplate.html"
            });
			$route.reload();
		},errorService.defaultFailOnCall, function() {
            $scope.$emit('dataLoaded');
        });
	};

	// New documents added
	$scope.validateNewDocuments = function(){

		if ($scope.question.newRelatedDocuments
		 	&& $scope.question.newRelatedDocuments.length > 0) {

			questionsService.addNewDocuments($routeParams.questionId, $scope.question.newRelatedDocuments, function() {
                // Notification
                notify({
                  message: "question.message.newDocuments.added",
                  classes: "alert-success",
                  container: "#main-container",
                  templateUrl: "views/notificationTemplate.html"
                });
				$route.reload();

			}, errorService.defaultFailOnCall);
		}
	};

    $scope.getExistingThemes = function(search) {
     var newThemes = $scope.existing.themes.slice();
      if (search && newThemes.indexOf(search) === -1) {
        newThemes.unshift(search);
      }
      return newThemes;
    }

	$scope.addTheme = function(themeList) {
	    if (!$scope.question.themes) {
	        $scope.question.themes = [];
        }
	    if (themeList) {
	        var themes = themeList.split( /,\s*/ );
	        for (var i = 0; i < themes.length; i++) {
	            var theme = themes[i].trim();
                if ($scope.question.themes.indexOf(theme) == -1) {
			        $scope.question.themes.push(theme);
                }
	        }
		}
        $scope.keywordsError = false;
        $scope.toAddTheme = {};// reset field
	};

	$scope.removeTheme = function(theme) {
		var position = $scope.question.themes.indexOf(theme);
		if (theme && position != -1) {
			$scope.question.themes.splice(position, 1);
		}
		if ($scope.question.themes.length == 0) {
            $scope.themesError = true;
		}
	}

	$scope.removeContributor = function(contributor) {
		var position = $scope.question.contributors.indexOf(contributor);
		if (contributor && position != -1) {
			$scope.question.contributors.splice(position, 1);
		}
	}

	// Modals part for documents
	$scope.modalSearchDocuments = function (documentList) {

		var modalInstance = $uibModal.open({
			templateUrl: 'views/documents/modalDocumentSearch.html',
			controller: 'ModalSearchDocumentsCtrl',
			size: 'lg',
			resolve : {
			    presentDocumentIds : function() {
				    if (angular.isDefined($scope.question.relatedDocuments)) {
					    return $scope.question.relatedDocuments.map(function(document) {
					        return document.id;
					    });
			        } else {
			            return [];
			        }
				}
			}
		});

		modalInstance.result.then(function (selectedDocument) {
			var already = false;
			for (var i = 0; i < documentList.length; i++) {
				if (documentList[i].id == selectedDocument.id) {
					already = true;
				}
			}
			if (!already) {
				documentList.push(selectedDocument);
			}
		});
	};

	$scope.modalCreateDocument = function (documentList) {

		var modalInstance = $uibModal.open({
			templateUrl: 'views/documents/modalDocumentEdit.html',
			controller: 'ModalCreateDocumentsCtrl',
			size: 'lg'
		});

		modalInstance.result.then(function (selectedDocument) {
			var already = false;
			for (var i = 0; i < documentList.length; i++) {
				if (documentList[i].id == selectedDocument.id) {
					already = true
				}
			}
			if (!already) {
				documentList.push(selectedDocument);
			}
		});
	};

	$scope.removeDocument = function(document, documentList) {
		var position = documentList.indexOf(document);
		if (document && position != -1) {
			documentList.splice(position, 1);
		}
	};

	// Modals part for parent questions
	$scope.modalSearchParents = function () {

		var modalInstance = $uibModal.open({
			templateUrl: 'views/questions/modalQuestionSearch.html',
			controller: 'ModalSearchQuestionsCtrl',
			size: 'lg',
			resolve : {
			    presentQuestionIds : function() {
			        var presentQuestionIds = [];
				    if (angular.isDefined($scope.question.parents)) {
					    presentQuestionIds = $scope.question.parents.map(function(question) {
					        return question.id;
					    });
			        }
			        presentQuestionIds.push($scope.question.id);
			        return presentQuestionIds;
                }
			}
		});

		modalInstance.result.then(function (selectedQuestion) {
			var already = false;
			if ($scope.question.id && $scope.question.id == selectedQuestion.id) {
				already = true;
			}
			if (! $scope.question.parents) {
				$scope.question.parents = [];
			} else {
				for (var i = 0; i < $scope.question.parents.length; i++) {
					if ($scope.question.parents[i].id == selectedQuestion.id) {
						already = true;
					}
				}
			}
			if (!already) {
				$scope.question.parents.push(selectedQuestion);
			}
		});
	};

	$scope.removeParent = function(parent) {
		var position = $scope.question.parents.indexOf(parent);
		if (parent && position != -1) {
			$scope.question.parents.splice(position, 1);
		}
	};

	$scope.isClient = function() {
		var isClient = false;
		angular.forEach($scope.question.clients, function(client) {
			if (client.id == $scope.context.currentUser.id) {
				isClient = true;
			}
		});
		return isClient;
	};

	$scope.modalEditLink = function (link) {

		var modalInstance = $uibModal.open({
			templateUrl: 'views/links/modalLinkEdit.html',
			controller: 'ModalEditLinkCtrl',
			size: 'lg',
			resolve : {
				currentLink : function() {
				    if (angular.isDefined(link)) {
					    return link;
			        } else {
			            return { name : "", url: ""};
			        }
				}
			}
		});

		modalInstance.result.then(function (link) {
			var already = false;
			for (var i = 0; i < $scope.question.links.length; i++) {
				if (link.id && $scope.question.links[i].id == link.id) {
					already = true;
					$scope.question.links[i] = link;
				}
			}
			if (!already) {
				$scope.question.links.push(link);
			}
		});
	};

	$scope.removeLink = function(link) {
		var position = $scope.question.links.indexOf(link);
		if (link && position != -1) {
			$scope.question.links.splice(position, 1);
		}
	};

	$scope.showModalQuestionHierarchy = function() {

		var modalInstance = $uibModal.open({
			templateUrl: 'views/questions/modalHierarchy.html',
			controller: 'ModalQuestionHierarchyCtrl',
			size: 'lg',
			resolve : {
				currentQuestion : $scope.question
			}
		});

	};

}]);

coselmarControllers.controller('ModalSearchDocumentsCtrl', function ($scope, $uibModalInstance, presentDocumentIds, documentService, errorService) {


	$scope.searchKeywords = [];

    $scope._isNotPresent = function(document) {
      return presentDocumentIds.indexOf(document.id) == -1;
    }

    $scope._filterDocuments = function(documents) {
        var filtered = documents.filter($scope._isNotPresent);
        return filtered;
    };

	documentService.getDocuments($scope.searchKeywords, function(documents) {
		$scope.documents = $scope._filterDocuments(documents);
	}, errorService.defaultFailOnCall);

	$scope.searchDocuments = function(searchKeywords) {
		documentService.getDocuments(searchKeywords, function(documents) {
			$scope.documents = $scope._filterDocuments(documents);
		}, errorService.defaultFailOnCall);
	};

	$scope.select = function (document) {
		$uibModalInstance.close(document);
	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

});

coselmarControllers.controller('ModalSearchQuestionsCtrl', function ($scope, $uibModalInstance, presentQuestionIds, questionsService, errorService) {

	$scope.searchKeywords = [];
	$scope.searchOptions = { 'type' : '', 'privacy' : '', 'status' : '', 'fullTextSearch' : []};

    $scope._isNotPresent = function(question) {
      return presentQuestionIds.indexOf(question.id) == -1;
    }

    $scope._filterQuestions = function(questions) {
        var filtered = questions.filter($scope._isNotPresent);
        return filtered;
    };

	questionsService.getQuestions($scope.searchOptions, function(questions) {
		$scope.questions = $scope._filterQuestions(questions);
	}, errorService.defaultFailOnCall);

	$scope.searchQuestions = function(searchKeywords) {
		$scope.searchOptions = { 'privacy' : '', 'status' : '', 'fullTextSearch' : searchKeywords};
		questionsService.getQuestions($scope.searchOptions, function(questions) {
			$scope.questions = $scope._filterQuestions(questions);
		}, errorService.defaultFailOnCall);

	};

	$scope.select = function (question) {
		$uibModalInstance.close(question);
	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

});

coselmarControllers.controller('ModalCreateDocumentsCtrl', function ($scope, $uibModalInstance, documentService, errorService) {

	$scope.document = {'privacy': 'PUBLIC', 'keywords' : []};
	$scope.upload = {};
	$scope.modalExisting = {'keywords' : [], 'types' : []};

	documentService.findAllKeywords(function(result) {
		$scope.modalExisting.keywords = result;
	});

	documentService.findAllTypes(function(result) {
		$scope.modalExisting.types = result;
	});

	$scope.addKeyword = function(keywordList) {
	    if (keywordList) {
	        var keywords = keywordList.split( /,\s*/ );
	        for (var i = 0; i < keywords.length; i++) {
	            var keyword = keywords[i].trim();
	            if ($scope.document.keywords.indexOf(keyword) == -1) {
	                $scope.document.keywords.push(keyword);
                }
            }
        }
        $scope.keywordsError = false;
	};

	$scope.removeKeyword = function(keyword) {
		var position = $scope.document.keywords.indexOf(keyword);
		if (keyword && position != -1) {
			$scope.document.keywords.splice(position, 1);
		}

		if ($scope.document.keywords.length == 0) {
            $scope.keywordsError = true;
		}
	}

	$scope.create = function (isValidForm) {

    // Adapt publicationDate to send a timestamp
    if (angular.isDate($scope.document.publicationDate)) {
        $scope.document.publicationDate = $scope.document.publicationDate.getTime();
    }

    // Adapt external link format if protocol is not set
    if ($scope.document.externalUrl) {
        var urlPattern = /^http[s]?:\/\/.*$/;
        if (!urlPattern.test($scope.document.externalUrl)) {
          $scope.document.externalUrl = 'http://' + $scope.document.externalUrl;
        }
    }

		if ($scope.document.keywords.length == 0) {
		  $scope.keywordsError = true;

        } else if (!$scope.document.externalUrl && !$scope.upload.file) {
		  $scope.hasErrors = true;
		  $scope.missingFileOrExternalError = true;

        } else if (isValidForm) {
          $scope.hasErrors = false;
          documentService.saveDocument($scope.document, function(document) {
              if ($scope.upload.file) {
                var documentId = document.id;
                documentService.saveDocumentFile(documentId, $scope.upload.file, function() {
			      $uibModalInstance.close(document);
                });
              } else {
                $uibModalInstance.close(document);
              }
            }, errorService.defaultFailOnCall);
		} else {
		  $scope.hasErrors = true;
		}
	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

});

coselmarControllers.controller('ModalEditLinkCtrl', function ($scope, $uibModalInstance, currentLink) {

	$scope.link = currentLink;
    $scope.invalidUrl = false;

	$scope.create = function () {

        if ($scope.link.url === "") {
            $scope.invalidUrl = true;
        } else {
            $uibModalInstance.close($scope.link);
        }

	};

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

});

coselmarControllers.controller('ModalQuestionHierarchyCtrl', function ($scope, $uibModalInstance, currentQuestion, questionsService) {

	$scope.question = currentQuestion;
	$scope.depth = 2;
	$scope.ancestors = [];
	$scope.descendants = [];
	$scope.hierarchyTree = { name : $scope.question.title,
	                         url : "#/questions/" + $scope.question.id,
	                         tooltip : $scope.question.themes.join(),
	                         fontWeight : "bold",
	                         parents : [], children : [] };

    $scope.ancestorsReady = false;
    $scope.descendantsReady = false;

    var loadHierarchy = function() {
        if ($scope.ancestorsReady && $scope.descendantsReady) {
            var tree = CollapsibleTree("#questionHierarchy");
            tree.init($scope.hierarchyTree);
            $scope.ancestorsReady = false;
            $scope.descendantsReady = false;
        }
    }

    $scope.loadAncestors = function() {
        var searchParams = { questionId : $scope.question.id, depth: $scope.depth};
        questionsService.getAncestors(searchParams, function(ancestors){
            $scope.ancestors = ancestors;
            // load parents
            angular.forEach($scope.ancestors, function(value, key) {
                var parent = { name : value.title,
	                         url : "#/questions/" + value.id,
	                         tooltip : value.themes.join(),
	                         isparent: true };
                var subParents = loadParents(value, $scope.depth - 1);
                if (subParents.length > 0) {
                    parent.parents = subParents;
                }
                this.push(parent);
            }, $scope.hierarchyTree.parents);
            $scope.ancestorsReady = true;
            loadHierarchy();
        });
    };

    $scope.loadDescendants = function() {
        var searchParams = { questionId : $scope.question.id, depth: $scope.depth};
        questionsService.getDescendants(searchParams, function(descendants){
            $scope.descendants = descendants;
            // load children
            angular.forEach($scope.descendants, function(value, key) {
                var child = { name : value.title,
	                         url : "#/questions/" + value.id,
	                         tooltip : value.themes.join(),
	                         isparent: false };
                var subChildren = loadChildren(value, $scope.depth);
                if (subChildren.length > 0) {
                    child.children = subChildren;
                }
                this.push(child);
            }, $scope.hierarchyTree.children);
            $scope.descendantsReady = true;
            loadHierarchy();
        });
    };

    var loadChildren = function(treeNode, depth) {
        var children = [];
        if (depth > 0) {
            angular.forEach(treeNode.descendants, function(value, key) {
                var child = { name : value.title, isparent: false };
                var subChildren = loadChildren(value, depth - 1);
                if (subChildren.length > 0) {
                    child.children = subChildren;
                }
                this.push(child);
            }, children)
        }
        return children;
    };

    var loadParents = function(treeNode, depth) {
        var parents = [];
        if (depth > 0) {
            angular.forEach(treeNode.ancestors, function(value, key) {
                var parent = { name : value.title, isparent: true };
                var subParents = loadParents(value, depth - 1);
                if (subParents.length > 0) {
                    parent.parents = subParents;
                }
                this.push(parent);
            }, parents)
        }
        return parents;
    };

    $scope.loadAncestors();
    $scope.loadDescendants();

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};

});



/////////////////////////////////////////////////
///////////  Referential Part  //////////////////
/////////////////////////////////////////////////

// Controller for Referential Page
coselmarControllers.controller("ReferentialCtrl", ['$scope', '$routeParams', '$location', 'questionsService', 'documentService',
 					function($scope, $routeParams, $location, questionsService, documentService){

	$scope.result = { questions : undefined, documents : undefined};

	var remoteSearchInReferential = function() {
		if ($scope.search.onQuestions && $scope.search.keywords.length > 0) {
			var questionSearchOptions = { 'privacy' : '', 'status' : '', 'fullTextSearch' : $scope.search.keywords};
			questionsService.getQuestions(questionSearchOptions, function(questions) {
				$scope.result.questions = questions;
			});
		};

		if ($scope.search.onDocuments && $scope.search.keywords.length > 0) {
			documentService.getDocuments($scope.search.keywords, function(documents) {
				$scope.result.documents = documents;
			}, function() {
			    $scope.result.documents = [];
			});
		};

	};

	//init scope : if routeParams, use values
    if ($routeParams &&
     ($routeParams.keywords
        || $routeParams.onDocuments
        || $routeParams.onQuestions)) {

	    $scope.search = { keywords : [], onDocuments: false, onQuestions: false };

        var keywords = $routeParams.keywords;
        if (Array.isArray(keywords)) {
            $scope.search.keywords = keywords;
        } else if (keywords) {
            $scope.search.keywords.push(keywords);
        }

        var onQuestions = $routeParams.onQuestions;
        if (onQuestions) {
            $scope.search.onQuestions = onQuestions;
        }

        var onDocuments = $routeParams.onDocuments;
        if (onDocuments) {
            $scope.search.onDocuments = onDocuments;
        }

        // Launch search
	    remoteSearchInReferential();

    } else {
        // else, default values for screen init, no search is done
	    $scope.search = { keywords : [], onDocuments: true, onQuestions: true };
    }

	$scope.searchInReferential = function(){

		if ($scope.search.keywords) {
			$location.search('keywords', $scope.search.keywords);
		} else {
			$location.search('keywords', null);
		}

		if ($scope.search.onDocuments) {
			$location.search('onDocuments', $scope.search.onDocuments);
		} else {
			$location.search('onDocuments', null);
		}

		if ($scope.search.onQuestions) {
			$location.search('onQuestions', $scope.search.onQuestions);
		} else {
			$location.search('onQuestions', null);
		}
	};

}]);


/////////////////////////////////////////////////
//////////////  Admin Part  /////////////////////
/////////////////////////////////////////////////

// Controller for Admin Page
coselmarControllers.controller("AdminCtrl", ['$scope', '$routeParams', '$location', 'adminService',
 					function($scope, $routeParams, $location, adminService){

	$scope.status = { 'indexRefresh' : 'none', 'massimport' : 'none', 'importResult' : undefined };
	$scope.importResult = undefined;

	$scope.refreshIndex = function() {
		$scope.status.indexRefresh = "inprogress";

		adminService.refreshIndex(function() {
					$scope.status.indexRefresh = "finish";

				},function(error) {
					$scope.status.indexRefresh = "error";
			});

	};

	$scope.importDocumentsZip = function() {
		$scope.status.massimport = "inprogress";

		adminService.importDocumentsZip($scope.uploadFile, function(importResult) {
		            if (importResult.success) {
					    $scope.status.massimport = "finish";
		            } else {
					    $scope.status.massimport = "fail";
					    $scope.status.importResult = importResult;
		            }

				},function(error) {
					$scope.status.massimport = "error";
			});

	};

}]);

/**
 * AngularJS default filter with the following expression:
 * "person in people | filter: {name: $select.search, age: $select.search}"
 * performs a AND between 'name: $select.search' and 'age: $select.search'.
 * We want to perform a OR.
 *
 * Extract from angular-ui-select demo :
 * https://github.com/angular-ui/ui-select/blob/master/examples/demo.js
 */
coselmarControllers.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop] && item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});


/**
 * A generic confirmation modal for risky actions such delete.
 *
 * Usage: Add attributes: ng-confirm-message="Are you sure"? ng-confirm-click="riskyAction()"
 */
coselmarControllers.directive('ngConfirmClick', ['$uibModal', function($uibModal) {

	var ModalInstanceCtrl = function($scope, $uibModalInstance) {
		$scope.ok = function() {
			$uibModalInstance.close();
		};

		$scope.cancel = function() {
			$uibModalInstance.dismiss('cancel');
		};
	};

    return {
        restrict: 'A',
        scope:{
          onConfirm : "&ngConfirmClick",
        },

        link: function(scope, element, attrs) {
          	element.bind('click', function() {
				var message = attrs.ngConfirmMessage || "Are you sure ?";

				var modalHtml = '<div class="modal-title"><h2>Confirm Action</h2></div>';
				modalHtml += '<div class="modal-body"> {{\'' + message + '\' | translate}}</div>';
				modalHtml += '<div class="modal-footer"><button class="btn btn-action btn-success float-right" ng-click="ok()">OK</button><button class="btn btn-action btn-disable" ng-click="cancel()">{{ \'common.button.cancel\' | translate }}</button></div>';

				var modalInstance = $uibModal.open({
				  	template: modalHtml,
				  	controller: ModalInstanceCtrl
				});

				modalInstance.result.then(function() {
				  	scope.onConfirm();
				}, function() {
				  	//Modal dismissed
				});

          	});

        }
    }
}]);

/**
 * Must override datepicker directive to prevent error with dateformat
 * see : https://github.com/angular-ui/bootstrap/issues/2659
 */
coselmarControllers.directive('datepickerPopup', function (){
  return {
    restrict: 'EAC',
    require: 'ngModel',
    link: function(scope, element, attr, controller) {
      //remove the default formatter from the input directive to prevent conflict
      controller.$formatters.shift();
    }
  }
});


coselmarControllers.directive('paginationTool', function() {
  return {
//    restrict: 'E',
    templateUrl: 'views/paginationTemplate.html'
  };
});
