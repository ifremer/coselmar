/*
 * #%L
 * Coselmar :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
coselmarServices.factory('errorService', ['$location', 'notify', function($location, notify){

    var reject401 = function() {
        // create return to path wanted
        var returnTo = $location.path();
        $location.path("/401").search('returnTo', returnTo);
    };
	var reject403 = function() {
        // create return to path wanted
        $location.path("/403");
    };
	var reject404 = function() {
        // create return to path wanted
        $location.path("/404");
    };

	return {
	    reject401 : reject401,
	    reject403 : reject403,
	    reject404 : reject404,

        // Default function for error during call processing
        defaultFailOnCall : function(error) {

            if (error.status == 403) {
                reject403();

            } else if (error.status == 401) {
                reject401();

            } else if (error.status == 404) {
                reject404();

            } else {
                notify({
                  message: "common.message.internalError",
                  classes: "alert-danger",
                  container: "#main-container",
                  templateUrl: "views/notificationTemplate.html"
                });

            }
        }
	};
}]);