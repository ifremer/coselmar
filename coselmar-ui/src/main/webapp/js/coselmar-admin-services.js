/*
 * #%L
 * Coselmar :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
coselmarServices.factory('adminService', ['$resource', 'coselmarConfig', function($resource, coselmarConfig){
	return new Admin($resource, coselmarConfig);
}]);

function Admin(resource, config){


	this.resource = resource;

	var baseURL = config.BASE_URL + "v1/admin";

	this.refreshIndex = function(successFunction, failFunction){

		var serviceURL = baseURL + "/lucene/index";
		// Refresh Lucene Index
		var adminResource = resource(serviceURL, null, {
		  'refresh': {
		   	method: 'POST',
		    headers : {
		  	  'Content-Type' : 'application/x-www-form-urlencoded'
		    },
		}});

		adminResource.save(null, successFunction, failFunction);
	}

	this.importDocumentsZip = function(file, successFunction, failFunction){

		var formData = new FormData();
        formData.append("uploadFile", file);

		var serviceURL = baseURL + "/documents/zip";

		// Launch import !
		var adminResource = resource(serviceURL, null, {
			'upload': {
			  method:'POST',
			  transformRequest: angular.identity,
			  headers: {'Content-Type': undefined, 'Content-Transfer-Encoding': 'utf-8'}
			}
		});
		adminResource.upload(null, formData, successFunction, failFunction);
	}

};