/*
 * #%L
 * Coselmar :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2016 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
var paginationBinder = angular.module('coselmarApp');

paginationBinder.controller('paginationController', function($scope) {
  $scope.pageSizes = [10, 25, 50, 100];

  $scope.pageContext = {
    pageNumber : 0,
    pageSize : 25,
    nbPages : 0,
    firstPage : false,
    lastPage : false,
  };

  $scope._loadPage = function(pageNumber, pageSize) {
    $scope.$emit('loadPage', pageNumber, pageSize);
  };

  $scope.updatePageSize = function() {
    $scope._loadPage(0, $scope.pageContext.pageSize);
  };

  $scope.toPage = function(pageNumber) {
    $scope._loadPage(pageNumber, $scope.pageContext.pageSize);
  };

  $scope.toFirstPage = function() {
    $scope._loadPage(0, $scope.pageContext.pageSize);
  };

  $scope.toLastPage = function() {
    $scope._loadPage($scope.pageContext.nbPages, $scope.pageContext.pageSize);
  };

  $scope.toNextPage = function() {
    $scope._loadPage($scope.pageContext.pageNumber + 1, $scope.pageContext.pageSize);
  };

  $scope.toPreviousPage = function() {
    $scope._loadPage($scope.pageContext.pageNumber - 1, $scope.pageContext.pageSize);
  };

  $scope._updateContext = function(paginationParameter, nbElements) {
    $scope.pageContext.pageNumber = paginationParameter.pageNumber;
    $scope.pageContext.pageSize = paginationParameter.pageSize;

    if (paginationParameter.pageSize <= 0) {
      $scope.pageContext.nbPages = 0;
    } else {
      $scope.pageContext.nbPages = Math.max(0 ,Math.ceil(nbElements / paginationParameter.pageSize) - 1);
    }
    $scope.pageContext.firstPage = $scope.pageContext.pageNumber == 0;
    $scope.pageContext.lastPage = $scope.pageContext.pageNumber == $scope.pageContext.nbPages;

  };

  /* On reçoit une notification de chargement de la page (du scope parent normalement) */
  $scope.$on('pageLoaded', function(event, paginationParameter, elementsCount) {
    $scope._updateContext(paginationParameter, elementsCount);
  });

  // Chargement initial
  if ($scope.paginationData && $scope.paginationData.currentPage) {
    $scope._updateContext($scope.paginationData.currentPage, $scope.paginationData.count);
  }

});