/*
 * #%L
 * Coselmar :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
coselmarServices.factory('userService', ['$resource', 'coselmarConfig', function($resource, coselmarConfig){
	return new User($resource, coselmarConfig);
}]);

function User(resource, config){


	this.resource = resource;

	var baseURL = config.BASE_URL + "v1/users";
	var baseV2URL = config.BASE_URL + "v2/users";
	var exportURL = config.BASE_URL + "v1/export/users";

	this.saveUser = function(user, successFunction, failFunction, finallyFunction){

		var param = $.param({ 'user': JSON.stringify(user)});

		// Save the User
		var serviceURl = baseURL;
		if (user.id) {
			serviceURl = baseURL + "/" + user.id
		}
		var userResource = resource(serviceURl, null, {
			'save': {
			  method:'POST'
			}
		});
		userResource.save(null, param, successFunction, failFunction).$promise.finally(finallyFunction);
	}

	this.getUser = function(id, successFunction, failFunction, finallyFunction){
		// Load the User
		var userResource = resource(baseURL + '/:userId', {userId:'@userId'});
		userResource.get({userId:id}, successFunction, failFunction).$promise.finally(finallyFunction);
	}

	this.deleteUser = function(id, scope, successFunction){

		var userResource = resource(baseURL + '/:userId', {userId:'@id'});
		userResource.delete({userId:id}, successFunction);
	}

	this.getUsers = function(searchKeywords, showDisable, successFunction, failFunction, finallyFunction){
		// Load all users
		var userResource = resource(baseURL, {'search' : {'fullTextSearch' : searchKeywords, 'activeAndInactive': showDisable, 'active': true}});
		userResource.query(successFunction, failFunction).$promise.finally(finallyFunction);
	};

	this.getPaginatedUsers = function(searchBean, successFunction, failFunction, finallyFunction){
		// Load all users
		searchBean.active = true;
		var userResource = resource(baseV2URL, {'search' : searchBean});
		userResource.get(successFunction, failFunction).$promise.finally(finallyFunction);
	};

	this.getAdvancedUsers = function(searchExample, successFunction, failFunction, finallyFunction){
		// Load all users
		var userResource = resource(baseURL, {'search' : searchExample});
		userResource.query(successFunction, failFunction).$promise.finally(finallyFunction);
	};

	this.login = function(mail, password, successFunction, failFunction){

		var credentials = $.param({
			'mail' : mail,
			'password' : password
		});

		var userResource = resource(baseURL + "/login", null, {
			'post' : {
				method: 'POST',
            	headers : {
                	'Content-Type' : 'application/x-www-form-urlencoded;charset=utf-8'
            	},
			}});
		userResource.post(credentials, successFunction, failFunction);
	};

	this.sendNewPassword = function(mail, successFunction, failFunction){

		var params = $.param({
			'userMail' : mail
		});

		var userResource = resource(baseURL + "/password", null, {
			'post' : {
				method: 'POST',
            	headers : {
                	'Content-Type' : 'application/x-www-form-urlencoded;charset=utf-8'
            	},
			}});
		userResource.post(params, successFunction, failFunction);
	};

	this.getExportUsersUrl = function() {
		return exportURL;
	};

	this.getUserProjects = function(params, successFunction){

		var userResource = resource(baseURL + '/:userId/projects', {userId: params.userId, userRole : params.role});

		userResource.query(successFunction);
	};

};