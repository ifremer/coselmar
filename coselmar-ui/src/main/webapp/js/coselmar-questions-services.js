/*
 * #%L
 * Coselmar :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

coselmarServices.factory('questionsService', ['$resource', '$http', 'coselmarConfig', function($resource, $http, coselmarConfig){
	return new Question($resource, $http, coselmarConfig);
}]);

function Question(resource, http, config){


	this.resource = resource;
	var baseURL = config.BASE_URL + "v1/questions";
	var baseV2URL = config.BASE_URL + "v2/questions";
	var exportURL = config.BASE_URL + "v1/export/questions";
	var usersURL = config.BASE_URL + "v1/users";

	this.saveQuestion = function(question, successFunction, failFunction, finallyFunction){

		var formData = new FormData();
		formData.append("question", JSON.stringify(question));
		var questionParam = $.param({'question' : JSON.stringify(question)});

		// Save the Question
		var serviceURl = baseURL;
		if (question.id) {
			serviceURl = baseURL + "/" + question.id
		}
		var questionResource = resource(serviceURl, null, {
			'save': {
			  method:'POST',
			}
		});
		questionResource.save(null, questionParam, successFunction, failFunction).$promise.finally(finallyFunction);
	};

	this.findUsers = function(example, successFunction) {
		var userResource = resource(usersURL, {'search': example});
		userResource.query(successFunction);

	};

	this.getQuestions = function(searchOptions, successFunction, failFunction, finallyFunction) {

		if (searchOptions.privacy
		  && searchOptions.privacy.toUpperCase() != "PRIVATE"
		  && searchOptions.privacy.toUpperCase() != "PUBLIC" ) {

			searchOptions.privacy = undefined;
		}

		if (searchOptions.status && searchOptions.status.toUpperCase() != "IN_PROGRESS"
          && searchOptions.status.toUpperCase() != "CLOSED"
		  && searchOptions.status.toUpperCase() != "ADJOURNED") {

			searchOptions.status = undefined;
		}

		var questionResource = resource(baseURL, {'searchOption' : searchOptions});
		questionResource.query().$promise.then(successFunction, failFunction).finally(finallyFunction);
	};

	this.getPaginatedQuestions = function(searchOptions, successFunction, failFunction, finallyFunction) {

		if (searchOptions.privacy
		  && searchOptions.privacy.toUpperCase() != "PRIVATE"
		  && searchOptions.privacy.toUpperCase() != "PUBLIC" ) {

			searchOptions.privacy = undefined;
		}

		if (searchOptions.status && searchOptions.status.toUpperCase() != "IN_PROGRESS"
          && searchOptions.status.toUpperCase() != "CLOSED"
		  && searchOptions.status.toUpperCase() != "ADJOURNED") {

			searchOptions.status = undefined;
		}

		var questionResource = resource(baseV2URL, {'searchOption' : searchOptions});
		questionResource.get().$promise.then(successFunction, failFunction).finally(finallyFunction);
	};

	this.exportSearchResult = function(searchOptions, successFunction, failFunction) {

		if (searchOptions.privacy
		  && searchOptions.privacy.toUpperCase() != "PRIVATE"
		  && searchOptions.privacy.toUpperCase() != "PUBLIC" ) {

			searchOptions.privacy = undefined;
		}

		if (searchOptions.status && searchOptions.status.toUpperCase() != "IN_PROGRESS"
          && searchOptions.status.toUpperCase() != "CLOSED"
		  && searchOptions.status.toUpperCase() != "ADJOURNED") {

			searchOptions.status = undefined;
		}

		http.get(exportURL, {params: {'searchOption' : searchOptions}}).then(successFunction, failFunction);

	};

	this.getPublicQuestions = function(successFunction, failFunction) {

		var questionResource = resource(baseURL + "/public");
		questionResource.query().$promise.then(successFunction, failFunction);
	};

	this.deleteQuestion = function(questionId, successFunction, failFunction) {
		var questionResource = resource(baseURL + "/" + questionId);
		questionResource.delete().$promise.then(successFunction, failFunction);
	};

	this.getQuestion = function(questionId, successFunction, failFunction, finallyFunction) {
		var questionResource = resource(baseURL + "/" + questionId);
		questionResource.get().$promise.then(successFunction, failFunction).finally(finallyFunction);
	};

	this.addNewDocuments = function(questionId, documents, successFunction, failFunction) {

		var documentsParam = $.param({'documents': JSON.stringify(documents)})

		var serviceURl = baseURL + "/" + questionId + "/documents";
		var questionResource = resource(serviceURl, null, {
			'save': {
			  method:'POST',
			  isArray: true,
			}
		});
		questionResource.save(null, documentsParam, successFunction, failFunction);
	};

	this.findAllTypes = function(successFunction){
		var questionResource = resource(baseURL + '/types');
		questionResource.query(successFunction);
	};

	this.findAllThemes = function(successFunction){
		var questionResource = resource(baseURL + '/themes');
		questionResource.query(successFunction);
	};

	this.getExportQuestionsUrl = function() {
		return exportURL;
	};

	this.getAncestors = function(searchParams, successFunction) {
	    var ancestorsURL = baseURL + "/" + searchParams.questionId + "/ancestors";
		var questionResource = resource(ancestorsURL, {'depth' : searchParams.depth});
		questionResource.query().$promise.then(successFunction);
	};

	this.getDescendants = function(searchParams, successFunction) {
	    var descendantsURL = baseURL + "/" + searchParams.questionId + "/descendants";
		var questionResource = resource(descendantsURL, {'depth' : searchParams.depth});
		questionResource.query().$promise.then(successFunction);
	};

    this.getTopWords = function(questionId, successFunction, failFunction) {

        var topWordsUrl = baseURL + "/" + questionId + "/topwords";

		http.get(topWordsUrl).then(successFunction, failFunction);

    };

};