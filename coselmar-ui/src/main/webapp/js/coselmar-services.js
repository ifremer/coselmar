/*
 * #%L
 * Coselmar :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
var coselmarServices = angular.module('coselmarServices', ['ngResource']);

coselmarServices.factory('documentService', ['$resource', 'coselmarConfig', function($resource, coselmarConfig){
	return new Document($resource, coselmarConfig);
}]);

function Document(resource, config){

	this.resource = resource;

	var baseURL = config.BASE_URL + "v1/documents";
	var baseV2URL = config.BASE_URL + "v2/documents";
	var usersURL = config.BASE_URL + "v1/users";

    this.saveDocument = function(document, successFunction, failFunction, finallyFunction) {

		var param = $.param({ 'document': JSON.stringify(document)});

		var serviceURL = baseURL;
		if (document.id) {
			serviceURL = baseURL + "/" + document.id;
		}

		// Save the document
		var saveDocumentResource = resource(serviceURL, null, {
		    'save': { method: 'POST'}
		});

        saveDocumentResource.save(null, param, successFunction, failFunction).$promise.finally(finallyFunction);
    };

	this.saveDocumentFile = function(documentId, file, successFunction, failFunction) {

		var formData = new FormData();
		if (file) {
			formData.append("uploadFile", file);
		}

		var serviceURL = baseURL + "/" + documentId + "/file";

		// Save the document
		var docResource = resource(serviceURL, null, {
			'upload': {
			  method:'POST',
			  transformRequest: angular.identity,
			  headers: {'Content-Type': undefined, 'Content-Transfer-Encoding': 'utf-8'}
			}
		});
		docResource.upload(null, formData, successFunction, failFunction);
	};

	this.createDocument = function(metadata, file, successFunction, failFunction) {

		var formData = new FormData();
		if (file) {
			formData.append("uploadFile", file);
		}
		formData.append("document", JSON.stringify(metadata));

		// Save the document
		var docResource = resource(baseURL, null, {
			'upload': {
			  method:'POST',
			  transformRequest: function (data, headersGetterFunction) {
				return data;
			  },
			  headers: {'Content-Type': undefined, 'Content-Transfer-Encoding': 'utf-8'}
			}
		});
		docResource.upload(null, formData, successFunction, failFunction);
	};

	this.getDocument = function(id, successFunction, failFunction, finallyFunction){
		// Load the document
		var docResource = resource(baseURL + '/:documentId', {documentId:'@documentId'});
		docResource.get({documentId:id}, successFunction, failFunction).$promise.finally(finallyFunction);
	};

	this.deleteDocument = function(id, scope, successFunction, failFunction){

		// Load the document
		var docResource = resource(baseURL + '/:documentId', {documentId:'@id'});
		docResource.delete({documentId:id}, successFunction, failFunction);
	};

	this.getDocumentFile = function(id, scope){
		// Load the document
		var docResource = resource(baseURL + '/:documentId/file', {documentId:'@documentId'});
		docResource.get({documentId:id}, function(file){
			// redirect to document page ?
		});
	};

	this.getDocuments = function(searchKeywords, successFunction, failFunction, finallyFunction){
		// Load all documents
		var docResource = resource(baseURL, {'searchBean' : {'fullTextSearch' : searchKeywords}});
		docResource.query(successFunction, failFunction).$promise.finally(finallyFunction);
	};

	this.findAllTypes = function(successFunction){
		var docResource = resource(baseURL + '/types');
		docResource.query(successFunction);
	};

	this.findAllKeywords = function(successFunction){
		var docResource = resource(baseURL + '/keywords');
		docResource.query(successFunction);
	};

	this.findExperts = function(example, successFunction) {
		var userResource = resource(usersURL + '/experts', {'search': example});
		userResource.query(successFunction);

	};

	this.getAdvancedDocuments = function(searchExample, successFunction, failFunction, finallyFunction){
		// Load documents with search example
		var docResource = resource(baseURL, {'searchBean' : searchExample});
		docResource.query(successFunction, failFunction).$promise.finally(finallyFunction);
	};

	this.getPaginatedDocuments = function(searchExample, successFunction, failFunction, finallyFunction){
		// Load documents with search example
		var docResource = resource(baseV2URL, {'searchBean' : searchExample});
		docResource.get(successFunction, failFunction).$promise.finally(finallyFunction);
	};

	this.getSearchBibliography = function(searchExample, successFunction, failFunction, finallyFunction){
		// Load documents with search example
		var docResource = resource(baseV2URL + "/citations", {'searchBean' : searchExample});
		docResource.query(successFunction, failFunction).$promise.finally(finallyFunction);
	};

};