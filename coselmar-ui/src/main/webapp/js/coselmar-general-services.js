/*
 * #%L
 * Coselmar :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

coselmarServices.factory('generalService', ['$http', 'coselmarConfig', function($http, coselmarConfig){
	return new GeneralService($http, coselmarConfig);
}]);

function GeneralService(http, config){

	this.http = http;

	var baseURL = config.BASE_URL + "v1/general";

    this.getTopWords = function(successFunction, failFunction) {

        var topWordsUrl = baseURL + "/topwords";

		http.get(topWordsUrl).then(successFunction, failFunction);

    };

};