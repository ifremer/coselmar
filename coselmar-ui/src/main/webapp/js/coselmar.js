/*
 * #%L
 * Coselmar :: UI
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
var coselmarApp = angular.module("coselmarApp", ['ngRoute', 'ngResource', 'ngMessages', 'ngAnimate',
 'pascalprecht.translate', 'tmh.dynamicLocale', 'cgNotify',
 'angular-jwt', 'angular-jqcloud', 'coselmarControllers', 'coselmarServices']);

coselmarApp.config(['$routeProvider', function($routeProvider) {
    $routeProvider
    //documents
    .when('/documents', {
        controller : 'DocumentsCtrl',
        templateUrl : 'views/documents/documents.html'

    }).when('/documents/new', {
        controller : 'NewDocumentCtrl',
        templateUrl : 'views/documents/newdocument.html'

    }).when('/documents/:documentId', {
        controller : 'DocumentViewCtrl',
        templateUrl : 'views/documents/document.html'

    //users
    }).when('/users', {
        controller : 'UsersCtrl',
        templateUrl : 'views/users/users.html'

    }).when('/users/new', {
        controller : 'NewUserCtrl',
        templateUrl : 'views/users/newuser.html'

    }).when('/users/forgotPassword', {
        controller : 'NewPasswordCtrl',
        templateUrl : 'views/users/newPassword.html'

    }).when('/users/:userId', {
        controller : 'UserViewCtrl',
        templateUrl : 'views/users/user.html'

    //questions
    }).when('/questions', {
        controller : 'QuestionsCtrl',
        templateUrl : 'views/questions/questions.html'

    }).when('/questions/new', {
        controller : 'QuestionCtrl',
        templateUrl : 'views/questions/newquestion.html'

    }).when('/questions/:questionId', {
        controller : 'QuestionCtrl',
        templateUrl : 'views/questions/question.html'

    // Referential search
    }).when('/referential', {
        controller : 'ReferentialCtrl',
        templateUrl : 'views/referential/referential.html'

    // Admin tools
    }).when('/admin', {
        controller : 'AdminCtrl',
        templateUrl : 'views/admin/admintools.html'

    // Error pages
    }).when('/404', {
        templateUrl : 'views/errors/404.html'

    }).when('/403', {
        templateUrl : 'views/errors/403.html'

    }).when('/401', {
        controller : 'unauthorizedCtrl',
        templateUrl : 'views/errors/401.html'


    }).otherwise({
        redirectTo: '/',
        controller : 'homeConnectedCtrl',
        templateUrl : 'views/home.html'
    });
}]);

// Inject Json Web Token if present in all request
coselmarApp.config(function Config($httpProvider, jwtInterceptorProvider) {
	jwtInterceptorProvider.tokenGetter = function() {
    	return localStorage.getItem('coselmar-jwt');
  	}
  	$httpProvider.interceptors.push('jwtInterceptor');
});

// Intercept error
coselmarApp.config(function($httpProvider) {

    $httpProvider.interceptors.push(function($q, $location) {
        return {
            'responseError': function(rejection) {

//                if (rejection.status == 401) {
//                    // create return to path wanted
//                    var returnTo = $location.path();
//                    $location.path("/401").search('returnTo', returnTo);
//
//                } else if (rejection.status == 403) {
//                    $location.path("/403");
//
//                } else if (rejection.status == 404) {
////                    $location.path("/404");
//
//                } else {
//                    $location.path("/");
//
//                }
                return $q.reject(rejection);
            }
        };
    });

  // edit content-type for send data to the server
  $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
});

// i18n provider
coselmarApp.config(function($translateProvider, tmhDynamicLocaleProvider) {
  // define translations variables
  $translateProvider.translations('en', translateEN);
  $translateProvider.translations('fr', translateFR);
  // defined default
  $translateProvider.preferredLanguage('en');
  $translateProvider.fallbackLanguage('en');

  tmhDynamicLocaleProvider.localeLocationPattern("i18n/angular-locale_{{locale}}.js");
//  tmhDynamicLocaleProvider.useStorage('$cookieStore');
});