package org.apache.lucene.misc;

/*
 * #%L
 * Coselmar :: Rest Services
 * %%
 * Copyright (C) 2014 - 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.lucene.index.Fields;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.MultiFields;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;

import java.util.Comparator;

/**
 *
 * <code>HighFreqTermsMultiField</code> class extends {@link HighFreqTerms} to allow extracts the top n most frequent terms
 * (by document frequency ) from an existing Lucene index and reports their document frequency for several fields
 *
 * @see HighFreqTerms
 */
public class HighFreqTermsMultiFields extends HighFreqTerms {

    public static TermStats[] getHighFreqTermsMultiFields(IndexReader reader, int numTerms, String[] fieldNames, Comparator<TermStats> comparator) throws Exception {

        TermStatsQueue tiq = new TermStatsQueue(numTerms, comparator);
        TermsEnum te;

        if (fieldNames != null) {
            Fields fields = MultiFields.getFields(reader);
            for (String field : fieldNames) {
                Terms terms = fields.terms(field);
                if (terms != null) {
                    te = terms.iterator();
                    tiq.fill(field, te);
                }
            }
        } else {
            Fields fields = MultiFields.getFields(reader);
            if (fields.size() == 0) {
                throw new RuntimeException("no fields found for this index");
            }
            for (String fieldName : fields) {
                Terms terms = fields.terms(fieldName);
                if (terms != null) {
                    tiq.fill(fieldName, terms.iterator());
                }
            }
        }

        TermStats[] result = new TermStats[tiq.size()];
        // we want highest first so we read the queue and populate the array
        // starting at the end and work backwards
        int count = tiq.size() - 1;
        while (tiq.size() != 0) {
            result[count] = tiq.pop();
            count--;
        }
        return result;
    }

}
