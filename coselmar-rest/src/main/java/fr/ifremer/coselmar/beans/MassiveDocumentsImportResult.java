package fr.ifremer.coselmar.beans;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2017 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class MassiveDocumentsImportResult {

    protected boolean success = true;
    protected List<String> missingFiles;
    protected List<String> documentsWithoutAttachment;
    protected boolean errorSystem;
    protected ImportSystemErrorType systemErrorType;

    public MassiveDocumentsImportResult() {
        this.missingFiles = new ArrayList<>();
        this.documentsWithoutAttachment = new ArrayList<>();
    }

    public boolean isSuccess() {
        return this.success;
    }

    public List<String> getMissingFiles() {
        return missingFiles;
    }

    public void setMissingFiles(List<String> missingFiles) {
        this.success = false;
        this.missingFiles = missingFiles;
    }

    public void addMissingFile(String missingFileName) {
        this.missingFiles.add(missingFileName);
        this.success = false;
    }

    public List<String> getDocumentsWithoutAttachment() {
        return documentsWithoutAttachment;
    }

    public void setDocumentsWithoutAttachment(List<String> documentsWithoutAttachment) {
        this.success = false;
        this.documentsWithoutAttachment = documentsWithoutAttachment;
    }

    public void addDocumentWithoutAttachment(String documentWithoutAttachment) {
        this.success = false;
        this.documentsWithoutAttachment.add(documentWithoutAttachment);
    }

    public boolean isErrorSystem() {
        return errorSystem;
    }

    public void setSystemErrorType(ImportSystemErrorType importSystemErrorType) {
        this.systemErrorType = importSystemErrorType;
        this.errorSystem = true;
        this.success = false;
    }

    public enum ImportSystemErrorType {
        UNABLE_TO_READ_ZIP,
        UNABLE_TO_FIND_CSV,
        UNABLE_TO_READ_CSV,
        UNABLE_TO_READ_ZIP_ENTRY,
        ;
    }
}
