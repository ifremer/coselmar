package fr.ifremer.coselmar.beans;

/*
 * #%L
 * Coselmar :: Rest Services
 * %%
 * Copyright (C) 2014 - 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ext.AbstractExportModel;

import static org.nuiton.i18n.I18n.t;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class UserExportModel extends AbstractExportModel<UserBean> {

    protected static final ValueFormatter<Boolean> USER_ACTIVE_FORMATTER = new ValueFormatter<Boolean>() {
        @Override
        public String format(Boolean value) {
            String result;
            if (value) {
                result = t("user.metadata.status.enable");

            } else {
                result = t("user.metadata.status.disable");
            }
            return result;
        }
    };

    protected static final ValueFormatter<String> USER_ROLE_FORMATTER = new ValueFormatter<String>() {
        @Override
        public String format(String value) {
            String result = "";
            if (StringUtils.isNotBlank(value)) {
                result = t("user.metadata.role." + value.toLowerCase());

            }
            return result;
        }
    };

    public UserExportModel() {
        super(';');

        modelBuilder.newColumnForExport(t("user.metadata.firstName"), UserBean.PROPERTY_FIRST_NAME);
        modelBuilder.newColumnForExport(t("user.metadata.name"), UserBean.PROPERTY_NAME);
        modelBuilder.newColumnForExport(t("user.metadata.mail"), UserBean.PROPERTY_MAIL);
        modelBuilder.newColumnForExport(t("user.metadata.qualification"), UserBean.PROPERTY_QUALIFICATION);
        modelBuilder.newColumnForExport(t("user.metadata.organization"), UserBean.PROPERTY_ORGANIZATION);
        modelBuilder.newColumnForExport(t("user.metadata.organization"), UserBean.PROPERTY_ROLE, USER_ROLE_FORMATTER);
        modelBuilder.newColumnForExport(t("user.metadata.status"), UserBean.PROPERTY_ACTIVE,USER_ACTIVE_FORMATTER);
    }

}
