package fr.ifremer.coselmar.beans;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2016 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class QuestionTreeNode implements Serializable {

    protected String id;

    protected String title;

    protected Date submissionDate;

    protected Date deadline;

    protected Set<String> themes;

    protected String type;

    protected List<QuestionTreeNode> ancestors;

    protected List<QuestionTreeNode> descendants;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getSubmissionDate() {
        return submissionDate != null ?  new Date(submissionDate.getTime()) : null;
    }

    public void setSubmissionDate(Date submissionDate) {
        if (submissionDate != null) {
            this.submissionDate = new Date(submissionDate.getTime());
        } else {
            this.submissionDate = null;
        }
    }

    public Date getDeadline() {
        return deadline != null ? new Date(deadline.getTime()) : null;
    }

    public void setDeadline(Date deadline) {
        if (deadline != null) {
            this.deadline = new Date(deadline.getTime());
        } else {
            this.deadline = null;
        }
    }

    public Set<String> getThemes() {
        return themes;
    }

    public void setThemes(Set<String> themes) {
        this.themes = themes;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<QuestionTreeNode> getAncestors() {
        return ancestors;
    }

    public void setAncestors(List<QuestionTreeNode> ancestors) {
        this.ancestors = ancestors;
    }

    public List<QuestionTreeNode> getDescendants() {
        return descendants;
    }

    public void setDescendants(List<QuestionTreeNode> descendants) {
        this.descendants = descendants;
    }
}
