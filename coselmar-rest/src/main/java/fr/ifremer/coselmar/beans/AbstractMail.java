package fr.ifremer.coselmar.beans;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.util.Locale;

/**
 * @author ymartel <martel@codelutin.com>
 */
public abstract class AbstractMail {

    protected Locale locale;

    protected String to;

    private String coselmarUrl;

    public AbstractMail(Locale locale) {
        this.locale = locale;
    }

    public Locale getLocale() {
        return locale;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String mail) {
        this.to = mail;
    }

    public abstract String getSubject();

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    public boolean isRecipientProvided() {
        boolean isRecipientProvided = StringUtils.isNotBlank(this.to);
        return isRecipientProvided;
    }

    public String getCoselmarUrl() {
        return coselmarUrl;
    }

    public void setCoselmarUrl(String coselmarUrl) {
        this.coselmarUrl = coselmarUrl;
    }
}
