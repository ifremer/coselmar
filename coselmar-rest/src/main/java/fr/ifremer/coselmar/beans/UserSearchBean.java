package fr.ifremer.coselmar.beans;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.List;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class UserSearchBean extends UserBean {

    private static final long serialVersionUID = -2665085000774964576L;

    protected Integer limit;
    protected Integer page;
    protected List<String> fullTextSearch;
    protected boolean activeAndInactive;

    public UserSearchBean(String id, String firstName, String name, String mail, String phoneNumber, String role, String qualification, String organization, boolean active) {
        super(id, firstName, name, mail, phoneNumber, role, qualification, organization, active);
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<String> getFullTextSearch() {
        return fullTextSearch;
    }

    public void setFullTextSearch(List<String> fullTextSearch) {
        this.fullTextSearch = fullTextSearch;
    }

    public boolean isActiveAndInactive() {
        return activeAndInactive;
    }

    public void setActiveAndInactive(boolean activeAndInactive) {
        this.activeAndInactive = activeAndInactive;
    }
}
