package fr.ifremer.coselmar.beans;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2017 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import org.nuiton.csv.Common;
import org.nuiton.csv.ValueParser;
import org.nuiton.csv.ext.AbstractImportModel;

import java.text.ParseException;
import java.util.Set;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class DocumentImportModel extends AbstractImportModel<DocumentBean> {

    protected static final ValueParser<Set<String>> LIST_STRING_PARSER = new ValueParser<Set<String>>() {
        @Override
        public Set<String> parse(String value) throws ParseException {
            return Sets.newHashSet(value.split(","));
        }
    };

    protected static final Common.DateValue DATE_PARSER = new Common.DateValue("yyyy/MM/dd");

    public DocumentImportModel() {
        super(';');

        newMandatoryColumn("name", "name");
        newMandatoryColumn("type", "type");
        newMandatoryColumn("keywords", "keywords", LIST_STRING_PARSER);
        newOptionalColumn("authors", "authors");
        newMandatoryColumn("summary", "summary");
        newOptionalColumn("license", "license");
        newOptionalColumn("copyright", "copyright");
        newOptionalColumn("language", "language");
        newOptionalColumn("publicationDate", "publicationDate", DATE_PARSER);
        newOptionalColumn("comment", "comment");
        newMandatoryColumn("citation", "citation");
        newMandatoryColumn("fileName", "fileName");
        newMandatoryColumn("externalUrl", "externalUrl");
    }

    @Override
    public DocumentBean newEmptyInstance() {
        return DocumentBean.newEmptyInstance();
    }

}
