package fr.ifremer.coselmar.beans;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class UserBean implements Serializable {

    public static final String PROPERTY_FIRST_NAME = "firstName";
    public static final String PROPERTY_NAME = "name";
    public static final String PROPERTY_MAIL = "mail";
    public static final String PROPERTY_ROLE = "role";
    public static final String PROPERTY_QUALIFICATION = "qualification";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_PHONE_NUMBER = "phoneNumber";
    public static final String PROPERTY_ACTIVE = "active";

    protected String id;
    protected String firstName;
    protected String name;
    protected String mail;
    protected String role;
    protected String qualification;
    protected String organization;
    protected String password;
    protected String newPassword;
    protected String phoneNumber;
    protected boolean active;

    public UserBean(String id, String firstName, String name,
                    String mail, String phoneNumber, String role,
                    String qualification, String organization, boolean active) {
        this.id = id;
        this.firstName = firstName;
        this.name = name;
        this.mail = mail;
        this.role = role;
        this.qualification = qualification;
        this.organization = organization;
        this.phoneNumber = phoneNumber;
        this.active = active;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
