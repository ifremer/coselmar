package fr.ifremer.coselmar.beans;

/*
 * #%L
 * Coselmar :: Rest Services
 * %%
 * Copyright (C) 2014 - 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.csv.Common;
import org.nuiton.csv.ValueFormatter;
import org.nuiton.csv.ext.AbstractExportModel;
import org.nuiton.util.DateUtil;

import java.util.Collection;

import static org.nuiton.i18n.I18n.t;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class QuestionExportModel extends AbstractExportModel<QuestionBean> {

    protected static final ValueFormatter<Collection<String>> COLLECTION_STRING_FORMATTER = new ValueFormatter<Collection<String>>() {
        @Override
        public String format(Collection<String> value) {
            String result = "";
            if (value != null && !value.isEmpty()) {
                result = Joiner.on(",").join(value);
            }
            return result;
        }
    };

    protected static final ValueFormatter<Collection<Object>> COLLECTION_COUNTER_VALUE_FORMATTER = new ValueFormatter<Collection<Object>>() {
        @Override
        public String format(Collection<Object> value) {
            String result = "0";
            if (value != null) {
                result = String.valueOf(value.size());
            }
            return result;
        }
    };

    protected static final ValueFormatter<String> QUESTION_STATUS_FORMATTER = new ValueFormatter<String>() {
        @Override
        public String format(String value) {
            String result = "";
            if (StringUtils.isNotBlank(value)) {
                result = t("question.metadata.status." + value.toLowerCase());

            }
            return result;
        }
    };

    protected static final Common.DateValue DATE_FORMATTER = new Common.DateValue(DateUtil.DEFAULT_PATTERN);

    public QuestionExportModel() {
        super(';');

        modelBuilder.newColumnForExport(t("question.metadata.title"), QuestionBean.PROPERTY_TITLE);
        modelBuilder.newColumnForExport(t("question.metadata.submissionDate"), QuestionBean.PROPERTY_SUBMISSION_DATE, DATE_FORMATTER);
        modelBuilder.newColumnForExport(t("question.metadata.status"), QuestionBean.PROPERTY_STATUS, QUESTION_STATUS_FORMATTER);
        modelBuilder.newColumnForExport(t("question.metadata.themes"), QuestionBean.PROPERTY_THEMES, COLLECTION_STRING_FORMATTER);
        modelBuilder.newColumnForExport(t("question.metadata.deadline"), QuestionBean.PROPERTY_DEADLINE, DATE_FORMATTER);
        modelBuilder.newColumnForExport(t("question.metadata.participants"), QuestionBean.PROPERTY_PARTICIPANTS, COLLECTION_COUNTER_VALUE_FORMATTER);
        modelBuilder.newColumnForExport(t("question.metadata.clients"), QuestionBean.PROPERTY_CLIENTS, COLLECTION_COUNTER_VALUE_FORMATTER);
        modelBuilder.newColumnForExport(t("question.metadata.relatedDocuments"), QuestionBean.PROPERTY_RELATED_DOCUMENTS, COLLECTION_COUNTER_VALUE_FORMATTER);
    }

}
