package fr.ifremer.coselmar.beans;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class DocumentSearchBean extends DocumentBean {

    private static final long serialVersionUID = -2665085000774964576L;

    protected Integer limit;
    protected Integer page;
    protected List<String> fullTextSearch;

    protected Date depositAfterDate;

    protected Date depositBeforeDate;

    protected Date publicationAfterDate;

    protected Date publicationBeforeDate;


    public DocumentSearchBean(String id, String name, String ownerName, String ownerId, String privacy,
                        Date depositDate, Collection<String> keywords,
                        String type, String summary, String language, Date publicationDate,
                        String authors, String license, String copyright,
                        boolean withFile, String mimeType, String externalUrl,
                        String comment, String fileName, String citation) {
        super(id, name, ownerName, ownerId, privacy, depositDate, keywords, type, summary, language, publicationDate, authors, license, copyright, withFile, mimeType, externalUrl, comment, fileName, citation);
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<String> getFullTextSearch() {
        return fullTextSearch;
    }

    public void setFullTextSearch(List<String> fullTextSearch) {
        this.fullTextSearch = fullTextSearch;
    }

    public Date getDepositAfterDate() {
        return depositAfterDate != null ?  new Date(depositAfterDate.getTime()) : null;
    }

    public void setDepositAfterDate(Date depositAfterDate) {
        if (depositAfterDate != null) {
            this.depositAfterDate = new Date(depositAfterDate.getTime());
        } else {
            this.depositAfterDate = null;
        }
    }

    public Date getDepositBeforeDate() {
        return depositBeforeDate != null ? new Date(depositBeforeDate.getTime()) : null;
    }

    public void setDepositBeforeDate(Date depositBeforeDate) {
        if (depositBeforeDate != null) {
            this.depositBeforeDate = new Date(depositBeforeDate.getTime());
        } else {
            this.depositBeforeDate = null;
        }
    }

    public Date getPublicationAfterDate() {
        return publicationAfterDate != null ? new Date(publicationAfterDate.getTime()) : null;
    }

    public void setPublicationAfterDate(Date publicationAfterDate) {
        if (publicationAfterDate != null) {
            this.publicationAfterDate = new Date(publicationAfterDate.getTime());
        } else {
            this.publicationAfterDate = null;
        }
    }

    public Date getPublicationBeforeDate() {
        return publicationBeforeDate != null ? new Date(publicationBeforeDate.getTime()) : null;
    }

    public void setPublicationBeforeDate(Date publicationBeforeDate) {
        if (publicationBeforeDate != null) {
            this.publicationBeforeDate = new Date(publicationBeforeDate.getTime());
        }
        this.publicationBeforeDate = null;
    }
}
