package fr.ifremer.coselmar.beans;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class QuestionBean implements Serializable {

    protected String id;


    public static final String PROPERTY_TITLE = "title";
    protected String title;

    public static final String PROPERTY_SUBMISSION_DATE = "submissionDate";
    protected Date submissionDate;


    public static final String PROPERTY_DEADLINE = "deadline";
    protected Date deadline;

    public static final String PROPERTY_THEMES = "themes";
    protected Set<String> themes;

    protected String summary;

    protected Date closingDate;

    protected String type;

    public static final String PROPERTY_PARTICIPANTS = "participants";
    protected Set<UserBean> participants;

    protected Set<UserBean> supervisors;

    protected Set<UserBean> contributors;

    public static final String PROPERTY_CLIENTS = "clients";
    protected Set<UserBean> clients;

    protected Set<QuestionBean> parents;
    protected Set<QuestionBean> children;

    protected String privacy;

    public static final String PROPERTY_RELATED_DOCUMENTS = "relatedDocuments";
    protected Set<DocumentBean> relatedDocuments;

    public static final String PROPERTY_STATUS = "status";
    protected String status;

    protected Set<String> externalExperts;

    protected Boolean isRestricted;

    protected String conclusion;

    protected Set<DocumentBean> closingDocuments;

    protected Set<LinkBean> links;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getSubmissionDate() {
        return submissionDate != null ? new Date(submissionDate.getTime()) : null;
    }

    public void setSubmissionDate(Date submissionDate) {
        if (submissionDate != null) {
            this.submissionDate = new Date(submissionDate.getTime());
        } else {
            this.submissionDate = null;
        }
    }

    public Date getDeadline() {
        return deadline != null ? new Date(deadline.getTime()) : null;
    }

    public void setDeadline(Date deadline) {
        if (deadline != null) {
            this.deadline = new Date(deadline.getTime());
        } else {
            this.deadline = null;
        }
    }

    public Set<String> getThemes() {
        return themes;
    }

    public void setThemes(Set<String> themes) {
        this.themes = themes;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Date getClosingDate() {
        return closingDate != null ? new Date(closingDate.getTime()) : null;
    }

    public void setClosingDate(Date closingDate) {
        if (closingDate != null) {
            this.closingDate = new Date(closingDate.getTime());
        } else {
            this.closingDate = null;
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Set<UserBean> getParticipants() {
        return participants;
    }

    public void setParticipants(Set<UserBean> participants) {
        this.participants = participants;
    }

    public void addParticipant(UserBean participant) {
        if (this.participants == null) {
            this.participants = new HashSet<>();
        }
        this.participants.add(participant);
    }

    public Set<UserBean> getSupervisors() {
        return supervisors;
    }

    public void setSupervisors(Set<UserBean> supervisors) {
        this.supervisors = supervisors;
    }

    public void addSupervisor(UserBean supervisor) {
        if (this.supervisors == null) {
            this.supervisors = new HashSet<>();
        }
        this.supervisors.add(supervisor);
    }

    public Set<UserBean> getContributors() {
        return contributors;
    }

    public void setContributors(Set<UserBean> contributors) {
        this.contributors = contributors;
    }

    public void addContributor(UserBean contributor) {
        if (this.contributors == null) {
            this.contributors = new HashSet<>();
        }
        this.contributors.add(contributor);
    }

    public Set<UserBean> getClients() {
        return clients;
    }

    public void setClients(Set<UserBean> clients) {
        this.clients = clients;
    }

    public void addClient(UserBean client) {
        if (this.clients == null) {
            this.clients = new HashSet<>();
        }
        this.clients.add(client);
    }

    public Set<QuestionBean> getParents() {
        return parents;
    }

    public void setParents(Set<QuestionBean> parents) {
        this.parents = parents;
    }

    public void addParent(QuestionBean parent) {
        if(this.parents == null) {
            this.parents = new HashSet<>();
        }
        this.parents.add(parent);
    }

    public Set<QuestionBean> getChildren() {
        return children;
    }

    public void setChildren(Set<QuestionBean> children) {
        this.children = children;
    }

    public void addChild(QuestionBean child) {
        if(this.children == null) {
            this.children = new HashSet<>();
        }
        this.children.add(child);
    }

    public String getPrivacy() {
        return privacy;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    public Set<DocumentBean> getRelatedDocuments() {
        return relatedDocuments;
    }

    public void setRelatedDocuments(Set<DocumentBean> relatedDocuments) {
        this.relatedDocuments = relatedDocuments;
    }

    public void addRelatedDocument(DocumentBean relatedDocument) {
        if (this.relatedDocuments == null) {
            this.relatedDocuments = new HashSet<>();
        }
        this.relatedDocuments.add(relatedDocument);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Set<String> getExternalExperts() {
        return externalExperts;
    }

    public void setExternalExperts(Set<String> externalExperts) {
        this.externalExperts = externalExperts;
    }

    public Boolean getIsRestricted() {
        return isRestricted;
    }

    public void setRestricted(boolean isRestricted) {
        this.isRestricted = isRestricted;
    }

    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    public Set<DocumentBean> getClosingDocuments() {
        return this.closingDocuments;
    }

    public void setClosingDocuments(Set<DocumentBean> closingDocuments) {
        this.closingDocuments = closingDocuments;
    }

    public void addClosingDocument(DocumentBean closingDocument) {
        if (this.closingDocuments == null) {
            this.closingDocuments = new HashSet<>();
        }
        this.closingDocuments.add(closingDocument);
    }

    public Set<LinkBean> getLinks() {
        return links;
    }

    public void setLinks(Set<LinkBean> links) {
        this.links = links;
    }

    public void addLink(LinkBean linkBean) {
        if (this.links == null) {
            this.links = new HashSet<>();
        }
        this.links.add(linkBean);
    }
} //Question
