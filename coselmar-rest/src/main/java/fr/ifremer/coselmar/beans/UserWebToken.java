package fr.ifremer.coselmar.beans;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class UserWebToken implements Serializable {

    public static final String CLAIMS_USER_ID = "userId";
    public static final String CLAIMS_FIRST_NAME = "firstName";
    public static final String CLAIMS_LAST_NAME = "lastName";
    public static final String CLAIMS_ROLE = "role";

    protected String userId;
    protected String firstName;
    protected String lastName;
    protected String role;

    public UserWebToken(String userId, String firstName, String lastName, String role) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
    }

    public UserWebToken(Map<String, Object> claims) {
        this.userId = (String) claims.get(CLAIMS_USER_ID);
        this.firstName = (String) claims.get(CLAIMS_FIRST_NAME);
        this.lastName = (String) claims.get(CLAIMS_LAST_NAME);
        this.role = ((String) claims.get(CLAIMS_ROLE)).toUpperCase();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Map<String, Object> toJwtClaims() {

        Map<String, Object> claims = new HashMap<>();
        claims.put(CLAIMS_USER_ID, this.userId);
        claims.put(CLAIMS_FIRST_NAME, this.firstName);
        claims.put(CLAIMS_LAST_NAME, this.lastName);
        claims.put(CLAIMS_ROLE, this.role);
        return claims;
    }

    public static Map<String, Object> toJwtClaims(String userId, String firstName, String lastName, String role) {

        Map<String, Object> claims = new HashMap<>();
        claims.put(CLAIMS_USER_ID, userId);
        claims.put(CLAIMS_FIRST_NAME, firstName);
        claims.put(CLAIMS_LAST_NAME, lastName);
        claims.put(CLAIMS_ROLE, role.toUpperCase());
        return claims;
    }

}
