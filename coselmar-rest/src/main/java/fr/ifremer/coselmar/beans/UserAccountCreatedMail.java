package fr.ifremer.coselmar.beans;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Strings;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.i18n.I18n;

import java.util.Locale;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class UserAccountCreatedMail extends AbstractMail {

    private UserBean user;

    private String password;

    public UserAccountCreatedMail(Locale locale) {
        super(locale);
    }

    public String getSubject() {
        String fullname = Strings.nullToEmpty(user.getFirstName()) + " " + Strings.nullToEmpty(user.getName());
        if (StringUtils.isBlank(fullname)) {
            return I18n.l(locale, "coselmar.service.mail.UserAccountCreatedMail.subject", user.getMail());
        }
        return I18n.l(locale, "coselmar.service.mail.UserAccountCreatedMail.subject", fullname);
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public String getPassword () {
        return password;
    }

    public void setPassword (String password) {
        this.password = password;
    }
}
