package fr.ifremer.coselmar.beans;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Date;
import java.util.List;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class QuestionSearchBean extends QuestionBean {

    private static final long serialVersionUID = -2665085000774964576L;

    protected Integer limit;
    protected Integer page;
    protected List<String> fullTextSearch;

    protected Date submissionAfterDate;

    protected Date submissionBeforeDate;

    protected Date deadlineAfterDate;

    protected Date deadlineBeforeDate;

    protected String participantId;
    protected String supervisorId;
    protected String contributorId;
    protected String clientId;


    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<String> getFullTextSearch() {
        return fullTextSearch;
    }

    public void setFullTextSearch(List<String> fullTextSearch) {
        this.fullTextSearch = fullTextSearch;
    }

    public Date getSubmissionAfterDate() {
        return submissionAfterDate != null ? new Date(submissionAfterDate.getTime()) : null;
    }

    public void setSubmissionAfterDate(Date submissionAfterDate) {
        if (submissionAfterDate != null) {
            this.submissionAfterDate = new Date(submissionAfterDate.getTime());
        } else {
            this.submissionAfterDate = null;
        }
    }

    public Date getSubmissionBeforeDate() {
        return submissionBeforeDate != null ? new Date(submissionBeforeDate.getTime()) : null;
    }

    public void setSubmissionBeforeDate(Date submissionBeforeDate) {
        if (submissionBeforeDate != null) {
            this.submissionBeforeDate = new Date(submissionBeforeDate.getTime());
        } else {
            this.submissionBeforeDate = null;
        }
    }

    public Date getDeadlineAfterDate() {
        return deadlineAfterDate != null ? new Date(deadlineAfterDate.getTime()) : null;
    }

    public void setDeadlineAfterDate(Date deadlineAfterDate) {
        if (deadlineAfterDate != null) {
            this.deadlineAfterDate = new Date(deadlineAfterDate.getTime());
        } else {
            this.deadlineAfterDate = null;
        }
    }

    public Date getDeadlineBeforeDate() {
        return deadlineBeforeDate != null ? new Date(deadlineBeforeDate.getTime()) : null;
    }

    public void setDeadlineBeforeDate(Date deadlineBeforeDate) {
        if (deadlineBeforeDate != null) {
            this.deadlineBeforeDate = new Date(deadlineBeforeDate.getTime());
        } else {
            this.deadlineBeforeDate = null;
        }
    }

    public String getParticipantId() {
        return participantId;
    }

    public void setParticipantId(String participantId) {
        this.participantId = participantId;
    }

    public String getSupervisorId() {
        return supervisorId;
    }

    public void setSupervisorId(String supervisorId) {
        this.supervisorId = supervisorId;
    }

    public String getContributorId() {
        return contributorId;
    }

    public void setContributorId(String contributorId) {
        this.contributorId = contributorId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
}
