package fr.ifremer.coselmar.beans;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class DocumentBean implements Serializable {

    // Standard Data
    protected String id;
    protected String name;
    protected String ownerName;
    protected String ownerId;
    protected String privacy;
    protected Set<String> keywords;
    protected Date depositDate;
    protected String authors;
    protected String license;
    protected String copyright;
    protected String type;
    protected String language;
    protected String summary;
    protected Date publicationDate;
    protected String comment;
    protected String fileName;
    protected String citation;

    // Document could be internal file or external link
    protected boolean withFile;
    protected String mimeType;
    protected String externalUrl;
    protected Set<QuestionBean> relatedQuestions;
    protected int nbRelatedQuestions;

    // If restricted, could have a list of restricted user
    protected Set<UserBean> authorizedUsers;

    public static DocumentBean newEmptyInstance() {
        return new DocumentBean();
    }

    private DocumentBean(){}

    public DocumentBean(String id, String name, String ownerName, String ownerId, String privacy,
                        Date depositDate, Collection<String> keywords,
                        String type, String summary, String language, Date publicationDate,
                        String authors, String license, String copyright,
                        boolean withFile, String mimeType, String externalUrl,
                        String comment, String fileName, String citation) {
        this.id = id;
        this.name = name;
        this.ownerName = ownerName;
        this.ownerId = ownerId;
        this.privacy = privacy;
        if (depositDate != null) {
            this.depositDate = new Date(depositDate.getTime());
        }
        if (keywords != null) {
            this.keywords = new HashSet<>(keywords);
        }
        this.mimeType = mimeType;

        this.type = type;
        this.summary = summary;
        this.language = language;
        if (publicationDate != null) {
            this.publicationDate = new Date(publicationDate.getTime());
        }

        this.authors = authors;
        this.license = license;
        this.copyright = copyright;

        this.withFile = withFile;
        this.mimeType = mimeType;
        this.externalUrl = externalUrl;
        this.fileName = fileName;

        this.comment = comment;
        this.authorizedUsers = new HashSet<>(0);

        this.citation = citation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getPrivacy() {
        return privacy;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    public Date getDepositDate() {
        return depositDate != null ? new Date(depositDate.getTime()) : null;
    }

    public void setDepositDate(Date depositDate) {
        if (depositDate != null) {
            this.depositDate = new Date(depositDate.getTime());
        } else {
            this.depositDate = null;
        }
    }

    public Set<String> getKeywords() {
        return keywords;
    }

    public void setKeywords(Set<String> keywords) {
        this.keywords = keywords;
    }

    public void addKeywords(Collection<String> keywords) {
        if (this.keywords == null) {
            this.keywords = new HashSet<>(keywords);
        }
        this.keywords.addAll(keywords);
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Date getPublicationDate() {
        return publicationDate != null ? new Date(publicationDate.getTime()) : null;
    }

    public void setPublicationDate(Date publicationDate) {
        if (publicationDate != null) {
            this.publicationDate = new Date(publicationDate.getTime());
        } else {
            this.publicationDate = null;
        }
    }

    public boolean isWithFile() {
        return withFile;
    }

    public void setWithFile(boolean isFile) {
        this.withFile = isFile;
    }

    public String getExternalUrl() {
        return externalUrl;
    }

    public void setExternalUrl(String externalUrl) {
        this.externalUrl = externalUrl;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Set<QuestionBean> getRelatedQuestions() {
        return relatedQuestions;
    }

    public void setRelatedQuestions(Set<QuestionBean> relatedQuestions) {
        this.relatedQuestions = relatedQuestions;
    }

    public void addRelatedQuestion(QuestionBean relatedQuestion) {
        if (this.relatedQuestions == null) {
            this.relatedQuestions = new HashSet<>();
        }
        this.relatedQuestions.add(relatedQuestion);
    }

    public int getNbRelatedQuestions() {
        return nbRelatedQuestions;
    }

    public void setNbRelatedQuestions(int nbRelatedQuestions) {
        this.nbRelatedQuestions = nbRelatedQuestions;
    }

    public Set<UserBean> getAuthorizedUsers() {
        return authorizedUsers;
    }

    public void setAuthorizedUsers(Set<UserBean> authorizedUsers) {
        this.authorizedUsers = authorizedUsers;
    }

    public void addAuthorizedUser(UserBean userBean) {
        if (this.authorizedUsers == null) {
            this.authorizedUsers = new HashSet<>();
        }
        this.authorizedUsers.add(userBean);
    }

    public String getCitation() {
        return citation;
    }

    public void setCitation(String citation) {
        this.citation = citation;
    }
}
