package fr.ifremer.coselmar.converter;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import fr.ifremer.coselmar.beans.DocumentBean;
import fr.ifremer.coselmar.beans.LinkBean;
import fr.ifremer.coselmar.beans.QuestionBean;
import fr.ifremer.coselmar.beans.UserBean;
import fr.ifremer.coselmar.persistence.entity.CoselmarUser;
import fr.ifremer.coselmar.persistence.entity.CoselmarUserGroup;
import fr.ifremer.coselmar.persistence.entity.CoselmarUserImpl;
import fr.ifremer.coselmar.persistence.entity.CoselmarUserRole;
import fr.ifremer.coselmar.persistence.entity.Document;
import fr.ifremer.coselmar.persistence.entity.DocumentImpl;
import fr.ifremer.coselmar.persistence.entity.Link;
import fr.ifremer.coselmar.persistence.entity.Privacy;
import fr.ifremer.coselmar.persistence.entity.Question;
import fr.ifremer.coselmar.persistence.entity.QuestionImpl;
import fr.ifremer.coselmar.persistence.entity.Status;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.persistence.TopiaIdFactory;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

/**
 * @author ymartel <martel@codelutin.com>
 */
public final class BeanEntityConverter {

    private BeanEntityConverter() {
        throw new IllegalAccessError("Utility class");
    }

    public static DocumentBean toBean(TopiaIdFactory idFactory, Document document) {
        String lightId = idFactory.getRandomPart(document.getTopiaId());
        Date depositeDate = document.getDepositDate();
        Date publicationDate = document.getPublicationDate();
        CoselmarUser documentOwner = document.getOwner();
        String owner = "N/A";
        String ownerId = "N/A";
        if (documentOwner != null) {
            String firstname = documentOwner.getFirstname();
            String lastname = documentOwner.getName();
            owner = StringUtils.join(Lists.newArrayList(StringUtils.defaultString(firstname), StringUtils.defaultString(lastname)), " ");
            ownerId = idFactory.getRandomPart(documentOwner.getTopiaId());
        }


        return new DocumentBean(lightId,
            document.getName(),
            owner,
            ownerId,
            document.getPrivacy().name(),
            depositeDate,
            document.getKeywords(),

            document.getType(),
            document.getSummary(),
            document.getLanguage(),
            publicationDate,

            document.getAuthors(),
            document.getLicense(),
            document.getCopyright(),

            document.isWithFile(),
            document.getMimeType(),
            document.getExternalUrl(),
            document.getComment(),
            document.getFileName(),

            document.getCitation()
        );
    }

    public static UserBean toBean(String lightId, CoselmarUser user) {
        return new UserBean(lightId,
            user.getFirstname(),
            user.getName(),
            user.getMail(),
            user.getPhoneNumber(),
            user.getRole().name(),
            user.getQualification(),
            user.getOrganization(),
            user.isActive());
    }

    public static CoselmarUser fromBean(UserBean userBean) {
        CoselmarUser user = new CoselmarUserImpl();

        user.setFirstname(userBean.getFirstName());
        user.setName(userBean.getName());
        user.setMail(userBean.getMail());
        String role = userBean.getRole();
        if (StringUtils.isNotBlank(role)) {
            user.setRole(CoselmarUserRole.valueOf(role));
        }
        user.setQualification(userBean.getQualification());
        user.setOrganization(userBean.getOrganization());
        user.setActive(userBean.isActive());
        user.setPhoneNumber(userBean.getPhoneNumber());

        return user;
    }

    public static QuestionBean toBean(TopiaIdFactory idFactory, Question question) {
        QuestionBean result = toLightBean(idFactory, question);

        CoselmarUserGroup participants = question.getParticipants();
        if (participants != null && participants.getMembers() != null) {
            for (CoselmarUser participant : participants.getMembers()) {
                String lightId = idFactory.getRandomPart(participant.getTopiaId());
                UserBean participantBean = toBean(lightId, participant);
                result.addParticipant(participantBean);
            }
        }

        Set<CoselmarUser> supervisors = question.getSupervisors();
        if (supervisors != null && !supervisors.isEmpty()) {
            for (CoselmarUser supervisor : supervisors) {
                String lightId = idFactory.getRandomPart(supervisor.getTopiaId());
                UserBean supervisorBean = toBean(lightId, supervisor);
                result.addSupervisor(supervisorBean);
            }
        }

        Collection<String> externalExperts = question.getExternalExperts();
        if (externalExperts != null && !externalExperts.isEmpty()) {
            result.setExternalExperts(Sets.newHashSet(externalExperts));
        }

        Set<CoselmarUser> clients = question.getClients();
        if (clients != null && !clients.isEmpty()) {
            for (CoselmarUser client : clients) {
                String lightId = idFactory.getRandomPart(client.getTopiaId());
                UserBean clientBean = toBean(lightId, client);
                result.addClient(clientBean);
            }
        }

        Set<CoselmarUser> contributors = question.getContributors();
        if (contributors != null && !contributors.isEmpty()) {
            for (CoselmarUser contributor : contributors) {
                String lightId = idFactory.getRandomPart(contributor.getTopiaId());
                UserBean contributorBean = toBean(lightId, contributor);
                result.addContributor(contributorBean);
            }
        }

        Collection<Document> relatedDocuments = question.getRelatedDocuments();
        if (relatedDocuments != null && !relatedDocuments.isEmpty()) {
            for (Document relatedDocument : relatedDocuments) {
                DocumentBean documentBean = toBean(idFactory, relatedDocument);
                result.addRelatedDocument(documentBean);
            }
        }

        Collection<Question> parents = question.getParents();
        if (parents != null && !parents.isEmpty()) {
            for (Question parent : parents) {
                QuestionBean questionBean = toLightBean(idFactory, parent);
                result.addParent(questionBean);
            }
        }

        result.setConclusion(question.getConclusion());

        return result;
    }

    public static QuestionBean toLightBean(TopiaIdFactory idFactory, Question question) {
        QuestionBean result = new QuestionBean();
        result.setId(idFactory.getRandomPart(question.getTopiaId()));

        result.setTitle(question.getTitle());
        result.setSummary(question.getSummary());
        result.setType(question.getType());
        result.setPrivacy(question.getPrivacy().name());
        result.setStatus(question.getStatus().name());

        Collection<String> theme = question.getTheme();
        if (theme != null && !theme.isEmpty()) {
            result.setThemes(new HashSet(theme));
        }

        Date submissionDate = question.getSubmissionDate();
        if (submissionDate != null) {
            result.setSubmissionDate(new Date(submissionDate.getTime()));
        }

        Date deadline = question.getDeadline();
        if (deadline != null) {
            result.setDeadline(new Date(deadline.getTime()));
        }

        Date closingDate = question.getClosingDate();
        if (closingDate != null) {
            result.setClosingDate(new Date(closingDate.getTime()));
            result.setConclusion(question.getConclusion());
        }

        Collection<Document> closingDocuments = question.getClosingDocuments();
        if (closingDocuments != null && !closingDocuments.isEmpty()) {
            for (Document relatedDocument : closingDocuments) {
                DocumentBean documentBean = toBean(idFactory, relatedDocument);
                result.addClosingDocument(documentBean);
            }
        }

        result.setRestricted(question.isUnavailable());

        Collection<Link> links = question.getLinks();
        if (links != null && !links.isEmpty()) {
            for (Link link : links) {
                LinkBean linkBean = new LinkBean();
                linkBean.setId(idFactory.getRandomPart(link.getTopiaId()));
                linkBean.setName(link.getName());
                linkBean.setUrl(link.getUrl());
                result.addLink(linkBean);
            }
        }

        return result;
    }

    /**
     * Convert simple fields from {@link DocumentBean} into a {@link Document}.
     * So, this does not convert :
     * <ul>
     * <li>Document#owner</li>
     * <li>Document#restrictedList</li>
     * </ul>
     * If documentBean is null, simply return null Document.
     */
    public static Document fromSearchBean(DocumentBean documentBean) {
        if (documentBean == null) {
            return null;
        }

        Document document = new DocumentImpl();

        document.setName(documentBean.getName());

        Date depositDate = documentBean.getDepositDate();
        if (depositDate != null) {
            document.setDepositDate(new Date(depositDate.getTime()));
        }

        Set<String> keywords = documentBean.getKeywords();
        if (keywords != null) {
            document.setKeywords(new LinkedList(keywords));
        }

        document.setMimeType(documentBean.getMimeType());
        document.setCopyright(documentBean.getCopyright());
        document.setAuthors(documentBean.getAuthors());
        document.setLicense(documentBean.getLicense());
        document.setLanguage(documentBean.getLanguage());
        document.setType(documentBean.getType());
        String privacy = documentBean.getPrivacy();

        if (StringUtils.isNotBlank(privacy)) {
            document.setPrivacy(Privacy.valueOf(privacy.toUpperCase()));
        }

        Date publicationDate = documentBean.getPublicationDate();
        if (publicationDate != null) {
            document.setPublicationDate(new Date(publicationDate.getTime()));
        } else {
            document.setPublicationDate(null);
        }

        document.setSummary(documentBean.getSummary());
        document.setWithFile(documentBean.isWithFile());
        document.setExternalUrl(documentBean.getExternalUrl());
        document.setComment(documentBean.getComment());
        document.setFileName(documentBean.getFileName());

        document.setCitation(documentBean.getCitation());

        return document;
    }

    /**
     * Convert simple fields from {@link QuestionBean} into a {@link Question}.
     * So, this does not convert :
     * <ul>
     * <li>Question#experts</li>
     * <li>Question#contributors</li>
     * <li>Question#clients</li>
     * <li>Question#documents</li>
     * </ul>
     * If questionBean is null, simply return null Question.
     */
    public static Question fromBean(QuestionBean questionBean) {
        Question question = new QuestionImpl();

        question.setTitle(questionBean.getTitle());
        question.setSummary(questionBean.getSummary());
        question.setType(questionBean.getType());

        String privacy = questionBean.getPrivacy();
        if (StringUtils.isNotBlank(privacy)) {
            question.setPrivacy(Privacy.valueOf(privacy.toUpperCase()));
        }

        String status = questionBean.getStatus();
        if (StringUtils.isNotBlank(status)) {
            question.setStatus(Status.valueOf(status.toUpperCase()));
        }

        Collection<String> theme = questionBean.getThemes();
        if (theme != null && !theme.isEmpty()) {
            question.setTheme(new HashSet(theme));
        }

        Date submissionDate = questionBean.getSubmissionDate();
        if (submissionDate != null) {
            question.setSubmissionDate(new Date(submissionDate.getTime()));
        }

        Date deadline = questionBean.getDeadline();
        if (deadline != null) {
            question.setDeadline(new Date(deadline.getTime()));
        }

        Date closingDate = questionBean.getClosingDate();
        if (closingDate != null) {
            question.setClosingDate(new Date(closingDate.getTime()));
            question.setConclusion(questionBean.getConclusion());
        }

        question.setUnavailable(BooleanUtils.toBoolean(questionBean.getIsRestricted()));

        return question;
    }


}
