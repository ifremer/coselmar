package fr.ifremer.coselmar.converter;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.Date;

public class JsonHelper {

    private final Gson gson;

    public JsonHelper(boolean prettyPrint) {

        GsonBuilder gsonBuilder = new GsonBuilder();

        gsonBuilder.setExclusionStrategies(new ExclusionStrategy() {
            @Override
            public boolean shouldSkipField(FieldAttributes f) {
                return false;
            }

            @Override
            public boolean shouldSkipClass(Class<?> clazz) {
                return clazz == Class.class;
            }
        });

        gsonBuilder.registerTypeAdapter(Date.class, new JsonSerializer<Date>() {

            @Override
            public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {

                JsonElement result;

                if (src == null) {
                    result = JsonNull.INSTANCE;

                } else {
                    result = new JsonPrimitive(src.getTime());
                }

                return result;
            }

        });

        gsonBuilder.registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {

            @Override
            public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                return new Date(json.getAsLong());
            }

        });

        if (prettyPrint) {
            gsonBuilder.setPrettyPrinting();
        }

        this.gson = gsonBuilder.create();
    }

    public String toJson(Object model) {
        String json = gson.toJson(model);
        return json;
    }

    public <O> O fromJson(String json, Class<O> type) {
        O o = gson.fromJson(json, type);
        return o;
    }

    public <O> O fromJson(String json, Type type) {
        O o = gson.fromJson(json, type);
        return o;
    }

}
