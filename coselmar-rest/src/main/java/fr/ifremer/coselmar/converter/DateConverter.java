package fr.ifremer.coselmar.converter;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.beanutils.converters.AbstractConverter;

import java.util.Date;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class DateConverter extends AbstractConverter {

    @Override
    protected <T> T convertToType(Class<T> type, Object value) throws Throwable {
        Date result = null;
        if (value != null) {
            if (value.getClass().isAssignableFrom(Date.class)) {
                result = (Date) value;
            } else {
                Object o = ((Object[]) value)[0];
                String sTime = o.toString();
                Long time = Long.parseLong(sTime);
                result = new Date(time);
            }
        }
        return (T) result;
    }

    @Override
    protected Class<?> getDefaultType() {
        return Date.class;
    }
}
