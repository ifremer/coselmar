package fr.ifremer.coselmar.converter;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.reflect.TypeParameter;
import com.google.common.reflect.TypeToken;
import org.apache.commons.beanutils.converters.AbstractConverter;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class JsonArrayConverter<O> extends AbstractConverter {

    protected final Class<O[]> arrayType;

    protected final JsonHelper jsonHelper;

    public static <O> JsonArrayConverter<O> newConverter(Class<O> objectType) {
        return new JsonArrayConverter<>(objectType);
    }

    public JsonArrayConverter(Class<O> entityType) {

        this.arrayType = (Class<O[]>) new TypeToken<O[]>() {
        }
                .where(new TypeParameter<O>() {
                }, entityType)
                .getType();
        this.jsonHelper = new JsonHelper(false);
    }

    @Override
    protected <T> T convertToType(Class<T> type, Object value) throws Throwable {

        String stringValue;

        if (value instanceof String) {

            stringValue = (String) value;

        } else {

            stringValue = ((String[]) value)[0];

        }

        O[] values = jsonHelper.fromJson(stringValue, this.arrayType);

        return (T) values;

    }

    @Override
    public Class<?> getDefaultType() {
        return arrayType;
    }
}

