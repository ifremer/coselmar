package fr.ifremer.coselmar.converter;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coselmar.persistence.CoselmarEntityEnum;
import org.apache.commons.beanutils.converters.AbstractConverter;
import org.nuiton.topia.persistence.TopiaEntity;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class JsonConverter<O> extends AbstractConverter {

    protected Class<O> objectType;

    protected Class<O> implementationClass;

    protected final JsonHelper jsonHelper;

    public static <O> JsonConverter<O> newConverter(Class<O> objectType) {
        return newConverter(objectType, objectType);
    }

    public static <E extends TopiaEntity> JsonConverter<E> newEntityConverter(Class<E> objectType) {
        Class<E> implementationClass = CoselmarEntityEnum.getImplementationClass(objectType);
        return newConverter(objectType, implementationClass);
    }

    public static <O> JsonConverter<O> newConverter(Class<O> objectType, Class<O> implementationClass) {
        return new JsonConverter<>(objectType, implementationClass);
    }

    public JsonConverter(Class<O> objectType, Class<O> implementationClass) {
        this.objectType = objectType;
        this.jsonHelper = new JsonHelper(false);
        this.implementationClass = implementationClass;
    }

    @Override
    protected String convertToString(Object value) throws Throwable {

        String result = jsonHelper.toJson(value);
        return result;
    }

    @Override
    protected <T> T convertToType(Class<T> type, Object value) throws Throwable {

        String stringValue;

        if (value instanceof String) {
            stringValue = (String) value;

        } else {
            stringValue = ((String[]) value)[0];
        }

        T result = (T) jsonHelper.fromJson(stringValue, implementationClass);

        return result;
    }

    @Override
    protected Class<O> getDefaultType() {
        return objectType;
    }

}
