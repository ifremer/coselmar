package fr.ifremer.coselmar.services;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.JWTVerifyException;
import fr.ifremer.coselmar.beans.UserWebToken;
import fr.ifremer.coselmar.config.CoselmarServicesConfig;
import fr.ifremer.coselmar.exceptions.CoselmarTechnicalException;
import fr.ifremer.coselmar.persistence.CoselmarPersistenceContext;
import fr.ifremer.coselmar.persistence.entity.CoselmarUser;
import fr.ifremer.coselmar.persistence.entity.CoselmarUserGroupTopiaDao;
import fr.ifremer.coselmar.persistence.entity.CoselmarUserTopiaDao;
import fr.ifremer.coselmar.persistence.entity.DocumentTopiaDao;
import fr.ifremer.coselmar.persistence.entity.QuestionTopiaDao;
import fr.ifremer.coselmar.services.errors.InvalidCredentialException;
import fr.ifremer.coselmar.services.v1.DocumentsWebService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.call.HttpContext;
import org.nuiton.topia.persistence.TopiaNoResultException;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

/**
 * @author ymartel <martel@codelutin.com>
 */
public abstract class CoselmarWebServiceSupport extends WebMotionController implements CoselmarService {

    private static final Log log = LogFactory.getLog(CoselmarWebServiceSupport.class);
    protected static final String AUTHORIZATION_HEADER = "Authorization";

    private CoselmarServicesContext servicesContext;

    @Override
    public void setServicesContext(CoselmarServicesContext servicesContext) {
        this.servicesContext = servicesContext;
    }

    protected CoselmarServicesContext getServicesContext() {
        //try to get it from Request context
        HttpContext context;
        try  {
            context = getContext();
        } catch (NullPointerException e) {
            // not web context ? //XXX ymartel use because unit test on Documents mass import are not in web context ...
            context = null;
        }
        if (context != null) {
            CoselmarRestRequestContext requestContext = CoselmarRestRequestContext.getRequestContext(context);
            this.servicesContext = requestContext.getServicesContext();
        }
        return this.servicesContext;
    }

    // Delegate serviceContext //

    protected Date getNow() {
        return getServicesContext().getNow();
    }

    protected String getCleanMail(String email) {
        return getServicesContext().getCleanMail(email);
    }

    protected CoselmarPersistenceContext getPersistenceContext() {
        return getServicesContext().getPersistenceContext();
    }

    protected CoselmarServicesConfig getCoselmarServicesConfig() {
        return getServicesContext().getCoselmarServicesConfig();
    }

    protected Locale getLocale() {
        return servicesContext.getLocale();
    }

    // Services //

    public DocumentsWebService getDocumentsService() {
        return newService(DocumentsWebService.class);
    }

    protected <E extends CoselmarService> E newService(Class<E> serviceClass) {
        return getServicesContext().newService(serviceClass);
    }

    // Persistence //
    protected DocumentTopiaDao getDocumentDao() {
        return getPersistenceContext().getDocumentDao();
    }

    protected CoselmarUserTopiaDao getCoselmarUserDao() {
        return getPersistenceContext().getCoselmarUserDao();
    }

    protected CoselmarUserGroupTopiaDao getCoselmarUserGroupDao() {
        return getPersistenceContext().getCoselmarUserGroupDao();
    }

    protected QuestionTopiaDao getQuestionDao() {
        return getPersistenceContext().getQuestionDao();
    }

    public void commit() {
        getPersistenceContext().commit();
    }

    public void rollback() {
        getPersistenceContext().rollback();
    }

    /**
     * Just get the Authorization header from context
     * @return
     */
    protected String getAuthorizationHeader() {
        return getContext().getHeader(AUTHORIZATION_HEADER);
    }

    /**
     * Check the authorization code.
     * Get the token from the authorization and reconstitute JWT user token.
     *
     * @param authorization :   authorization containing the encoding token
     * @return corresponding {@link fr.ifremer.coselmar.beans.UserWebToken}
     *
     * @throws InvalidCredentialException if token is not valid.
     *
     * @Deprecated since 0.8 : prefer use {@link #checkUserAuthentication(String)} that also check user validity.
     */
    @Deprecated
    protected UserWebToken checkAuthentication(String authorization) throws InvalidCredentialException {
        try {

            String webSecurityKey = getServicesContext().getCoselmarServicesConfig().getWebSecurityKey();
            JWTVerifier jwtVerifier = new JWTVerifier(webSecurityKey, "audience");

            String token = StringUtils.replace(authorization, "Bearer ", "");
            Map<String, Object> claims = jwtVerifier.verify(token);
            UserWebToken userWebToken = new UserWebToken(claims);
            return userWebToken;

        } catch (NoSuchAlgorithmException|InvalidKeyException|IOException e) {
            // This should not happened or this is really exceptional !
            if (log.isErrorEnabled()) {
                log.error("Error during JWT verification : wrong Algorithm !", e);
            }
            throw new CoselmarTechnicalException(e);

        } catch (SignatureException e) {
            // Invalid Signature ! It's a Fake !
            if (log.isErrorEnabled()) {
                log.error("Error during JWT verification : bad signature!", e);
            }
            throw new InvalidCredentialException("Error with signature");

        } catch (JWTVerifyException e) {
            // Error during Payload verification
            if (log.isErrorEnabled()) {
                log.error("Error during JWT verification : bad claims!", e);
            }
            throw new InvalidCredentialException("Error with claims");

        } catch (IllegalStateException e) {
            // No token set
            if (log.isErrorEnabled()) {
                log.error("Error during JWT verification : no token!", e);
            }
            throw new InvalidCredentialException("Seems no user connected");

        }

    }

    /**
     * Check the authorization code.
     * Get the token from the authorization and reconstitute JWT user token.
     * From this token, retrieve the user from database.
     *
     * @param authorization :   authorization containing the encoding token
     *
     * @return corresponding {@link fr.ifremer.coselmar.persistence.entity.CoselmarUser}
     *
     * @throws InvalidCredentialException if token is not valid or if user does not exist.
     */
    protected CoselmarUser checkUserAuthentication(String authorization) throws InvalidCredentialException {
        try {

            String webSecurityKey = getServicesContext().getCoselmarServicesConfig().getWebSecurityKey();
            JWTVerifier jwtVerifier = new JWTVerifier(webSecurityKey, "audience");

            String token = StringUtils.replace(authorization, "Bearer ", "");
            Map<String, Object> claims = jwtVerifier.verify(token);
            UserWebToken userWebToken = new UserWebToken(claims);

            // check user still exist
            String userId = getFullUserIdFromShort(userWebToken.getUserId());
            CoselmarUser coselmarUser = getCoselmarUserDao().forTopiaIdEquals(userId).findAny();

            return coselmarUser;

        } catch (NoSuchAlgorithmException|InvalidKeyException|IOException e) {
            // This should not happened or this is really exceptional !
            if (log.isErrorEnabled()) {
                log.error("Error during JWT verification : wrong Algorithm !", e);
            }
            throw new CoselmarTechnicalException(e);

        } catch (SignatureException e) {
            // Invalid Signature ! It's a Fake !
            if (log.isErrorEnabled()) {
                log.error("Error during JWT verification : bad signature!", e);
            }
            throw new InvalidCredentialException("Error with signature");

        } catch (JWTVerifyException e) {
            // Error during Payload verification
            if (log.isErrorEnabled()) {
                log.error("Error during JWT verification : bad claims!", e);
            }
            throw new InvalidCredentialException("Error with claims");

        } catch (TopiaNoResultException e) {
            // User not found, maybe old token ? Or well faked !
            if (log.isErrorEnabled()) {
                log.error("Error during authorization check : user does not exist !", e);
            }
            throw new InvalidCredentialException("User not known");

        } catch (IllegalStateException e) {
            // No token set
            if (log.isErrorEnabled()) {
                log.error("Error during JWT verification : no token!", e);
            }
            throw new InvalidCredentialException("Seems no user connected");

        }

    }

    /**
     * Construct the full id of an user from the random part contained in
     * {@link fr.ifremer.coselmar.beans.UserWebToken}.
     *
     * @param shortUserId   :   the short id, corresponding to the random part of the id
     *
     * @return the complete id, with class FQN.
     */
    protected String getFullUserIdFromShort(String shortUserId) {
        return CoselmarUser.class.getCanonicalName() + getPersistenceContext().getTopiaIdFactory().getSeparator() + shortUserId;
    }

    /**
     * Construct the full id of an entity from the random part contained in a Bean.
     *
     * @param clazz     :   the entity class, we want full id (use to compose the id)
     * @param shortId   :   the short id, corresponding to the random part of the id
     *
     * @return the complete id, with class FQN.
     */
    protected String getFullIdFromShort(Class clazz, String shortId) {
        return clazz.getCanonicalName() + getPersistenceContext().getTopiaIdFactory().getSeparator() + shortId;
    }

    /**
     * Construct a light id with only the random part.
     *
     * @param fullId   :   the full id from which extract the random part
     *
     * @return a light id.
     */
    protected String getShortIdFromFull(String fullId) {
        return getPersistenceContext().getTopiaIdFactory().getRandomPart(fullId);
    }
}
