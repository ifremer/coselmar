package fr.ifremer.coselmar.services.indexation;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coselmar.beans.QuestionBean;
import fr.ifremer.coselmar.config.CloudWordUtils;
import fr.ifremer.coselmar.services.CoselmarSimpleServiceSupport;
import fr.ifremer.coselmar.services.StringLongMapValueComparator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.misc.HighFreqTerms;
import org.apache.lucene.misc.HighFreqTermsMultiFields;
import org.apache.lucene.misc.TermStats;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.TermQuery;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * This Services provides operations about indexed Objects.
 * <ul>
 * <li>cleanning of the indexation db</li>
 * <li>top word from the indexation db about specific {@link QuestionBean} and {@link fr.ifremer.coselmar.beans.DocumentBean} attributes</li>
 * </ul>
 *
 * The purpose is to use power of a indexation db (lucene) to increase search on
 * document text field, and make easier fulltext search
 *
 * @author ymartel <martel@codelutin.com>
 */
public class TransverseIndexationService extends CoselmarSimpleServiceSupport {

    private static final Log log = LogFactory.getLog(TransverseIndexationService.class);

    public static final int MAGICAL_SEARCH_SIZE = 1000;
    public static final String INDEXATION_FIELD_TYPE = "type";

    protected void cleanAllIndex() throws IOException {
        BooleanQuery query = new BooleanQuery.Builder()
            .add(new TermQuery(new Term(INDEXATION_FIELD_TYPE, QuestionsIndexationService.INDEXATION_DOCUMENT_TYPE)), BooleanClause.Occur.SHOULD)
            .add(new TermQuery(new Term(INDEXATION_FIELD_TYPE, DocumentsIndexationService.INDEXATION_DOCUMENT_TYPE)), BooleanClause.Occur.SHOULD)
            .build();
        getLuceneUtils().getIndexWriter().deleteDocuments(query);
        getLuceneUtils().getIndexWriter().commit();
    }

    public Map<String, Long> getTopTerms() throws IOException, ParseException {

        DirectoryReader indexReader = DirectoryReader.open(getLuceneUtils().getIndexWriter());

        Map<String, Long> topWords = new LinkedHashMap<>();
        try {
            String[] searchedFields = {
                QuestionsIndexationService.QUESTION_TITLE_CLOUD_TAG_PROPERTY,
                QuestionsIndexationService.QUESTION_THEME_CLOUD_TAG_PROPERTY,
                DocumentsIndexationService.DOCUMENT_NAME_CLOUD_TAG_PROPERTY,
                DocumentsIndexationService.DOCUMENT_KEYWORD_CLOUD_TAG_PROPERTY,
                DocumentsIndexationService.DOCUMENT_FILE_CONTENT_INDEX_PROPERTY,
            };
            TermStats[] highFreqTerms =  HighFreqTermsMultiFields.getHighFreqTermsMultiFields(indexReader, MAGICAL_SEARCH_SIZE, searchedFields, new HighFreqTerms.TotalTermFreqComparator());

            for (TermStats termStats : highFreqTerms) {
                long totalTermFreq = termStats.totalTermFreq;
                String value = termStats.termtext.utf8ToString();

                if (CloudWordUtils.isCloudableTerm(value)) {

                    if (topWords.containsKey(value)) {
                        topWords.put(value, topWords.get(value) + totalTermFreq);
                    } else {
                        topWords.put(value, totalTermFreq);
                    }
                }
            }

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Erreur during topWords search", e);
            }
        }

        indexReader.close();
        StringLongMapValueComparator valueComparator = new StringLongMapValueComparator(topWords);
        TreeMap<String, Long> sortedResult = new TreeMap(valueComparator);
        sortedResult.putAll(topWords);

        TreeMap result = new TreeMap();
        int count = 20;
        for (Map.Entry<String, Long> key : sortedResult.entrySet()) {
            result.put(key.getKey(), key.getValue());
            if (--count == 0) {
                break;
            }

        }
        return result;
    }
}