package fr.ifremer.coselmar.services.indexation;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coselmar.beans.DocumentBean;
import fr.ifremer.coselmar.config.CloudWordUtils;
import fr.ifremer.coselmar.services.CoselmarSimpleServiceSupport;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Fields;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.util.BytesRef;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This Services provides operation about {@link fr.ifremer.coselmar.persistence.entity.Document}
 * or more exactly {@link fr.ifremer.coselmar.beans.DocumentBean} indexation :
 * <ul>
 * <li>registration of a document in the indexation db</li>
 * <li>modification of a document in the indexation db</li>
 * <li>documents search from the indexation db</li>
 * </ul>
 *
 * The purpose is to use power of a indexation db (lucene) to increase search on
 * document text field, and make easier fulltext search
 *
 * @author ymartel <martel@codelutin.com>
 */
public class DocumentsIndexationService extends CoselmarSimpleServiceSupport {

    protected static final String DOCUMENT_ID_INDEX_PROPERTY = "documentId";
    protected static final String DOCUMENT_NAME_INDEX_PROPERTY = "documentName";
    protected static final String DOCUMENT_AUTHORS_INDEX_PROPERTY = "documentAuthors";
    protected static final String DOCUMENT_SUMMARY_INDEX_PROPERTY = "documentSummary";
    protected static final String DOCUMENT_KEYWORD_INDEX_PROPERTY = "documentKeyword";
    protected static final String DOCUMENT_NAME_CLOUD_TAG_PROPERTY = "documentCloudTagName";
    protected static final String DOCUMENT_KEYWORD_CLOUD_TAG_PROPERTY = "documentCloudTagKeyword";
    protected static final String DOCUMENT_FILE_CONTENT_INDEX_PROPERTY = "documentFileContent";
    protected static final String INDEXATION_DOCUMENT_TYPE = "documentindextype";

    public void indexDocument(DocumentBean document, String fileContent) throws IOException {

        Document doc = new Document();
        doc.add(new StringField(DOCUMENT_ID_INDEX_PROPERTY, document.getId(), Field.Store.YES));

        String documentName = document.getName();
        String documentSummary = document.getSummary();

        doc.add(new TextField(DOCUMENT_NAME_INDEX_PROPERTY, documentName, Field.Store.YES));
        if (StringUtils.isNotBlank(document.getAuthors())) {
            doc.add(new TextField(DOCUMENT_AUTHORS_INDEX_PROPERTY, document.getAuthors(), Field.Store.YES));
        }
        doc.add(new Field(DOCUMENT_SUMMARY_INDEX_PROPERTY, documentSummary, LuceneUtils.TYPE_STORED));
        doc.add(new Field(TransverseIndexationService.INDEXATION_FIELD_TYPE, INDEXATION_DOCUMENT_TYPE, TextField.TYPE_STORED));

        // Cloud Tag management
        if (documentName.length() >= CloudWordUtils.CLOUD_TAG_WORD_MIN_SIZE) {
            doc.add(new Field(DOCUMENT_NAME_CLOUD_TAG_PROPERTY,  documentName.replaceAll("'", " "), LuceneUtils.TYPE_STORED));
        }
//        if (documentSummary.length() >= CloudTagUtils.CLOUD_TAG_WORD_MIN_SIZE) {
//            doc.add(new TextField(DOCUMENT_SUMMARY_CLOUD_TAG_PROPERTY, documentSummary, Field.Store.YES));
//        }

        Set<String> keywords = document.getKeywords();
        if (keywords != null) {
            for (String keyword : keywords) {
                doc.add(new Field(DOCUMENT_KEYWORD_INDEX_PROPERTY, keyword, TextField.TYPE_STORED));

                // Cloud Tag management
                if (keyword.length() >= CloudWordUtils.CLOUD_TAG_WORD_MIN_SIZE) {
                    doc.add(new Field(DOCUMENT_KEYWORD_CLOUD_TAG_PROPERTY, keyword.replaceAll("'", " "), LuceneUtils.TYPE_STORED));
                }
            }
        }

        if (StringUtils.isNotBlank(fileContent)) {
            doc.add(new Field(DOCUMENT_FILE_CONTENT_INDEX_PROPERTY, fileContent, LuceneUtils.TYPE_STORED));
        }

        getLuceneUtils().getIndexWriter().addDocument(doc);
        getLuceneUtils().getIndexWriter().commit();

    }

    public List<String> searchDocuments(String text) throws IOException, ParseException {
        DirectoryReader ireader = DirectoryReader.open(getLuceneUtils().getIndexWriter());
        IndexSearcher isearcher = new IndexSearcher(ireader);

        String[] words = text.split(" ");

        // Parse a simple query that searches for the "text":
        BooleanQuery.Builder queryBuilder = new BooleanQuery.Builder();

        BooleanQuery.Builder nameQueryBuilder = new BooleanQuery.Builder();
        BooleanQuery.Builder summaryQueryBuilder = new BooleanQuery.Builder();
        BooleanQuery.Builder authorsQueryBuilder = new BooleanQuery.Builder();
        BooleanQuery.Builder documentFileQueryBuilder = new BooleanQuery.Builder();

        for (String word : words) {
            String wildWord = String.format("*%s*", word.toLowerCase());
            nameQueryBuilder.add(new WildcardQuery(new Term(DOCUMENT_NAME_INDEX_PROPERTY, wildWord)), BooleanClause.Occur.MUST);
            summaryQueryBuilder.add(new WildcardQuery(new Term(DOCUMENT_SUMMARY_INDEX_PROPERTY, wildWord)), BooleanClause.Occur.MUST);
            authorsQueryBuilder.add(new WildcardQuery(new Term(DOCUMENT_AUTHORS_INDEX_PROPERTY, wildWord)), BooleanClause.Occur.MUST);
            documentFileQueryBuilder.add(new WildcardQuery(new Term(DOCUMENT_FILE_CONTENT_INDEX_PROPERTY, wildWord)), BooleanClause.Occur.MUST);
        }

        queryBuilder.add(nameQueryBuilder.build(), BooleanClause.Occur.SHOULD);
        queryBuilder.add(summaryQueryBuilder.build(), BooleanClause.Occur.SHOULD);
        queryBuilder.add(authorsQueryBuilder.build(), BooleanClause.Occur.SHOULD);
        queryBuilder.add(documentFileQueryBuilder.build(), BooleanClause.Occur.SHOULD);

        queryBuilder.add(new TermQuery(new Term(DOCUMENT_KEYWORD_INDEX_PROPERTY, text.toLowerCase())), BooleanClause.Occur.SHOULD);


        // Combine that with the type
        BooleanQuery.Builder fullQueryBuilder = new BooleanQuery.Builder();
        fullQueryBuilder.add(queryBuilder.build(), BooleanClause.Occur.MUST);
        fullQueryBuilder.add(new TermQuery(new Term(TransverseIndexationService.INDEXATION_FIELD_TYPE, INDEXATION_DOCUMENT_TYPE)), BooleanClause.Occur.MUST);

        ScoreDoc[] hits = isearcher.search(fullQueryBuilder.build(), TransverseIndexationService.MAGICAL_SEARCH_SIZE).scoreDocs;

        List<String> documentIds = new ArrayList(hits.length);

        for (ScoreDoc hit : hits) {
            Document doc = isearcher.doc(hit.doc);
            String documentId = doc.get(DOCUMENT_ID_INDEX_PROPERTY);
            documentIds.add(documentId);
        }

        ireader.close();
        return documentIds;
    }

    public List<String> searchDocuments(List<String> texts) throws IOException, ParseException {
        DirectoryReader ireader = DirectoryReader.open(getLuceneUtils().getIndexWriter());
        IndexSearcher isearcher = new IndexSearcher(ireader);


        BooleanQuery.Builder keywordsQueryBuilder = new BooleanQuery.Builder();

        for (String text : texts) {
            String[] words = text.split(" ");

            // Parse a simple query that searches for the "text":
            BooleanQuery.Builder queryBuilder = new BooleanQuery.Builder();

            BooleanQuery.Builder nameQueryBuilder = new BooleanQuery.Builder();
            BooleanQuery.Builder summaryQueryBuilder = new BooleanQuery.Builder();
            BooleanQuery.Builder authorsQueryBuilder = new BooleanQuery.Builder();
            BooleanQuery.Builder documentFileQueryBuilder = new BooleanQuery.Builder();

            for (String word : words) {
                String wildWord = "*" + word.toLowerCase() + "*";
                nameQueryBuilder.add(new WildcardQuery(new Term(DOCUMENT_NAME_INDEX_PROPERTY, wildWord)), BooleanClause.Occur.MUST);
                summaryQueryBuilder.add(new WildcardQuery(new Term(DOCUMENT_SUMMARY_INDEX_PROPERTY, wildWord)), BooleanClause.Occur.MUST);
                authorsQueryBuilder.add(new WildcardQuery(new Term(DOCUMENT_AUTHORS_INDEX_PROPERTY, wildWord)), BooleanClause.Occur.MUST);
                documentFileQueryBuilder.add(new WildcardQuery(new Term(DOCUMENT_FILE_CONTENT_INDEX_PROPERTY, wildWord)), BooleanClause.Occur.MUST);
            }

            queryBuilder.add(nameQueryBuilder.build(), BooleanClause.Occur.SHOULD);
            queryBuilder.add(summaryQueryBuilder.build(), BooleanClause.Occur.SHOULD);
            queryBuilder.add(authorsQueryBuilder.build(), BooleanClause.Occur.SHOULD);
            queryBuilder.add(documentFileQueryBuilder.build(), BooleanClause.Occur.SHOULD);

            queryBuilder.add(new TermQuery(new Term(DOCUMENT_KEYWORD_INDEX_PROPERTY, text.toLowerCase())), BooleanClause.Occur.SHOULD);


            // Combine that with the type
            //XXX ymartel : put to Occur.SHOULD to make an "OR"
            keywordsQueryBuilder.add(queryBuilder.build(), BooleanClause.Occur.MUST);
        }

        BooleanQuery.Builder fullQueryBuilder = new BooleanQuery.Builder();
        fullQueryBuilder.add(keywordsQueryBuilder.build(), BooleanClause.Occur.MUST);
        fullQueryBuilder.add(new TermQuery(new Term(TransverseIndexationService.INDEXATION_FIELD_TYPE, INDEXATION_DOCUMENT_TYPE)), BooleanClause.Occur.MUST);

        ScoreDoc[] hits = isearcher.search(fullQueryBuilder.build(), TransverseIndexationService.MAGICAL_SEARCH_SIZE).scoreDocs;

        List<String> documentIds = new ArrayList(hits.length);

        for (ScoreDoc hit : hits) {
            Document doc = isearcher.doc(hit.doc);
            String documentId = doc.get(DOCUMENT_ID_INDEX_PROPERTY);
            documentIds.add(documentId);
        }

        ireader.close();
        return documentIds;
    }

    public void updateDocument(DocumentBean document, String fileContent) throws IOException {
        DirectoryReader ireader = DirectoryReader.open(getLuceneUtils().getIndexWriter());
        IndexSearcher isearcher = new IndexSearcher(ireader);

        // Retrieve document
        BooleanQuery.Builder queryBuilder = new BooleanQuery.Builder();
        queryBuilder.add(new TermQuery(new Term(DOCUMENT_ID_INDEX_PROPERTY, document.getId())), BooleanClause.Occur.MUST);
        queryBuilder.add(new TermQuery(new Term(TransverseIndexationService.INDEXATION_FIELD_TYPE, INDEXATION_DOCUMENT_TYPE)), BooleanClause.Occur.MUST);

        ScoreDoc[] hits = isearcher.search(queryBuilder.build(), TransverseIndexationService.MAGICAL_SEARCH_SIZE).scoreDocs;
        if (hits.length > 0) {
            Document doc = new Document();
            doc.add(new StringField(DOCUMENT_ID_INDEX_PROPERTY, document.getId(), Field.Store.YES));

            String documentName = document.getName();
            String documentSummary = document.getSummary();
            doc.add(new TextField(DOCUMENT_NAME_INDEX_PROPERTY, documentName, Field.Store.YES));
            if (StringUtils.isNotBlank(document.getAuthors())) {
                doc.add(new TextField(DOCUMENT_AUTHORS_INDEX_PROPERTY, document.getAuthors(), Field.Store.YES));
            }
            doc.add(new Field(DOCUMENT_SUMMARY_INDEX_PROPERTY, documentSummary, LuceneUtils.TYPE_STORED));

            // Cloud Tag management
            if (documentName.length() >= CloudWordUtils.CLOUD_TAG_WORD_MIN_SIZE) {
                doc.add(new Field(DOCUMENT_NAME_CLOUD_TAG_PROPERTY, documentName.replaceAll("'", " "), LuceneUtils.TYPE_STORED));
            }
//            if (documentSummary.length() >= CloudTagUtils.CLOUD_TAG_WORD_MIN_SIZE) {
//                doc.add(new TextField(DOCUMENT_SUMMARY_CLOUD_TAG_PROPERTY, documentSummary, Field.Store.YES));
//            }

            Set<String> keywords = document.getKeywords();
            if (keywords != null) {
                for (String keyword : keywords) {
                    doc.add(new Field(DOCUMENT_KEYWORD_INDEX_PROPERTY, keyword, TextField.TYPE_STORED));

                    // Cloud Tag management
                    if (keyword.length() >= CloudWordUtils.CLOUD_TAG_WORD_MIN_SIZE) {
                        doc.add(new Field(DOCUMENT_KEYWORD_CLOUD_TAG_PROPERTY, keyword.replaceAll("'", " "), LuceneUtils.TYPE_STORED));
                    }
                }
            }

            doc.add(new Field(TransverseIndexationService.INDEXATION_FIELD_TYPE, INDEXATION_DOCUMENT_TYPE, TextField.TYPE_STORED));

            if (StringUtils.isNotBlank(fileContent)) {
                doc.add(new Field(DOCUMENT_FILE_CONTENT_INDEX_PROPERTY, fileContent, LuceneUtils.TYPE_STORED));
            }

            getLuceneUtils().getIndexWriter().updateDocument(new Term(DOCUMENT_ID_INDEX_PROPERTY, document.getId()), doc);
            getLuceneUtils().getIndexWriter().commit();
        }

        ireader.close();
    }

    public void deleteDocument(String documentId) throws IOException {

        // Retrieve document
        BooleanQuery.Builder queryBuilder = new BooleanQuery.Builder();
        queryBuilder.add(new TermQuery(new Term(DOCUMENT_ID_INDEX_PROPERTY, documentId)), BooleanClause.Occur.MUST);
        queryBuilder.add(new TermQuery(new Term(TransverseIndexationService.INDEXATION_FIELD_TYPE, INDEXATION_DOCUMENT_TYPE)), BooleanClause.Occur.MUST);

        getLuceneUtils().getIndexWriter().deleteDocuments(queryBuilder.build());
        getLuceneUtils().getIndexWriter().commit();

    }

    public void cleanIndex() throws IOException {
        BooleanQuery query = new BooleanQuery.Builder()
            .add(new TermQuery(new Term(TransverseIndexationService.INDEXATION_FIELD_TYPE, INDEXATION_DOCUMENT_TYPE)), BooleanClause.Occur.MUST)
            .build();
        getLuceneUtils().getIndexWriter().deleteDocuments(query);
        getLuceneUtils().getIndexWriter().commit();
    }


    public Map<String, Long> getTopDocumentsTerms(List<String> documentIds) throws IOException {

        DirectoryReader ireader = DirectoryReader.open(getLuceneUtils().getIndexWriter());
        IndexSearcher isearcher = new IndexSearcher(ireader);

        // Combine that with the type
        BooleanQuery.Builder queryBuilder = new BooleanQuery.Builder();
        queryBuilder.add(new TermQuery(new Term(TransverseIndexationService.INDEXATION_FIELD_TYPE, INDEXATION_DOCUMENT_TYPE)), BooleanClause.Occur.MUST);

        BooleanQuery.Builder questionIdBuilder = new BooleanQuery.Builder();
        for (String documentId : documentIds) {
            if(StringUtils.isNotBlank(documentId)) {
                questionIdBuilder.add(new TermQuery(new Term(DOCUMENT_ID_INDEX_PROPERTY, documentId.toLowerCase())), BooleanClause.Occur.SHOULD);
            }
        }
        queryBuilder.add(questionIdBuilder.build(), BooleanClause.Occur.MUST);

        TopDocs hits = isearcher.search(queryBuilder.build(), 100);
        ScoreDoc[] scoreDocs = hits.scoreDocs;

        Map<String, Long> result = new LinkedHashMap<>();

        for (ScoreDoc scoreDoc : scoreDocs) {
            Fields termVectors = ireader.getTermVectors(scoreDoc.doc);
            if (termVectors != null) {
                for (String termVector : termVectors) {
                    Terms vector = ireader.getTermVector(scoreDoc.doc, termVector);
                    TermsEnum termsEnum = vector.iterator();
                    BytesRef bytesRef = termsEnum.next();
                    while(bytesRef  != null){
                        String term = bytesRef.utf8ToString().toLowerCase();
                        long totalTermFreq = termsEnum.totalTermFreq();

                        if (CloudWordUtils.isCloudableTerm(term)) {
                            if (result.containsKey(term)) {
                                result.put(term, result.get(term) + totalTermFreq);
                            } else {
                                result.put(term, totalTermFreq);
                            }
                        }
                        bytesRef = termsEnum.next();
                    }
                }
            }
        }

        ireader.close();
        return result;
    }


}
