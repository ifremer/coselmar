package fr.ifremer.coselmar.services;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coselmar.converter.JsonHelper;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.call.HttpContext;
import org.debux.webmotion.server.mapping.Mapping;
import org.debux.webmotion.server.render.Render;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class CoselmarRender<T> extends Render {

    protected T model;

    public CoselmarRender(T model) {
        this.model = model;
    }

    @Override
    public void create(Mapping mapping, Call call) throws IOException, ServletException {

        HttpContext context = call.getContext();
        HttpServletResponse response = context.getResponse();
        response.setContentType("application/json");

        CoselmarServicesApplicationContext applicationContext =
                CoselmarServicesApplicationContext.getApplicationContext(context.getServletContext());

        //TODO ymartel 20141030 : think about formException render
//        if (model instanceof InvalidFormException) {
//
//            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
//
//        }

        boolean devMode = applicationContext.getApplicationConfig().isDevMode();

        JsonHelper gson = new JsonHelper(devMode);
        String json = gson.toJson(model);

        PrintWriter out = context.getOut();
        out.print(json);

    }
}
