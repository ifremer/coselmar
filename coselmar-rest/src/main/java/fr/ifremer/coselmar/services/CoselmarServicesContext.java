package fr.ifremer.coselmar.services;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coselmar.config.CoselmarServicesConfig;
import fr.ifremer.coselmar.persistence.CoselmarPersistenceContext;
import fr.ifremer.coselmar.persistence.CoselmarTopiaApplicationContext;
import fr.ifremer.coselmar.services.indexation.LuceneUtils;

import java.util.Date;
import java.util.Locale;

/**
 * Created by martel on 30/10/14.
 */
public interface CoselmarServicesContext {

    Date getNow();

    CoselmarTopiaApplicationContext getTopiaApplicationContext();

    CoselmarPersistenceContext getPersistenceContext();

    CoselmarServicesConfig getCoselmarServicesConfig();

    LuceneUtils getLuceneUtils();

    <E extends CoselmarService> E newService(Class<E> serviceClass);

    Locale getLocale();

    String getCleanMail(String email);

    String generatePassword();

    String generateSalt();

    String encodePassword(String salt, String password);

}
