package fr.ifremer.coselmar.services.indexation;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coselmar.beans.QuestionBean;
import fr.ifremer.coselmar.beans.QuestionSearchBean;
import fr.ifremer.coselmar.config.CloudWordUtils;
import fr.ifremer.coselmar.services.CoselmarSimpleServiceSupport;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Fields;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.misc.HighFreqTerms;
import org.apache.lucene.misc.HighFreqTermsMultiFields;
import org.apache.lucene.misc.TermStats;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.BooleanClause;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.util.BytesRef;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This Services provides operation about {@link fr.ifremer.coselmar.persistence.entity.Document}
 * or more exactly {@link fr.ifremer.coselmar.beans.DocumentBean} indexation :
 * <ul>
 * <li>registration of a document in the indexation db</li>
 * <li>modification of a document in the indexation db</li>
 * <li>documents search from the indexation db</li>
 * </ul>
 *
 * The purpose is to use power of a indexation db (lucene) to increase search on
 * document text field, and make easier fulltext search
 *
 * @author ymartel <martel@codelutin.com>
 */
public class QuestionsIndexationService extends CoselmarSimpleServiceSupport {

    protected static final String QUESTION_ID_INDEX_PROPERTY = "questionId";
    protected static final String QUESTION_TITLE_INDEX_PROPERTY = "questionTitle";
    protected static final String QUESTION_SUMMARY_INDEX_PROPERTY = "questionSummary";
    protected static final String QUESTION_THEME_INDEX_PROPERTY = "questionTheme";
    protected static final String QUESTION_STATUS_INDEX_PROPERTY = "questionStatus";
    protected static final String QUESTION_PRIVACY_INDEX_PROPERTY = "questionPrivacy";
    protected static final String QUESTION_TITLE_CLOUD_TAG_PROPERTY = "questionCloudTagTitle";
    protected static final String QUESTION_SUMMARY_CLOUD_TAG_PROPERTY = "questionCloudTagSummary";
    protected static final String QUESTION_THEME_CLOUD_TAG_PROPERTY = "questionCloudTagTheme";
    protected static final String INDEXATION_DOCUMENT_TYPE = "questionindextype";

    public void indexQuestion(QuestionBean question) throws IOException {

        // First : try to find if already exist to update it
        DirectoryReader ireader = DirectoryReader.open(getLuceneUtils().getIndexWriter());
        IndexSearcher isearcher = new IndexSearcher(ireader);

        // Retrieve document
        BooleanQuery query = new BooleanQuery.Builder()
            .add(new TermQuery(new Term(QUESTION_ID_INDEX_PROPERTY, question.getId())), BooleanClause.Occur.MUST)
            .add(new TermQuery(new Term(TransverseIndexationService.INDEXATION_FIELD_TYPE, INDEXATION_DOCUMENT_TYPE)), BooleanClause.Occur.MUST)
            .build();

        ScoreDoc[] hits = isearcher.search(query, TransverseIndexationService.MAGICAL_SEARCH_SIZE).scoreDocs;
        String questionTitle = question.getTitle();
        String questionSummary = question.getSummary();
        if (hits.length > 0) {
            Document doc = new Document();

            doc.add(new StringField(QUESTION_ID_INDEX_PROPERTY, question.getId(), Field.Store.YES));

            doc.add(new TextField(QUESTION_TITLE_INDEX_PROPERTY, questionTitle, Field.Store.YES));
            doc.add(new Field(QUESTION_SUMMARY_INDEX_PROPERTY, questionSummary, LuceneUtils.TYPE_STORED));

            // Cloud Tag management
            if (questionTitle.length() >= CloudWordUtils.CLOUD_TAG_WORD_MIN_SIZE) {
                doc.add(new Field(QUESTION_TITLE_CLOUD_TAG_PROPERTY,  questionTitle.replaceAll("'", " "), LuceneUtils.TYPE_STORED));
            }
//            if (questionSummary.length() >= CloudTagUTil.CLOUD_TAG_WORD_MIN_SIZE) {
//                doc.add(new TextField(QUESTION_SUMMARY_CLOUD_TAG_PROPERTY, questionSummary, Field.Store.YES));
//            }

            Set<String> themes = question.getThemes();
            if (themes != null) {
                for (String theme : themes) {
                    doc.add(new TextField(QUESTION_THEME_INDEX_PROPERTY, theme, Field.Store.YES));

                    // Cloud Tag management
                    if (theme.length() >= CloudWordUtils.CLOUD_TAG_WORD_MIN_SIZE) {
                        doc.add(new Field(QUESTION_THEME_CLOUD_TAG_PROPERTY, theme.replaceAll("'", " "), LuceneUtils.TYPE_STORED));
                    }
                }
            }

            doc.add(new TextField(QUESTION_STATUS_INDEX_PROPERTY, question.getStatus(), Field.Store.YES));

            doc.add(new TextField(QUESTION_PRIVACY_INDEX_PROPERTY, question.getPrivacy(), Field.Store.YES));

            doc.add(new Field(TransverseIndexationService.INDEXATION_FIELD_TYPE, INDEXATION_DOCUMENT_TYPE, TextField.TYPE_STORED));

            getLuceneUtils().getIndexWriter().updateDocument(new Term(QUESTION_ID_INDEX_PROPERTY, question.getId()), doc);

        } else {
            // Not exist yet : add it to index

            Document doc = new Document();
            doc.add(new StringField(QUESTION_ID_INDEX_PROPERTY, question.getId(), Field.Store.YES));

            doc.add(new TextField(QUESTION_TITLE_INDEX_PROPERTY, questionTitle, Field.Store.YES));
            doc.add(new Field(QUESTION_SUMMARY_INDEX_PROPERTY, questionSummary, LuceneUtils.TYPE_STORED));

            doc.add(new TextField(QUESTION_STATUS_INDEX_PROPERTY, question.getStatus(), Field.Store.YES));
            doc.add(new TextField(QUESTION_PRIVACY_INDEX_PROPERTY, question.getPrivacy(), Field.Store.YES));

            // Cloud Tag management
            if (questionTitle.length() >= CloudWordUtils.CLOUD_TAG_WORD_MIN_SIZE) {
                doc.add(new Field(QUESTION_TITLE_CLOUD_TAG_PROPERTY, questionTitle.replaceAll("'", " "), LuceneUtils.TYPE_STORED));
            }
//            if (questionSummary.length() >= CloudTagUTil.CLOUD_TAG_WORD_MIN_SIZE) {
//                doc.add(new TextField(QUESTION_SUMMARY_CLOUD_TAG_PROPERTY, questionSummary, Field.Store.YES));
//            }

            Set<String> themes = question.getThemes();
            if (themes != null) {
                for (String theme : themes) {
                    doc.add(new Field(QUESTION_THEME_INDEX_PROPERTY, theme, TextField.TYPE_STORED));

                    // Cloud Tag management
                    if (theme.length() >= CloudWordUtils.CLOUD_TAG_WORD_MIN_SIZE) {
                        doc.add(new Field(QUESTION_THEME_CLOUD_TAG_PROPERTY, theme.replaceAll("'", " "), LuceneUtils.TYPE_STORED));
                    }
                }
            }

            doc.add(new Field(TransverseIndexationService.INDEXATION_FIELD_TYPE, INDEXATION_DOCUMENT_TYPE, TextField.TYPE_STORED));

            getLuceneUtils().getIndexWriter().addDocument(doc);

        }

        // Commit, close reader.
        getLuceneUtils().getIndexWriter().commit();
        ireader.close();

    }

    public List<String> searchQuestion(QuestionSearchBean searchBean) throws IOException, ParseException {
        DirectoryReader ireader = DirectoryReader.open(getLuceneUtils().getIndexWriter());
        IndexSearcher isearcher = new IndexSearcher(ireader);

        // Combine that with the type
        BooleanQuery.Builder queryBuilder = new BooleanQuery.Builder();
        queryBuilder.add(new TermQuery(new Term(TransverseIndexationService.INDEXATION_FIELD_TYPE, INDEXATION_DOCUMENT_TYPE)), BooleanClause.Occur.MUST);

        String searchPrivacy = searchBean.getPrivacy();
        if(StringUtils.isNotBlank(searchPrivacy)) {
            queryBuilder.add(new TermQuery(new Term(QUESTION_PRIVACY_INDEX_PROPERTY, searchPrivacy.toLowerCase())), BooleanClause.Occur.MUST);
        }

        String searchStatus = searchBean.getStatus();
        if(StringUtils.isNotBlank(searchStatus)) {
            queryBuilder.add(new TermQuery(new Term(QUESTION_STATUS_INDEX_PROPERTY, searchStatus.toLowerCase())), BooleanClause.Occur.MUST);
        }

        // Keywords part
        List<String> keywords = searchBean.getFullTextSearch();
        if (keywords != null && !keywords.isEmpty()) {
            BooleanQuery.Builder keywordsQueryBuilder = new BooleanQuery.Builder();

            for (String text : keywords) {

                String[] words = text.replaceAll("[^a-zA-Z ]", "").toLowerCase().split(" ");

                // Parse a simple query that searches for the "text":

                BooleanQuery.Builder nameQueryBuilder = new BooleanQuery.Builder();
                BooleanQuery.Builder summaryQueryBuilder = new BooleanQuery.Builder();

                for (String word : words) {
                    String wildWord = String.format("*%s*", word.toLowerCase());
                    nameQueryBuilder.add(new WildcardQuery(new Term(QUESTION_TITLE_INDEX_PROPERTY, wildWord)), BooleanClause.Occur.MUST);
                    summaryQueryBuilder.add(new WildcardQuery(new Term(QUESTION_SUMMARY_INDEX_PROPERTY, wildWord)), BooleanClause.Occur.MUST);
                }

                BooleanQuery nameQuery = nameQueryBuilder.build();
                BooleanQuery summaryQuery = summaryQueryBuilder.build();


                BooleanQuery query = new BooleanQuery.Builder()
                    .add(nameQuery, BooleanClause.Occur.SHOULD)
                    .add(summaryQuery, BooleanClause.Occur.SHOULD)
                    .add(new TermQuery(new Term(QUESTION_THEME_INDEX_PROPERTY, text.toLowerCase())), BooleanClause.Occur.SHOULD)
                    .build();

                keywordsQueryBuilder.add(query, BooleanClause.Occur.MUST);
            }

            BooleanQuery keywordsQuery = keywordsQueryBuilder.build();
            // add to complete query
            queryBuilder.add(keywordsQuery, BooleanClause.Occur.MUST);
        }

        BooleanQuery fullQuery = queryBuilder.build();
        ScoreDoc[] hits = isearcher.search(fullQuery, TransverseIndexationService.MAGICAL_SEARCH_SIZE).scoreDocs;

        List<String> documentIds = new ArrayList(hits.length);

        for (ScoreDoc hit : hits) {
            Document doc = isearcher.doc(hit.doc);
            String documentId = doc.get(QUESTION_ID_INDEX_PROPERTY);
            documentIds.add(documentId);
        }

        ireader.close();
        return documentIds;
    }

    public void deleteQuestion(String documentId) throws IOException {

        // Retrieve document
        BooleanQuery query = new BooleanQuery.Builder()
            .add(new TermQuery(new Term(QUESTION_ID_INDEX_PROPERTY, documentId)), BooleanClause.Occur.MUST)
            .add(new TermQuery(new Term(TransverseIndexationService.INDEXATION_FIELD_TYPE, INDEXATION_DOCUMENT_TYPE)), BooleanClause.Occur.MUST)
            .build();

        getLuceneUtils().getIndexWriter().deleteDocuments(query);
        getLuceneUtils().getIndexWriter().commit();

    }

    protected void cleanIndex() throws IOException {
        BooleanQuery query = new BooleanQuery.Builder()
            .add(new TermQuery(new Term(TransverseIndexationService.INDEXATION_FIELD_TYPE, INDEXATION_DOCUMENT_TYPE)), BooleanClause.Occur.SHOULD)
            .build();
        getLuceneUtils().getIndexWriter().deleteDocuments(query);
        getLuceneUtils().getIndexWriter().commit();
    }

    public Map<String, Long> getTopTerms() throws IOException, ParseException {

        DirectoryReader indexReader = DirectoryReader.open(getLuceneUtils().getIndexWriter());

        Map<String, Long> result = new LinkedHashMap<>();
        try {
            String[] searchedFields = {QUESTION_TITLE_INDEX_PROPERTY, QUESTION_SUMMARY_INDEX_PROPERTY, QUESTION_THEME_INDEX_PROPERTY};
            TermStats[] highFreqTerms =  HighFreqTermsMultiFields.getHighFreqTermsMultiFields(indexReader, 20, searchedFields, new HighFreqTerms.TotalTermFreqComparator());
            for (TermStats termStats : highFreqTerms) {
                long totalTermFreq = termStats.totalTermFreq;
                String value = termStats.termtext.utf8ToString();

                if (result.containsKey(value)) {
                    result.put(value, result.get(value) + totalTermFreq);
                } else {
                    result.put(value, totalTermFreq);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        indexReader.close();
        return result;
    }

    public Map<String, Long> getTopQuestionsTerms(List<String> questionIds) throws IOException {

        DirectoryReader ireader = DirectoryReader.open(getLuceneUtils().getIndexWriter());
        IndexSearcher isearcher = new IndexSearcher(ireader);

        // Combine that with the type
        BooleanQuery.Builder queryBuilder = new BooleanQuery.Builder();
        queryBuilder.add(new TermQuery(new Term(TransverseIndexationService.INDEXATION_FIELD_TYPE, INDEXATION_DOCUMENT_TYPE)), BooleanClause.Occur.MUST);

        BooleanQuery.Builder questionIdBuilder = new BooleanQuery.Builder();
        for (String questionId : questionIds) {
            if(StringUtils.isNotBlank(questionId)) {
                questionIdBuilder.add(new TermQuery(new Term(QUESTION_ID_INDEX_PROPERTY, questionId.toLowerCase())), BooleanClause.Occur.SHOULD);
            }
        }
        queryBuilder.add(questionIdBuilder.build(), BooleanClause.Occur.MUST);

        TopDocs hits = isearcher.search(queryBuilder.build(), 100);
        ScoreDoc[] scoreDocs = hits.scoreDocs;

        Map<String, Long> result = new LinkedHashMap<>();

        for (ScoreDoc scoreDoc : scoreDocs) {
            Fields termVectors = ireader.getTermVectors(scoreDoc.doc);
            if (termVectors != null) {
                for (String termVector : termVectors) {
                    Terms vector = ireader.getTermVector(scoreDoc.doc, termVector);
                    TermsEnum termsEnum = vector.iterator();
                    BytesRef bytesRef = termsEnum.next();
                    while(bytesRef  != null){
                        String term = bytesRef.utf8ToString().toLowerCase();
                        long totalTermFreq = termsEnum.totalTermFreq();

                        if (CloudWordUtils.isCloudableTerm(term)) {
                            if (result.containsKey(term)) {
                                result.put(term, result.get(term) + totalTermFreq);
                            } else {
                                result.put(term, totalTermFreq);
                            }
                        }
                        bytesRef = termsEnum.next();
                    }
                }
            }
        }

        ireader.close();
        return result;
    }


}
