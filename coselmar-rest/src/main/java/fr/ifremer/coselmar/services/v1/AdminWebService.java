package fr.ifremer.coselmar.services.v1;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coselmar.beans.DocumentBean;
import fr.ifremer.coselmar.beans.QuestionBean;
import fr.ifremer.coselmar.beans.UserWebToken;
import fr.ifremer.coselmar.converter.BeanEntityConverter;
import fr.ifremer.coselmar.exceptions.CoselmarTechnicalException;
import fr.ifremer.coselmar.persistence.entity.CoselmarUserRole;
import fr.ifremer.coselmar.persistence.entity.Document;
import fr.ifremer.coselmar.persistence.entity.Question;
import fr.ifremer.coselmar.services.CoselmarWebServiceSupport;
import fr.ifremer.coselmar.services.errors.InvalidCredentialException;
import fr.ifremer.coselmar.services.errors.UnauthorizedException;
import fr.ifremer.coselmar.services.indexation.DocumentsIndexationService;
import fr.ifremer.coselmar.services.indexation.QuestionsIndexationService;
import fr.ifremer.coselmar.services.indexation.TikaUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;

import java.io.IOException;
import java.util.List;

import static org.apache.commons.logging.LogFactory.getLog;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class AdminWebService extends CoselmarWebServiceSupport {

    private static final Log log = getLog(AdminWebService.class);

    public void refreshLuceneIndex() throws UnauthorizedException, InvalidCredentialException {

        // Check authentication
        String authorization = getContext().getHeader("Authorization");
        UserWebToken userWebToken = checkAuthentication(authorization);

        // Who is allowed here ? Admin and user himself
        if (!StringUtils.equals(userWebToken.getRole(), CoselmarUserRole.ADMIN.name())) {
            if (log.isDebugEnabled()) {
                String message = String.format("A non admin user try to refresh lucene index.");
                log.debug(message);
            }
            throw new UnauthorizedException("Not allowed to execute operation");
        }


        QuestionsIndexationService questionsIndexationService = getServicesContext().newService(QuestionsIndexationService.class);
        try {
            getServicesContext().getLuceneUtils().clearIndex();
            refreshDocumentsIndex();


            // Get all questions
            List<Question> questions = getQuestionDao().findAll();
            for (Question question : questions) {
                QuestionBean questionBean = BeanEntityConverter.toLightBean(getPersistenceContext().getTopiaIdFactory(), question);
                questionsIndexationService.indexQuestion(questionBean);
            }

            if (log.isDebugEnabled()) {
                String message = String.format("Index was refreshed");
                log.debug(message);
            }
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Unable to index new document", e);
            }
            throw new CoselmarTechnicalException("Error during Index Refresh", e);
        }
    }

    protected void refreshDocumentsIndex() throws IOException {
        DocumentsIndexationService documentsIndexationService = getServicesContext().newService(DocumentsIndexationService.class);
        documentsIndexationService.cleanIndex();
        // get All documents
        List<Document> documents = getDocumentDao().findAll();
        for (Document document : documents) {
            DocumentBean documentBean = BeanEntityConverter.toBean(getPersistenceContext().getTopiaIdFactory(), document);
            if (document.isWithFile()) {
                // Refresh file information
                String fileContent = TikaUtils.getFileContent(document.getFilePath());
                documentsIndexationService.indexDocument(documentBean, fileContent);
                // Refresh database content
                document.setFileContent(fileContent);
                getDocumentDao().update(document);
            }
        }
        commit();
    }

}
