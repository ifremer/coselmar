package fr.ifremer.coselmar.services.v1;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import fr.ifremer.coselmar.beans.CloudWord;
import fr.ifremer.coselmar.beans.DocumentBean;
import fr.ifremer.coselmar.beans.LinkBean;
import fr.ifremer.coselmar.beans.QuestionBean;
import fr.ifremer.coselmar.beans.QuestionExportModel;
import fr.ifremer.coselmar.beans.QuestionSearchBean;
import fr.ifremer.coselmar.beans.QuestionSearchExample;
import fr.ifremer.coselmar.beans.QuestionTreeNode;
import fr.ifremer.coselmar.beans.QuestionUserRole;
import fr.ifremer.coselmar.beans.UserBean;
import fr.ifremer.coselmar.beans.UserWebToken;
import fr.ifremer.coselmar.converter.BeanEntityConverter;
import fr.ifremer.coselmar.exceptions.CoselmarTechnicalException;
import fr.ifremer.coselmar.persistence.entity.CoselmarUser;
import fr.ifremer.coselmar.persistence.entity.CoselmarUserGroup;
import fr.ifremer.coselmar.persistence.entity.CoselmarUserRole;
import fr.ifremer.coselmar.persistence.entity.Document;
import fr.ifremer.coselmar.persistence.entity.Link;
import fr.ifremer.coselmar.persistence.entity.LinkImpl;
import fr.ifremer.coselmar.persistence.entity.Privacy;
import fr.ifremer.coselmar.persistence.entity.Question;
import fr.ifremer.coselmar.persistence.entity.QuestionImpl;
import fr.ifremer.coselmar.persistence.entity.Status;
import fr.ifremer.coselmar.services.CoselmarWebServiceSupport;
import fr.ifremer.coselmar.services.errors.InvalidCredentialException;
import fr.ifremer.coselmar.services.errors.NoResultException;
import fr.ifremer.coselmar.services.errors.UnauthorizedException;
import fr.ifremer.coselmar.services.indexation.DocumentsIndexationService;
import fr.ifremer.coselmar.services.indexation.QuestionsIndexationService;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.queryparser.classic.ParseException;
import org.debux.webmotion.server.render.Render;
import org.nuiton.csv.Export;
import org.nuiton.topia.persistence.TopiaIdFactory;
import org.nuiton.topia.persistence.TopiaNoResultException;
import org.nuiton.util.DateUtil;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class QuestionsWebService extends CoselmarWebServiceSupport {

    private static final Log log = LogFactory.getLog(QuestionsWebService.class);

    protected static final List<String> RESTRICTED_ACCESS_USERS = Lists.newArrayList(CoselmarUserRole.CLIENT.name(), CoselmarUserRole.MEMBER.name());

    public void addQuestion(QuestionBean question) throws InvalidCredentialException, UnauthorizedException {

        // Check authentication
        String authorization = getAuthorizationHeader();
        UserWebToken userWebToken = checkAuthentication(authorization);

        // Only Supervisor can add question
        String userRole = userWebToken.getRole();

        if (!StringUtils.equalsIgnoreCase(CoselmarUserRole.SUPERVISOR.name(), userRole)) {
            String message = String.format("User %s %s ('%s') is not allowed to add question",
                userWebToken.getFirstName(), userWebToken.getLastName(), userWebToken.getUserId());
            if (log.isWarnEnabled()) {
                log.warn(message);
            }
            throw new UnauthorizedException(message);

        }

        // retrieve user who will be assigned as question supervisor
        String fullId = getFullUserIdFromShort(userWebToken.getUserId());

        CoselmarUser supervisor;
        try {
            supervisor = getCoselmarUserDao().forTopiaIdEquals(fullId).findUnique();
        } catch (TopiaNoResultException tnre) {
            // Should not happened, cause user are not really deleted
            String message = String.format("Logged user ('%s') does not exist.", fullId);
            if (log.isErrorEnabled()) {
                log.error(message);
            }
            throw new InvalidCredentialException(message);
        }

        Preconditions.checkNotNull(question);
        Preconditions.checkNotNull(question.getTitle());
        Preconditions.checkNotNull(question.getSummary());
        Preconditions.checkNotNull(question.getType());
        Preconditions.checkNotNull(question.getThemes());

        // let's go
        Question questionEntity = getQuestionDao().create();

        questionEntity.setUnavailable(false);

        // Question basics

        questionEntity.setTitle(question.getTitle());

        questionEntity.setSummary(question.getSummary());

        questionEntity.setType(question.getType());

        Set<String> themes = question.getThemes();
        if (themes != null) {
            questionEntity.setTheme(new HashSet(themes));
        }

        // By default, privacy is private
        String privacy = question.getPrivacy();
        Privacy realPrivacy = privacy  != null ? Privacy.valueOf(privacy.toUpperCase()) : Privacy.PRIVATE;
        questionEntity.setPrivacy(realPrivacy);

        // On creation, Status is IN_PROGRESS
        questionEntity.setStatus(Status.IN_PROGRESS);

        // Manage Dates : submission & deadline
        Date submissionDate = question.getSubmissionDate();
        if (submissionDate != null) {
            questionEntity.setSubmissionDate(new Date(submissionDate.getTime()));
        } else {
            questionEntity.setSubmissionDate(new Date());
        }

        Date deadline = question.getDeadline();
        if (deadline != null) {
            questionEntity.setDeadline(new Date(deadline.getTime()));
        }


        // Users around the question

        // First Supervisor is the one creating this question
        questionEntity.addSupervisors(supervisor);

        questionEntity.addAllExternalExperts(question.getExternalExperts());

        // Retrieve the clients
        Set<UserBean> clients = question.getClients();
        if (clients != null && !clients.isEmpty()) {
            Set<CoselmarUser> clientEntities = retrieveUsers(clients);
            questionEntity.addAllClients(clientEntities);
        }

        // For participants, should create a dedicated group,
        // that could be used to restrict documents access
        Set<UserBean> participants = question.getParticipants();
        CoselmarUserGroup participantGroup = getCoselmarUserGroupDao().create();
        if (participants != null && !participants.isEmpty()) {
            participantGroup.setName(question.getTitle());

            Set<CoselmarUser> expertEntities = retrieveUsers(participants);
            participantGroup.addAllMembers(expertEntities);

        }
        questionEntity.setParticipants(participantGroup);

        // Retrieve the supervisor
        Set<UserBean> supervisors = question.getSupervisors();
        if (supervisors != null && !supervisors.isEmpty()) {
            Set<CoselmarUser> supervisorEntities = retrieveUsers(supervisors);
            questionEntity.addAllSupervisors(supervisorEntities);
        }

        // Note : no contributors now, contributors are old participants,
        // excluded from process


        // Hierarchy of questions
        Set<QuestionBean> parents = question.getParents();
        if (parents != null && !parents.isEmpty()) {
            Set<Question> questions = retrieveQuestions(parents);
            questionEntity.addAllParents(questions);
        }


        // Documents on init
        Set<DocumentBean> relatedDocuments = question.getRelatedDocuments();
        if (relatedDocuments != null && !relatedDocuments.isEmpty()) {
            Set<Document> documents = retrieveDocuments(relatedDocuments);
            // Manage restriction list for document with Privacy.RESTRICTED
            for (Document document : documents) {
                if (document.getPrivacy() == Privacy.RESTRICTED) {
                    document.addRestrictedList(participantGroup);
                }
            }

            questionEntity.addAllRelatedDocuments(documents);
        }

        // Links
        Set<LinkBean> links = question.getLinks();
        if (links != null && !links.isEmpty()) {
            for (LinkBean link : links) {
                Link linkEntity = null;
                String linkId = link.getId();
                if (StringUtils.isNotBlank(linkId)) {
                    linkEntity = getPersistenceContext().getLinkDao().forTopiaIdEquals(getFullIdFromShort(Link.class, linkId)).findUniqueOrNull();
                }
                if (linkEntity == null) {
                    linkEntity = new LinkImpl();
                }
                linkEntity.setName(link.getName());
                linkEntity.setUrl(link.getUrl());

                if (StringUtils.isNotBlank(linkEntity.getTopiaId())) {
                    getPersistenceContext().getLinkDao().update(linkEntity);
                } else {
                    getPersistenceContext().getLinkDao().create(linkEntity);
                }

                questionEntity.addLinks(linkEntity);
            }
        }

        commit();

        QuestionBean result = BeanEntityConverter.toBean(getPersistenceContext().getTopiaIdFactory(), questionEntity);

        QuestionsIndexationService questionsIndexationService = getServicesContext().newService(QuestionsIndexationService.class);
        try {
            questionsIndexationService.indexQuestion(result);
            if (log.isDebugEnabled()) {
                String message = String.format("Question '%s' added to index", result.getTitle());
                log.debug(message);
            }
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Unable to index new question", e);
            }
        }
    }

    public List<QuestionBean> getQuestions(QuestionSearchBean searchOption) throws InvalidCredentialException, UnauthorizedException {

        // Check authentication
        String authorization = getAuthorizationHeader();
        CoselmarUser currentUser = checkUserAuthentication(authorization);

        List<Question> questionList;

        if (searchOption != null) {
            questionList = getAllFilteredQuestions(currentUser, searchOption);

        } else {
            questionList = getAllQuestions(currentUser);
        }

        List<QuestionBean> result = convert(currentUser, questionList);

        return result;
    }

    public PaginationResult<QuestionBean> getPaginatedQuestions(QuestionSearchBean searchOption) throws InvalidCredentialException, UnauthorizedException {

        // Check authentication
        String authorization = getAuthorizationHeader();
        CoselmarUser currentUser = checkUserAuthentication(authorization);

        PaginationResult<Question> paginatedQuestions;

        if (searchOption != null) {
            paginatedQuestions = getPaginatedFilteredQuestions(currentUser, searchOption);

        } else {
            List<Question> allQuestions = getAllQuestions(currentUser);
            paginatedQuestions = PaginationResult.of(allQuestions, allQuestions.size(), PaginationParameter.of(0, -1));
        }

        PaginationResult<QuestionBean> result = convert(currentUser, paginatedQuestions);

        return result;
    }

    public List<QuestionBean> getPublicQuestions() throws InvalidCredentialException, UnauthorizedException {

        // No authentication needed, just filter to get last 5 public questions
        QuestionSearchExample searchOption = QuestionSearchExample.newDefaultSearchExample();
        QuestionImpl example = new QuestionImpl();
        example.setPrivacy(Privacy.PUBLIC);
        example.setStatus(Status.IN_PROGRESS);
        searchOption.setExample(example);
        searchOption.setLimit(5);
        searchOption.setPage(1);

        PaginationParameter paginationParameter = PaginationParameter.of(0, 5, Question.PROPERTY_SUBMISSION_DATE, true);

        PaginationResult<Question> questionList = getQuestionDao().findWithSearchExample(searchOption, paginationParameter);

        List<QuestionBean> result = new ArrayList<>(questionList.getElements().size());

        for (Question question : questionList.getElements()) {
            TopiaIdFactory topiaIdFactory = getPersistenceContext().getTopiaIdFactory();

            QuestionBean questionBean = BeanEntityConverter.toLightBean(topiaIdFactory, question);

            result.add(questionBean);
        }

        return result;
    }

    public void deleteQuestion(String questionId) throws InvalidCredentialException, UnauthorizedException {

        // Check authentication
        String authorization = getAuthorizationHeader();
        UserWebToken userWebToken = checkAuthentication(authorization);

        // Only Supervisor can delete question
        String userRole = userWebToken.getRole();

        if (!StringUtils.equalsIgnoreCase(CoselmarUserRole.SUPERVISOR.name(), userRole)
            && !StringUtils.equalsIgnoreCase(CoselmarUserRole.ADMIN.name(), userRole)) {
            String message = String.format("User %s %s ('%s') is not allowed to delete question",
                userWebToken.getFirstName(), userWebToken.getLastName(), userWebToken.getUserId());
            if (log.isWarnEnabled()) {
                log.warn(message);
            }
            throw new UnauthorizedException(message);

        }

        String fullUserId = getFullIdFromShort(CoselmarUser.class, userWebToken.getUserId());

        try {
            getCoselmarUserDao().forTopiaIdEquals(fullUserId).findUnique();
        } catch (TopiaNoResultException tnre) {
            // Should not happened, cause user are not really deleted
            String message = String.format("Logged user ('%s') does not exist.", fullUserId);
            if (log.isErrorEnabled()) {
                log.error(message);
            }
            throw new InvalidCredentialException(message);
        }

        // Retrieve Question
        String fullQuestionId = getFullIdFromShort(Question.class, questionId);
        Question question = getQuestionDao().forTopiaIdEquals(fullQuestionId).findUnique();

        // Participant group should be deleted, and so, we should remove it from Document using this group
        CoselmarUserGroup participantGroup = question.getParticipants();
        if (participantGroup != null) {
            List<Document> documents = getDocumentDao().forRestrictedListContains(participantGroup).findAll();
            for (Document document : documents) {
                document.removeRestrictedList(participantGroup);
            }
            question.addAllContributors(participantGroup.getMembers());
            getPersistenceContext().getCoselmarUserGroupDao().delete(participantGroup);
        }

        // Question become unavailable
        question.setUnavailable(true);
        question.setStatus(Status.DELETED);


        commit();

        // Remove it from index too
        QuestionsIndexationService questionsIndexationService = getServicesContext().newService(QuestionsIndexationService.class);
        try {
            questionsIndexationService.deleteQuestion(questionId);
            if (log.isDebugEnabled()) {
                String message = String.format("Question '%s' deleted from index", questionId);
                log.debug(message);
            }
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Unable to remove question from index", e);
            }
        }
    }

    public QuestionBean getQuestion(String questionId) throws InvalidCredentialException, UnauthorizedException {

        // Check authentication
        String authorization = getAuthorizationHeader();
        CoselmarUser currentUser = checkUserAuthentication(authorization);

        // Supervisor can get all the question elements
        // Expert can get all the question elements if public or if he is participant of the question
        // Client can get the question (not the documents) if he is client of the question.

        // Member cannot access to question
        if (CoselmarUserRole.MEMBER == currentUser.getRole()) {
            String message = String.format("User %s %s ('%s') is not allowed to view question",
                currentUser.getFirstname(), currentUser.getName(), getShortIdFromFull(currentUser.getTopiaId()));
            if (log.isWarnEnabled()) {
                log.warn(message);
            }
            throw new UnauthorizedException(message);

        }


        // Retrieve Question
        String fullQuestionId = getFullIdFromShort(Question.class, questionId);
        Question question = getQuestionDao().forTopiaIdEquals(fullQuestionId).findUnique();

        if (CoselmarUserRole.CLIENT == currentUser.getRole()) {
            // Client User can access if it is client of question
            checkIsClientAllowed(question, currentUser);

        }

        QuestionBean result = BeanEntityConverter.toBean(getPersistenceContext().getTopiaIdFactory(), question);
        //manager child
        List<Question> children = getQuestionDao().forParentsContains(question).findAll();
        for (Question child : children) {
            QuestionBean childBean = BeanEntityConverter.toLightBean(getPersistenceContext().getTopiaIdFactory(), child);
            result.addChild(childBean);
        }

        // Client is not allowed to see documents
        if (CoselmarUserRole.CLIENT == currentUser.getRole()
            || CoselmarUserRole.SUPERVISOR != currentUser.getRole() && question.getClients() != null && question.getClients().contains(currentUser)) {

            // clients does not have to see all documents
            result.setRelatedDocuments(null);

        // If document is private, only participants could check it
        } else if (CoselmarUserRole.EXPERT == currentUser.getRole() && question.getPrivacy() == Privacy.PRIVATE) {

            CoselmarUserGroup participants = question.getParticipants();

            if (participants == null || !participants.getMembers().contains(currentUser)) {
                // Non participant only see title, privacy and hierarchy
                result = new QuestionBean();
                result.setTitle(question.getTitle());
                result.setPrivacy(question.getPrivacy().name());
                result.setRestricted(true);
                for (Question parent : question.getParents()) {
                    result.addParent(BeanEntityConverter.toLightBean(getPersistenceContext().getTopiaIdFactory(), parent));
                }
                for (Question child : children) {
                    QuestionBean childBean = BeanEntityConverter.toLightBean(getPersistenceContext().getTopiaIdFactory(), child);
                    result.addChild(childBean);
                }
            }
        }

        return result;
    }

    public void addDocuments(String questionId, DocumentBean[] documents) throws InvalidCredentialException, UnauthorizedException {

        // Check authentication
        String authorization = getAuthorizationHeader();
        UserWebToken userWebToken = checkAuthentication(authorization);

        // Only Supervisor can add documents
        String userRole = userWebToken.getRole();

        if (!StringUtils.equalsIgnoreCase(CoselmarUserRole.SUPERVISOR.name(), userRole)
            && !StringUtils.equalsIgnoreCase(CoselmarUserRole.ADMIN.name(), userRole)
            && !StringUtils.equalsIgnoreCase(CoselmarUserRole.EXPERT.name(), userRole)) {

            String message = String.format("User %s %s ('%s') is not allowed to add document",
                userWebToken.getFirstName(), userWebToken.getLastName(), userWebToken.getUserId());
            if (log.isWarnEnabled()) {
                log.warn(message);
            }
            throw new UnauthorizedException(message);

        }

        String fullUserId = getFullIdFromShort(CoselmarUser.class, userWebToken.getUserId());

        CoselmarUser currentUser;
        try {
            currentUser = getCoselmarUserDao().forTopiaIdEquals(fullUserId).findUnique();
        } catch (TopiaNoResultException tnre) {
            // Should not happened, cause user are not really deleted
            String message = String.format("Logged user ('%s') does not exist.", fullUserId);
            if (log.isErrorEnabled()) {
                log.error(message);
            }
            throw new InvalidCredentialException(message);
        }

        // Retrieve Question
        String fullQuestionId = getFullIdFromShort(Question.class, questionId);
        Question question = getQuestionDao().forTopiaIdEquals(fullQuestionId).findUnique();

        // Check expert authorization on the document
        checkIsParticipant(question, currentUser);


        // Retrieve all documents
        Collection<Document> questionDocuments = question.getRelatedDocuments();
        if (documents != null && documents.length > 0) {

            Set<Document> documentEntities = retrieveDocuments(Lists.newArrayList(documents));
            // Manage restriction list for document with Privacy.RESTRICTED
            for (Document document : documentEntities) {
                if (document.getPrivacy() == Privacy.RESTRICTED) {
                    document.addRestrictedList(question.getParticipants());
                }
                if (!questionDocuments.contains(document)) {
                    question.addRelatedDocuments(document);
                }
            }
        }

        commit();
    }

    public void saveQuestion(QuestionBean question) throws InvalidCredentialException, UnauthorizedException {

        // Check authentication
        String authorization = getAuthorizationHeader();
        CoselmarUser supervisor = checkUserAuthentication(authorization);

        // Only Supervisor can save question

        if (CoselmarUserRole.SUPERVISOR != supervisor.getRole()) {
            String message = String.format("User %s %s ('%s') is not allowed to save question",
                supervisor.getFirstname(), supervisor.getName(), supervisor.getTopiaId());
            if (log.isWarnEnabled()) {
                log.warn(message);
            }
            throw new UnauthorizedException(message);

        }

        Preconditions.checkNotNull(question);
        Preconditions.checkNotNull(question.getTitle());
        Preconditions.checkNotNull(question.getSummary());
        Preconditions.checkNotNull(question.getType());
        Preconditions.checkNotNull(question.getThemes());


        // let's go
        Question questionEntity;
        // An update
        String questionId = question.getId();
        boolean inEdition = StringUtils.isNotBlank(questionId);
        if (inEdition) {
            String fullQuestionId = getFullIdFromShort(Question.class, questionId);
            questionEntity = getQuestionDao().forTopiaIdEquals(fullQuestionId).findUnique();

        } else {
            // or a create
            questionEntity = getQuestionDao().create();
        }

        // Question basics

        questionEntity.setTitle(question.getTitle());

        questionEntity.setSummary(question.getSummary());

        questionEntity.setType(question.getType());

        Set<String> themes = question.getThemes();
        if (themes != null) {
            questionEntity.setTheme(new HashSet(themes));
        }

        // By default, privacy is private
        String privacy = question.getPrivacy();
        Privacy realPrivacy = privacy  != null ? Privacy.valueOf(privacy.toUpperCase()) : Privacy.PRIVATE;
        questionEntity.setPrivacy(realPrivacy);

        // On creation, Status is IN_PROGRESS
        if (inEdition) {
            String status = question.getStatus();
            questionEntity.setStatus(status != null ? Status.valueOf(status.toUpperCase()) : Status.IN_PROGRESS);

            // If it is a close or adjourn update, put a closing date, if it is a reopen, remove closing date
            if (Lists.newArrayList(Status.CLOSED.name(), Status.ADJOURNED.name()).contains(status) && questionEntity.getClosingDate() == null) {
                questionEntity.setClosingDate(new Date());

            // it could be a reopen ...
            } else if (questionEntity.getClosingDate() != null && !Lists.newArrayList(Status.CLOSED.name(), Status.ADJOURNED.name()).contains(status)) {
                questionEntity.setClosingDate(null);
                // XXX ymartel 2015/02/04 : Should we do ?
//                questionEntity.setConclusion(null);
//                questionEntity.clearClosingDocuments();
            }

        } else {
            questionEntity.setStatus(Status.IN_PROGRESS);
        }

        // Manage Dates : submission & deadline
        Date submissionDate = question.getSubmissionDate();
        if (submissionDate != null) {
            questionEntity.setSubmissionDate(new Date(submissionDate.getTime()));
        } else {
            questionEntity.setSubmissionDate(new Date());
        }

        Date deadline = question.getDeadline();
        if (deadline != null) {
            questionEntity.setDeadline(new Date(deadline.getTime()));
        } else {
            questionEntity.setDeadline(null);
        }


        // Users around the question

        // First Supervisor is the one creating this question
        questionEntity.addSupervisors(supervisor);

        Set<String> externalExperts = question.getExternalExperts();
        questionEntity.clearExternalExperts();
        if (externalExperts != null && !externalExperts.isEmpty()) {
            questionEntity.addAllExternalExperts(externalExperts);
        }

        // Retrieve the clients
        Set<UserBean> clients = question.getClients();
        questionEntity.clearClients();
        if (clients != null && !clients.isEmpty()) {
            Set<CoselmarUser> clientEntities = retrieveUsers(clients);
            questionEntity.addAllClients(clientEntities);
        }

        // Retrieve the contributors
        Set<UserBean> contributors = question.getContributors();
        questionEntity.clearContributors();
        if (contributors != null && !contributors.isEmpty()) {
            Set<CoselmarUser> contributorEntities = retrieveUsers(contributors);
            questionEntity.addAllContributors(contributorEntities);
        }

        // For participants, should create a dedicated group,
        // that could be used to restrict documents access
        Set<UserBean> participants = question.getParticipants();
        CoselmarUserGroup participantGroup;

        //On update, get the existing group, make a diff : removed participants become contributor
        if (inEdition) {
            participantGroup = questionEntity.getParticipants();
            participantGroup.setName(question.getTitle());

            if (participants != null && !participants.isEmpty()) {
                Set<CoselmarUser> expertEntities = retrieveUsers(participants);

                // For each already assigned participants, if not in new list, assign them as contributors
                for (CoselmarUser participantBefore : participantGroup.getMembers()) {
                    if (!expertEntities.contains(participantBefore)) {
                        questionEntity.addContributors(participantBefore);
                    }
                }

                participantGroup.clearMembers();
                participantGroup.addAllMembers(expertEntities);
            } else {
                questionEntity.addAllContributors(participantGroup.getMembers());
                participantGroup.clearMembers();
            }

        } else {
            // If not update, create new group
            participantGroup = getCoselmarUserGroupDao().create();
            participantGroup.setName(question.getTitle());

            if (participants != null && !participants.isEmpty()) {
                Set<CoselmarUser> expertEntities = retrieveUsers(participants);
                participantGroup.addAllMembers(expertEntities);
            }
        }
        questionEntity.setParticipants(participantGroup);

        // Retrieve the supervisor
        Set<UserBean> supervisors = question.getSupervisors();
        questionEntity.clearSupervisors();
        if (supervisors != null && !supervisors.isEmpty()) {
            Set<CoselmarUser> supervisorEntities = retrieveUsers(supervisors);
            questionEntity.addAllSupervisors(supervisorEntities);
        }

        // Note : no contributors now, contributors are old participants,
        // excluded from process


        // Hierarchy of questions
        Set<QuestionBean> parents = question.getParents();
        if (parents != null && !parents.isEmpty()) {
            Set<Question> questions = retrieveQuestions(parents);
            questionEntity.clearParents();
            questionEntity.addAllParents(questions);

        } else if(inEdition) {
            questionEntity.clearParents();
        }


        // Manage Documents list
        Set<DocumentBean> relatedDocuments = question.getRelatedDocuments();
        Set<Document> documents = null;

        if (relatedDocuments != null && !relatedDocuments.isEmpty()) {
            documents = retrieveDocuments(relatedDocuments);
        }

        // In edition : make a diff, remove restricted list for removed documents, clear documents
        if (inEdition) {
            Set<Document> presentDocuments = questionEntity.getRelatedDocuments();

            if (documents == null) {
                for (Document presentDocument : presentDocuments) {
                    if (presentDocument.getPrivacy() == Privacy.RESTRICTED) {
                        presentDocument.removeRestrictedList(participantGroup);
                    }
                }
                questionEntity.clearRelatedDocuments();
            } else {
                for (Document presentDocument : presentDocuments) {
                    if (!documents.contains(presentDocument) && presentDocument.getPrivacy() == Privacy.RESTRICTED) {
                        presentDocument.removeRestrictedList(participantGroup);
                    }
                }
                questionEntity.clearRelatedDocuments();
            }
        }

        if (relatedDocuments != null && !relatedDocuments.isEmpty()) {
            // Manage restriction list for document with Privacy.RESTRICTED
            for (Document document : documents) {
                if (document.getPrivacy() == Privacy.RESTRICTED) {
                    document.addRestrictedList(participantGroup);
                }
            }
            questionEntity.addAllRelatedDocuments(documents);
        }


        questionEntity.setConclusion(question.getConclusion());

        // Closing documents : here we re-go
        Set<DocumentBean> closingDocumentBeans = question.getClosingDocuments();

        Set<Document> closingDocuments = null;
        if (closingDocumentBeans != null && !closingDocumentBeans.isEmpty()) {
            closingDocuments = retrieveDocuments(closingDocumentBeans);
        }

        // In edition : make a diff, remove restricted list for removed closing documents, clear closing documents
        if (inEdition) {
            Set<Document> presentClosingDocuments = questionEntity.getClosingDocuments();

            if (closingDocuments == null) {
                for (Document presentClosingDocument : presentClosingDocuments) {
                    if (presentClosingDocument.getPrivacy() == Privacy.RESTRICTED) {
                        presentClosingDocument.removeRestrictedList(participantGroup);
                    }
                }
                questionEntity.clearClosingDocuments();

            } else {
                for (Document presentClosingDocument : presentClosingDocuments) {
                    if (!closingDocuments.contains(presentClosingDocument) && presentClosingDocument.getPrivacy() == Privacy.RESTRICTED) {
                        presentClosingDocument.removeRestrictedList(participantGroup);
                    }
                }
                questionEntity.clearClosingDocuments();
            }
        }

        if (closingDocuments != null) {
            // Manage restriction list for document with Privacy.RESTRICTED
            for (Document document : closingDocuments) {
                if (document.getPrivacy() == Privacy.RESTRICTED) {
                    document.addRestrictedList(participantGroup);
                }
            }

            questionEntity.addAllClosingDocuments(closingDocuments);
        }

        // Links
        Set<LinkBean> links = question.getLinks();
        questionEntity.clearLinks();
        if (links != null && !links.isEmpty()) {
            for (LinkBean link : links) {
                Link linkEntity = null;
                String linkId = link.getId();
                if (StringUtils.isNotBlank(linkId)) {
                    String linkFullId = getFullIdFromShort(Link.class, linkId);
                    linkEntity = getPersistenceContext().getLinkDao().forTopiaIdEquals(linkFullId).findUniqueOrNull();
                }
                if (linkEntity == null) {
                    linkEntity = new LinkImpl();
                }
                linkEntity.setName(link.getName());
                linkEntity.setUrl(link.getUrl());

                if (StringUtils.isNotBlank(linkEntity.getTopiaId())) {
                    getPersistenceContext().getLinkDao().update(linkEntity);
                } else {
                    getPersistenceContext().getLinkDao().create(linkEntity);
                }

                questionEntity.addLinks(linkEntity);
            }
        }

        commit();

        QuestionBean result = BeanEntityConverter.toBean(getPersistenceContext().getTopiaIdFactory(), questionEntity);

        QuestionsIndexationService questionsIndexationService = getServicesContext().newService(QuestionsIndexationService.class);
        try {
            questionsIndexationService.indexQuestion(result);
            if (log.isDebugEnabled()) {
                String message = String.format("Question '%s' added to index", result.getTitle());
                log.debug(message);
            }
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Unable to index new question", e);
            }
        }
    }

    public List<String> getThemes() throws InvalidCredentialException, UnauthorizedException {

        //XXX ymartel 20141211 : do we need authentication check for that ?
//        // Check authentication
//        String authorization = getContext().getHeader("Authorization");
//        UserWebToken userWebToken = checkAuthentication(authorization);
//
//        // Check current user
//        String fullCurrentUserId = getFullUserIdFromShort(userWebToken.getUserId());
//        getCoselmarUserDao().forTopiaIdEquals(fullCurrentUserId).findAny();

        List<String> themes = getQuestionDao().findAllThemes();

        return themes;
    }

    public List<String> getTypes() throws InvalidCredentialException, UnauthorizedException {

        //XXX ymartel 20141211 : do we need authentication check for that ?
//        // Check authentication
//        String authorization = getContext().getHeader("Authorization");
//        UserWebToken userWebToken = checkAuthentication(authorization);
//
//        // Check current user
//        String fullCurrentUserId = getFullUserIdFromShort(userWebToken.getUserId());
//        getCoselmarUserDao().forTopiaIdEquals(fullCurrentUserId).findAny();

        List<String> types = getQuestionDao().findAllTypes();

        return types;
    }

    public Render exportQuestions(QuestionSearchBean searchOption) throws InvalidCredentialException, UnauthorizedException {

        List<QuestionBean> questions = getQuestions(searchOption);

        QuestionExportModel exportModel = new QuestionExportModel();

        String exportData;
        try {
            exportData = Export.exportToString(exportModel, questions);
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Error during export", e);
            }
            throw new CoselmarTechnicalException("Unable to export datas");
        }

        return renderDownload(IOUtils.toInputStream(exportData), "export-projects", "text/csv");

    }

    public Render exportSearchedQuestions(String token, QuestionSearchBean searchOption) throws InvalidCredentialException, UnauthorizedException {

        CoselmarUser currentUser = checkUserAuthentication(token);

        List<Question> questionList;

        if (searchOption != null) {
            questionList = getAllFilteredQuestions(currentUser, searchOption);

        } else {
            questionList = getAllQuestions(currentUser);
        }

        List<QuestionBean> questions = convert(currentUser, questionList);

        QuestionExportModel exportModel = new QuestionExportModel();

        String exportData;
        try {
            exportData = Export.exportToString(exportModel, questions);
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Error during export", e);
            }
            throw new CoselmarTechnicalException("Unable to export datas");
        }

        return renderDownload(IOUtils.toInputStream(exportData), "export-projects-result.csv", "text/csv");

    }

    public List<QuestionTreeNode> getAncestors(String questionId, int depth) throws InvalidCredentialException, UnauthorizedException {

        // Check authentication
        String authorization = getAuthorizationHeader();
        CoselmarUser currentUser = checkUserAuthentication(authorization);

        // Supervisor can get all the question elements
        // Expert can get all the question elements if public or if he is participant of the question
        // Client can get the question (not the documents) if he is client of the question.

        // Member cannot access to question
        if (CoselmarUserRole.MEMBER == currentUser.getRole()) {
            String message = String.format("User %s %s ('%s') is not allowed to view question",
                currentUser.getFirstname(), currentUser.getName(), getShortIdFromFull(currentUser.getTopiaId()));
            if (log.isWarnEnabled()) {
                log.warn(message);
            }
            throw new UnauthorizedException(message);

        }

        // Retrieve Question
        String fullQuestionId = getFullIdFromShort(Question.class, questionId);
        Question question = getQuestionDao().forTopiaIdEquals(fullQuestionId).findUnique();

        if (CoselmarUserRole.CLIENT == currentUser.getRole()) {
            // Client User can access if it is client of question
            checkIsClientAllowed(question, currentUser);

        }

        List<QuestionTreeNode> result;
        if (depth > 0) {
            result = buildAncestorsTree(fullQuestionId, depth);
        } else {
            result = Collections.emptyList();
        }

        return result;
    }

    public List<QuestionTreeNode> getDescendants(String questionId, int depth) throws InvalidCredentialException, UnauthorizedException {

        // Check authentication
        String authorization = getAuthorizationHeader();
        CoselmarUser currentUser = checkUserAuthentication(authorization);

        // Supervisor can get all the question elements
        // Expert can get all the question elements if public or if he is participant of the question
        // Client can get the question (not the documents) if he is client of the question.

        // Member cannot access to question
        if (CoselmarUserRole.MEMBER == currentUser.getRole()) {
            String message = String.format("User %s %s ('%s') is not allowed to view question",
                currentUser.getFirstname(), currentUser.getName(), getShortIdFromFull(currentUser.getTopiaId()));
            if (log.isWarnEnabled()) {
                log.warn(message);
            }
            throw new UnauthorizedException(message);

        }

        // Retrieve Question
        String fullQuestionId = getFullIdFromShort(Question.class, questionId);
        Question question = getQuestionDao().forTopiaIdEquals(fullQuestionId).findUnique();

        if (CoselmarUserRole.CLIENT == currentUser.getRole()) {
            // Client User can access if it is client of question
            checkIsClientAllowed(question, currentUser);

        }

        List<QuestionTreeNode> result;
        if (depth > 0) {
            result = buildDescendantsTree(fullQuestionId, depth);
        } else {
            result = Collections.emptyList();
        }

        return result;
    }

    public List<QuestionBean> getUserQuestions(String userId, QuestionUserRole userRole) throws InvalidCredentialException, UnauthorizedException {

        // Check authentication
        String authorization = getAuthorizationHeader();
        CoselmarUser currentUser = checkUserAuthentication(authorization);

        // Member cannot access to list of questions
        if (CoselmarUserRole.MEMBER == currentUser.getRole()) {
            String message = String.format("User %s %s ('%s') is not allowed to view question",
                currentUser.getFirstname(), currentUser.getName(), getShortIdFromFull(currentUser.getTopiaId()));
            if (log.isWarnEnabled()) {
                log.warn(message);
            }
            throw new UnauthorizedException(message);
        }

        QuestionSearchBean searchBean = new QuestionSearchBean();

        String userFullId = getFullUserIdFromShort(userId);
        switch (userRole) {
            case CONTRIBUTOR:
                searchBean.setContributorId(userFullId);
                break;
            case SUPERVISOR:
                searchBean.setSupervisorId(userFullId);
                break;
            case PARTICIPANT:
                searchBean.setParticipantId(userFullId);
                break;
            case CLIENT:
                searchBean.setClientId(userFullId);
                break;
            default:
                return Collections.emptyList();
        }

        List<Question> questionList = getAllFilteredQuestions(currentUser, searchBean);

        List<QuestionBean> result = convert(currentUser, questionList);

        return result;

    }

    public List<CloudWord> getTopWords(String questionId) throws InvalidCredentialException, UnauthorizedException, NoResultException {

        // Check authentication
        String authorization = getAuthorizationHeader();
        checkUserAuthentication(authorization);

        // Retrieve Question
        String fullQuestionId = getFullIdFromShort(Question.class, questionId);
        Question question = getQuestionDao().forTopiaIdEquals(fullQuestionId).findUnique();

        List<CloudWord> topWords = new ArrayList<>();
        if (getCoselmarServicesConfig().isPostgresqlDatabase()) {
            try {
                topWords = getQuestionDao().findTopWords(getFullIdFromShort(Question.class, questionId));
            } catch (TopiaNoResultException e) {
                if (log.isErrorEnabled()) {
                    log.error("Try to find top words for non existing questionId" + questionId, e);
                }
                throw new NoResultException("Question does not exist");
            }
        } else {

            QuestionsIndexationService questionsIndexationService = getServicesContext().newService(QuestionsIndexationService.class);
            DocumentsIndexationService documentsIndexationService = getServicesContext().newService(DocumentsIndexationService.class);
            try {
                Map<String, Long> topQuestionsTerms = questionsIndexationService.getTopQuestionsTerms(Lists.newArrayList(questionId));
                List<String> shortDocumentIds = getShortDocumentIds(question);
                Map<String, Long> topDocumentsTerms = documentsIndexationService.getTopDocumentsTerms(shortDocumentIds);
                for (Map.Entry<String, Long> documentTermFreq : topDocumentsTerms.entrySet()) {
                    String term = documentTermFreq.getKey();
                    Long frequence = documentTermFreq.getValue();
                    if (topQuestionsTerms.containsKey(term)) {
                        topQuestionsTerms.put(term, topQuestionsTerms.get(term) + frequence);
                    } else {
                        topQuestionsTerms.put(term, frequence);
                    }
                }

                for (Map.Entry<String, Long> termFreq : topQuestionsTerms.entrySet()) {
                    String term = termFreq.getKey();
                    CloudWord cloudWord = new CloudWord(term, termFreq.getValue());
                    topWords.add(cloudWord);
                }

            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    log.error("Unable to index new question", e);
                }
            }

            // Sort by CloudTag#weight DESC
            topWords = ImmutableList.copyOf(Ordering.natural().onResultOf(new Function<CloudWord, Long>() {
                public Long apply(CloudWord input) {
                    return input.getWeight();
                }
            }).reverse().sortedCopy(topWords));
        }

        return topWords;
    }

    ////////////////////////////////////////////////////////////////////////////
    ///////////////////////     Internal Parts     /////////////////////////////
    ////////////////////////////////////////////////////////////////////////////


    protected void checkIsParticipant(Question question, CoselmarUser currentUser) throws UnauthorizedException {
        String userRole = currentUser.getRole().name();
        Set<CoselmarUser> questionsParticipants = question.getParticipants().getMembers();

        if (StringUtils.equalsIgnoreCase(CoselmarUserRole.EXPERT.name(), userRole)
            && !questionsParticipants.contains(currentUser)) {

            String message = String.format("Expert %s %s ('%s') is not allowed to add document",
                currentUser.getFirstname(), currentUser.getName(), currentUser.getTopiaId());
            if (log.isWarnEnabled()) {
                log.warn(message);
            }
            throw new UnauthorizedException(message);

        }
    }

    protected void checkIsClientAllowed(Question question, CoselmarUser currentUser) throws UnauthorizedException {
        String userRole = currentUser.getRole().name();
        Set<CoselmarUser> questionsClients = question.getClients();

        if (StringUtils.equalsIgnoreCase(CoselmarUserRole.CLIENT.name(), userRole)
            && questionsClients != null
            && !questionsClients.contains(currentUser)) {

            String message = String.format("Client %s %s ('%s') is not allowed to access question %s",
                currentUser.getFirstname(), currentUser.getName(), currentUser.getTopiaId(), question.getTopiaId());
            if (log.isWarnEnabled()) {
                log.warn(message);
            }
            throw new UnauthorizedException(message);

        }
    }

    protected Set<CoselmarUser> retrieveUsers(Collection<UserBean> userBeans) {
        Function<UserBean, String> getIds = new Function<UserBean, String>() {
            @Override
            public String apply(UserBean userBean) {
                return getFullIdFromShort(CoselmarUser.class, userBean.getId());
            }
        };

        Collection<String> userIds = Collections2.transform(userBeans, getIds);
        List<CoselmarUser> coselmarUsers = getCoselmarUserDao().forTopiaIdIn(userIds).findAll();
        return new HashSet<>(coselmarUsers);

    }

    protected Set<Question> retrieveQuestions(Collection<QuestionBean> questionBeans) {
        Function<QuestionBean, String> getIds = new Function<QuestionBean, String>() {
            @Override
            public String apply(QuestionBean questionBean) {
                return getFullIdFromShort(Question.class, questionBean.getId());
            }
        };

        Collection<String> questionIds = Collections2.transform(questionBeans, getIds);
        List<Question> questions = getQuestionDao().forTopiaIdIn(questionIds).findAll();
        return new HashSet<>(questions);

    }

    protected Set<Document> retrieveDocuments(Collection<DocumentBean> documentBeans) {
        Function<DocumentBean, String> getIds = new Function<DocumentBean, String>() {
            @Override
            public String apply(DocumentBean documentBean) {
                return getFullIdFromShort(Document.class, documentBean.getId());
            }
        };

        Collection<String> documentIds = Collections2.transform(documentBeans, getIds);
        List<Document> documents = getDocumentDao().forTopiaIdIn(documentIds).findAll();
        return new HashSet<>(documents);

    }

    /**
     * Methods that retrieve all questions depending of current user role permission :
     * <ul>
     *   <li>{@code ADMIN} and {@code SUPERVISOR} can access to all questions ;</li>
     *   <li>{@code MEMBER} can access to all public questions ;</li>
     *   <li>{@code EXPERT} can access to all private questions where he is participant or client and all public questions ;</li>
     *   <li>{@code CLIENT} can access to all questions where he is client.</li>
     * </ul>
     *
     * @param currentUser   :   current user that make request
     *
     * @return list of all question user is authorized to access.
     * @throws UnauthorizedException
     */
    protected List<Question> getAllQuestions(CoselmarUser currentUser) throws UnauthorizedException {
        String currentUserRole = currentUser.getRole().name();

        List<Question> questionList;
        if (StringUtils.equalsIgnoreCase(CoselmarUserRole.ADMIN.name(), currentUserRole)
            || StringUtils.equalsIgnoreCase(CoselmarUserRole.SUPERVISOR.name(), currentUserRole)) {
            questionList = getQuestionDao().findAll();

        } else if (StringUtils.equalsIgnoreCase(CoselmarUserRole.MEMBER.name(), currentUserRole)) {
            questionList = getQuestionDao().forPrivacyEquals(Privacy.PUBLIC).findAll();

        } else if (StringUtils.equalsIgnoreCase(CoselmarUserRole.EXPERT.name(), currentUserRole)) {
            questionList = getQuestionDao().findForExpert(currentUser);

        } else if (StringUtils.equalsIgnoreCase(CoselmarUserRole.CLIENT.name(), currentUserRole)) {
            questionList = getQuestionDao().forClientsContains(currentUser).findAll();
        } else {
            String message = "Not allowed to access this page";
            if (log.isWarnEnabled()) {
                log.warn("Unknown user type try to access questions list.");
            }
            throw new UnauthorizedException(message);
        }
        return questionList;
    }

    /**
     * Methods that retrieve all questions matching search filter (based on
     * title, summary, themes, status, privacy), also depending of current user role permission :
     * <ul>
     *   <li>{@code ADMIN} and {@code SUPERVISOR} can access to all questions ;</li>
     *   <li>{@code MEMBER} can only access to all public questions (so, if filter is on private, it has no result) ;</li>
     *   <li>{@code EXPERT} can access to all private questions where he is participant or client and all public questions ;</li>
     *   <li>{@code CLIENT} can access to all questions where he is client.</li>
     * </ul>
     *
     * @param currentUser   :   current user that make request
     * @param searchBean    :   bean containing search param to filter result
     *
     * @return list of all question user is authorized to access.
     * @throws UnauthorizedException
     *
     * @deprecated use {@link #getPaginatedFilteredQuestions(CoselmarUser, QuestionSearchBean)}
     */
    protected List<Question> getAllFilteredQuestions(CoselmarUser currentUser, QuestionSearchBean searchBean) throws UnauthorizedException {

        PaginationResult<Question> paginatedFilteredQuestions = getPaginatedFilteredQuestions(currentUser, searchBean);
        return paginatedFilteredQuestions.getElements();
    }

    /**
     * Methods that retrieve questions matching search filter (based on an example
     * or/and fullText search) also depending of current user role permission :
     * <ul>
     *   <li>{@code ADMIN} and {@code SUPERVISOR} can access to all questions ;</li>
     *   <li>{@code MEMBER} can only access to all public questions (so, if filter is on private, it has no result) ;</li>
     *   <li>{@code EXPERT} can access to all private questions where he is participant or client and all public questions ;</li>
     *   <li>{@code CLIENT} can access to all questions where he is client.</li>
     * </ul>
     * The result is paginated, and only the elements of asking page are returned.
     *
     * @param currentUser   :   current user that make request
     * @param searchBean    :   bean containing search param to filter result
     *
     * @return {@link PaginationResult} of questions user is authorized to access.
     * @throws UnauthorizedException
     */
    protected PaginationResult<Question> getPaginatedFilteredQuestions(CoselmarUser currentUser, QuestionSearchBean searchBean) throws UnauthorizedException {

        QuestionsIndexationService questionsIndexationService = getServicesContext().newService(QuestionsIndexationService.class);

        //Try to retrieve corresponding questionIds from the index if searchBean given
        List<String> fromIndexQuestionIds = null;
        if (searchBean != null && searchBean.getFullTextSearch() != null && !searchBean.getFullTextSearch().isEmpty()) {
            try {
                List<String> questionIds = questionsIndexationService.searchQuestion(searchBean);
                fromIndexQuestionIds = getQuestionsFullId(questionIds);
                if (log.isInfoEnabled()) {
                    log.info("Found " + fromIndexQuestionIds.size() + " questions from index.");
                }

            } catch (IOException |ParseException e) {
                if (log.isErrorEnabled()) {
                    log.error("Unable to search by lucene, make search directly in database", e);
                }
            }

        }

        PaginationResult<Question> paginatedQuestions;

        String currentUserRole = currentUser.getRole().name();

        QuestionSearchExample searchExample = QuestionSearchExample.newDefaultSearchExample();
        if (searchBean != null) {
            if (searchBean.getPage() != null) {
                searchExample.setPage(searchBean.getPage());
            }
            if (searchBean.getLimit() != null) {
                searchExample.setLimit(searchBean.getLimit());
            }
            searchExample.setFullTextSearch(searchBean.getFullTextSearch());

            Question example = BeanEntityConverter.fromBean(searchBean);
            searchExample.setExample(example);

            if (searchBean.getSubmissionAfterDate() != null) {
                Date submissionAfterDate = DateUtil.getEndOfDay(DateUtil.getYesterday(searchBean.getSubmissionAfterDate()));
                searchExample.setSubmissionAfterDate(submissionAfterDate);
            }

            if (searchBean.getSubmissionBeforeDate() != null) {
                Date submissionBeforeDate = DateUtil.getEndOfDay(searchBean.getSubmissionBeforeDate());
                searchExample.setSubmissionBeforeDate(submissionBeforeDate);
            }

            if (searchBean.getDeadlineAfterDate() != null) {
                Date deadlineAfterDate = DateUtil.getEndOfDay(DateUtil.getYesterday(searchBean.getDeadlineAfterDate()));
                searchExample.setDeadlineAfterDate(deadlineAfterDate);
            }

            if (searchBean.getDeadlineBeforeDate() != null) {
                Date deadlineBeforeDate = DateUtil.getEndOfDay(searchBean.getDeadlineBeforeDate());
                searchExample.setDeadlineBeforeDate(deadlineBeforeDate);
            }

            if (StringUtils.isNotBlank(searchBean.getParticipantId())) {
                CoselmarUser user = getCoselmarUserDao().forTopiaIdEquals(searchBean.getParticipantId()).findAnyOrNull();
                if (user != null) {
                    searchExample.setParticipant(user);
                }
            }

            if (StringUtils.isNotBlank(searchBean.getContributorId())) {
                CoselmarUser user = getCoselmarUserDao().forTopiaIdEquals(searchBean.getContributorId()).findAnyOrNull();
                if (user != null) {
                    example.addContributors(user);
                }
            }
            if (StringUtils.isNotBlank(searchBean.getSupervisorId())) {
                CoselmarUser user = getCoselmarUserDao().forTopiaIdEquals(searchBean.getSupervisorId()).findAnyOrNull();
                if (user != null) {
                    example.addSupervisors(user);
                }
            }
            if (StringUtils.isNotBlank(searchBean.getClientId())) {
                CoselmarUser user = getCoselmarUserDao().forTopiaIdEquals(searchBean.getClientId()).findAnyOrNull();
                if (user != null) {
                    example.addClients(user);
                }
            }

        }


        // Supervisor or Admin : access to all questions, no restriction
        PaginationParameter paginationParameter = searchExample.getPaginationParameter();
        if (StringUtils.equalsIgnoreCase(CoselmarUserRole.ADMIN.name(), currentUserRole)
            || StringUtils.equalsIgnoreCase(CoselmarUserRole.SUPERVISOR.name(), currentUserRole)) {

            if (fromIndexQuestionIds != null && !fromIndexQuestionIds.isEmpty()) {
                paginatedQuestions = getQuestionDao().forTopiaIdIn(fromIndexQuestionIds).findPage(paginationParameter);

            // classical search with DAO
            } else {
                paginatedQuestions = getQuestionDao().findWithSearchExample(searchExample, paginationParameter);

            }

        // Member : access to public question only
        } else if (StringUtils.equalsIgnoreCase(CoselmarUserRole.MEMBER.name(), currentUserRole)) {

            if (StringUtils.equals(searchBean.getPrivacy(), Privacy.PRIVATE.name())) {
                // Asking for private question ? directly no result !
                paginatedQuestions = PaginationResult.of(new ArrayList<Question>(0), 0, PaginationParameter.of(0, -1));

            } else {

                if (fromIndexQuestionIds != null && !fromIndexQuestionIds.isEmpty()) {
                    paginatedQuestions = getQuestionDao().forTopiaIdIn(fromIndexQuestionIds).findPage(paginationParameter);

                // classical search with DAO
                } else {
                    paginatedQuestions = getQuestionDao().findWithSearchExample(searchExample, paginationParameter);
                }
            }

        // Expert : access to all public question and private question if he is participant or client
        } else if (StringUtils.equalsIgnoreCase(CoselmarUserRole.EXPERT.name(), currentUserRole)) {
            if (fromIndexQuestionIds != null && !fromIndexQuestionIds.isEmpty()) {
                paginatedQuestions = getQuestionDao().findForExpert(currentUser, fromIndexQuestionIds, paginationParameter);

            } else {
                paginatedQuestions = getQuestionDao().findForExpert(currentUser, searchExample, paginationParameter);
            }

        // Client : access to question he is client
        } else if (StringUtils.equalsIgnoreCase(CoselmarUserRole.CLIENT.name(), currentUserRole)) {
            if (fromIndexQuestionIds != null && !fromIndexQuestionIds.isEmpty()) {
                paginatedQuestions = getQuestionDao().findForClient(currentUser, fromIndexQuestionIds, paginationParameter);

            } else {
                paginatedQuestions = getQuestionDao().findForClient(currentUser, searchExample, paginationParameter);
            }

        } else {
            String message = "Not allowed to access this page";
            if (log.isWarnEnabled()) {
                log.warn("Unknown user type try to access questions list.");
            }
            throw new UnauthorizedException(message);
        }
        return paginatedQuestions;
    }

    protected List<String> getQuestionsFullId(List<String> questionShortIds) {

        Function<String, String> getFullIds = new Function<String, String>() {
            @Override
            public String apply(String shortId) {
                return Question.class.getCanonicalName() + getPersistenceContext().getTopiaIdFactory().getSeparator() + shortId;
            }
        };

        List<String> fullIds = Lists.transform(questionShortIds, getFullIds);
        return fullIds;
    }

    protected List<QuestionBean> convert(CoselmarUser currentUser, List<Question> questionList) {
        List<QuestionBean> questions = new ArrayList<>(questionList.size());

        for (Question question : questionList) {
            TopiaIdFactory topiaIdFactory = getPersistenceContext().getTopiaIdFactory();

            QuestionBean questionBean;
            if (RESTRICTED_ACCESS_USERS.contains(currentUser.getRole().name())) {
                questionBean = BeanEntityConverter.toLightBean(topiaIdFactory, question);

            } else {
                questionBean = BeanEntityConverter.toBean(topiaIdFactory, question);
            }

            questions.add(questionBean);
        }
        return questions;
    }

    protected PaginationResult<QuestionBean> convert(CoselmarUser currentUser, PaginationResult<Question> paginatedQuestions) {
        List<QuestionBean> questions = new ArrayList<>(paginatedQuestions.getElements().size());

        for (Question question : paginatedQuestions.getElements()) {
            TopiaIdFactory topiaIdFactory = getPersistenceContext().getTopiaIdFactory();

            QuestionBean questionBean;
            if (RESTRICTED_ACCESS_USERS.contains(currentUser.getRole().name())) {
                questionBean = BeanEntityConverter.toLightBean(topiaIdFactory, question);

            } else {
                questionBean = BeanEntityConverter.toBean(topiaIdFactory, question);
            }

            questions.add(questionBean);
        }

        PaginationResult paginatedQuestionBeans = PaginationResult.of(questions, paginatedQuestions.getCount(), paginatedQuestions.getCurrentPage());
        return paginatedQuestionBeans;
    }

    protected List<QuestionTreeNode> buildAncestorsTree(String questionId, int depth) {
        // Depth = 0 : it is the end of the tree !
        if (depth == 0) {
            return Collections.emptyList();
        }

        Question question = getQuestionDao().forTopiaIdEquals(questionId).findUnique();
        Set<Question> parents = question.getParents();

        List<QuestionTreeNode> result;

        if (parents == null || parents.isEmpty()) {
            result = Collections.emptyList();

        } else {
            result = new ArrayList<>(parents.size());

            // Convert parent to QuestionTreeNode
            for (Question parent : parents) {
                QuestionTreeNode ancestor = new QuestionTreeNode();
                String parentId = parent.getTopiaId();
                ancestor.setId(getShortIdFromFull(parentId));
                ancestor.setTitle(parent.getTitle());
                ancestor.setThemes(Sets.newHashSet(parent.getTheme()));
                Date deadline = parent.getDeadline();
                if (deadline != null) {
                    ancestor.setDeadline(new Date(deadline.getTime()));
                }
                Date submissionDate = parent.getSubmissionDate();
                if (submissionDate != null) {
                    ancestor.setSubmissionDate(new Date (submissionDate.getTime()));
                }
                ancestor.setType(parent.getType());
                // Get ancestors of this parent
                List<QuestionTreeNode> parentAncestors = buildAncestorsTree(parentId, depth - 1);
                ancestor.setAncestors(parentAncestors);
                result.add(ancestor);
            }
        }

        return result;
    }

    protected List<QuestionTreeNode> buildDescendantsTree(String questionId, int depth) {
        // Depth = 0 : it is the end of the tree !
        if (depth == 0) {
            return Collections.emptyList();
        }

        Question question = getQuestionDao().forTopiaIdEquals(questionId).findUnique();
        List<Question> children = getQuestionDao().forParentsContains(question).findAll();

        List<QuestionTreeNode> result;

        if (children == null || children.isEmpty()) {
            result = Collections.emptyList();

        } else {
            result = new ArrayList<>(children.size());

            // Convert child to QuestionTreeNode
            for (Question child : children) {
                QuestionTreeNode descendant = new QuestionTreeNode();
                String parentId = child.getTopiaId();
                descendant.setId(getShortIdFromFull(parentId));
                descendant.setTitle(child.getTitle());
                descendant.setThemes(Sets.newHashSet(child.getTheme()));
                Date deadline = child.getDeadline();
                if (deadline != null) {
                    descendant.setDeadline(new Date(deadline.getTime()));
                }
                Date submissionDate = child.getSubmissionDate();
                if (submissionDate != null) {
                    descendant.setSubmissionDate(new Date (submissionDate.getTime()));
                }
                descendant.setType(child.getType());
                // Get descendants of this child
                List<QuestionTreeNode> childDescendants = buildDescendantsTree(parentId, depth - 1);
                descendant.setDescendants(childDescendants);
                result.add(descendant);
            }
        }

        return result;
    }

    protected List<String> getShortDocumentIds(Question question) {
        List<String> shortDocumentIds = new ArrayList<>();
        for (String relatedDocumentId : question.getRelatedDocumentsTopiaIds()) {
            String shortIdFromFull = getShortIdFromFull(relatedDocumentId);
            shortDocumentIds.add(shortIdFromFull);
        }
        for (String closingDocumentId : question.getClosingDocumentsTopiaIds()) {
            String shortIdFromFull = getShortIdFromFull(closingDocumentId);
            shortDocumentIds.add(shortIdFromFull);
        }
        return shortDocumentIds;
    }
}
