package fr.ifremer.coselmar.services.v1;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.coselmar.beans.CloudWord;
import fr.ifremer.coselmar.exceptions.CoselmarTechnicalException;
import fr.ifremer.coselmar.persistence.entity.CoselmarUserRole;
import fr.ifremer.coselmar.services.CoselmarWebServiceSupport;
import fr.ifremer.coselmar.services.indexation.TransverseIndexationService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.queryparser.classic.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class GeneralWebService extends CoselmarWebServiceSupport {

    private static final Log log = LogFactory.getLog(GeneralWebService.class);

    protected static final List<String> RESTRICTED_ACCESS_USERS = Lists.newArrayList(CoselmarUserRole.CLIENT.name(), CoselmarUserRole.MEMBER.name());

    public List<CloudWord> getTopWords() {

        TransverseIndexationService questionsIndexationService = getServicesContext().newService(TransverseIndexationService.class);

        Map<String, Long> topTerms = null;
        try {
            topTerms = questionsIndexationService.getTopTerms();
        } catch (IOException|ParseException e) {
            if (log.isErrorEnabled()) {
                log.error("Unable to search by lucene, make search directly in database", e);
            }
            throw new CoselmarTechnicalException("Unable to get most frequecy words", e);
        }

        List<CloudWord> cloudWords = new ArrayList<>(topTerms.size());
        for (Map.Entry<String, Long> wordFrequency : topTerms.entrySet()) {
            CloudWord cloudWord = new CloudWord(wordFrequency.getKey(), wordFrequency.getValue());
            cloudWords.add(cloudWord);
        }

        return cloudWords;
    }
}
