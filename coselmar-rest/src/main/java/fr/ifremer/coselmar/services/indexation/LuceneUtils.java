package fr.ifremer.coselmar.services.indexation;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coselmar.config.CoselmarServicesConfig;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.NIOFSDirectory;
import org.apache.tika.Tika;

import java.io.File;
import java.io.IOException;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class LuceneUtils {

    private static final Log log = LogFactory.getLog(LuceneUtils.class);

    protected Analyzer analyzer;
    protected final IndexWriterConfig indexationConfig = new IndexWriterConfig(getAnalyzer());
    protected IndexWriter indexWriter;

    public static final FieldType TYPE_STORED = new FieldType();
    static {
        TYPE_STORED.setOmitNorms(true);
        TYPE_STORED.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
        TYPE_STORED.setStored(true);
        TYPE_STORED.setStoreTermVectors(true);
        TYPE_STORED.setStoreTermVectorPositions(true);
        TYPE_STORED.setStoreTermVectorOffsets(true);
        TYPE_STORED.setStoreTermVectorPayloads(true);
        TYPE_STORED.setTokenized(true);
        TYPE_STORED.freeze();
    }

    protected CoselmarServicesConfig servicesConfig;

    public LuceneUtils(CoselmarServicesConfig servicesConfig) {
        this.servicesConfig = servicesConfig;
    }

    protected Analyzer getAnalyzer() {
        if (analyzer == null) {
            analyzer = new StandardAnalyzer(EnglishAnalyzer.getDefaultStopSet());
            //Use simple analyzer to index all words and be able to search with "close word" classified in StandardAnalyzer
//            analyzer = new SimpleAnalyzer();
        }
        return analyzer;

    }

    public IndexWriter getIndexWriter() throws IOException {
        if (indexWriter == null) {
            File indexDirectory = servicesConfig.getIndexDirectory();
            Directory index = NIOFSDirectory.open(indexDirectory.toPath());

            indexationConfig.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
            indexWriter = new IndexWriter(index, indexationConfig);
        }
        return indexWriter;
    }

    public void closeWriter() {
        if (indexWriter != null) {
            try {
                indexWriter.close();
            } catch (IOException | IllegalStateException e) {
                if (log.isErrorEnabled()) {
                    log.error("Unable to close lucene index writer", e);
                }
            }
        }
    }

    public void clearIndex() throws IOException {
        getIndexWriter().deleteAll();
        getIndexWriter().commit();
    }

}
