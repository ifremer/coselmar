package fr.ifremer.coselmar.services.injector;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coselmar.services.CoselmarRestRequestContext;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.call.HttpContext;
import org.debux.webmotion.server.handler.ExecutorParametersInjectorHandler;
import org.debux.webmotion.server.mapping.Mapping;

import java.lang.reflect.Type;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class CoselmarRestRequestContextInjector implements ExecutorParametersInjectorHandler.Injector {

    @Override
    public CoselmarRestRequestContext getValue(Mapping m, Call call, String name, Class<?> type, Type generic) {

        CoselmarRestRequestContext result = null;
        if (CoselmarRestRequestContext.class.isAssignableFrom(type)) {
            HttpContext httpContext = call.getContext();

            result = CoselmarRestRequestContext.getRequestContext(httpContext);
        }

        return result;

    }
}
