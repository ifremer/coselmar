package fr.ifremer.coselmar.services;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.debux.webmotion.server.call.HttpContext;

import javax.servlet.http.HttpServletResponse;

/**
 * @author ymartel <martel@codelutin.com>
 */
public final class CoselmarRestUtil {

    public static final String HEADER_ACCESS_CONTROL_REQUEST_HEADERS = "Access-Control-Request-Headers";

    public static final String HEADER_ACCESS_CONTROL_ALLOW_HEADERS = "Access-Control-Allow-Headers";

    private CoselmarRestUtil() {
        throw new IllegalAccessError("Utility class");
    }

    public static void prepareResponse(HttpContext context) {

        HttpServletResponse response = context.getResponse();
        response.setHeader(HttpContext.HEADER_ACCESS_CONTROL_ALLOW_ORIGIN, "*");
        response.setHeader(HttpContext.HEADER_ACCESS_CONTROL_ALLOW_CREDENTIALS, "true");

    }

    public static void addOptionCorsHeaders(HttpContext context) {

        String requestHeaders = context.getHeader(HEADER_ACCESS_CONTROL_REQUEST_HEADERS);

        if (StringUtils.isNotBlank(requestHeaders)) {
            context.getResponse().addHeader(HEADER_ACCESS_CONTROL_ALLOW_HEADERS, requestHeaders);
        }
    }
}
