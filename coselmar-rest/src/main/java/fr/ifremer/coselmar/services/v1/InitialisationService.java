package fr.ifremer.coselmar.services.v1;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coselmar.persistence.entity.CoselmarUser;
import fr.ifremer.coselmar.persistence.entity.CoselmarUserRole;
import fr.ifremer.coselmar.services.CoselmarSimpleServiceSupport;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class InitialisationService extends CoselmarSimpleServiceSupport {

    /**
     * Create Two default users for the application <strong>if there is no users in application</strong>.
     * So, this operation is called in {@link fr.ifremer.coselmar.services.CoselmarApplicationContext#init()}, when application is starting.
     *
     */
    public void createDefaultUsers() {

        if (getCoselmarUserDao().count() == 0) {

            // set an admin user
            CoselmarUser admin = getCoselmarUserDao().create();
            admin.setFirstname("Admin");
            admin.setMail("admin@temporary.coselmar");
            admin.setOrganization("Coselmar");
            admin.setRole(CoselmarUserRole.ADMIN);
            admin.setActive(true);

            String salt = servicesContext.generateSalt();
            String encodedPassword = servicesContext.encodePassword(salt, "manager1234");
            admin.setPassword(encodedPassword);
            admin.setSalt(salt);

            commit();

            // Set a lambda expert
            CoselmarUser lambdaUser = getCoselmarUserDao().create();
            lambdaUser.setFirstname("Lambda");
            lambdaUser.setName("Expert");
            lambdaUser.setMail("lambda.expert@temporary.coselmar");
            lambdaUser.setRole(CoselmarUserRole.EXPERT);
            lambdaUser.setActive(true);

            String lambdaSalt = servicesContext.generateSalt();
            String lambdaPassword = servicesContext.encodePassword(lambdaSalt, "manager1234");
            lambdaUser.setPassword(lambdaPassword);
            lambdaUser.setSalt(lambdaSalt);

            // Set a default supervisor
            CoselmarUser defaultSupervisor = getCoselmarUserDao().create();
            defaultSupervisor.setFirstname("Default");
            defaultSupervisor.setName("Supervisor");
            defaultSupervisor.setMail("default.supervisor@temporary.coselmar");
            defaultSupervisor.setRole(CoselmarUserRole.SUPERVISOR);
            defaultSupervisor.setActive(true);

            String defaultSupSalt = servicesContext.generateSalt();
            String defaultSupPassword = servicesContext.encodePassword(defaultSupSalt, "manager1234");
            defaultSupervisor.setPassword(defaultSupPassword);
            defaultSupervisor.setSalt(defaultSupSalt);

            commit();

        }
    }

}
