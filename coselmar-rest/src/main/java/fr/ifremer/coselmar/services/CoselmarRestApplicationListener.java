package fr.ifremer.coselmar.services;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ifremer.coselmar.beans.DocumentBean;
import fr.ifremer.coselmar.beans.DocumentSearchBean;
import fr.ifremer.coselmar.beans.LinkBean;
import fr.ifremer.coselmar.beans.QuestionBean;
import fr.ifremer.coselmar.beans.QuestionSearchBean;
import fr.ifremer.coselmar.beans.QuestionSearchExample;
import fr.ifremer.coselmar.beans.UserBean;
import fr.ifremer.coselmar.beans.UserSearchBean;
import fr.ifremer.coselmar.converter.DateConverter;
import fr.ifremer.coselmar.converter.JsonArrayConverter;
import fr.ifremer.coselmar.converter.JsonConverter;
import fr.ifremer.coselmar.services.injector.CoselmarRestRequestContextInjector;
import fr.ifremer.coselmar.services.injector.CoselmarServicesInjector;
import org.debux.webmotion.server.WebMotionServerListener;
import org.debux.webmotion.server.call.ServerContext;
import org.debux.webmotion.server.mapping.Mapping;

import java.util.Date;
import java.util.Set;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class CoselmarRestApplicationListener implements WebMotionServerListener {

    protected static final Set<Class<?>> BEAN_TYPES = Sets.<Class<?>>newHashSet(
        DocumentBean.class,
        UserBean.class,
        QuestionBean.class,
        UserSearchBean.class,
        QuestionSearchBean.class,
        DocumentSearchBean.class,
        QuestionSearchExample.class,
        LinkBean.class
    );

    @Override
    public void onStart(Mapping mapping, ServerContext serverContext) {

        // init application context //
        CoselmarServicesApplicationContext applicationContext =
            CoselmarServicesApplicationContext.getApplicationContext();

        CoselmarServicesApplicationContext.setApplicationContext(serverContext.getServletContext(), applicationContext);

        // init converters //

        serverContext.addConverter(new DateConverter(), Date.class);

        for (Class<?> beanType : BEAN_TYPES) {

            serverContext.addConverter(JsonConverter.newConverter(beanType), beanType);

            JsonArrayConverter<?> converter = JsonArrayConverter.newConverter(beanType);
            serverContext.addConverter(converter, converter.getDefaultType());

        }

        // init injectors //
        serverContext.addInjector(new CoselmarRestRequestContextInjector());
        serverContext.addInjector(new CoselmarServicesInjector());

    }

    @Override
    public void onStop(ServerContext serverContext) {

        // Get application context
        CoselmarServicesApplicationContext applicationContext =
            CoselmarServicesApplicationContext.getApplicationContext(serverContext.getServletContext());

        // close it (and all underlined resources)
        if (applicationContext != null) {
            applicationContext.close();
        }

    }
}
