package fr.ifremer.coselmar.services.v1;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import fr.ifremer.coselmar.beans.DocumentBean;
import fr.ifremer.coselmar.beans.DocumentImportModel;
import fr.ifremer.coselmar.beans.DocumentSearchBean;
import fr.ifremer.coselmar.beans.DocumentSearchExample;
import fr.ifremer.coselmar.beans.FileInfos;
import fr.ifremer.coselmar.beans.MassiveDocumentsImportResult;
import fr.ifremer.coselmar.beans.QuestionBean;
import fr.ifremer.coselmar.beans.UserBean;
import fr.ifremer.coselmar.beans.UserWebToken;
import fr.ifremer.coselmar.converter.BeanEntityConverter;
import fr.ifremer.coselmar.exceptions.CoselmarTechnicalException;
import fr.ifremer.coselmar.persistence.entity.CoselmarUser;
import fr.ifremer.coselmar.persistence.entity.CoselmarUserGroup;
import fr.ifremer.coselmar.persistence.entity.CoselmarUserRole;
import fr.ifremer.coselmar.persistence.entity.Document;
import fr.ifremer.coselmar.persistence.entity.Privacy;
import fr.ifremer.coselmar.persistence.entity.Question;
import fr.ifremer.coselmar.services.CoselmarWebServiceSupport;
import fr.ifremer.coselmar.services.errors.InvalidCredentialException;
import fr.ifremer.coselmar.services.errors.NoResultException;
import fr.ifremer.coselmar.services.errors.UnauthorizedException;
import fr.ifremer.coselmar.services.indexation.DocumentsIndexationService;
import fr.ifremer.coselmar.services.indexation.TikaUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.lucene.queryparser.classic.ParseException;
import org.debux.webmotion.server.call.UploadFile;
import org.debux.webmotion.server.render.Render;
import org.nuiton.csv.Import;
import org.nuiton.csv.ImportRuntimeException;
import org.nuiton.topia.persistence.TopiaNoResultException;
import org.nuiton.util.DateUtil;
import org.nuiton.util.pagination.PaginationResult;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import static org.apache.commons.logging.LogFactory.getLog;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class DocumentsWebService extends CoselmarWebServiceSupport {

    private static final Log log = getLog(DocumentsWebService.class);

    public static final List<String> DOCUMENT_CREATE_ALLOWED_USER_ROLES =
        Lists.newArrayList(CoselmarUserRole.ADMIN.name(), CoselmarUserRole.SUPERVISOR.name(), CoselmarUserRole.EXPERT.name());

    /** Kind of admin role **/
    public static final List<String> DOCUMENT_SUPER_USER_ROLES =
        Lists.newArrayList(CoselmarUserRole.ADMIN.name(), CoselmarUserRole.SUPERVISOR.name());

    public static final List<String> DOCUMENT_VIEW_ALLOWED_USER_ROLES =
        Lists.newArrayList(
            CoselmarUserRole.ADMIN.name(),
            CoselmarUserRole.SUPERVISOR.name(),
            CoselmarUserRole.EXPERT.name()
        );
    protected static final String DESCRIPTION_CSV_FILE_NAME = "description.csv";

    public DocumentBean getDocument(String documentId) throws InvalidCredentialException, UnauthorizedException {

        // Check authentication
        String authorization = getAuthorizationHeader();
        CoselmarUser currentUser = checkUserAuthentication(authorization);

        // reconstitute full id
        String fullId = getDocumentFullId(documentId);

        Document document = getDocumentDao().forTopiaIdEquals(fullId).findUnique();

        // If document if public, only admin, supervisor and expert could view it
        // If document if private, only admin, supervisor and owner could view it
        if (!isAllowedToAccessDocument(currentUser, document)) {

            String message = String.format("User %s %s ('%s') try to access to document '%s'",
                currentUser.getFirstname(), currentUser.getName(), getShortIdFromFull(currentUser.getTopiaId()), documentId);
            if (log.isWarnEnabled()) {
                log.warn(message);
            }
            throw new UnauthorizedException(message);

        }

        DocumentBean documentBean = BeanEntityConverter.toBean(getPersistenceContext().getTopiaIdFactory(), document);

        // Manage related Question
        List<Question> relatedQuestions = getQuestionDao().forRelatedDocumentsContains(document).findAll();
        for (Question relatedQuestion : relatedQuestions) {
            QuestionBean questionBean = BeanEntityConverter.toLightBean(getPersistenceContext().getTopiaIdFactory(), relatedQuestion);
            documentBean.addRelatedQuestion(questionBean);
        }
        // Manage related Question
        List<Question> relatedQuestionsAsClosing = getQuestionDao().forClosingDocumentsContains(document).findAll();
        for (Question relatedQuestion : relatedQuestionsAsClosing) {
            QuestionBean questionBean = BeanEntityConverter.toLightBean(getPersistenceContext().getTopiaIdFactory(), relatedQuestion);
            documentBean.addRelatedQuestion(questionBean);
        }

        // Manage restricted access
        if (document.getPrivacy() == Privacy.RESTRICTED) {
            Set<CoselmarUserGroup> restrictedList = document.getRestrictedList();
            for (CoselmarUserGroup usersGroup : restrictedList) {
                if (StringUtils.equalsIgnoreCase(fullId, usersGroup.getName())) {
                    Set<CoselmarUser> members = usersGroup.getMembers();
                    for (CoselmarUser member : members) {
                        String userLightId = getPersistenceContext().getTopiaIdFactory().getRandomPart(member.getTopiaId());
                        UserBean userBean = BeanEntityConverter.toBean(userLightId, member);
                        documentBean.addAuthorizedUser(userBean);
                    }
                }
            }
        }


        return documentBean;
    }

    public List<DocumentBean> getDocuments(DocumentSearchBean searchBean) throws InvalidCredentialException {

        PaginationResult<DocumentBean> paginatedDocuments = getPaginatedDocuments(searchBean);

        return paginatedDocuments.getElements();
    }

    public PaginationResult<DocumentBean> getPaginatedDocuments(DocumentSearchBean searchBean) throws InvalidCredentialException {

        // Check authentication
        String authorization = getAuthorizationHeader();
        CoselmarUser currentUser = checkUserAuthentication(authorization);

        CoselmarUserRole currentUserRole = currentUser.getRole();

        DocumentSearchExample searchExample = DocumentSearchExample.newDefaultSearchExample();
        if (searchBean != null) {
            if (searchBean.getPage() != null) {
                searchExample.setPage(searchBean.getPage());
            }
            if (searchBean.getLimit() != null) {
                searchExample.setLimit(searchBean.getLimit());
            }
            searchExample.setFullTextSearch(searchBean.getFullTextSearch());

            Document example = BeanEntityConverter.fromSearchBean(searchBean);
            searchExample.setExample(example);

            searchExample.setOwnerName(searchBean.getOwnerName());

            if (searchBean.getDepositAfterDate() != null) {
                Date depositAfterDate = DateUtil.getEndOfDay(DateUtil.getYesterday(searchBean.getDepositAfterDate()));
                searchExample.setDepositAfterDate(depositAfterDate);
            }

            if (searchBean.getDepositBeforeDate() != null) {
                Date depositBeforeDate = DateUtil.getEndOfDay(searchBean.getDepositBeforeDate());
                searchExample.setDepositBeforeDate(depositBeforeDate);
            }

            if (searchBean.getPublicationAfterDate() != null) {
                Date publicationAfterDate = DateUtil.getEndOfDay(DateUtil.getYesterday(searchBean.getPublicationAfterDate()));
                searchExample.setPublicationAfterDate(publicationAfterDate);
            }

            if (searchBean.getPublicationBeforeDate() != null) {
                Date publicationBeforeDate = DateUtil.getEndOfDay(searchBean.getPublicationBeforeDate());
                searchExample.setPublicationBeforeDate(publicationBeforeDate);
            }
        }


        PaginationResult<Document> paginatedDocuments;

        // Admin and Supervisor can see all documents (public, private and restricted)
        if (Lists.newArrayList(CoselmarUserRole.ADMIN, CoselmarUserRole.SUPERVISOR).contains(currentUserRole)) {
            List<String> searchKeywords = searchBean.getFullTextSearch();
            if (searchKeywords != null && !searchKeywords.isEmpty()) {
                DocumentsIndexationService documentsIndexationService = getServicesContext().newService(DocumentsIndexationService.class);

                try {
                    List<String> documentIds = documentsIndexationService.searchDocuments(searchKeywords);
                    List<String> documentFullIds = getDocumentsFullId(documentIds);

                    Long count = getDocumentDao().forTopiaIdIn(documentFullIds).count();
                    List<Document> documentList = getDocumentDao().forTopiaIdIn(documentFullIds).find(searchExample.getPaginationParameter());

                    paginatedDocuments = PaginationResult.of(documentList, count, searchExample.getPaginationParameter());

                } catch (IOException | ParseException e) {
                    if (log.isErrorEnabled()) {
                        log.error("Unable to search by lucene, make search directly in database", e);
                    }
                    paginatedDocuments = getDocumentDao().findPaginatedContainingAllKeywords(searchKeywords, searchExample.getPaginationParameter());

                }
            } else {
                paginatedDocuments = getDocumentDao().findAllByExample(null, searchExample);
            }

        } else {
            //Other can only see public, his own private and restricted for which he is allowed
            paginatedDocuments = getDocumentDao().findAllByExample(currentUser, searchExample);
        }

        List<DocumentBean> documentBeans = new ArrayList<>(paginatedDocuments.getElements().size());

        for (Document document : paginatedDocuments.getElements()) {
            DocumentBean documentBean = BeanEntityConverter.toBean(getPersistenceContext().getTopiaIdFactory(), document);

            // Manage related Question
            long relatedQuestions = getQuestionDao().forRelatedDocumentsContains(document).count();
            // Don't forget use as closing documents
            long relatedQuestionsAsClosing = getQuestionDao().forClosingDocumentsContains(document).count();

            documentBean.setNbRelatedQuestions((int) (relatedQuestions + relatedQuestionsAsClosing));

            documentBeans.add(documentBean);
        }

        PaginationResult result = PaginationResult.of(documentBeans, paginatedDocuments.getCount(), paginatedDocuments.getCurrentPage());
        return result;
    }

    public List<String> getSearchBibliography(DocumentSearchBean searchBean) throws InvalidCredentialException {

        // Check authentication
        String authorization = getAuthorizationHeader();
        CoselmarUser currentUser = checkUserAuthentication(authorization);

        CoselmarUserRole currentUserRole = currentUser.getRole();

        DocumentSearchExample searchExample = DocumentSearchExample.newDefaultSearchExample();
        if (searchBean != null) {
            // For this search result, no pagination
            searchExample.setPage(searchExample.DEFAULT_PAGE);
            searchExample.setLimit(searchExample.DEFAULT_PAGE_SIZE);

            searchExample.setFullTextSearch(searchBean.getFullTextSearch());

            Document example = BeanEntityConverter.fromSearchBean(searchBean);
            searchExample.setExample(example);

            searchExample.setOwnerName(searchBean.getOwnerName());

            if (searchBean.getDepositAfterDate() != null) {
                Date depositAfterDate = DateUtil.getEndOfDay(DateUtil.getYesterday(searchBean.getDepositAfterDate()));
                searchExample.setDepositAfterDate(depositAfterDate);
            }

            if (searchBean.getDepositBeforeDate() != null) {
                Date depositBeforeDate = DateUtil.getEndOfDay(searchBean.getDepositBeforeDate());
                searchExample.setDepositBeforeDate(depositBeforeDate);
            }

            if (searchBean.getPublicationAfterDate() != null) {
                Date publicationAfterDate = DateUtil.getEndOfDay(DateUtil.getYesterday(searchBean.getPublicationAfterDate()));
                searchExample.setPublicationAfterDate(publicationAfterDate);
            }

            if (searchBean.getPublicationBeforeDate() != null) {
                Date publicationBeforeDate = DateUtil.getEndOfDay(searchBean.getPublicationBeforeDate());
                searchExample.setPublicationBeforeDate(publicationBeforeDate);
            }
        }


        List<String> citations;
        // Admin and Supervisor can see all documents (public, private and restricted)
        if (Lists.newArrayList(CoselmarUserRole.ADMIN, CoselmarUserRole.SUPERVISOR).contains(currentUserRole)) {
            List<String> searchKeywords = searchBean.getFullTextSearch();
            if (searchKeywords != null && !searchKeywords.isEmpty()) {
                DocumentsIndexationService documentsIndexationService = getServicesContext().newService(DocumentsIndexationService.class);

                try {
                    List<String> documentIds = documentsIndexationService.searchDocuments(searchKeywords);
                    List<String> documentFullIds = getDocumentsFullId(documentIds);

                    citations = getDocumentDao().findCitationsByDocumentIds(documentFullIds);

                } catch (IOException | ParseException e) {
                    if (log.isErrorEnabled()) {
                        log.error("Unable to search by lucene, make search directly in database", e);
                    }
                    citations = getDocumentDao().findCitationsByDocumentFromKeywords(searchKeywords);

                }
            } else {
                citations = getDocumentDao().findCitationsByDocumentExample(null, searchExample);
            }

        } else {
            //Other can only see public, his own private and restricted for which he is allowed
            citations = getDocumentDao().findCitationsByDocumentExample(currentUser, searchExample);
        }

        Iterables.removeIf(citations, new Predicate<String>() {
            @Override
            public boolean apply(String input) {
                return StringUtils.isBlank(input);
            }
        });

        return citations;
    }

    public DocumentBean addDocument(DocumentBean document, UploadFile uploadFile) throws InvalidCredentialException, UnauthorizedException {

        // Check authentication
        String authorization = getAuthorizationHeader();
        UserWebToken userWebToken = checkAuthentication(authorization);

        // Only Expert or Supervisor can add document
        String userRole = userWebToken.getRole();
        if (!DOCUMENT_CREATE_ALLOWED_USER_ROLES.contains(userRole.toUpperCase())) {
            String message = String.format("User %s %s ('%s') is not allowed to add document",
                userWebToken.getFirstName(), userWebToken.getLastName(), userWebToken.getUserId());
            if (log.isWarnEnabled()) {
                log.warn(message);
            }
            throw new UnauthorizedException(message);
        }

        Preconditions.checkNotNull(document);

        // retrieve user who will be assigned as document owner
        String fullId = getFullUserIdFromShort(userWebToken.getUserId());

        CoselmarUser owner;
        try {
            owner = getCoselmarUserDao().forTopiaIdEquals(fullId).findUnique();
        } catch (TopiaNoResultException tnre) {
            // Should not happened, cause user are not really deleted
            String message = String.format("Seems that logged user ('%s') does not exist anymore.", fullId);
            if (log.isErrorEnabled()) {
                log.error(message, tnre);
            }
            throw new InvalidCredentialException(message);
        }

        FileInfos fileInfos = null;
        if (uploadFile != null) {
            fileInfos = manageDocumentFile(uploadFile, owner);
        }
        DocumentBean result = createDocument(document, fileInfos, owner);

        commit();

        return result;

    }

    public void addDocumentFile(String documentId, UploadFile uploadFile) throws InvalidCredentialException, UnauthorizedException {


        Preconditions.checkNotNull(documentId);
        Preconditions.checkNotNull(uploadFile);

        // Check authentication
        String authorization = getAuthorizationHeader();
        CoselmarUser currentUser = checkUserAuthentication(authorization);

        String documentFullId = getFullIdFromShort(Document.class, documentId);
        Document documentEntity = getDocumentDao().forTopiaIdEquals(documentFullId).findAny();

        // Only Owner Expert or Supervisor/Admin can add document file
        if (!DOCUMENT_SUPER_USER_ROLES.contains(currentUser.getRole().name())
            && documentEntity.getOwner() != currentUser) {
            String message = String.format("User %s %s ('%s') is not allowed to add document file",
                currentUser.getFirstname(), currentUser.getName(), getShortIdFromFull(currentUser.getTopiaId()));
            if (log.isWarnEnabled()) {
                log.warn(message);
            }
            throw new UnauthorizedException(message);
        }

        // Get owner to place correctly the file
        CoselmarUser owner = documentEntity.getOwner();
        FileInfos fileInfos = manageDocumentFile(uploadFile, owner);
        String filePath = fileInfos.getFinalFilePath();
        String contentType = fileInfos.getMimeType();
        // Read file content
        String fileContent = TikaUtils.getFileContent(filePath);

        // If document has already a file, remove it
        if (StringUtils.isNotBlank(documentEntity.getFilePath())) {
            File documentFile = new File(documentEntity.getFilePath());
            FileUtils.deleteQuietly(documentFile);
        }

        documentEntity.setWithFile(true);
        documentEntity.setMimeType(contentType);
        documentEntity.setFilePath(filePath);
        documentEntity.setFileName(uploadFile.getName());
        documentEntity.setFileContent(fileContent);

        // Should update document index information to put the file
        DocumentBean documentBean = BeanEntityConverter.toBean(getPersistenceContext().getTopiaIdFactory(), documentEntity);

        DocumentsIndexationService documentsIndexationService = getServicesContext().newService(DocumentsIndexationService.class);
        try {
            documentsIndexationService.updateDocument(documentBean, fileContent);
            if (log.isDebugEnabled()) {
                String message = String.format("Document '%s' was updated in index", documentEntity.getName());
                log.debug(message);
            }
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Unable to update document index information", e);
            }
        }

        commit();

    }


    public Render getDocumentFile(String documentId) throws NoResultException {

        // reconstitute full id
        String fullId =getDocumentFullId(documentId);

        Document document = getDocumentDao().forTopiaIdEquals(fullId).findUnique();
        //TODO ymartel 20141103 : manage file owner ?

        // Get file attached to document
        String filePath = document.getFilePath();
        if (StringUtils.isBlank(filePath)) {
            throw new NoResultException("No File");
        }
        File documentFile = new File(filePath);

        String fileName = StringUtils.isNotBlank(document.getFileName()) ? document.getFileName() : document.getName();
        String fileMimeType = document.getMimeType();
        try {
            InputStream fileStream = new FileInputStream(documentFile);
            return renderDownload(fileStream, fileName, fileMimeType);

        } catch (FileNotFoundException e) {
            if (log.isErrorEnabled()) {
                String message = String.format("Unable to retrieve file %s", fileName);
                log.error(message, e);
            }
            throw new NoResultException("File does not exist");
        }

    }

    public void saveDocument(DocumentBean document) throws InvalidCredentialException, UnauthorizedException {
        Preconditions.checkNotNull(document);
        Preconditions.checkNotNull(document.getId());

        // Check authentication
        String authorization = getAuthorizationHeader();
        CoselmarUser currentUser = checkUserAuthentication(authorization);

        String documentId = getDocumentFullId(document.getId());

        Document documentEntity = getDocumentDao().forTopiaIdEquals(documentId).findUnique();

        if (!isAllowedToAccessDocument(currentUser, documentEntity)) {

            String message = String.format("User %s %s ('%s') try to modify document '%s'",
                currentUser.getFirstname(), currentUser.getName(), getShortIdFromFull(currentUser.getTopiaId()), documentId);
            if (log.isWarnEnabled()) {
                log.warn(message);
            }
            throw new UnauthorizedException(message);

        }

        boolean isUsedByQuestions = getQuestionDao().forRelatedDocumentsContains(documentEntity).exists();
        if (isUsedByQuestions && currentUser.getRole() != CoselmarUserRole.ADMIN) {
            String message = "Document is used by some questions, cannot be modified.";
            if (log.isWarnEnabled()) {
                log.warn(message);
            }
            throw new UnauthorizedException(message);
        }

        documentEntity.setName(document.getName());
        String oldPrivacy = documentEntity.getPrivacy().name();
        String newPrivacy = document.getPrivacy().toUpperCase();
        documentEntity.setPrivacy(Privacy.valueOf(newPrivacy));

        // Manage privacy : if restricted, create an UserGroup with authorized users
        if (StringUtils.equals(Privacy.RESTRICTED.name(), newPrivacy)) {
            Set<UserBean> authorizedUsers = document.getAuthorizedUsers();
            Set<CoselmarUser> coselmarUsers = retrieveUsers(authorizedUsers);

            Set<CoselmarUserGroup> restrictedList = documentEntity.getRestrictedList();
            CoselmarUserGroup restrictedUsers = null;
            if (restrictedList != null && !restrictedList.isEmpty()) {

                // Try to retrieve the UserGroup with documentId as name (corresponding to user choice)
                for (CoselmarUserGroup usersList : restrictedList) {
                    if (StringUtils.equalsIgnoreCase(documentId, usersList.getName())) {
                        restrictedUsers = usersList;
                        restrictedUsers.clearMembers();
                        break;
                    }
                }
            }
            if (restrictedUsers == null) {
                restrictedUsers = getCoselmarUserGroupDao().create();
                restrictedUsers.setName(documentEntity.getTopiaId());
            }

            restrictedUsers.addAllMembers(coselmarUsers);
            documentEntity.addRestrictedList(restrictedUsers);

        } else if (StringUtils.equals(Privacy.RESTRICTED.name(), oldPrivacy)) {
            // retrieve restrictive users list and remove it
            CoselmarUserGroup restrictedUsers = null;
            for (CoselmarUserGroup usersList : documentEntity.getRestrictedList()) {
                if (StringUtils.equalsIgnoreCase(documentId, usersList.getName())) {
                    restrictedUsers = usersList;
                    break;
                }
            }

            // Remove group from document, and delete it !
            if (restrictedUsers != null) {
                documentEntity.removeRestrictedList(restrictedUsers);
                getCoselmarUserGroupDao().delete(restrictedUsers);
            }
        }

        documentEntity.clearKeywords();
        documentEntity.addAllKeywords(document.getKeywords());

        documentEntity.setType(document.getType());
        documentEntity.setSummary(document.getSummary());
        documentEntity.setLanguage(document.getLanguage());
        Date publicationDate = document.getPublicationDate();
        if (publicationDate != null) {
            documentEntity.setPublicationDate(new Date(publicationDate.getTime()));
        } else {
            documentEntity.setPublicationDate(null);
        }

        // Legal / copyright part
        documentEntity.setAuthors(document.getAuthors());
        documentEntity.setCopyright(document.getCopyright());
        documentEntity.setLicense(document.getLicense());

        // Resource part
        documentEntity.setExternalUrl(document.getExternalUrl());
        // If had file info and now no file info : remove file
        boolean hadFile = documentEntity.isWithFile();
        if (hadFile && StringUtils.isBlank(document.getFileName())) {
            deleteAttachedFile(documentEntity);
        }

        documentEntity.setComment(document.getComment());

        documentEntity.setCitation(document.getCitation());

        commit();

        // Update index information for this document
        DocumentBean result = BeanEntityConverter.toBean(getPersistenceContext().getTopiaIdFactory(), documentEntity);

        DocumentsIndexationService documentsIndexationService = getServicesContext().newService(DocumentsIndexationService.class);
        try {
            documentsIndexationService.indexDocument(result,  null); // no document file for the moment here
            if (log.isDebugEnabled()) {
                String message = String.format("Document '%s' was updated in index", document.getName());
                log.debug(message);
            }
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Unable to update document index information", e);
            }
        }

    }

    public void deleteDocument(String documentId) throws InvalidCredentialException, UnauthorizedException {

        // Check authentication
        String authorization = getAuthorizationHeader();
        CoselmarUser currentUser = checkUserAuthentication(authorization);

        // reconstitute full id
        String fullId = getDocumentFullId(documentId);

        Document document = getDocumentDao().forTopiaIdEquals(fullId).findUnique();

        if (!DOCUMENT_SUPER_USER_ROLES.contains(currentUser.getRole().name())
            && document.getOwner() != currentUser) {

            String message = String.format("User %s %s ('%s') try to delete document '%s'",
                currentUser.getFirstname(), currentUser.getName(), getShortIdFromFull(currentUser.getTopiaId()), documentId);
            if (log.isWarnEnabled()) {
                log.warn(message);
            }
            throw new UnauthorizedException(message);

        }

        // Delete physical file
        deleteAttachedFile(document);

        getDocumentDao().delete(document);

        commit();

        //Remove index entry
        DocumentsIndexationService documentsIndexationService = getServicesContext().newService(DocumentsIndexationService.class);
        try {
            documentsIndexationService.deleteDocument(documentId);
            if (log.isDebugEnabled()) {
                String message = String.format("Document '%s' removed from index", documentId);
                log.debug(message);
            }
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Unable to remove document entry from index", e);
            }
        }
    }

    public List<String> getKeywords() throws InvalidCredentialException, UnauthorizedException {

        List<String> themes = getDocumentDao().findAllKeywords();

        return themes;
    }

    public List<String> getTypes() throws InvalidCredentialException, UnauthorizedException {

        List<String> types = getDocumentDao().findAllTypes();

        return types;
    }

    public MassiveDocumentsImportResult uploadZipDocuments(UploadFile uploadFile) throws InvalidCredentialException, UnauthorizedException {

        Preconditions.checkNotNull(uploadFile);

        // Check authentication
        String authorization = getAuthorizationHeader();
        CoselmarUser currentUser = checkUserAuthentication(authorization);

        // Only Supervisor/Admin can add Zip documents
        if (!DOCUMENT_SUPER_USER_ROLES.contains(currentUser.getRole().name())) {
            String message = String.format("User %s %s ('%s') is not allowed to upload mass document files",
                currentUser.getFirstname(), currentUser.getName(), getShortIdFromFull(currentUser.getTopiaId()));
            if (log.isWarnEnabled()) {
                log.warn(message);
            }
            throw new UnauthorizedException(message);
        }
        return importFromZip(uploadFile.getFile(), currentUser);

    }

    ////////////////////////////////////////////////////////////////////////////
    ///////////////////////     Internal Parts     /////////////////////////////
    ////////////////////////////////////////////////////////////////////////////

    /**
     * From a {@link DocumentBean}, create a {@link Document}.
     * If there is a {@link FileInfos}, get the file data.
     *
     * @return a DocumentBean with all created {@link Document} data.
     */
    protected DocumentBean createDocument(DocumentBean documentBean, FileInfos fileInfos, CoselmarUser owner) {

        // Document Metadata
        Document documentEntity = createDocumentMetadataFromBean(documentBean, owner);

        // If document has a file, manage it !
        String fileContent = "";

        if (fileInfos != null) {
            String filePath = fileInfos.getFinalFilePath();
            String contentType = fileInfos.getMimeType();
            if (StringUtils.isNotBlank(fileInfos.getActualFilePath())) {
                fileContent = TikaUtils.getFileContent(fileInfos.getActualFilePath());
            } else {
                fileContent = TikaUtils.getFileContent(filePath);
            }

            documentEntity.setWithFile(true);
            documentEntity.setMimeType(contentType);
            documentEntity.setFilePath(filePath);
            documentEntity.setFileContent(fileContent);
        } else {
            documentEntity.setWithFile(false);
        }

        DocumentBean result = BeanEntityConverter.toBean(getPersistenceContext().getTopiaIdFactory(), documentEntity);

        // Indexation job
        DocumentsIndexationService documentsIndexationService = getServicesContext().newService(DocumentsIndexationService.class);
        try {
            documentsIndexationService.indexDocument(result, fileContent);
            if (log.isDebugEnabled()) {
                String message = String.format("Document '%s' added to index", documentBean.getName());
                log.debug(message);
            }
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Unable to index new document", e);
            }
        }
        return result;
    }

    /**
     * Convert metadata fields from {@link DocumentBean} into a {@link Document} and put default values where no given (such as {@link Document#PROPERTY_DEPOSIT_DATE} for example).
     * This also manage owner and restriction but <strong>does not manage</strong> attached file part.
     * No commit are done here.
     */
    public Document createDocumentMetadataFromBean(DocumentBean documentBean, CoselmarUser owner) {
        Preconditions.checkNotNull(documentBean);

        // Document Metadata
        Document document = getDocumentDao().create();

        document.setOwner(owner);

        document.setName(documentBean.getName());

        String privacy = documentBean.getPrivacy().toUpperCase();
        document.setPrivacy(Privacy.valueOf(privacy));

        // Manage privacy : if restricted, create an UserGroup with authorized users
        if (StringUtils.equals(Privacy.RESTRICTED.name(), privacy)) {
            Set<UserBean> authorizedUsers = documentBean.getAuthorizedUsers();
            Set<CoselmarUser> coselmarUsers = retrieveUsers(authorizedUsers);
            CoselmarUserGroup restrictedUsers = getCoselmarUserGroupDao().create();
            restrictedUsers.setName(document.getTopiaId());

            restrictedUsers.addAllMembers(coselmarUsers);
            document.addRestrictedList(restrictedUsers);
        }

        document.addAllKeywords(documentBean.getKeywords());

        Date depositDate = documentBean.getDepositDate();
        if (depositDate != null) {
            document.setDepositDate(new Date(depositDate.getTime()));
        } else {
            document.setDepositDate(new Date());
        }

        document.setType(documentBean.getType());
        document.setSummary(documentBean.getSummary());
        document.setLanguage(documentBean.getLanguage());
        Date publicationDate = documentBean.getPublicationDate();
        if (publicationDate != null) {
            document.setPublicationDate(new Date(publicationDate.getTime()));
        }

        document.setCitation(documentBean.getCitation());
        document.setComment(documentBean.getComment());

        // Legal / copyright part
        document.setAuthors(documentBean.getAuthors());
        document.setCopyright(documentBean.getCopyright());
        document.setLicense(documentBean.getLicense());

        document.setExternalUrl(documentBean.getExternalUrl());

        return document;
    }

    /**
     * When a Document is sent, it could have a File part : this manage the
     * upload file. The file is stored in the user specific directory, and the
     * contentType of document is returned, cause need in Document Metadata.
     *
     * @return the upload file Metadata
     */
    protected FileInfos manageDocumentFile(UploadFile uploadFile, CoselmarUser owner) {
        Preconditions.checkNotNull(uploadFile);

        // Document File
        String fileName = uploadFile.getName();
        File uploadedFile = uploadFile.getFile();
        String contentType = uploadFile.getContentType();

        if (log.isDebugEnabled()) {
            String message = String.format("File name : %s, content-type : %s", fileName, contentType);
            log.debug(message);
        }

        // put the document in the good directory
        String filePath = getDocumentFileDestPath(owner, fileName);

        File destFile = new File(filePath);
        try {
            FileUtils.moveFile(uploadedFile, destFile);
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("error during File transfer", e);
            }
            throw new CoselmarTechnicalException("Internal error during file transfer", e);
        }

        FileInfos fileInfos = new FileInfos();
        fileInfos.setFileName(fileName);
        fileInfos.setMimeType(contentType);
        fileInfos.setFinalFilePath(destFile.getAbsolutePath());
        return fileInfos;
    }

    protected String getDocumentFileDestPath(CoselmarUser owner, String fileName) {
        String userPath = getUserDocumentPath(owner);

        String storageFileName = getFileStorageName(fileName);
        return userPath + File.separator + storageFileName;
    }

    protected String getFileStorageName(String fileName) {
        Date now = getNow();
        String formattedDay = DateUtil.formatDate(now, "yyyyMMddHHmm");
        String prefix = formattedDay + "-";
        return prefix + fileName;
    }

    protected String getUserDocumentPath(CoselmarUser user) {
        File dataDirectory = getCoselmarServicesConfig().getDataDirectory();
        String absolutePath = dataDirectory.getAbsolutePath();
        String userFolder = StringUtils.replaceChars(user.getFirstname() + "-" + user.getName(), " ", "_");
        String userPath = absolutePath + File.separator + userFolder;
        return userPath;
    }

    /**
     * Check if an user can access to a document metadata, according to its
     * privacy settings and the user grant.
     *
     * @param user      :   Current User,
     *                      containing {@link fr.ifremer.coselmar.persistence.entity.CoselmarUserRole} as String
     * @param document  :   the document user trying to access.
     *
     * @return true is user can access, false else.
     */
    protected boolean isAllowedToAccessDocument(CoselmarUser user, Document document) {
        boolean isAuthorized = false;

        String viewerRole = user.getRole().name().toUpperCase();

        if (DOCUMENT_SUPER_USER_ROLES.contains(viewerRole) || document.getOwner() == user) {
            return true;
        }

        // For public : only admin/supervisor/expert can access
        if (document.getPrivacy() == Privacy.PUBLIC) {
            isAuthorized = DOCUMENT_VIEW_ALLOWED_USER_ROLES.contains(viewerRole);

        // For Private : only admin/supervisor/owner can access
        } else if (document.getPrivacy() == Privacy.PRIVATE) {
            CoselmarUser documentOwner = document.getOwner();
            boolean isOwner = StringUtils.equals(documentOwner.getTopiaId(), user.getTopiaId());
            isAuthorized = isOwner || Lists.newArrayList(CoselmarUserRole.ADMIN.name(), CoselmarUserRole.SUPERVISOR.name()).contains(viewerRole);

        } else if (document.getPrivacy() == Privacy.RESTRICTED) {
            Set<CoselmarUserGroup> restrictedLists = document.getRestrictedList();
            for (CoselmarUserGroup restrictedList : restrictedLists) {
                if (restrictedList.containsMembers(user)) {
                    isAuthorized = true;
                    break;
                }
            }
        }

        return isAuthorized;
    }

    protected String getDocumentFullId(String documentId) {
        return Document.class.getCanonicalName() + getPersistenceContext().getTopiaIdFactory().getSeparator() + documentId;
    }

    protected List<String> getDocumentsFullId(List<String> documentShortIds) {

        Function<String, String> getFullIds = new Function<String, String>() {
            @Override
            public String apply(String shortId) {
                return getDocumentFullId(shortId);
            }
        };

        List<String> fullIds = Lists.transform(documentShortIds, getFullIds);
        return fullIds;
    }

    /**
     * Delete physically Document File, and update Document entity setting
     * filePath and fileName to null and withFile boolean to false
     */
    protected void deleteAttachedFile(Document document) {
        if (StringUtils.isNotBlank(document.getFilePath())) {
            File documentFile = new File(document.getFilePath());
            FileUtils.deleteQuietly(documentFile);

            document.setFilePath(null);
            document.setFileName(null);
            document.setWithFile(false);
        }
    }

    protected Set<CoselmarUser> retrieveUsers(Collection<UserBean> userBeans) {
        Function<UserBean, String> getIds = new Function<UserBean, String>() {
            @Override
            public String apply(UserBean userBean) {
                return getFullIdFromShort(CoselmarUser.class, userBean.getId());
            }
        };

        Collection<String> userIds = Collections2.transform(userBeans, getIds);
        List<CoselmarUser> coselmarUsers = getCoselmarUserDao().forTopiaIdIn(userIds).findAll();
        return new HashSet<>(coselmarUsers);

    }

    protected MassiveDocumentsImportResult importFromZip(File file, CoselmarUser currentUser) {
        MassiveDocumentsImportResult importResult = new MassiveDocumentsImportResult();
        // File should be a Zip
        ZipFile zipFile;
        try {
            zipFile = new ZipFile(file);
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("error during ZipFile transfer", e);
            }
            importResult.setSystemErrorType(MassiveDocumentsImportResult.ImportSystemErrorType.UNABLE_TO_READ_ZIP);
            return importResult;
        }

        InputStream descriptionInputStream = null;
        try {
            // Get descriptions.csv : it should contains all DocumentBean information
            ZipEntry descriptionEntry = zipFile.getEntry(DESCRIPTION_CSV_FILE_NAME);
            if (descriptionEntry == null) {
                importResult.setSystemErrorType(MassiveDocumentsImportResult.ImportSystemErrorType.UNABLE_TO_FIND_CSV);
                return importResult;
            }

            try {
                descriptionInputStream = zipFile.getInputStream(descriptionEntry);
            } catch (IOException e) {
                String message = String.format("Unable to read '%s' from zip file", DESCRIPTION_CSV_FILE_NAME);
                if (log.isErrorEnabled()) {
                    log.error(message, e);
                }
                importResult.setSystemErrorType(MassiveDocumentsImportResult.ImportSystemErrorType.UNABLE_TO_FIND_CSV);
                return importResult; // Direct break : cannot continue without this file
            }

            // Now, read CSV ...
            DocumentImportModel csvModel = new DocumentImportModel();
            Import<DocumentBean> importer = Import.newImport(csvModel, descriptionInputStream);

            File dataDirectory = getCoselmarServicesConfig().getDataDirectory();
            String dataPath = dataDirectory.getAbsolutePath();
            String zipTempPath = dataPath + File.separator + DateUtil.formatDate(getNow(), "yyyyMMddHHmm");
            Path dir = Paths.get(zipTempPath);
            try {
                Files.createDirectories(dir);
            } catch (IOException e) {
                if (log.isErrorEnabled()) {
                    String message = "Unable to create temp path for zip import";
                    log.error(message, e);
                }
                throw new CoselmarTechnicalException("Unable to unzip file : error with unzip directory", e);
            }

            // ... and read each entries and retrieve associated File
            try {
                for (DocumentBean documentBean : importer) {
                    // cf https://forge.codelutin.com/issues/9205?issue_count=18&issue_position=4&next_issue_id=9198&prev_issue_id=9206#note-2
                    documentBean.setPrivacy(Privacy.PUBLIC.name());
                    String externalUrl = documentBean.getExternalUrl();
                    String fileName = StringUtils.strip(documentBean.getFileName());

                    FileInfos fileInfos = null;
                    boolean documentOk = true;
                    // We must have at least an external URL or a file with the document
                    if (StringUtils.isBlank(externalUrl) && StringUtils.isBlank(fileName)) {
                        importResult.addDocumentWithoutAttachment(documentBean.getName());

                    } else if (StringUtils.isNotBlank(fileName)) {
                        fileInfos = new FileInfos();

                        ZipEntry zipFileEntry = zipFile.getEntry(fileName);
                        if (zipFileEntry == null) {
                            importResult.addMissingFile(fileName);
                            documentOk = false;

                        } else {
                            try {
                                InputStream zipFileInputStream = zipFile.getInputStream(zipFileEntry);
                                String fileMimeType = TikaUtils.getFileMimeType(fileName);
                                String futureFilePath = getDocumentFileDestPath(currentUser, fileName);

                                String storageFileName = getFileStorageName(fileName);

                                // Push file stream in temporary folder
                                File zipEntryFile = new File(zipTempPath + File.separator + storageFileName);
                                zipEntryFile.createNewFile();
                                Files.copy(zipFileInputStream, zipEntryFile.toPath(), StandardCopyOption.REPLACE_EXISTING);

                                // Create fileInfos
                                fileInfos.setFileName(fileName);
                                fileInfos.setFinalFilePath(futureFilePath);
                                fileInfos.setActualFilePath(zipEntryFile.getAbsolutePath());
                                fileInfos.setMimeType(fileMimeType);

                                try {
                                    zipFileInputStream.close();
                                } catch (IOException e) {
                                    if (log.isErrorEnabled()) {
                                        log.error("Unable to close stream from ZipEntry : " + fileName, e);
                                    }
                                }
                            } catch (IOException e) {
                                String message = String.format("Unable to retrieve '%s' from zip file", fileName);
                                if (log.isErrorEnabled()) {
                                    log.error(message, e);
                                }
                                importResult.setSystemErrorType(MassiveDocumentsImportResult.ImportSystemErrorType.UNABLE_TO_READ_ZIP_ENTRY);
                                importResult.addMissingFile(fileName);
                                documentOk = false;
                            }
                        }
                    }

                    // Create document
                    if (documentOk) {
                        createDocument(documentBean, fileInfos, currentUser);
                    }

                }
            } catch (ImportRuntimeException ire) {
                if (log.isErrorEnabled()) {
                    log.error("Error with CSV file", ire);
                }
                importResult.setSystemErrorType(MassiveDocumentsImportResult.ImportSystemErrorType.UNABLE_TO_READ_CSV);
                return importResult;
            } finally {
                // Close importer
                importer.close();
            }

            if (importResult.isSuccess()) {
                // All is ok !
                try {
                    // Ok, let move all files from temp storage to real one !
                    File tmpDir = new File(zipTempPath);
                    FileUtils.copyDirectory(tmpDir, new File(getUserDocumentPath(currentUser)));
                    FileUtils.deleteDirectory(tmpDir);
                    commit();
                } catch (IOException e) {
                    // Big problem if we can't move files into real folder !
                    String message = String.format("Unable to move files from temp folder '%s' to final folder", zipTempPath);
                    if (log.isErrorEnabled()) {
                        log.error(message, e);
                    }
                    refreshDocumentsIndex();
                    throw new CoselmarTechnicalException(message, e);
                }

            } else {
                // Something wrong happened... rollback, and refresh lucene to avoid new data
                rollback();
                refreshDocumentsIndex();
            }

        } finally {
            // Be sure we close description stream
            IOUtils.closeQuietly(descriptionInputStream);
            // and zip file
            IOUtils.closeQuietly(zipFile);
        }

        return importResult;
    }

    protected void refreshDocumentsIndex() {
        AdminWebService adminWebService = getServicesContext().newService(AdminWebService.class);
        try {
            adminWebService.refreshDocumentsIndex();
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Unable to refresh Lucene Documents index. Data should be corrupted", e);
            }
        }
    }
}
