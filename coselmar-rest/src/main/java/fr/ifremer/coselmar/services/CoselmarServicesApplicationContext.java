package fr.ifremer.coselmar.services;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.coselmar.config.CoselmarServicesConfig;
import fr.ifremer.coselmar.persistence.CoselmarPersistenceContext;
import fr.ifremer.coselmar.persistence.CoselmarTopiaApplicationContext;
import fr.ifremer.coselmar.persistence.CoselmarTopiaPersistenceContext;
import fr.ifremer.coselmar.services.indexation.LuceneUtils;
import fr.ifremer.coselmar.services.v1.InitialisationService;
import org.apache.commons.logging.Log;
import org.apache.log4j.LogManager;
import org.apache.log4j.PropertyConfigurator;
import org.nuiton.i18n.I18n;
import org.nuiton.i18n.init.DefaultI18nInitializer;
import org.nuiton.i18n.init.I18nInitializer;

import javax.servlet.ServletContext;
import java.io.File;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.apache.commons.logging.LogFactory.getLog;


/**
 * @author ymartel <martel@codelutin.com>
 */
public class CoselmarServicesApplicationContext implements CoselmarApplicationContext {

    private static Log log = getLog(CoselmarServicesApplicationContext.class);

    protected static final String APPLICATION_CONTEXT_PARAMETER = "coselmar_CoselmarApplicationContext";

    protected static CoselmarServicesApplicationContext applicationContext;

    public static CoselmarServicesApplicationContext getApplicationContext() {

        if (applicationContext == null) {

            CoselmarServicesConfig applicationConfig = new CoselmarServicesConfig("coselmar-services.properties");

            Map<String, String> topiaProperties = applicationConfig.getTopiaProperties();

            CoselmarTopiaApplicationContext coselmarTopiaApplicationContext = new CoselmarTopiaApplicationContext(topiaProperties);

            LuceneUtils luceneUtils = new LuceneUtils(applicationConfig);

            applicationContext = new CoselmarServicesApplicationContext(applicationConfig, coselmarTopiaApplicationContext, luceneUtils);

            applicationContext.init();
        }
        return applicationContext;
    }

    public static CoselmarServicesApplicationContext getApplicationContext(ServletContext servletContext) {
        CoselmarServicesApplicationContext result =
            (CoselmarServicesApplicationContext) servletContext.getAttribute(APPLICATION_CONTEXT_PARAMETER);
        return result;
    }

    public static void setApplicationContext(CoselmarServicesApplicationContext applicationContext) {
        CoselmarServicesApplicationContext.applicationContext = applicationContext;
    }

    public static void setApplicationContext(ServletContext servletContext,
                                             CoselmarServicesApplicationContext applicationContext) {
        servletContext.setAttribute(APPLICATION_CONTEXT_PARAMETER, applicationContext);
    }

    protected AtomicBoolean started;

    protected AtomicBoolean closed;

    protected CoselmarTopiaApplicationContext topiaApplicationContext;

    protected CoselmarServicesConfig applicationConfig;

    protected LuceneUtils luceneUtils;

    protected CoselmarServicesApplicationContext(CoselmarServicesConfig applicationConfig,
                                                 CoselmarTopiaApplicationContext topiaApplicationContext,
                                                 LuceneUtils luceneUtils) {

        Preconditions.checkNotNull(applicationConfig, "Configuration can not be null!");
        Preconditions.checkNotNull(topiaApplicationContext, "topiaApplicationContext can not be null!");

        this.applicationConfig = applicationConfig;
        this.topiaApplicationContext = topiaApplicationContext;
        this.luceneUtils = luceneUtils;
        this.started = new AtomicBoolean(false);
        this.closed = new AtomicBoolean(false);
    }

    @Override
    public CoselmarTopiaApplicationContext getTopiaApplicationContext() {
        return this.topiaApplicationContext;
    }

    @Override
    public CoselmarServicesConfig getApplicationConfig() {
        return this.applicationConfig;
    }

    @Override
    public CoselmarTopiaPersistenceContext newPersistenceContext() {

        CoselmarTopiaPersistenceContext persistenceContext = topiaApplicationContext.newPersistenceContext();
        return persistenceContext;
    }

    @Override
    public LuceneUtils getLuceneUtils() {
        return this.luceneUtils;
    }

    @Override
    public CoselmarServicesContext newServiceContext(CoselmarPersistenceContext persistenceContext, Locale locale) {

        DefaultCoselmarServicesContext newServiceContext = new DefaultCoselmarServicesContext();
        newServiceContext.setCoselmarServicesConfig(applicationConfig);
        newServiceContext.setLuceneUtils(luceneUtils);
        newServiceContext.setTopiaApplicationContext(topiaApplicationContext);
        newServiceContext.setPersistenceContext(persistenceContext);
        newServiceContext.setLocale(locale);
        return newServiceContext;
    }

    @Override
    public void close() {

        if (closed.get()) {

            if (log.isWarnEnabled()) {
                log.warn("Already closed");
            }
            return;
        }

        if (!started.get()) {

            if (log.isWarnEnabled()) {
                log.warn("Not started");
            }
            return;
        }

        if (topiaApplicationContext != null && !topiaApplicationContext.isClosed()) {

            if (log.isInfoEnabled()) {
                log.info("stopping Coselmar, will close persistence context");
            }
            topiaApplicationContext.close();
        }

        if (luceneUtils != null ) {

            if (log.isInfoEnabled()) {
                log.info("Close Lucene Writer");
            }
            luceneUtils.closeWriter();
        }

        closed.set(true);
        started.set(false);
    }

    @Override
    public void init() {

        if (started.get()) {

            if (log.isWarnEnabled()) {
                log.warn("Already started!");
            }
            return;
        }

        Preconditions.checkState(applicationConfig != null, "No configuration initialized!");
        Preconditions.checkState(topiaApplicationContext != null, "No topiaApplicationContext initialized!");
        Preconditions.checkState(luceneUtils != null, "No LuceneUtils initialized!");

        if (applicationConfig.isLogConfigurationProvided()) {

            File log4jConfigurationFile = applicationConfig.getLogConfigurationFile();

            String log4jConfigurationFileAbsolutePath = log4jConfigurationFile.getAbsolutePath();

            if (log4jConfigurationFile.exists()) {

                if (log.isInfoEnabled()) {
                    log.info("will use logging configuration " + log4jConfigurationFileAbsolutePath);
                }

                // reset logger configuration
                LogManager.resetConfiguration();

                // use generate log config file
                PropertyConfigurator.configure(log4jConfigurationFileAbsolutePath);

                log = getLog(CoselmarServicesApplicationContext.class);

            } else {
                if (log.isWarnEnabled()) {
                    log.warn("there is no file " + log4jConfigurationFileAbsolutePath + ". Default logging configuration will be used.");
                }
            }

        } else {
            log.info("will use default logging configuration");
        }

        I18nInitializer initializer = new DefaultI18nInitializer("coselmar-i18n");
        // to show none translated sentences
        initializer.setMissingKeyReturnNull(true);

        I18n.init(initializer, Locale.FRANCE);

        {//Init some users
            CoselmarTopiaPersistenceContext persistenceContext = newPersistenceContext();
            CoselmarServicesContext serviceContext = newServiceContext(persistenceContext, Locale.FRANCE);
            serviceContext.newService(InitialisationService.class).createDefaultUsers();
            persistenceContext.close();
        }

        started.set(true);
    }

}
