package fr.ifremer.coselmar.services.filter;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coselmar.persistence.CoselmarPersistenceContext;
import fr.ifremer.coselmar.services.CoselmarRestRequestContext;
import fr.ifremer.coselmar.services.CoselmarRestUtil;
import fr.ifremer.coselmar.services.CoselmarServicesApplicationContext;
import fr.ifremer.coselmar.services.CoselmarServicesContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.debux.webmotion.server.WebMotionFilter;
import org.debux.webmotion.server.call.Call;
import org.debux.webmotion.server.call.HttpContext;
import org.debux.webmotion.server.render.Render;
import org.debux.webmotion.server.render.RenderStatus;

import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class CoselmarRestRequestFilter extends WebMotionFilter {

    public static final String REQUEST_PERMISSION_PARAMETER = "permission";

    public static final String REQUEST_HEADER_SESSION_TOKEN = "X-Coselmar-Session-Token";

    /** Logger. */
    private static final Log log = LogFactory.getLog(CoselmarRestRequestFilter.class);

    public void inject(Call call, HttpContext context) {

        prepareRequestContext(context);

        if (HttpContext.METHOD_OPTIONS.equals(context.getMethod())) {

            Render render = call.getRender();

            if (render instanceof RenderStatus &&
                HttpServletResponse.SC_OK == ((RenderStatus) render).getCode()) {

                // operation accepted
                CoselmarRestUtil.addOptionCorsHeaders(context);
            }
        }

        CoselmarRestUtil.prepareResponse(context);

        doProcess();

    }

    protected CoselmarRestRequestContext prepareRequestContext(HttpContext context) {

        Locale locale = getUserLocale(context);

        CoselmarServicesApplicationContext applicationContext =
            CoselmarServicesApplicationContext.getApplicationContext(context.getServletContext());

        CoselmarPersistenceContext persistenceContext =
            CoselmarTopiaTransactionFilter.getPersistenceContext(context.getRequest());

        CoselmarServicesContext serviceContext = applicationContext.newServiceContext(persistenceContext, locale);

        CoselmarRestRequestContext requestContext = new CoselmarRestRequestContext();
        requestContext.setServicesContext(serviceContext);

        CoselmarRestRequestContext.setRequestContext(context, requestContext);

        return requestContext;

    }

    protected Locale getUserLocale(HttpContext context) {
        String language = context.getHeader(HttpContext.HEADER_LANGUAGE);

        if (log.isInfoEnabled()) {
            log.info("Found Accept-Language: " + language);
        }

        // search best locale accepted by the server 'fr' or 'en'
        if (language != null) {
            String[] allLanguage = language.split(",");
            int i = 0;
            do {
                language = allLanguage[i].substring(0, 2);
                if ( !language.equals("fr") && !language.equals("en") ) {
                    // language not accepted
                    language = null;
                }
                i++;
            } while ( language == null && i < allLanguage.length );
        }

        if (language == null) {

            language = Locale.FRENCH.getLanguage();

            if (log.isInfoEnabled()) {
                log.info("Use default language: " + language);
            }

        }

        Locale locale = new Locale(language);
        return locale;

    }
}
