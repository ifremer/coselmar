package fr.ifremer.coselmar.services.v1;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2016 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coselmar.beans.HealthBean;
import fr.ifremer.coselmar.beans.QuestionSearchBean;
import fr.ifremer.coselmar.persistence.entity.Privacy;
import fr.ifremer.coselmar.services.CoselmarWebServiceSupport;
import fr.ifremer.coselmar.services.indexation.QuestionsIndexationService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class HealthService extends CoselmarWebServiceSupport {

    private static final Log log = LogFactory.getLog(HealthService.class);

    public HealthBean getHealth() {
        String version = getCoselmarServicesConfig().getVersion();
        boolean devMode = getCoselmarServicesConfig().isDevMode();

        boolean dbUp = false;
        try {
            getQuestionDao().count();
            dbUp = true;

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Error during QuestionDao call", e);
            }
        }

        boolean indexationUp = false;
        QuestionsIndexationService questionsIndexationService = getServicesContext().newService(QuestionsIndexationService.class);
        try {
            QuestionSearchBean searchBean = new QuestionSearchBean();
            searchBean.setPrivacy(Privacy.PUBLIC.name());
            questionsIndexationService.searchQuestion(searchBean);
            indexationUp = true;
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Error during Indexation service call", e);
            }
        }

        HealthBean healthBean = new HealthBean();
        healthBean.setVersion(version);
        healthBean.setDbUp(dbUp);
        healthBean.setIndexationUp(indexationUp);
        healthBean.setDevMode(devMode);
        return healthBean;
    }

}
