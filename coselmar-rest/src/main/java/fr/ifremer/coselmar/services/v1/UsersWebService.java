package fr.ifremer.coselmar.services.v1;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.auth0.jwt.Algorithm;
import com.auth0.jwt.JWTSigner;
import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheException;
import com.github.mustachejava.MustacheFactory;
import com.google.common.base.Preconditions;
import fr.ifremer.coselmar.beans.AbstractMail;
import fr.ifremer.coselmar.beans.LostPasswordMail;
import fr.ifremer.coselmar.beans.UserAccountCreatedMail;
import fr.ifremer.coselmar.beans.UserBean;
import fr.ifremer.coselmar.beans.UserExportModel;
import fr.ifremer.coselmar.beans.UserPasswordChangedMail;
import fr.ifremer.coselmar.beans.UserSearchBean;
import fr.ifremer.coselmar.beans.UserWebToken;
import fr.ifremer.coselmar.config.CoselmarServicesConfig;
import fr.ifremer.coselmar.converter.BeanEntityConverter;
import fr.ifremer.coselmar.exceptions.CoselmarTechnicalException;
import fr.ifremer.coselmar.persistence.SearchRequestBean;
import fr.ifremer.coselmar.persistence.entity.CoselmarUser;
import fr.ifremer.coselmar.persistence.entity.CoselmarUserRole;
import fr.ifremer.coselmar.services.CoselmarWebServiceSupport;
import fr.ifremer.coselmar.services.errors.InvalidCredentialException;
import fr.ifremer.coselmar.services.errors.MailAlreadyExistingException;
import fr.ifremer.coselmar.services.errors.UnauthorizedException;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.debux.webmotion.server.render.Render;
import org.nuiton.csv.Export;
import org.nuiton.topia.persistence.TopiaNoResultException;
import org.nuiton.util.StringUtil;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.io.StringWriter;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static org.apache.commons.logging.LogFactory.getLog;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class UsersWebService extends CoselmarWebServiceSupport {

    private static final Log log = getLog(UsersWebService.class);

    public UserBean getUser(String userId) throws InvalidCredentialException, UnauthorizedException, TopiaNoResultException {

        // Check authentication
        String authorization = getContext().getHeader("Authorization");
        CoselmarUser user = checkUserAuthentication(authorization);

        UserBean userBean = BeanEntityConverter.toBean(userId, user);
        return userBean;
    }

    public List<UserBean> getUsers(UserSearchBean search) throws InvalidCredentialException, UnauthorizedException {

        PaginationResult<UserBean> paginatedUsers = getPaginatedUsers(search);
        List<UserBean> result = paginatedUsers.getElements();

        return result;
    }

    public PaginationResult<UserBean> getPaginatedUsers(UserSearchBean search) throws InvalidCredentialException, UnauthorizedException {

        // Check authentication
        String authorization = getContext().getHeader("Authorization");
        UserWebToken userWebToken = checkAuthentication(authorization);

        // Who is allowed here ? Admin and user himself
        if (!StringUtils.equals(userWebToken.getRole(), CoselmarUserRole.ADMIN.name())
            && !StringUtils.equals(userWebToken.getRole(), CoselmarUserRole.SUPERVISOR.name())) {
            if (log.isDebugEnabled()) {
                String message = String.format("A non admin, non supervisor user is trying to access users list");
                log.debug(message);
            }
            throw new UnauthorizedException("Not allowed to see users");
        }

        PaginationResult<CoselmarUser> usersPage;
        if (search != null) {
            // Search default parameter if not given
            SearchRequestBean requestBean = new SearchRequestBean();
            requestBean.setLimit(search.getLimit());
            requestBean.setPage(search.getPage());
            requestBean.setFullTextSearch(search.getFullTextSearch());

            CoselmarUser example = BeanEntityConverter.fromBean(search);

            usersPage = getCoselmarUserDao().findAllByExample(example, search.isActiveAndInactive(), requestBean);

        } else {
            PaginationParameter paginationParameter = PaginationParameter.of(0, -1);
            usersPage = getCoselmarUserDao().forAll().findPage(paginationParameter);

        }

        List<UserBean> result = new ArrayList<>(usersPage.getElements().size());

        for (CoselmarUser user : usersPage.getElements()) {
            String userLightId = getPersistenceContext().getTopiaIdFactory().getRandomPart(user.getTopiaId());
            UserBean userBean = BeanEntityConverter.toBean(userLightId, user);
            result.add(userBean);
        }

        PaginationResult paginationResult = PaginationResult.of(result, usersPage.getCount(), usersPage.getCurrentPage());

        return paginationResult;
    }

    public List<UserBean> getExperts(UserSearchBean search) throws InvalidCredentialException, UnauthorizedException {

        // Check authentication
        String authorization = getContext().getHeader("Authorization");
        CoselmarUser currentUser = checkUserAuthentication(authorization);

        // Who is allowed here ? Admin, Supervisor, Expert
        if (currentUser.getRole() != CoselmarUserRole.ADMIN
            && currentUser.getRole() != CoselmarUserRole.SUPERVISOR
            && currentUser.getRole() != CoselmarUserRole.EXPERT) {
            if (log.isDebugEnabled()) {
                String message = String.format("A non admin, non supervisor, non expert user is trying to access experts list");
                log.debug(message);
            }
            throw new UnauthorizedException("Not allowed to see experts");
        }

        List<CoselmarUser> userList;
        if (search != null) {
            // Search default parameter if not given
            SearchRequestBean requestBean = new SearchRequestBean();
            requestBean.setLimit(search.getLimit());
            requestBean.setPage(search.getPage());
            requestBean.setFullTextSearch(search.getFullTextSearch());

            CoselmarUser example = BeanEntityConverter.fromBean(search);
            example.setRole(CoselmarUserRole.EXPERT);// we search experts here, force it !

            PaginationResult<CoselmarUser> userPaginationResult = getCoselmarUserDao().findAllByExample(example, search.isActiveAndInactive(), requestBean);
            userList = userPaginationResult.getElements();

        } else {
            userList = getCoselmarUserDao().forRoleEquals(CoselmarUserRole.EXPERT).findAll();

        }

        List<UserBean> result = new ArrayList<>(userList.size());

        for (CoselmarUser user : userList) {
            String userLightId = getPersistenceContext().getTopiaIdFactory().getRandomPart(user.getTopiaId());
            UserBean userBean = BeanEntityConverter.toBean(userLightId, user);
            result.add(userBean);
        }

        return result;
    }

    public void addUser(UserBean user) throws MailAlreadyExistingException, InvalidCredentialException, UnauthorizedException {
        Preconditions.checkNotNull(user);

        // Check authentication
        String authorization = getAuthorizationHeader();
        UserWebToken userWebToken = checkAuthentication(authorization);

        // Who is allowed here ? Admin and Superviseur
        if (!StringUtils.equals(userWebToken.getRole(), CoselmarUserRole.ADMIN.name())
            && StringUtils.equals(userWebToken.getRole(), CoselmarUserRole.SUPERVISOR.name())
                && !StringUtils.equals(user.getRole(), CoselmarUserRole.CLIENT.name())
            ) {
            if (log.isDebugEnabled()) {
                String message = String.format("A non admin, non supervisor user is trying to create user");
                log.debug(message);
            }
            throw new UnauthorizedException("Not allowed to add user");
        }

        CoselmarUser userEntity = getCoselmarUserDao().create();

        userEntity.setFirstname(user.getFirstName());
        userEntity.setName(user.getName());

        String mail = getCleanMail(user.getMail());
        if (StringUtils.isNotBlank(mail)) {
            checkMailUniqueness(mail, null);
            userEntity.setMail(mail);
        }

        userEntity.setRole(CoselmarUserRole.valueOf(user.getRole().toUpperCase()));
        userEntity.setQualification(user.getQualification());
        userEntity.setOrganization(user.getOrganization());
        userEntity.setActive(true);

        String password = user.getPassword();
        if (StringUtils.isBlank(password)) {
            password = getServicesContext().generatePassword();
        }

        //generate a password & a salt
        String salt = getServicesContext().generateSalt();
        String encodedPassword = getServicesContext().encodePassword(salt, password);
        userEntity.setPassword(encodedPassword);
        userEntity.setSalt(salt);

        commit();

        // send mail to user with password
        if (StringUtils.isNotBlank(mail) && StringUtil.isEmail(mail)) {
            UserAccountCreatedMail userAccountCreatedMail = new UserAccountCreatedMail(getServicesContext().getLocale());
            userAccountCreatedMail.setUser(user);
            userAccountCreatedMail.setPassword(password);
            userAccountCreatedMail.setTo(user.getMail());

            sendMail(userAccountCreatedMail);
        }
    }

    public void modifyUser(UserBean user) throws InvalidCredentialException, UnauthorizedException, InvalidParameterException, TopiaNoResultException, MailAlreadyExistingException {

        // Check authentication
        String authorization = getContext().getHeader("Authorization");
        UserWebToken userWebToken = checkAuthentication(authorization);

        boolean isAdmin = StringUtils.equals(userWebToken.getRole(), CoselmarUserRole.ADMIN.name());
        boolean isSupervisor4Client = StringUtils.equals(userWebToken.getRole(), CoselmarUserRole.SUPERVISOR.name()) && StringUtils.equals(user.getRole(), CoselmarUserRole.CLIENT.name());

        String userId = user.getId();
        if (StringUtils.isBlank(userId)) {
            throw new InvalidParameterException("User.id is mandatory");
        }

        // Admin does not need to give password, he should not know it anyway !
        if (StringUtils.isBlank(user.getPassword()) && !isAdmin && !isSupervisor4Client) {
            throw new InvalidParameterException("User.password is mandatory");
        }

        // Who is allowed here ? Admin and user himself only and Supervisor if it is a "client" type user
        if (!isAdmin && !StringUtils.equals(userWebToken.getUserId(), userId) && !isSupervisor4Client) {
            if (log.isDebugEnabled()) {
                String message = String.format("A non admin user try to modify account details with shortId '%s'", userId);
                log.debug(message);
            }
            throw new UnauthorizedException("Not allowed to modify user details");
        }

        // Ok, now, retrieve this user
        String fullId = CoselmarUser.class.getCanonicalName() +
            getPersistenceContext().getTopiaIdFactory().getSeparator() + userId;
        CoselmarUser coselmarUser = getCoselmarUserDao().forTopiaIdEquals(fullId).findAny();

        // Last check : the password
        if (!isAdmin && !isSupervisor4Client) {
            checkPassword(coselmarUser.getPassword(), coselmarUser.getSalt(), user.getPassword());
        }

        // Ok, now let's start the user update !

        // start with mail : should be unique
        String mail = user.getMail();
        if (StringUtils.isNotBlank(mail)) {
            checkMailUniqueness(mail, fullId);
            coselmarUser.setMail(mail);
        } else {
            coselmarUser.setMail(null);
        }

        String firstName = user.getFirstName();
        if (StringUtils.isNotBlank(firstName)) {
            coselmarUser.setFirstname(firstName);
        } else {
            user.setFirstName(coselmarUser.getFirstname());
        }

        String userName = user.getName();
        if (StringUtils.isNotBlank(userName)) {
            coselmarUser.setName(userName);
        } else {
            // only in case of mail need
            user.setName(coselmarUser.getName());
        }

        String userRole = user.getRole();
        if (StringUtils.isNotBlank(userRole) && isAdmin) { // only admin can update UserRole
            coselmarUser.setRole(CoselmarUserRole.valueOf(userRole.toUpperCase()));
        }

        String organization = user.getOrganization();
        if (StringUtils.isNotBlank(organization)) {
            coselmarUser.setOrganization(organization);
        }

        String qualification = user.getQualification();
        if (StringUtils.isNotBlank(qualification)) {
            coselmarUser.setQualification(qualification);
        }

        String phoneNumber = user.getPhoneNumber();
        if (StringUtils.isNotBlank(phoneNumber)) {
            coselmarUser.setPhoneNumber(phoneNumber);
        }

        String newPassword = user.getNewPassword();
        if (StringUtils.isNotBlank(newPassword)) {
            String salt = getServicesContext().generateSalt();
            String encodedPassword = getServicesContext().encodePassword(salt, newPassword);
            coselmarUser.setSalt(salt);
            coselmarUser.setPassword(encodedPassword);

            //if it is a modification by Admin, send new mail to user
            if ( (isAdmin || isSupervisor4Client) && StringUtil.isEmail(coselmarUser.getMail())) {
                UserPasswordChangedMail userPasswordChangedMail = new UserPasswordChangedMail(getServicesContext().getLocale());
                userPasswordChangedMail.setUser(user);
                userPasswordChangedMail.setPassword(newPassword);
                userPasswordChangedMail.setTo(coselmarUser.getMail());
                sendMail(userPasswordChangedMail);
            }
        }

        boolean active = user.isActive();
        coselmarUser.setActive(active);

        commit();
    }

    public Render login(String mail, String password) throws InvalidCredentialException {
        Preconditions.checkNotNull(mail);
        Preconditions.checkNotNull(password);

        CoselmarUser user = getCoselmarUserDao().forMailEquals(getCleanMail(mail)).addEquals(CoselmarUser.PROPERTY_ACTIVE, true).findAnyOrNull();

        if (user == null) {
            throw new InvalidCredentialException("Invalid mail");
        }

        String salt = user.getSalt();
        checkPassword(user.getPassword(), salt, password);

        // return a Json Web Token for authentication
        JWTSigner jwtSigner = new JWTSigner(getCoselmarServicesConfig().getWebSecurityKey());
        JWTSigner.Options signerOption = new JWTSigner.Options();
        signerOption.setAlgorithm(Algorithm.HS384);

        String userTopiaId = user.getTopiaId();
        String shortId = getPersistenceContext().getTopiaIdFactory().getRandomPart(userTopiaId);
        Map<String, Object> claims = UserWebToken.toJwtClaims(shortId, user.getFirstname(), user.getName(), user.getRole().name());
        String webToken = jwtSigner.sign(claims, signerOption);

        return renderJSON("jwt", webToken);

    }

    public void deleteUser(String userId) throws InvalidCredentialException, UnauthorizedException {

        // Check authentication
        String authorization = getContext().getHeader("Authorization");
        UserWebToken userWebToken = checkAuthentication(authorization);

        boolean isAdmin = StringUtils.equals(userWebToken.getRole(), CoselmarUserRole.ADMIN.name());

        // Only admin is authorized to do this
        if (!isAdmin) {
            if (log.isDebugEnabled()) {
                String message = String.format("A non admin user try to delete account with shortId '%s'", userId);
                log.debug(message);
            }
            throw new UnauthorizedException("Not allowed to delete user");
        }

        // reconstitute full id
        String fullId = CoselmarUser.class.getCanonicalName() + "_" + userId;

        CoselmarUser user = getCoselmarUserDao().forTopiaIdEquals(fullId).findUnique();

        getCoselmarUserDao().delete(user);

        commit();
    }

    public void generateNewPassword(String userMail) {
        // Retrieve user
        CoselmarUser user = getCoselmarUserDao().forMailEquals(userMail).findUnique();

        // create new password
        String password = getServicesContext().generatePassword();

        // Salt it, encode it !
        String salt = getServicesContext().generateSalt();
        String encodedPassword = getServicesContext().encodePassword(salt, password);
        user.setPassword(encodedPassword);
        user.setSalt(salt);

        // commit, and send mail !
        commit();

        LostPasswordMail lostPasswordMail = new LostPasswordMail(getServicesContext().getLocale());
        String shortId = getPersistenceContext().getTopiaIdFactory().getRandomPart(user.getTopiaId());
        UserBean userBean = BeanEntityConverter.toBean(shortId, user);
        lostPasswordMail.setUser(userBean);
        lostPasswordMail.setPassword(password);
        lostPasswordMail.setTo(user.getMail());

        sendMail(lostPasswordMail);

    }

    public Render exportSearchedUsers(String token, UserSearchBean searchOption) throws InvalidCredentialException, UnauthorizedException {

        CoselmarUser currentUser = checkUserAuthentication(token);

        // Who is allowed here ? Admin and user himself
        if (currentUser.getRole() != CoselmarUserRole.ADMIN
            && currentUser.getRole() != CoselmarUserRole.SUPERVISOR) {
            if (log.isDebugEnabled()) {
                String message = String.format("A non admin, non supervisor user is trying to access users list");
                log.debug(message);
            }
            throw new UnauthorizedException("Not allowed to see users");
        }

        List<CoselmarUser> userList;
        if (searchOption != null) {
            // Search default parameter if not given
            SearchRequestBean requestBean = new SearchRequestBean();
            requestBean.setLimit(searchOption.getLimit());
            requestBean.setPage(searchOption.getPage());
            requestBean.setFullTextSearch(searchOption.getFullTextSearch());

            CoselmarUser example = BeanEntityConverter.fromBean(searchOption);

            PaginationResult<CoselmarUser> userPaginationResult = getCoselmarUserDao().findAllByExample(example, searchOption.isActiveAndInactive(), requestBean);
            userList = userPaginationResult.getElements();

        } else {
            userList = getCoselmarUserDao().findAll();

        }

        List<UserBean> users = new ArrayList<>(userList.size());

        for (CoselmarUser user : userList) {
            String userLightId = getPersistenceContext().getTopiaIdFactory().getRandomPart(user.getTopiaId());
            UserBean userBean = BeanEntityConverter.toBean(userLightId, user);
            users.add(userBean);
        }

        UserExportModel exportModel = new UserExportModel();

        String exportData;
        try {
            exportData = Export.exportToString(exportModel, users);
        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Error during export", e);
            }
            throw new CoselmarTechnicalException("Unable to export datas", e);
        }

        return renderDownload(IOUtils.toInputStream(exportData), "export-users-result.csv", "text/csv");

    }

    /////////////////////////////////////////////
    /////////////    Internal Part      /////////
    /////////////////////////////////////////////

    /**
     * Check if the mail is already used by an other user
     * @param mail      : simply mail to check
     * @param userId    : the current user#id : this parameter is needed to exclude from the search this user, cause it could already have this mail
     * @throws InvalidParameterException if the mail is already used.
     */
    protected void checkMailUniqueness(String mail, String userId) throws MailAlreadyExistingException {
        boolean mailAlreadyUsed;

        if (StringUtils.isNotBlank(userId)) {
            mailAlreadyUsed = getCoselmarUserDao().forMailEquals(mail).addNotEquals(CoselmarUser.PROPERTY_TOPIA_ID, userId).exists();

        } else {
            mailAlreadyUsed = getCoselmarUserDao().forMailEquals(mail).exists();
        }

        if (mailAlreadyUsed) {
            String msg = String.format("mail '%s' is already used", mail);
            throw new MailAlreadyExistingException(msg);
        }
    }

    /**
     * Check that the password is the same : encode the one given with the salt,
     * and compare with the user password from database
     *
     * @param currentPassword : password from database
     * @param salt            : salt use to encode database password
     * @param password        : given password we want to check
     * @throws InvalidCredentialException if the given password is not the same as one from database
     */
    protected void checkPassword(String currentPassword, String salt, String password) throws InvalidCredentialException {
        String encodedPassword = getServicesContext().encodePassword(salt, password);

        if (!encodedPassword.equals(currentPassword)){
            throw new InvalidCredentialException("Invalid password given");
        }
    }

    /////////////////////////////////////////////
    ///////////////   MAIL PART   ///////////////
    /////////////////////////////////////////////

    protected void sendMail(AbstractMail mail) {

        if (getCoselmarServicesConfig().isDevMode()) {

            if (log.isInfoEnabled()) {
                log.info("an email should have been sent if not in devMode: to = " +
                    mail.getTo() + ". subject = '" + mail.getSubject() +
                    "'. body = \n" + getBody(mail));
            }

            if (!mail.isRecipientProvided() && log.isWarnEnabled()) {
                log.warn("email has no recipient, would not have been sent " + mail);
            }

        } else {

            CoselmarServicesConfig applicationConfig = getCoselmarServicesConfig();

            mail.setCoselmarUrl(applicationConfig.getApplicationUrl());

            String body = getBody(mail);

            if (mail.isRecipientProvided()) {

                Email newEmail = new SimpleEmail();
                newEmail.setHostName(applicationConfig.getSmtpHost());
                newEmail.setSmtpPort(applicationConfig.getSmtpPort());
                newEmail.setCharset(Charsets.UTF_8.name());
                newEmail.setSubject(mail.getSubject());

                try {
                    newEmail.setFrom(applicationConfig.getSmtpFrom());
                    String to = mail.getTo();
                    newEmail.addTo(to);
                    newEmail.setMsg(body);

                    newEmail.send();

                } catch (EmailException e) {
                    throw new CoselmarTechnicalException(e);
                }

            } else {
                if (log.isErrorEnabled()) {
                    log.error("email has no recipient, won't be sent " + mail);
                }
            }
        }
    }

    protected String getBody(AbstractMail mail) {

        Mustache mustache = getMustache(mail);
        StringWriter stringWriter = new StringWriter();
        mustache.execute(stringWriter, mail);

        return stringWriter.toString();
    }

    protected Mustache getMustache(AbstractMail mail) {

        MustacheFactory mustacheFactory = new DefaultMustacheFactory("mail/");
        Locale locale = mail.getLocale();
        String templateName = mail.getClass().getSimpleName() + "_" + locale.getLanguage() + ".mustache";

        Mustache mustache;
        try {
            mustache = mustacheFactory.compile(templateName);

        } catch (MustacheException e) {
            // fallback with no locale
            templateName = mail.getClass().getSimpleName() + ".mustache";
            mustache = mustacheFactory.compile(templateName);
        }

        return mustache;
    }
}
