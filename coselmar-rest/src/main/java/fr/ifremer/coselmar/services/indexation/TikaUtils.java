package fr.ifremer.coselmar.services.indexation;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2017 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @author ymartel (martel@codelutin.com)
 */
public final class TikaUtils {

    private static final Log log = LogFactory.getLog(TikaUtils.class);

    public static final List<String> READABLE_TEXT_MIMETYPES = Arrays.asList("text/plain",
                                                                "application/pdf",
                                                                "application/vnd.oasis.opendocument.text",
                                                                "application/vnd.oasis.opendocument.presentation",
                                                                "application/msword",
                                                                "application/mspowerpoint",
                                                                "application/powerpoint",
                                                                "application/vnd.ms-powerpoint",
                                                                "text/html"
                                                              );

    private static final Tika TIKA = new Tika();

    private TikaUtils() {
        throw new IllegalAccessError("Utility class");
    }

    public static String getFileContent(String filePath) {
        String fileContent = "";
        File file = new File(filePath);
        try {
            String mimeType = TIKA.detect(file);
            // Can we read it ?
            if (StringUtils.isNotBlank(mimeType) && READABLE_TEXT_MIMETYPES.contains(mimeType.toLowerCase())) {
                fileContent = TIKA.parseToString(file);
            }
        } catch (IOException e) {
            if (log.isErrorEnabled()) {
                log.error("Unable to read file " + filePath, e);
            }
        } catch (TikaException e) {
            if (log.isErrorEnabled()) {
                log.error("Unable to get file content from Tika : " + filePath, e);
            }
        }
        return fileContent;
    }

    public static String getFileMimeType(String filePath) {
        String mimeType = TIKA.detect(filePath);
        return mimeType;
    }
}
