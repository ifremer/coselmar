package fr.ifremer.coselmar.services;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.debux.webmotion.server.call.HttpContext;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class CoselmarRestRequestContext {

    protected static final String REQUEST_COSELMAR_REQUEST_CONTEXT = "coselmar_CoselmarRequestContext";

    public static CoselmarRestRequestContext getRequestContext(HttpContext httpContext) {

        CoselmarRestRequestContext result = (CoselmarRestRequestContext)
                httpContext.getRequest().getAttribute(REQUEST_COSELMAR_REQUEST_CONTEXT);
        return result;
    }

    public static void setRequestContext(HttpContext httpContext,
                                         CoselmarRestRequestContext requestContext) {
        httpContext.getRequest().setAttribute(REQUEST_COSELMAR_REQUEST_CONTEXT, requestContext);
    }

    protected CoselmarServicesContext servicesContext;

    public void setServicesContext(CoselmarServicesContext servicesContext) {
        this.servicesContext = servicesContext;
    }

    public CoselmarServicesContext getServicesContext() {
        return this.servicesContext;
    }

}
