package fr.ifremer.coselmar.services;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coselmar.config.CoselmarServicesConfig;
import fr.ifremer.coselmar.exceptions.CoselmarTechnicalException;
import fr.ifremer.coselmar.persistence.CoselmarPersistenceContext;
import fr.ifremer.coselmar.persistence.CoselmarTopiaApplicationContext;
import fr.ifremer.coselmar.services.indexation.LuceneUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

import static org.apache.commons.logging.LogFactory.getLog;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class DefaultCoselmarServicesContext implements CoselmarServicesContext {

    private static final Log log = getLog(DefaultCoselmarServicesContext.class);

    protected static final int SALT_SIZE = 16;

    protected CoselmarServicesConfig servicesConfig;

    protected CoselmarPersistenceContext persistenceContext;

    protected Locale locale;

    protected LuceneUtils luceneUtils;

    private CoselmarTopiaApplicationContext topiaApplicationContext;

    public void setCoselmarServicesConfig(CoselmarServicesConfig servicesConfig) {
        this.servicesConfig = servicesConfig;
    }

    public void setTopiaApplicationContext(CoselmarTopiaApplicationContext topiaApplicationContext) {
        this.topiaApplicationContext = topiaApplicationContext;
    }

    public void setPersistenceContext(CoselmarPersistenceContext persistenceContext) {
        this.persistenceContext = persistenceContext;
    }

    public void setLuceneUtils(LuceneUtils luceneUtils) {
        this.luceneUtils = luceneUtils;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    @Override
    public Date getNow() {
        Date now = new Date();
        return now;
    }

    @Override
    public CoselmarTopiaApplicationContext getTopiaApplicationContext() {
        return this.topiaApplicationContext;
    }

    @Override
    public CoselmarPersistenceContext getPersistenceContext() {
        return this.persistenceContext;
    }

    @Override
    public CoselmarServicesConfig getCoselmarServicesConfig() {
        return this.servicesConfig;
    }

    @Override
    public LuceneUtils getLuceneUtils() {
        return this.luceneUtils;
    }

    @Override
    public <E extends CoselmarService> E newService(Class<E> serviceClass) {

        E service;

        try {

            Constructor<E> constructor = serviceClass.getConstructor();

            service = constructor.newInstance();

        } catch (NoSuchMethodException e) {

            throw new CoselmarTechnicalException("all services must provide a default public constructor", e);

        } catch (InvocationTargetException | InstantiationException | IllegalAccessException e) {

            throw new CoselmarTechnicalException("unable to instantiate coselmar service", e);

        }

        service.setServicesContext(this);

        return service;
    }

    @Override
    public Locale getLocale() {
        if (this.locale == null) {
            this.locale = Locale.getDefault();
        }
        return this.locale;
    }

    @Override
    public String getCleanMail(String email) {
        return email == null ? null : StringUtils.lowerCase(email.trim());
    }

    @Override
    public String generateSalt() {

        SecureRandom secureRandom;
        try {
            secureRandom = SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException e) {
            secureRandom = new SecureRandom();
        }
        byte[] salt = new byte[SALT_SIZE];
        secureRandom.nextBytes(salt);
        return Arrays.toString(salt);
    }

    @Override
    public String generatePassword() {

        return RandomStringUtils.randomAlphanumeric(8);
    }

    @Override
    public String encodePassword(String salt, String password) {

        String encodedPassword = getSecurePassword(salt, password, getCoselmarServicesConfig().getEncryptionAlgorithm());
        return encodedPassword;
    }

    /**
     *
     * @param algorithm : Instance Type that will be loaded from MessageDigest,
     *                  should be : MD5, SHA-1, SHA-256, SHA-384, SHA-512.
     */
    protected static String getSecurePassword(String salt, String passwordToHash, String algorithm) {

        String generatedPassword;

        try {
            MessageDigest digest = MessageDigest.getInstance(algorithm);
            digest.update(salt.getBytes());
            byte[] passwordBytes = digest.digest(passwordToHash.getBytes());
            StringBuilder stringBuilder = new StringBuilder();
            for(int i=0; i < passwordBytes.length ;i++)
            {
                stringBuilder.append(Integer.toString((passwordBytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = stringBuilder.toString();

        } catch (NoSuchAlgorithmException e) {
            String errorMessage = "Unable to encode password";
            if (log.isErrorEnabled()) {
                log.error(errorMessage, e);
            }
            throw new CoselmarTechnicalException(errorMessage, e);
        }

        return generatedPassword;
    }

}
