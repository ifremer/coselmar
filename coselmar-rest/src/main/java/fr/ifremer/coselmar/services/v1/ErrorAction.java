package fr.ifremer.coselmar.services.v1;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coselmar.services.CoselmarRestUtil;
import org.debux.webmotion.server.WebMotionController;
import org.debux.webmotion.server.call.HttpContext;
import org.debux.webmotion.server.render.Render;

import javax.servlet.http.HttpServletResponse;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class ErrorAction extends WebMotionController {

    public Render on500(HttpContext context, Exception e) {

        CoselmarRestUtil.prepareResponse(context);

        Render render = renderError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
        return render;

    }

    public Render on404(HttpContext context, Exception e) {

        CoselmarRestUtil.prepareResponse(context);

        Render render = renderError(HttpServletResponse.SC_NOT_FOUND, e.getMessage());
        return render;

    }

    public Render on403(HttpContext context, Exception e) {

        CoselmarRestUtil.prepareResponse(context);

        Render render = renderError(HttpServletResponse.SC_FORBIDDEN, e.getMessage());
        return render;

    }

    public Render on401(HttpContext context, Exception e) {

        CoselmarRestUtil.prepareResponse(context);

        Render render = renderError(HttpServletResponse.SC_UNAUTHORIZED, e.getMessage());
        return render;

    }

    public Render on409(HttpContext context, Exception e) {

        CoselmarRestUtil.prepareResponse(context);

        Render render = renderError(HttpServletResponse.SC_CONFLICT, e.getMessage());
        return render;

    }
}
