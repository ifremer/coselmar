package fr.ifremer.coselmar.services.filter;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coselmar.persistence.CoselmarPersistenceContext;
import fr.ifremer.coselmar.persistence.CoselmarTopiaPersistenceContext;
import fr.ifremer.coselmar.services.CoselmarServicesApplicationContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.topia.persistence.TopiaException;
import org.nuiton.web.filter.TypedTopiaTransactionFilter;

import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class CoselmarTopiaTransactionFilter extends TypedTopiaTransactionFilter<CoselmarPersistenceContext> {

    private static final Log log = LogFactory.getLog(CoselmarTopiaTransactionFilter.class);

    public CoselmarTopiaTransactionFilter() {
        super(CoselmarPersistenceContext.class);
    }

    @Override
    protected CoselmarPersistenceContext beginTransaction(ServletRequest request) throws TopiaException {

        CoselmarServicesApplicationContext applicationContext =
            CoselmarServicesApplicationContext.getApplicationContext(request.getServletContext());

        CoselmarTopiaPersistenceContext persistenceContext = applicationContext.newPersistenceContext();

        return persistenceContext;

    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

        super.init(filterConfig);

        if (log.isInfoEnabled()) {
            log.info("Init Topia Filter");
        }
    }

}
