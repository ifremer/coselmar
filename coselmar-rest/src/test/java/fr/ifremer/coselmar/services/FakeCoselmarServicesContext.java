package fr.ifremer.coselmar.services;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Preconditions;
import fr.ifremer.coselmar.config.CoselmarServicesConfig;
import fr.ifremer.coselmar.persistence.CoselmarTopiaApplicationContext;
import fr.ifremer.coselmar.persistence.CoselmarTopiaPersistenceContext;
import fr.ifremer.coselmar.services.indexation.LuceneUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;
import java.util.Locale;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class FakeCoselmarServicesContext extends DefaultCoselmarServicesContext {

    private static final Log log =
        LogFactory.getLog(FakeCoselmarServicesContext.class);

    protected Date date;


    public static FakeCoselmarServicesContext newServiceContext(Date now,
                                     Locale locale,
                                     CoselmarServicesConfig serviceConfig,
                                     CoselmarTopiaApplicationContext applicationcontext,
                                     CoselmarTopiaPersistenceContext persistenceContext,
                                     LuceneUtils luceneUtils) {

        FakeCoselmarServicesContext serviceContext = new FakeCoselmarServicesContext();
        serviceContext.setPersistenceContext(persistenceContext);
        serviceContext.setCoselmarServicesConfig(serviceConfig);
        serviceContext.setTopiaApplicationContext(applicationcontext);
        serviceContext.setLuceneUtils(luceneUtils);
        serviceContext.setLocale(locale);
        serviceContext.setDate(now);
        return serviceContext;
    }

    @Override
    public Date getNow() {
        Preconditions.checkState(date != null, "you must provide a date before running service test");
        if (log.isTraceEnabled()) {
            log.trace("injecting fake date in service: " + date);
        }
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
