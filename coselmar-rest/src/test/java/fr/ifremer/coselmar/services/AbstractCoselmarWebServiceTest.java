package fr.ifremer.coselmar.services;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coselmar.persistence.CoselmarPersistenceContext;
import org.apache.commons.logging.Log;
import org.debux.webmotion.unittest.WebMotionTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.nuiton.util.DateUtil;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

import static org.apache.commons.logging.LogFactory.getLog;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class AbstractCoselmarWebServiceTest extends WebMotionTest {

    private static final Log log = getLog(AbstractCoselmarWebServiceTest.class);

    @Rule
    public final FakeCoselmarApplicationContext application = new FakeCoselmarApplicationContext("coselmar-test.properties");

    @Override
    protected int getPort() {
        return application.getPort();
    }

    @Before
    public void startServer() throws Exception {

        CoselmarServicesApplicationContext applicationContext =
            new CoselmarServicesApplicationContext(application.getApplicationConfig(),
                application.getTopiaApplicationContext(), application.getLuceneUtils()) {

                @Override
                public CoselmarServicesContext newServiceContext(CoselmarPersistenceContext persistenceContext, Locale locale) {

                    FakeCoselmarServicesContext servicesContext = FakeCoselmarServicesContext.newServiceContext(
                        DateUtil.createDate(1, 1, 2014),
                        Locale.FRANCE,
                        application.getApplicationConfig(),
                        application.getTopiaApplicationContext(),
                        application.newPersistenceContext(),
                        application.getLuceneUtils());
                    return servicesContext;

                }

            };

        applicationContext.init();

        CoselmarServicesApplicationContext.setApplicationContext(applicationContext);

        super.startServer();

    }

    @Override
    protected String getServerBaseDirectory() {

        return new File(application.getTestBasedir(),
            "tomcat_" + application.getPort()).getAbsolutePath();

    }

    @After
    public void stopServer() throws Exception {

        if (log.isTraceEnabled()) {
            log.trace("closing application context " + application);
        }

        application.close();
        server.stop();
        server.destroy();

    }

    protected void showTestResult(String content) throws IOException {

        String testName = application.getMethodName();

        if (log.isInfoEnabled()) {
            log.info("test *" + testName + "* result\n" + content);
        }

    }
}
