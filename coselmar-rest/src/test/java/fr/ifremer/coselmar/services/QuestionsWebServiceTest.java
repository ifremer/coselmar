package fr.ifremer.coselmar.services;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.auth0.jwt.JWTVerifier;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import fr.ifremer.coselmar.beans.QuestionBean;
import fr.ifremer.coselmar.beans.QuestionUserRole;
import fr.ifremer.coselmar.converter.JsonHelper;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class QuestionsWebServiceTest extends AbstractCoselmarWebServiceTest {

    protected FakeCoselmarServicesContext serviceContext;

    protected FakeCoselmarServicesContext getServiceContext() {

        if (serviceContext == null) {
            serviceContext = application.newServiceContext(application.newPersistenceContext(), Locale.FRANCE);
        }

        return serviceContext;
    }

    @Test
    public void testSearchQuestions() throws Exception {

        //First : login !
        Request loginRequest = createRequest("/v1/users/login")
                .addParameter("mail", "default.supervisor@temporary.coselmar")
                .addParameter("password", "manager1234")
                .Post();

        Response loginResponse = loginRequest.execute();
        String loginContent = loginResponse.returnContent().asString();
        Assert.assertNotNull(loginContent);
        showTestResult(loginContent);

        Gson gson = new Gson();
        Map<String, String> loginMap = gson.fromJson(loginContent, Map.class);

        String supervisorToken = loginMap.get("jwt");

        // Create first document
        String documentOneTitle = "Ceci est le titre";
        Request addQuestionsRequest = createRequest("/v1/questions")
                .addParameter("question",
                    "{title: '" + documentOneTitle + "', summary: 'ceci est le resume',"
                        + " submissionDate: '" + (new Date()).getTime() + "',"
                        + " themes: ['test', 'questionOne'], type: 'question',"
                        + " privacy : 'PUBLIC' }")
                .Post()
                .addHeader("Authorization", "Bearer " + supervisorToken);

        HttpResponse addQuestionResponse = addQuestionsRequest.execute().returnResponse();
        Assert.assertEquals(200, addQuestionResponse.getStatusLine().getStatusCode());

        // Create second document
        String documentTwoTitle = "There s someone missing. The question s Who?";
        addQuestionsRequest = createRequest("/v1/questions")
                .addParameter("question",
                    "{title: '" + documentTwoTitle + "',"
                        + " summary: 'Something old, Something new, Something borrowed, Something blue.',"
                        + " submissionDate: '" + (new Date()).getTime() + "',"
                        + " themes: ['big bang two', 'Pandorica', 'River', 'Universe'], type: 'question',"
                        + " privacy : 'PUBLIC' }")
                .Post()
                .addHeader("Authorization", "Bearer " + supervisorToken);

        addQuestionResponse = addQuestionsRequest.execute().returnResponse();
        Assert.assertEquals(200, addQuestionResponse.getStatusLine().getStatusCode());

        // And a third document !
        String documentThreeTitle = "Private things";
        addQuestionsRequest = createRequest("/v1/questions")
                .addParameter("question",
                    "{title: '" + documentThreeTitle + "',"
                        + " summary: 'This question is private, zero access for others',"
                        + " submissionDate: '" + (new Date()).getTime() + "',"
                        + " themes: ['Universe', 'test', 'zero'], type: 'question',"
                        + " privacy : 'PRIVATE' }")
                .Post()
                .addHeader("Authorization", "Bearer " + supervisorToken);

        addQuestionResponse = addQuestionsRequest.execute().returnResponse();
        Assert.assertEquals(200, addQuestionResponse.getStatusLine().getStatusCode());

        // First search : as supervisor, no search bean : retrieve the three documents
        Request searchRequest = createRequest("/v1/questions")
                .addParameter("searchOption", "{}")
                .Get()
                .addHeader("Authorization", "Bearer " + supervisorToken);

        Response searchResponse = searchRequest.execute();
        String searchResultContent = searchResponse.returnContent().asString();
        JsonHelper jsonHelper = new JsonHelper(true);
        List<QuestionBean> result = jsonHelper.fromJson(searchResultContent, new TypeToken<ArrayList<QuestionBean>>(){}.getType());

        Assert.assertEquals(3, result.size());
        Assert.assertNotNull(result.get(0));
        Assert.assertNotNull(result.get(1));
        Assert.assertNotNull(result.get(2));


        // Second search : as supervisor, searchBean only with public privacy : retrieve two documents (one and two)
        searchRequest = createRequest("/v1/questions")
                .addParameter("searchOption", "{privacy : 'PUBLIC'}")
                .Get()
                .addHeader("Authorization", "Bearer " + supervisorToken);

        searchResponse = searchRequest.execute();
        searchResultContent = searchResponse.returnContent().asString();
        List<QuestionBean> publicPrivacyResult = jsonHelper.fromJson(searchResultContent, new TypeToken<ArrayList<QuestionBean>>(){}.getType());

        Assert.assertEquals(2, publicPrivacyResult.size());
        List<String> expectedTitles = Arrays.asList(documentOneTitle, documentTwoTitle);
        Assert.assertTrue(expectedTitles.contains(publicPrivacyResult.get(0).getTitle()));
        Assert.assertTrue(expectedTitles.contains(publicPrivacyResult.get(1).getTitle()));


        // Third search : as supervisor, searchBean with public privacy and test keywords : retrieve documentOne
        searchRequest = createRequest("/v1/questions")
                .addParameter("searchOption", "{privacy : 'public', fullTextSearch : ['test']}")
                .Get()
                .addHeader("Authorization", "Bearer " + supervisorToken);

        searchResponse = searchRequest.execute();
        searchResultContent = searchResponse.returnContent().asString();
        List<QuestionBean> publicTestResult = jsonHelper.fromJson(searchResultContent, new TypeToken<ArrayList<QuestionBean>>(){}.getType());

        Assert.assertEquals(1, publicTestResult.size());
        Assert.assertEquals(documentOneTitle, publicTestResult.get(0).getTitle());


        // Fourth search : as supervisor, searchBean with test and pandorica keywords : retrieve nothing
        searchRequest = createRequest("/v1/questions")
                .addParameter("searchOption", "{privacy : 'public', fullTextSearch : ['test', 'pandorica']}")
                .Get()
                .addHeader("Authorization", "Bearer " + supervisorToken);

        searchResponse = searchRequest.execute();
        searchResultContent = searchResponse.returnContent().asString();
        List<QuestionBean> shouldNoResult = jsonHelper.fromJson(searchResultContent, new TypeToken<ArrayList<QuestionBean>>(){}.getType());

        Assert.assertTrue(shouldNoResult.isEmpty());


        // Fifth search : as supervisor, searchBean with test keyword : retrieve documentOne and documentThree
        searchRequest = createRequest("/v1/questions")
                .addParameter("searchOption", "{fullTextSearch : ['test']}")
                .Get()
                .addHeader("Authorization", "Bearer " + supervisorToken);

        searchResponse = searchRequest.execute();
        searchResultContent = searchResponse.returnContent().asString();
        List<QuestionBean> testKeywordResult = jsonHelper.fromJson(searchResultContent, new TypeToken<ArrayList<QuestionBean>>(){}.getType());

        Assert.assertEquals(2, testKeywordResult.size());
        expectedTitles = Arrays.asList(documentOneTitle, documentThreeTitle);
        Assert.assertTrue(expectedTitles.contains(testKeywordResult.get(0).getTitle()));
        Assert.assertTrue(expectedTitles.contains(testKeywordResult.get(1).getTitle()));


        // Sixth search : as supervisor, searchBean with test and universe keywords : retrieve documentThree
        searchRequest = createRequest("/v1/questions")
                .addParameter("searchOption", "{fullTextSearch : ['test', 'Universe']}")
                .Get()
                .addHeader("Authorization", "Bearer " + supervisorToken);

        searchResponse = searchRequest.execute();
        searchResultContent = searchResponse.returnContent().asString();
        List<QuestionBean> testUniverseKeywordsResult = jsonHelper.fromJson(searchResultContent, new TypeToken<ArrayList<QuestionBean>>(){}.getType());

        Assert.assertEquals(1, testUniverseKeywordsResult.size());
        Assert.assertEquals(documentThreeTitle, testUniverseKeywordsResult.get(0).getTitle());



        //Have login for an expert !
        loginRequest = createRequest("/v1/users/login")
                .addParameter("mail", "lambda.expert@temporary.coselmar")
                .addParameter("password", "manager1234")
                .Post();

        loginResponse = loginRequest.execute();
        loginContent = loginResponse.returnContent().asString();
        Assert.assertNotNull(loginContent);
        showTestResult(loginContent);

        loginMap = gson.fromJson(loginContent, Map.class);

        String expertToken = loginMap.get("jwt");

        // Seventh search : as expert, no search bean : retrieve the two public documents
        searchRequest = createRequest("/v1/questions")
            .addParameter("searchOption", "{}")
            .Get()
            .addHeader("Authorization", "Bearer " + expertToken);

        searchResponse = searchRequest.execute();
        searchResultContent = searchResponse.returnContent().asString();
        result = jsonHelper.fromJson(searchResultContent, new TypeToken<ArrayList<QuestionBean>>(){}.getType());

        Assert.assertEquals(2, result.size());
        expectedTitles = Arrays.asList(documentOneTitle, documentTwoTitle);
        Assert.assertTrue(expectedTitles.contains(result.get(0).getTitle()));


        // Eighth search : as expert, searchBean only with public privacy : retrieve two documents (one and two) (same as seventh)
        searchRequest = createRequest("/v1/questions")
            .addParameter("searchOption", "{privacy : 'public'}")
            .Get()
            .addHeader("Authorization", "Bearer " + expertToken);

        searchResponse = searchRequest.execute();
        searchResultContent = searchResponse.returnContent().asString();
        publicPrivacyResult = jsonHelper.fromJson(searchResultContent, new TypeToken<ArrayList<QuestionBean>>(){}.getType());

        Assert.assertEquals(2, publicPrivacyResult.size());
        expectedTitles = Arrays.asList(documentOneTitle, documentTwoTitle);
        Assert.assertTrue(expectedTitles.contains(publicPrivacyResult.get(0).getTitle()));
        Assert.assertTrue(expectedTitles.contains(publicPrivacyResult.get(1).getTitle()));


        // Ninth search : as expert, searchBean with public privacy and test keywords : retrieve documentOne
        searchRequest = createRequest("/v1/questions")
            .addParameter("searchOption", "{privacy : 'public', fullTextSearch : ['test']}")
            .Get()
            .addHeader("Authorization", "Bearer " + expertToken);

        searchResponse = searchRequest.execute();
        searchResultContent = searchResponse.returnContent().asString();
        publicTestResult = jsonHelper.fromJson(searchResultContent, new TypeToken<ArrayList<QuestionBean>>(){}.getType());

        Assert.assertEquals(1, publicTestResult.size());
        Assert.assertEquals(documentOneTitle, publicTestResult.get(0).getTitle());


        // Tenth search : as supervisor, searchBean with test and pandorica keywords : retrieve nothing
        searchRequest = createRequest("/v1/questions")
            .addParameter("searchOption", "{privacy : 'public', fullTextSearch : ['test', 'pandorica']}")
            .Get()
            .addHeader("Authorization", "Bearer " + expertToken);

        searchResponse = searchRequest.execute();
        searchResultContent = searchResponse.returnContent().asString();
        shouldNoResult = jsonHelper.fromJson(searchResultContent, new TypeToken<ArrayList<QuestionBean>>(){}.getType());

        Assert.assertTrue(shouldNoResult.isEmpty());


        // Eleventh search : as expert, searchBean with pandorica keyword : retrieve nothing (documentThree is private)
        searchRequest = createRequest("/v1/questions")
            .addParameter("searchOption", "{fullTextSearch : ['zero']}")
            .Get()
            .addHeader("Authorization", "Bearer " + expertToken);

        searchResponse = searchRequest.execute();
        searchResultContent = searchResponse.returnContent().asString();
        shouldNoResult = jsonHelper.fromJson(searchResultContent, new TypeToken<ArrayList<QuestionBean>>(){}.getType());

        Assert.assertTrue(shouldNoResult.isEmpty());
    }

    @Test
    public void testSearchUserQuestions() throws Exception {

        String webSecurityKey = getServiceContext().getCoselmarServicesConfig().getWebSecurityKey();
        JWTVerifier jwtVerifier = new JWTVerifier(webSecurityKey, "audience");

        //First : login !
        Request loginRequest = createRequest("/v1/users/login")
                .addParameter("mail", "lambda.expert@temporary.coselmar")
                .addParameter("password", "manager1234")
                .Post();

        Response loginResponse = loginRequest.execute();
        String loginContent = loginResponse.returnContent().asString();
        Assert.assertNotNull(loginContent);
        showTestResult(loginContent);

        Gson gson = new Gson();
        Map<String, String> loginJson = gson.fromJson(loginContent, Map.class);

        String expertToken = loginJson.get("jwt");
        Map<String, Object> expertMap = jwtVerifier.verify(expertToken);
        String expertId = (String) expertMap.get("userId");

        //First : login !
        loginRequest = createRequest("/v1/users/login")
                .addParameter("mail", "default.supervisor@temporary.coselmar")
                .addParameter("password", "manager1234")
                .Post();

        loginResponse = loginRequest.execute();
        loginContent = loginResponse.returnContent().asString();
        Assert.assertNotNull(loginContent);
        showTestResult(loginContent);

        loginJson = gson.fromJson(loginContent, Map.class);

        String supervisorToken = loginJson.get("jwt");
        Map<String, Object> supervisorMap = jwtVerifier.verify(supervisorToken);
        String supervisorId = (String) supervisorMap.get("userId");


        // Create first document
        String documentOneTitle = "Ceci est le titre";
        Request addQuestionsRequest = createRequest("/v1/questions")
                .addParameter("question",
                    "{title: '" + documentOneTitle + "', summary: 'ceci est le resume',"
                        + " submissionDate: '" + (new Date()).getTime() + "',"
                        + " themes: ['test', 'questionOne'], type: 'question',"
                        + " participants: [{'id':  '" + expertId + "'}],"
                        + " supervisors: [{'id': '" + supervisorId + "'}],"
                        + " privacy : 'PUBLIC' }")
                .Post()
                .addHeader("Authorization", "Bearer " + supervisorToken);

        HttpResponse addQuestionResponse = addQuestionsRequest.execute().returnResponse();
        Assert.assertEquals(200, addQuestionResponse.getStatusLine().getStatusCode());

        // Create second document
        String documentTwoTitle = "There s someone missing. The question s Who?";
        addQuestionsRequest = createRequest("/v1/questions")
                .addParameter("question",
                    "{title: '" + documentTwoTitle + "',"
                        + " summary: 'Something old, Something new, Something borrowed, Something blue.',"
                        + " submissionDate: '" + (new Date()).getTime() + "',"
                        + " themes: ['big bang two', 'Pandorica', 'River', 'Universe'], type: 'question',"
                        + " participants: [{'id': '" + expertId + "'}, {'id': '" + supervisorId + "'}],"
                        + " privacy : 'PUBLIC' }")
                .Post()
                .addHeader("Authorization", "Bearer " + supervisorToken);

        addQuestionResponse = addQuestionsRequest.execute().returnResponse();
        Assert.assertEquals(200, addQuestionResponse.getStatusLine().getStatusCode());

        // And a third document !
        String documentThreeTitle = "Private things";
        addQuestionsRequest = createRequest("/v1/questions")
                .addParameter("question",
                    "{title: '" + documentThreeTitle + "',"
                        + " summary: 'This question is private, zero access for others',"
                        + " submissionDate: '" + (new Date()).getTime() + "',"
                        + " themes: ['Universe', 'test', 'zero'], type: 'question',"
                        + " participants: [{'id': '" + supervisorId + "'}],"
                        + " clients: [{'id': '" + expertId + "'}],"
                        + " privacy : 'PRIVATE' }")
                .Post()
                .addHeader("Authorization", "Bearer " + supervisorToken);

        addQuestionResponse = addQuestionsRequest.execute().returnResponse();
        Assert.assertEquals(200, addQuestionResponse.getStatusLine().getStatusCode());

        // First search : projects where expert is participant : one and two
        Request searchRequest = createRequest("/v1/users/" + expertId + "/projects")
                .addParameter("userRole", QuestionUserRole.PARTICIPANT.name())
                .Get()
                .addHeader("Authorization", "Bearer " + supervisorToken);

        Response searchResponse = searchRequest.execute();
        String searchResultContent = searchResponse.returnContent().asString();
        JsonHelper jsonHelper = new JsonHelper(true);
        List<QuestionBean> result = jsonHelper.fromJson(searchResultContent, new TypeToken<ArrayList<QuestionBean>>(){}.getType());

        Assert.assertEquals(2, result.size());
        Assert.assertNotNull(result.get(0));
        Assert.assertNotNull(result.get(1));
        List<String> expectedTitles = Arrays.asList(documentOneTitle, documentTwoTitle);
        Assert.assertTrue(expectedTitles.contains(result.get(0).getTitle()));
        Assert.assertTrue(expectedTitles.contains(result.get(1).getTitle()));


        // Second search : project where supervisor is participant : two and three
        searchRequest = createRequest("/v1/users/" + supervisorId + "/projects")
                .addParameter("userRole", QuestionUserRole.PARTICIPANT.name())
                .Get()
                .addHeader("Authorization", "Bearer " + supervisorToken);

        searchResponse = searchRequest.execute();
        searchResultContent = searchResponse.returnContent().asString();
        result = jsonHelper.fromJson(searchResultContent, new TypeToken<ArrayList<QuestionBean>>(){}.getType());

        Assert.assertEquals(2, result.size());
        expectedTitles = Arrays.asList(documentThreeTitle, documentTwoTitle);
        Assert.assertTrue(expectedTitles.contains(result.get(0).getTitle()));
        Assert.assertTrue(expectedTitles.contains(result.get(1).getTitle()));


        // Second search : project where expert is client : three
        searchRequest = createRequest("/v1/users/" + expertId + "/projects")
                .addParameter("userRole", QuestionUserRole.CLIENT.name())
                .Get()
                .addHeader("Authorization", "Bearer " + supervisorToken);

        searchResponse = searchRequest.execute();
        searchResultContent = searchResponse.returnContent().asString();
        result = jsonHelper.fromJson(searchResultContent, new TypeToken<ArrayList<QuestionBean>>(){}.getType());

        Assert.assertEquals(1, result.size());
        Assert.assertEquals(documentThreeTitle, result.get(0).getTitle());


        // Second search : project where supervisor is supervisor : all (cause he creates them)
        searchRequest = createRequest("/v1/users/" + supervisorId + "/projects")
                .addParameter("userRole", QuestionUserRole.SUPERVISOR.name())
                .Get()
                .addHeader("Authorization", "Bearer " + supervisorToken);

        searchResponse = searchRequest.execute();
        searchResultContent = searchResponse.returnContent().asString();
        result = jsonHelper.fromJson(searchResultContent, new TypeToken<ArrayList<QuestionBean>>(){}.getType());

        Assert.assertEquals(3, result.size());
        expectedTitles = Arrays.asList(documentOneTitle, documentTwoTitle, documentThreeTitle);
        Assert.assertTrue(expectedTitles.contains(result.get(0).getTitle()));
        Assert.assertTrue(expectedTitles.contains(result.get(1).getTitle()));
        Assert.assertTrue(expectedTitles.contains(result.get(2).getTitle()));

    }
}
