package fr.ifremer.coselmar.services.indexation;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import fr.ifremer.coselmar.beans.DocumentBean;
import fr.ifremer.coselmar.persistence.entity.Privacy;
import fr.ifremer.coselmar.services.AbstractCoselmarServiceTest;
import fr.ifremer.coselmar.services.CoselmarServicesContext;
import fr.ifremer.coselmar.services.FakeCoselmarServicesContext;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class DocumentsIndexationServiceTest extends AbstractCoselmarServiceTest {

    protected FakeCoselmarServicesContext serviceContext;

    protected FakeCoselmarServicesContext getServiceContext() {

        if (serviceContext == null) {
            serviceContext = application.newServiceContext(application.newPersistenceContext(), Locale.FRANCE);
        }

        return serviceContext;
    }

    @Test
    public void testAddDocument() throws Exception {
        CoselmarServicesContext serviceContext = getServiceContext();
        DocumentsIndexationService documentsIndexationService =
            serviceContext.newService(DocumentsIndexationService.class);

        DocumentBean documentOne = new DocumentBean("document1",
            "Ceci n'est pas un document", "John Doe", "user001", Privacy.PUBLIC.name(),
            new Date(), Lists.newArrayList("document", "test"), "testDocument",
            "This is not a fake document used for test", "fr", null, "Jack, Jane",
            null, null, false, null, "http://somewhere", "no comment", null, null);

        documentsIndexationService.indexDocument(documentOne, null);

    }

    @Test
    public void testSearchDocument() throws Exception {
        populate();

        CoselmarServicesContext serviceContext = getServiceContext();
        DocumentsIndexationService documentsIndexationService =
            serviceContext.newService(DocumentsIndexationService.class);

        List<String> documentMatchingDoctorIds = documentsIndexationService.searchDocuments("doctor");
        Assert.assertEquals(1, documentMatchingDoctorIds.size());
        Assert.assertEquals("document3", documentMatchingDoctorIds.get(0));

        List<String> documentMatchingAmyIds = documentsIndexationService.searchDocuments("amy");
        Assert.assertEquals(2, documentMatchingAmyIds.size());
        Assert.assertTrue(documentMatchingAmyIds.contains("document2"));
        Assert.assertTrue(documentMatchingAmyIds.contains("document3"));

        List<String> documentMatchingTheMasterIds = documentsIndexationService.searchDocuments("the Master");
        Assert.assertTrue(documentMatchingTheMasterIds.isEmpty());

        // change with 1.2 and change on analyzer : SimpleAnalyzer-> StandardAnalyzer
//        List<String> documentMatchingPartOfSummaryIds = documentsIndexationService.searchDocuments("This is part of");
        List<String> documentMatchingPartOfSummaryIds = documentsIndexationService.searchDocuments("part");
        Assert.assertEquals(1, documentMatchingPartOfSummaryIds.size());
        Assert.assertEquals("document3", documentMatchingPartOfSummaryIds.get(0));

        List<String> documentMatchingPartOfWordIds = documentsIndexationService.searchDocuments("documenta");
        Assert.assertEquals(1, documentMatchingPartOfWordIds.size());
        Assert.assertEquals("document3", documentMatchingPartOfWordIds.get(0));

        // change with 1.2 and change on analyzer : SimpleAnalyzer-> StandardAnalyzer
//        documentMatchingPartOfWordIds = documentsIndexationService.searchDocuments("Thi");
        documentMatchingPartOfWordIds = documentsIndexationService.searchDocuments("Docum");
        Assert.assertEquals(3, documentMatchingPartOfWordIds.size());
        Assert.assertTrue(documentMatchingPartOfWordIds.contains("document1"));
        Assert.assertTrue(documentMatchingPartOfWordIds.contains("document2"));
        Assert.assertTrue(documentMatchingPartOfWordIds.contains("document3"));

    }

    @Test
    public void testUpdateDocument() throws Exception {

        CoselmarServicesContext serviceContext = getServiceContext();
        DocumentsIndexationService documentsIndexationService =
            serviceContext.newService(DocumentsIndexationService.class);

        DocumentBean documentOne = new DocumentBean("document1",
            "Ceci n'est pas un document", "John Doe", "user001", Privacy.PUBLIC.name(),
            new Date(), Lists.newArrayList("document", "test"), "testDocument",
            "This is not a fake document used for test", "fr", null, "Jack, Jane",
            null, null, false, null, "http://somewhere", "no comment", null, null);

        documentsIndexationService.indexDocument(documentOne, null);

        List<String> documentMatchingDocumentIds = documentsIndexationService.searchDocuments("document");
        Assert.assertEquals(1, documentMatchingDocumentIds.size());
        Assert.assertEquals("document1", documentMatchingDocumentIds.get(0));

        List<String> documentMatchingJackIds = documentsIndexationService.searchDocuments("jack");
        Assert.assertEquals(1, documentMatchingJackIds.size());
        Assert.assertTrue(documentMatchingJackIds.contains("document1"));

        List<String> documentMatchingTheMasterIds = documentsIndexationService.searchDocuments("James");
        Assert.assertTrue(documentMatchingTheMasterIds.isEmpty());

//        List<String> documentMatchingPartOfSummaryIds = documentsIndexationService.searchDocuments("This is not a fake document");
        List<String> documentMatchingPartOfSummaryIds = documentsIndexationService.searchDocuments("fake document");
        Assert.assertEquals(1, documentMatchingPartOfSummaryIds.size());
        Assert.assertEquals("document1", documentMatchingPartOfSummaryIds.get(0));

        documentOne = new DocumentBean("document1",
            "Doc Premier", "John Doe", "user001", Privacy.PUBLIC.name(),
            new Date(), Lists.newArrayList("test", "update"), "testDocument",
//            "This is a fake doc updated for test", "fr", null, "James, JJ",
            "This is a faked doct updated for test", "fr", null, "James, JJ",
            null, null, false, null, "http://somewhere", "no comment", null, null);

        documentsIndexationService.updateDocument(documentOne, null);

        documentMatchingDocumentIds = documentsIndexationService.searchDocuments("document");
        Assert.assertTrue(documentMatchingDocumentIds.isEmpty());

        documentMatchingJackIds = documentsIndexationService.searchDocuments("jack");
        Assert.assertTrue(documentMatchingJackIds.isEmpty());

        documentMatchingTheMasterIds = documentsIndexationService.searchDocuments("James");
        Assert.assertEquals(1, documentMatchingTheMasterIds.size());
        Assert.assertEquals("document1", documentMatchingTheMasterIds.get(0));

//        documentMatchingPartOfSummaryIds = documentsIndexationService.searchDocuments("This is not a fake document");
        documentMatchingPartOfSummaryIds = documentsIndexationService.searchDocuments("fake document");
        Assert.assertTrue(documentMatchingPartOfSummaryIds.isEmpty());

//        documentMatchingPartOfSummaryIds = documentsIndexationService.searchDocuments("This is a fake doc");
        documentMatchingPartOfSummaryIds = documentsIndexationService.searchDocuments("fake doct");
        Assert.assertEquals(1, documentMatchingPartOfSummaryIds.size());
        Assert.assertEquals("document1", documentMatchingPartOfSummaryIds.get(0));

        List<String> documentMatchingKeywordIds = documentsIndexationService.searchDocuments("update");
        Assert.assertEquals(1, documentMatchingKeywordIds.size());
        Assert.assertEquals("document1", documentMatchingKeywordIds.get(0));

        List<String> documentMatchingPremierIds = documentsIndexationService.searchDocuments("premier");
        Assert.assertEquals(1, documentMatchingPremierIds.size());
        Assert.assertEquals("document1", documentMatchingPremierIds.get(0));

    }

    @Test
    public void testDeleteDocument() throws Exception {
        populate();

        CoselmarServicesContext serviceContext = getServiceContext();
        DocumentsIndexationService documentsIndexationService =
            serviceContext.newService(DocumentsIndexationService.class);

        //Delete document2
        documentsIndexationService.deleteDocument("document2");

        List<String> documentMatchingDoctorIds = documentsIndexationService.searchDocuments("doctor");
        Assert.assertEquals(1, documentMatchingDoctorIds.size());
        Assert.assertEquals("document3", documentMatchingDoctorIds.get(0));

        List<String> documentMatchingAmyIds = documentsIndexationService.searchDocuments("amy");
        Assert.assertEquals(1, documentMatchingAmyIds.size());
        Assert.assertTrue(documentMatchingAmyIds.contains("document3"));

        List<String> documentMatchingTheMasterIds = documentsIndexationService.searchDocuments("Another");
        Assert.assertTrue(documentMatchingTheMasterIds.isEmpty());

        List<String> documentMatchingPartOfSummaryIds = documentsIndexationService.searchDocuments("part");
        Assert.assertEquals(1, documentMatchingPartOfSummaryIds.size());
        Assert.assertEquals("document3", documentMatchingPartOfSummaryIds.get(0));


    }

    @Test
    public void testSearchDocumentWithKeywords() throws Exception {
        CoselmarServicesContext serviceContext = getServiceContext();
        DocumentsIndexationService documentsIndexationService =
            serviceContext.newService(DocumentsIndexationService.class);

        String documentOneId = "testSearchMultiple_document1";
        DocumentBean documentOne = new DocumentBean(documentOneId,
            "Ceci n'est pas un document", "John Doe", "user001", Privacy.PUBLIC.name(),
            new Date(), Lists.newArrayList("document", "test"), "testDocument",
            "This is not a fake document used for test", "fr", null, "Jack, Jane",
            null, null, false, null, "http://somewhere", "no comment", null, null);

        documentsIndexationService.indexDocument(documentOne, null);


        String documentTwoId = "testSearchMultiple_document2";
        DocumentBean documentTwo = new DocumentBean(documentTwoId,
            "Tardis documentation", "The Doctor", "user003", Privacy.PUBLIC.name(),
            new Date(), Lists.newArrayList("tardis", "documentation", "old", "new", "borrowed", "blue"), "testDocument",
            "This is part of document about the TARDIS", "fr", null, "The Doctor, Rose, Amy, River, Clara",
            null, null, false, null, "http://tardis.wikia.com/wiki/TARDIS", "no comment", null, null);
        documentsIndexationService.indexDocument(documentTwo, null);


        List<String> documentMatchingDoctorIds = documentsIndexationService.searchDocuments(Arrays.asList("doctor"));
        Assert.assertEquals(1, documentMatchingDoctorIds.size());
        Assert.assertEquals(documentTwoId, documentMatchingDoctorIds.get(0));

        List<String> documentMatchingAmyAndDoctorIds = documentsIndexationService.searchDocuments(Arrays.asList("amy", "doctor"));
        Assert.assertEquals(1, documentMatchingAmyAndDoctorIds.size());
        Assert.assertTrue(documentMatchingAmyAndDoctorIds.contains(documentTwoId));

        List<String> documentMatchingAmyAndJackIds = documentsIndexationService.searchDocuments(Arrays.asList("amy", "jack"));
        Assert.assertTrue(documentMatchingAmyAndJackIds.isEmpty());

        List<String> documentMatchingAmyAndRoseIds = documentsIndexationService.searchDocuments(Arrays.asList("amy", "Sarah Jane"));
        Assert.assertTrue(documentMatchingAmyAndRoseIds.isEmpty());

        List<String> documentMatchingDocumentIds = documentsIndexationService.searchDocuments(Arrays.asList("document", "about"));
        Assert.assertEquals(1, documentMatchingDocumentIds.size());
//        Assert.assertTrue(documentMatchingDocumentIds.contains(documentOneId));
        Assert.assertTrue(documentMatchingDocumentIds.contains(documentTwoId));

    }

    public void populate() throws Exception {
        CoselmarServicesContext serviceContext = getServiceContext();
        DocumentsIndexationService documentsIndexationService =
            serviceContext.newService(DocumentsIndexationService.class);

        DocumentBean documentOne = new DocumentBean("document1",
            "Ceci n'est pas un document", "John Doe", "user001", Privacy.PUBLIC.name(),
            new Date(), Lists.newArrayList("document", "test"), "testDocument",
            "This is not a fake document used for test", "fr", null, "Jack, Jane",
            null, null, false, null, "http://somewhere", "no comment", null, null);

        documentsIndexationService.indexDocument(documentOne, null);

        DocumentBean documentTwo = new DocumentBean("document2",
            "Another document", "Amy Pond", "user002", Privacy.PUBLIC.name(),
            new Date(), Lists.newArrayList("document", "test", "fish"), "testDocument",
            "This is just an other document used for test", "fr", null, "Amy, Rory",
            null, null, false, null, "http://somewhere", "no comment", null, null);

        documentsIndexationService.indexDocument(documentTwo, null);

        DocumentBean documentThree = new DocumentBean("document3",
            "Tardis documentation", "The Doctor", "user003", Privacy.PUBLIC.name(),
            new Date(), Lists.newArrayList("tardis", "documentation", "old", "new", "borrowed", "blue"), "testDocument",
            "This is part of documentation about the TARDIS", "fr", null, "The Doctor, Rose, Amy, River, Clara",
            null, null, false, null, "http://tardis.wikia.com/wiki/TARDIS", "no comment", null, null);

        documentsIndexationService.indexDocument(documentThree, null);

    }

    @After
    public void cleanUp() throws Exception {
        CoselmarServicesContext serviceContext = getServiceContext();
        DocumentsIndexationService documentsIndexationService =
            serviceContext.newService(DocumentsIndexationService.class);
        documentsIndexationService.cleanIndex();

    }
}
