package fr.ifremer.coselmar.services.indexation;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Sets;
import fr.ifremer.coselmar.beans.QuestionBean;
import fr.ifremer.coselmar.beans.QuestionSearchBean;
import fr.ifremer.coselmar.persistence.entity.Privacy;
import fr.ifremer.coselmar.persistence.entity.Status;
import fr.ifremer.coselmar.services.AbstractCoselmarServiceTest;
import fr.ifremer.coselmar.services.CoselmarServicesContext;
import fr.ifremer.coselmar.services.FakeCoselmarServicesContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.util.DateUtil;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class QuestionsIndexationServiceTest extends AbstractCoselmarServiceTest {

    protected FakeCoselmarServicesContext serviceContext;

    protected FakeCoselmarServicesContext getServiceContext() {

        if (this.serviceContext == null) {
            this.serviceContext = application.newServiceContext(application.newPersistenceContext(), Locale.FRANCE);
        }

        return this.serviceContext;
    }

    @Test
    public void testAddQuestion() throws Exception {
        CoselmarServicesContext serviceContext = getServiceContext();
        QuestionsIndexationService questionsIndexationService =
            serviceContext.newService(QuestionsIndexationService.class);

        QuestionBean question = new QuestionBean();
        question.setTitle("Awesome question");
        question.setSummary("Can we, just once, ask about it ?");
        question.setDeadline(DateUtil.createDateAfterToday(1, 1, 1));
        question.setExternalExperts(Sets.newHashSet("Amelia", "Rory", "River"));
        question.setSubmissionDate(new Date());
        question.setStatus(Status.OPEN.name());
        question.setPrivacy(Privacy.PUBLIC.name());
        question.setThemes(Sets.newHashSet("TARDIS", "Universe", "Time", "Space"));
        question.setId("question_" + System.currentTimeMillis());

        questionsIndexationService.indexQuestion(question);

    }

    @Test
    public void testSearchQuestion() throws Exception {

        QuestionBean questionOne = new QuestionBean();
        String questionOneId = "question_1_test_search" + System.currentTimeMillis();
        questionOne.setId(questionOneId);
        questionOne.setTitle("Awesome question");
        questionOne.setSummary("Can we, just once, ask about it ?");
        questionOne.setDeadline(DateUtil.createDateAfterToday(1, 0, 1));
        questionOne.setExternalExperts(Sets.newHashSet("Amelia", "Rory", "River"));
        questionOne.setSubmissionDate(new Date());
        questionOne.setStatus(Status.OPEN.name());
        questionOne.setPrivacy(Privacy.PUBLIC.name());
        questionOne.setThemes(Sets.newHashSet("TARDIS", "Universe", "Time", "Space"));

        QuestionBean questionTwo = new QuestionBean();
        String questionTwoId = "question_2_test_search" + System.currentTimeMillis();
        questionTwo.setId(questionTwoId);
        questionTwo.setTitle("The ultimate");
        questionTwo.setSummary("We need some question");
        questionTwo.setDeadline(DateUtil.createDateAfterToday(16, 0, 0));
        questionTwo.setSubmissionDate(new Date());
        questionTwo.setStatus(Status.OPEN.name());
        questionTwo.setPrivacy(Privacy.PUBLIC.name());
        questionTwo.setThemes(Sets.newHashSet("test", "question"));

        QuestionBean questionThree = new QuestionBean();
        String questionThreeId = "question_3_test_search" + System.currentTimeMillis();
        questionThree.setId(questionThreeId);
        questionThree.setTitle("There's someone missing. The question's Who?");
        questionThree.setSummary("Something old, Something new, Something borrowed, Something blue.");
        questionThree.setDeadline(DateUtil.createDateAfterToday(16, 0, 0));
        questionThree.setSubmissionDate(new Date());
        questionThree.setStatus(Status.OPEN.name());
        questionThree.setPrivacy(Privacy.PRIVATE.name());
        questionThree.setThemes(Sets.newHashSet("big bang two", "Pandorica", "River", "Universe"));


        CoselmarServicesContext serviceContext = getServiceContext();
        QuestionsIndexationService questionsIndexationService =
            serviceContext.newService(QuestionsIndexationService.class);

        questionsIndexationService.indexQuestion(questionOne);
        questionsIndexationService.indexQuestion(questionTwo);
        questionsIndexationService.indexQuestion(questionThree);

        // Ok, let's search now !
        QuestionSearchBean searchBean = new QuestionSearchBean();
        searchBean.setFullTextSearch(Arrays.asList("ultimate"));
        List<String> questionMatchingTitleIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertEquals(1, questionMatchingTitleIds.size());
        Assert.assertEquals(questionTwoId, questionMatchingTitleIds.get(0));

        searchBean.setFullTextSearch(Arrays.asList("amy"));
        List<String> questionMatchingAmyIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertTrue(questionMatchingAmyIds.isEmpty());

        searchBean.setFullTextSearch(Arrays.asList("Pandorica"));
        List<String> questionMatchingPandoricaIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertEquals(1, questionMatchingPandoricaIds.size());
        Assert.assertEquals(questionThreeId, questionMatchingPandoricaIds.get(0));

        searchBean.setFullTextSearch(Arrays.asList("universe"));
        List<String> questionMatchingUniverseIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertEquals(2, questionMatchingUniverseIds.size());
        Assert.assertTrue(questionMatchingUniverseIds.contains(questionThreeId));
        Assert.assertTrue(questionMatchingUniverseIds.contains(questionOneId));

        searchBean.setFullTextSearch(Arrays.asList("universe", "someone"));
        List<String> questionMatchingUniverseAndSomeoneIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertEquals(1, questionMatchingUniverseAndSomeoneIds.size());
        Assert.assertEquals(questionThreeId, questionMatchingUniverseAndSomeoneIds.get(0));

        searchBean.setFullTextSearch(Arrays.asList("Unknown"));
        List<String> documentMatchingUnknownIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertTrue(documentMatchingUnknownIds.isEmpty());

        searchBean.setFullTextSearch(Arrays.asList("Something blue"));
        List<String> documentMatchingPartOfSummaryIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertEquals(1, documentMatchingPartOfSummaryIds.size());
        Assert.assertEquals(questionThreeId, documentMatchingPartOfSummaryIds.get(0));

        searchBean.setFullTextSearch(Arrays.asList("Something borrowed,"));
        documentMatchingPartOfSummaryIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertEquals(1, documentMatchingPartOfSummaryIds.size());
        Assert.assertEquals(questionThreeId, documentMatchingPartOfSummaryIds.get(0));

        searchBean.setFullTextSearch(Arrays.asList("can we"));
        documentMatchingPartOfSummaryIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertEquals(1, documentMatchingPartOfSummaryIds.size());
        Assert.assertEquals(questionOneId, documentMatchingPartOfSummaryIds.get(0));

        searchBean.setFullTextSearch(null);
        searchBean.setPrivacy(Privacy.PUBLIC.name());
        List<String> documentMatchingPrivacyIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertEquals(2, documentMatchingPrivacyIds.size());
        Assert.assertTrue(documentMatchingPrivacyIds.contains(questionOneId));
        Assert.assertTrue(documentMatchingPrivacyIds.contains(questionTwoId));

        searchBean.setFullTextSearch(null);
        searchBean.setPrivacy(Privacy.PRIVATE.name());
        documentMatchingPrivacyIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertEquals(1, documentMatchingPrivacyIds.size());
        Assert.assertEquals(questionThreeId, documentMatchingPrivacyIds.get(0));

        searchBean.setFullTextSearch(Arrays.asList("Universe"));
        searchBean.setPrivacy(Privacy.PUBLIC.name());
        List<String> documentMatchingPrivacyAndUniverseKeywordIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertEquals(1, documentMatchingPrivacyAndUniverseKeywordIds.size());
        Assert.assertEquals(questionOneId, documentMatchingPrivacyAndUniverseKeywordIds.get(0));

        searchBean.setFullTextSearch(Arrays.asList("Something blue"));
        searchBean.setPrivacy(Privacy.PRIVATE.name());
        List<String> documentMatchingPrivacyAndPartOfSummaryIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertEquals(1, documentMatchingPrivacyAndPartOfSummaryIds.size());
        Assert.assertEquals(questionThreeId, documentMatchingPrivacyAndPartOfSummaryIds.get(0));

        searchBean.setFullTextSearch(Arrays.asList("Something blue"));
        searchBean.setPrivacy(Privacy.PUBLIC.name());
        documentMatchingPrivacyAndPartOfSummaryIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertTrue(documentMatchingPrivacyAndPartOfSummaryIds.isEmpty());

    }

    @Test
    public void testUpdateQuestion() throws Exception {

        QuestionBean questionOne = new QuestionBean();
        String questionOneId = "question_1_test_update_" + System.currentTimeMillis();
        questionOne.setId(questionOneId);
        questionOne.setTitle("Awesome question");
        questionOne.setSummary("Can we, just once, ask about it ?");
        questionOne.setDeadline(DateUtil.createDateAfterToday(1, 0, 1));
        questionOne.setExternalExperts(Sets.newHashSet("Amelia", "Rory", "River"));
        questionOne.setSubmissionDate(new Date());
        questionOne.setStatus(Status.OPEN.name());
        questionOne.setPrivacy(Privacy.PUBLIC.name());
        questionOne.setThemes(Sets.newHashSet("TARDIS", "Universe", "Time", "Space"));

        QuestionBean questionTwo = new QuestionBean();
        String questionTwoId = "question_2_test_update_" + System.currentTimeMillis();
        questionTwo.setId(questionTwoId);
        questionTwo.setTitle("There's someone missing. The question's Who?");
        questionTwo.setSummary("Something old, Something new, Something borrowed, Something blue.");
        questionTwo.setDeadline(DateUtil.createDateAfterToday(16, 0, 0));
        questionTwo.setSubmissionDate(new Date());
        questionTwo.setStatus(Status.OPEN.name());
        questionTwo.setPrivacy(Privacy.PUBLIC.name());
        questionTwo.setThemes(Sets.newHashSet("big bang two", "Pandorica", "River", "Universe"));


        CoselmarServicesContext serviceContext = getServiceContext();
        QuestionsIndexationService questionsIndexationService =
            serviceContext.newService(QuestionsIndexationService.class);

        questionsIndexationService.indexQuestion(questionOne);
        questionsIndexationService.indexQuestion(questionTwo);

        questionOne.setTitle("How old is the doctor who ?");
        questionOne.setPrivacy(Privacy.PRIVATE.name());
        questionsIndexationService.indexQuestion(questionOne);

        // Ok, let's search now !
        QuestionSearchBean searchBean = new QuestionSearchBean();
        searchBean.setFullTextSearch(Arrays.asList("awesome"));
        List<String> questionMatchingTitleIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertTrue(questionMatchingTitleIds.isEmpty());

        searchBean.setFullTextSearch(Arrays.asList("who"));
        questionMatchingTitleIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertEquals(2, questionMatchingTitleIds.size());
        Assert.assertTrue(questionMatchingTitleIds.contains(questionOneId));
        Assert.assertTrue(questionMatchingTitleIds.contains(questionTwoId));

        searchBean.setFullTextSearch(Arrays.asList("doctor"));
        questionMatchingTitleIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertEquals(1, questionMatchingTitleIds.size());
        Assert.assertTrue(questionMatchingTitleIds.contains(questionOneId));

        searchBean.setFullTextSearch(null);
        searchBean.setPrivacy(Privacy.PRIVATE.name());
        questionMatchingTitleIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertEquals(1, questionMatchingTitleIds.size());
        Assert.assertTrue(questionMatchingTitleIds.contains(questionOneId));

        // Edit an other time :
        questionOne.setTitle("Just let me change");
        questionOne.setPrivacy(Privacy.PUBLIC.name());
        questionsIndexationService.indexQuestion(questionOne);

        // Ok, let's search now !
        searchBean.setFullTextSearch(Arrays.asList("who"));
        searchBean.setPrivacy(Privacy.PUBLIC.name());
        questionMatchingTitleIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertEquals(1, questionMatchingTitleIds.size());
        Assert.assertEquals(questionTwoId, questionMatchingTitleIds.get(0));

    }

    @Test
    public void testDeleteQuestion() throws Exception {

        QuestionBean questionOne = new QuestionBean();
        String questionOneId = "question_1_test_delete_" + System.currentTimeMillis();
        questionOne.setId(questionOneId);
        questionOne.setTitle("Awesome question");
        questionOne.setSummary("Can we, just once, ask about it ?");
        questionOne.setDeadline(DateUtil.createDateAfterToday(1, 0, 1));
        questionOne.setExternalExperts(Sets.newHashSet("Amelia", "Rory", "River"));
        questionOne.setSubmissionDate(new Date());
        questionOne.setStatus(Status.OPEN.name());
        questionOne.setPrivacy(Privacy.PUBLIC.name());
        questionOne.setThemes(Sets.newHashSet("TARDIS", "Universe", "Time", "Space"));

        QuestionBean questionTwo = new QuestionBean();
        String questionTwoId = "question_2_test_delete_" + System.currentTimeMillis();
        questionTwo.setId(questionTwoId);
        questionTwo.setTitle("There's someone missing. The question's Who?");
        questionTwo.setSummary("Something old, Something new, Something borrowed, Something blue.");
        questionTwo.setDeadline(DateUtil.createDateAfterToday(16, 0, 0));
        questionTwo.setSubmissionDate(new Date());
        questionTwo.setStatus(Status.OPEN.name());
        questionTwo.setPrivacy(Privacy.PUBLIC.name());
        questionTwo.setThemes(Sets.newHashSet("big bang two", "Pandorica", "River", "Universe"));


        CoselmarServicesContext serviceContext = getServiceContext();
        QuestionsIndexationService questionsIndexationService =
            serviceContext.newService(QuestionsIndexationService.class);

        questionsIndexationService.indexQuestion(questionOne);
        questionsIndexationService.indexQuestion(questionTwo);

        // Ok, let's search now !
        QuestionSearchBean searchBean = new QuestionSearchBean();
        searchBean.setFullTextSearch(Arrays.asList("awesome"));
        List<String> questionMatchingTitleIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertEquals(1, questionMatchingTitleIds.size());
        Assert.assertEquals(questionOneId, questionMatchingTitleIds.get(0));

        searchBean.setFullTextSearch(Arrays.asList("missing"));
        questionMatchingTitleIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertEquals(1, questionMatchingTitleIds.size());
        Assert.assertEquals(questionTwoId, questionMatchingTitleIds.get(0));

        searchBean.setFullTextSearch(Arrays.asList("universe"));
        List<String> questionMatchingUniverseIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertEquals(2, questionMatchingUniverseIds.size());
        Assert.assertTrue(questionMatchingUniverseIds.contains(questionOneId));
        Assert.assertTrue(questionMatchingUniverseIds.contains(questionTwoId));

        // Ok, this was already tested as ok, let's delete questionOne
        questionsIndexationService.deleteQuestion(questionOneId);

        // and let's search same now !
        searchBean.setFullTextSearch(Arrays.asList("awesome"));
        questionMatchingTitleIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertTrue(questionMatchingTitleIds.isEmpty());

        searchBean.setFullTextSearch(Arrays.asList("missing"));
        questionMatchingTitleIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertEquals(1, questionMatchingTitleIds.size());
        Assert.assertTrue(questionMatchingTitleIds.contains(questionTwoId));

        searchBean.setFullTextSearch(Arrays.asList("universe"));
        questionMatchingUniverseIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertEquals(1, questionMatchingUniverseIds.size());
        Assert.assertTrue(questionMatchingUniverseIds.contains(questionTwoId));


        searchBean.setFullTextSearch(Arrays.asList("can we"));
        List<String> documentMatchingPartOfSummaryIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertTrue(documentMatchingPartOfSummaryIds.isEmpty());

        searchBean.setFullTextSearch(Arrays.asList("Something blue"));
        documentMatchingPartOfSummaryIds = questionsIndexationService.searchQuestion(searchBean);
        Assert.assertEquals(1, documentMatchingPartOfSummaryIds.size());
        Assert.assertEquals(questionTwoId, documentMatchingPartOfSummaryIds.get(0));

    }

    @Before
    public void cleanUp() throws Exception {
        CoselmarServicesContext serviceContext = getServiceContext();
        QuestionsIndexationService questionsIndexationService =
            serviceContext.newService(QuestionsIndexationService.class);

        questionsIndexationService.cleanIndex();

    }

    @Test
    public void testGetTopTerms() throws Exception {

        QuestionBean questionOne = new QuestionBean();
        String questionOneId = "question_1_test_search" + System.currentTimeMillis();
        questionOne.setId(questionOneId);
        questionOne.setTitle("Awesome question");
        questionOne.setSummary("Can we, just once, ask about it ?");
        questionOne.setDeadline(DateUtil.createDateAfterToday(1, 0, 1));
        questionOne.setExternalExperts(Sets.newHashSet("Amelia", "Rory", "River"));
        questionOne.setSubmissionDate(new Date());
        questionOne.setStatus(Status.OPEN.name());
        questionOne.setPrivacy(Privacy.PUBLIC.name());
        questionOne.setThemes(Sets.newHashSet("TARDIS", "Universe", "Time", "Space"));

        QuestionBean questionTwo = new QuestionBean();
        String questionTwoId = "question_2_test_search" + System.currentTimeMillis();
        questionTwo.setId(questionTwoId);
        questionTwo.setTitle("The ultimate");
        questionTwo.setSummary("We need some question");
        questionTwo.setDeadline(DateUtil.createDateAfterToday(16, 0, 0));
        questionTwo.setSubmissionDate(new Date());
        questionTwo.setStatus(Status.OPEN.name());
        questionTwo.setPrivacy(Privacy.PUBLIC.name());
        questionTwo.setThemes(Sets.newHashSet("test", "question"));

        QuestionBean questionThree = new QuestionBean();
        String questionThreeId = "question_3_test_search" + System.currentTimeMillis();
        questionThree.setId(questionThreeId);
        questionThree.setTitle("There's someone missing. The question 's Who?");
        questionThree.setSummary("Something old, Something new, Something borrowed, Something blue.");
        questionThree.setDeadline(DateUtil.createDateAfterToday(16, 0, 0));
        questionThree.setSubmissionDate(new Date());
        questionThree.setStatus(Status.OPEN.name());
        questionThree.setPrivacy(Privacy.PRIVATE.name());
        questionThree.setThemes(Sets.newHashSet("big bang two", "Pandorica", "River", "Universe"));


        CoselmarServicesContext serviceContext = getServiceContext();
        QuestionsIndexationService questionsIndexationService =
            serviceContext.newService(QuestionsIndexationService.class);

        questionsIndexationService.indexQuestion(questionOne);
        questionsIndexationService.indexQuestion(questionTwo);
        questionsIndexationService.indexQuestion(questionThree);

        // Ok, let's search now !
        Map<String, Long> topTerms = questionsIndexationService.getTopTerms();
        Assert.assertNotNull(topTerms);
        Assert.assertEquals(3, topTerms.get("question").longValue());
        Assert.assertEquals(2, topTerms.get("universe").longValue());
        Assert.assertEquals(1, topTerms.get("river").longValue());
        Assert.assertEquals(1, topTerms.get("test").longValue());
        Assert.assertEquals(4, topTerms.get("something").longValue());

    }

    @Test
    public void testGetTopDocumentsTerms() throws Exception {

        QuestionBean questionOne = new QuestionBean();
        String questionOneId = "question_1_test_search" + System.currentTimeMillis();
        questionOne.setId(questionOneId);
        questionOne.setTitle("Awesome question");
        questionOne.setSummary("Where is the tardis in time ?");
        questionOne.setDeadline(DateUtil.createDateAfterToday(1, 0, 1));
        questionOne.setExternalExperts(Sets.newHashSet("Amelia", "Rory", "River"));
        questionOne.setSubmissionDate(new Date());
        questionOne.setStatus(Status.OPEN.name());
        questionOne.setPrivacy(Privacy.PUBLIC.name());
        questionOne.setThemes(Sets.newHashSet("TARDIS", "Universe", "Time", "Space"));

        QuestionBean questionTwo = new QuestionBean();
        String questionThreeId = "question_3_test_search" + System.currentTimeMillis();
        questionTwo.setId(questionThreeId);
        questionTwo.setTitle("There's someone missing. The question's Who?");
        questionTwo.setSummary("Something old, Something new, Something borrowed, Something blue.");
        questionTwo.setDeadline(DateUtil.createDateAfterToday(16, 0, 0));
        questionTwo.setSubmissionDate(new Date());
        questionTwo.setStatus(Status.OPEN.name());
        questionTwo.setPrivacy(Privacy.PRIVATE.name());
        questionTwo.setThemes(Sets.newHashSet("big bang two", "Pandorica", "River", "Universe"));


        CoselmarServicesContext serviceContext = getServiceContext();
        QuestionsIndexationService questionsIndexationService =
            serviceContext.newService(QuestionsIndexationService.class);

        questionsIndexationService.indexQuestion(questionOne);
        questionsIndexationService.indexQuestion(questionTwo);

        // Ok, let's search now !
        Map<String, Long> topTerms = questionsIndexationService.getTopQuestionsTerms(Arrays.asList(questionOneId));
        Assert.assertNotNull(topTerms);
        Assert.assertEquals(1, topTerms.get("question").longValue());
        Assert.assertEquals(2, topTerms.get("tardis").longValue());
        Assert.assertEquals(2, topTerms.get("time").longValue());
        Assert.assertEquals(1, topTerms.get("space").longValue());

    }
}
