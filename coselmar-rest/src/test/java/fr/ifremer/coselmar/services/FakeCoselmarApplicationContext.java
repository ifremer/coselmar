package fr.ifremer.coselmar.services;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coselmar.config.CoselmarServicesConfig;
import fr.ifremer.coselmar.config.CoselmarServicesConfigOption;
import fr.ifremer.coselmar.persistence.CoselmarPersistenceContext;
import fr.ifremer.coselmar.persistence.CoselmarTopiaApplicationContext;
import fr.ifremer.coselmar.persistence.CoselmarTopiaPersistenceContext;
import fr.ifremer.coselmar.services.indexation.LuceneUtils;
import fr.ifremer.coselmar.services.v1.InitialisationService;
import org.apache.commons.logging.Log;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.nuiton.topia.persistence.TopiaConfigurationBuilder;
import org.nuiton.util.DateUtil;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

import static org.apache.commons.logging.LogFactory.getLog;


/**
 * @author ymartel <martel@codelutin.com>
 */
public class FakeCoselmarApplicationContext extends TestWatcher implements CoselmarApplicationContext {

    private static Log log = getLog(FakeCoselmarApplicationContext.class);

    public static final String TIMESTAMP = String.valueOf(System.nanoTime());

    protected static AtomicInteger portNumberCounter = new AtomicInteger(9999);

    protected File testBasedir;

    protected List<CoselmarTopiaPersistenceContext> openedTransactions = new LinkedList<>();

    protected CoselmarTopiaApplicationContext applicationContext;

    protected CoselmarServicesConfig configuration;

    protected LuceneUtils luceneUtils;

    protected String methodName;

    private Class<?> testClass;

    protected int currentPortNumber;

    protected final String configurationPath;

    public FakeCoselmarApplicationContext(String configurationPath) {
        this.configurationPath = configurationPath;
    }

    @Override
    protected void starting(Description description) {

        // get an available port
        currentPortNumber = getAvailablePort();
        if (log.isDebugEnabled()) {
            log.debug("Using port: " + currentPortNumber);
        }

        methodName = description.getMethodName();
        testClass = description.getTestClass();

        // get test directory
        testBasedir = getTestSpecificDirectory(
            description.getTestClass(),
            description.getMethodName());

        if (log.isDebugEnabled()) {
            log.debug("testBasedir: " + testBasedir);
        }

        init();

    }

    @Override
    public void finished(Description description) {

        close();
    }

    @Override
    public void init() {

        // --- create configuration --- //

        TopiaConfigurationBuilder topiaConfigurationBuilder = new TopiaConfigurationBuilder();
        Properties defaultvalues = topiaConfigurationBuilder.forTestDatabase(testClass, methodName).onlyCreateSchemaIfDatabaseIsEmpty().doNotValidateSchemaOnStartup().buildProperties();
        defaultvalues.put(CoselmarServicesConfigOption.DATA_DIRECTORY.getKey(), testBasedir.getAbsolutePath());

        configuration = new CoselmarServicesConfig(configurationPath, defaultvalues);

        // --- create topia application context --- //

        applicationContext = new CoselmarTopiaApplicationContext(topiaConfigurationBuilder.readProperties(defaultvalues));

        luceneUtils = new LuceneUtils(configuration);

        {//Init some users
            CoselmarTopiaPersistenceContext persistenceContext = newPersistenceContext();
            CoselmarServicesContext serviceContext = newServiceContext(persistenceContext, Locale.FRANCE);
            serviceContext.newService(InitialisationService.class).createDefaultUsers();
            persistenceContext.close();
        }

    }

    @Override
    public void close() {

        if (applicationContext != null && !applicationContext.isClosed()) {

            for (CoselmarTopiaPersistenceContext openedTransaction : openedTransactions) {

                if (log.isTraceEnabled()) {
                    log.trace("closing transaction " + openedTransaction);
                }

                if (!openedTransaction.isClosed()) {

                    openedTransaction.close();

                }

            }

            if (log.isTraceEnabled()) {
                log.trace("closing transaction " + applicationContext);
            }

            applicationContext.close();

        }

        if (luceneUtils != null ) {

            if (log.isInfoEnabled()) {
                log.info("Close Lucene Reader");
            }
            luceneUtils.closeWriter();
        }
    }

    @Override
    public CoselmarTopiaApplicationContext getTopiaApplicationContext() {
        return applicationContext;
    }

    @Override
    public CoselmarServicesConfig getApplicationConfig() {
        return configuration;
    }

    @Override
    public LuceneUtils getLuceneUtils() {
        return luceneUtils;
    }

    @Override
    public CoselmarTopiaPersistenceContext newPersistenceContext() {

        CoselmarTopiaPersistenceContext persistenceContext;

        persistenceContext = applicationContext.newPersistenceContext();

        if (log.isTraceEnabled()) {
            log.trace("opened transaction " + persistenceContext);
        }

        openedTransactions.add(persistenceContext);

        return persistenceContext;

    }

    @Override
    public FakeCoselmarServicesContext newServiceContext(CoselmarPersistenceContext persistenceContext, Locale locale) {

        FakeCoselmarServicesContext serviceContext = FakeCoselmarServicesContext.newServiceContext(
            DateUtil.createDate(1, 1, 2014),
            Locale.FRANCE,
            getApplicationConfig(),
            getTopiaApplicationContext(),
            newPersistenceContext(),
            getLuceneUtils());
        return serviceContext;

    }

    public File getTestBasedir() {
        return testBasedir;
    }

    public int getPort() {
        return currentPortNumber;
    }

    public String getMethodName() {
        return methodName;
    }

    protected int getAvailablePort() {

        int port = portNumberCounter.getAndIncrement();

        boolean portTaken = false;
        ServerSocket socket = null;

        try {

            socket = new ServerSocket(port);

        } catch (IOException e) {

            portTaken = true;

        } finally {

            if (socket != null)

                try {

                    socket.close();

                } catch (IOException e) {

                    if (log.isDebugEnabled()) {
                        log.debug("Already used port: " + port);
                    }

                }

        }

        if (portTaken) {

            port = getAvailablePort();

        }

        return port;

    }

    public static File getTestWorkdir() {
        File result;
        String base = System.getProperty("java.io.tmpdir");
        if (base == null || base.isEmpty()) {
            base = new File("").getAbsolutePath();
            if (log.isWarnEnabled()) {
                log.warn("'\"java.io.tmpdir\" not defined");
            }
        }
        result = new File(base);
        if (log.isDebugEnabled()) {
            log.debug("basedir for test " + result);
        }
        return result;
    }

    public static File getTestSpecificDirectory(Class<?> testClassName,
                                                String methodName) {

        File tempDirFile = getTestWorkdir();

        // create the directory to store database data
        String dataBasePath = testClassName.getName()
                              + File.separator // a directory with the test class name
                              + methodName// a sub-directory with the method name
                              + '_'
                              + TIMESTAMP; // and a timestamp
        File databaseFile = new File(tempDirFile, dataBasePath);
        return databaseFile;
    }

}
