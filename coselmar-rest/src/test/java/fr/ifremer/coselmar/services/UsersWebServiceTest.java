package fr.ifremer.coselmar.services;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.auth0.jwt.JWTVerifier;
import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.fluent.Response;
import org.junit.Assert;
import org.junit.Test;

import java.util.Locale;
import java.util.Map;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class UsersWebServiceTest extends AbstractCoselmarWebServiceTest {

    protected FakeCoselmarServicesContext serviceContext;

    protected FakeCoselmarServicesContext getServiceContext() {

        if (serviceContext == null) {
            serviceContext = application.newServiceContext(application.newPersistenceContext(), Locale.FRANCE);
        }

        return serviceContext;
    }

    @Test
    public void testAuthentication() throws Exception {

        Request loginRequest = createRequest("/v1/users/login")
                .addParameter("mail", "admin@temporary.coselmar")
                .addParameter("password", "manager1234")
                .Post();

        Response loginResponse = loginRequest.execute();
        String loginContent = loginResponse.returnContent().asString();
        Assert.assertNotNull(loginContent);
        showTestResult(loginContent);

        Gson gson = new Gson();
        Map<String, String> map = gson.fromJson(loginContent, Map.class);

        String webSecurityKey = getServiceContext().getCoselmarServicesConfig().getWebSecurityKey();
        JWTVerifier jwtVerifier = new JWTVerifier(webSecurityKey, "audience");
        String token = map.get("jwt");
        jwtVerifier.verify(token);
    }

    @Test
    public void testNewUserAuthentication() throws Exception {

        Request loginRequest = createRequest("/v1/users/login")
                .addParameter("mail", "admin@temporary.coselmar")
                .addParameter("password", "manager1234")
                .Post();

        Response loginResponse = loginRequest.execute();
        String loginContent = loginResponse.returnContent().asString();
        Assert.assertNotNull(loginContent);
        showTestResult(loginContent);

        Gson gson = new Gson();
        Map<String, String> loginMap = gson.fromJson(loginContent, Map.class);

        String adminToken = loginMap.get("jwt");


        Request addUserRequest = createRequest("/v1/users")
                .addParameter("user", "{firstName: 'test', name: 'her', mail: 'test@test.org', role: 'supervisor', qualification: 'unit tester', password : 'iamatester'}")
                .Post()
                .addHeader("Authorization", "Bearer " + adminToken);

        HttpResponse addUserResponse = addUserRequest.execute().returnResponse();
        Assert.assertEquals(200, addUserResponse.getStatusLine().getStatusCode());

        loginRequest = createRequest("/v1/users/login")
            .addParameter("mail", "test@test.org")
            .addParameter("password", "iamatester")
                .Post();

        loginResponse = loginRequest.execute();
        loginContent = loginResponse.returnContent().asString();
        Assert.assertNotNull(loginContent);
        showTestResult(loginContent);

        Map<String, String> newUserLoginMap = gson.fromJson(loginContent, Map.class);

        String webSecurityKey = getServiceContext().getCoselmarServicesConfig().getWebSecurityKey();
        JWTVerifier jwtVerifier = new JWTVerifier(webSecurityKey, "audience");
        String token = newUserLoginMap.get("jwt");
        jwtVerifier.verify(token);
    }

    @Test
    public void testGenerateNewPassword() throws Exception {

        Request newPasswordRequest = createRequest("/v1/users/password")
            .addParameter("userMail", "lambda.expert@temporary.coselmar")
            .Post();

        Response newPasswordResponse = newPasswordRequest.execute();
        Assert.assertEquals(200, newPasswordResponse.returnResponse().getStatusLine().getStatusCode());

        // Try log now
        Request loginRequest = createRequest("/v1/users/login")
                .addParameter("mail", "lambda.expert@temporary.coselmar")
                .addParameter("password", "manager1234")
                .Post();

        Response loginResponse = loginRequest.execute();
        StatusLine loginStatusLine = loginResponse.returnResponse().getStatusLine();
        Assert.assertEquals(401, loginStatusLine.getStatusCode());

    }
}
