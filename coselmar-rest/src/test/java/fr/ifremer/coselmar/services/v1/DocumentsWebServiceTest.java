package fr.ifremer.coselmar.services.v1;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coselmar.beans.MassiveDocumentsImportResult;
import fr.ifremer.coselmar.persistence.entity.CoselmarUser;
import fr.ifremer.coselmar.persistence.entity.CoselmarUserTopiaDao;
import fr.ifremer.coselmar.persistence.entity.Document;
import fr.ifremer.coselmar.persistence.entity.DocumentTopiaDao;
import fr.ifremer.coselmar.services.AbstractCoselmarServiceTest;
import fr.ifremer.coselmar.services.FakeCoselmarServicesContext;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.Locale;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class DocumentsWebServiceTest extends AbstractCoselmarServiceTest {

    protected FakeCoselmarServicesContext serviceContext;

    protected FakeCoselmarServicesContext getServiceContext() {

        if (serviceContext == null) {
            serviceContext = application.newServiceContext(application.newPersistenceContext(), Locale.FRANCE);
        }

        return serviceContext;
    }

    @Test
    public void testUploadZipDocuments() throws Exception {

        DocumentsWebService documentsWebService = getServiceContext().newService(DocumentsWebService.class);
//        URL zipFileURL = this.getClass().getResource("/documents.zip");
        InputStream resourceAsStream = this.getClass().getResourceAsStream("/documents.zip");
        Path tempZipPath = Files.createTempFile(null, null);
        Files.copy(resourceAsStream, tempZipPath, StandardCopyOption.REPLACE_EXISTING);
        resourceAsStream.close();
        File zipFile = tempZipPath.toFile();
        Assert.assertNotNull(zipFile);
        Assert.assertTrue(zipFile.exists());

        // create a test user
        CoselmarUserTopiaDao coselmarUserDao = getServiceContext().getPersistenceContext().getCoselmarUserDao();
        CoselmarUser coselmarUser = coselmarUserDao.create();
        coselmarUser.setName("Peuplu");
        coselmarUser.setFirstname("Jean");
        getServiceContext().getPersistenceContext().commit();

        MassiveDocumentsImportResult importResult = documentsWebService.importFromZip(zipFile, coselmarUser);
        Assert.assertNotNull(importResult);
        Assert.assertTrue(importResult.getMissingFiles().isEmpty());
        DocumentTopiaDao documentDao = getServiceContext().getPersistenceContext().getDocumentDao();
        List<Document> documents = documentDao.findAll();
        Assert.assertEquals(4, documents.size());
        String oneFilePath = documents.get(0).getFilePath();
        File oneFile = new File(oneFilePath);
        Assert.assertTrue(oneFile.exists());
    }

    @Test
    public void testUploadZipDocumentsWithErrors() throws Exception {

        DocumentsWebService documentsWebService = getServiceContext().newService(DocumentsWebService.class);
        InputStream resourceAsStream = this.getClass().getResourceAsStream("/documents_errors.zip");
        Path tempZipPath = Files.createTempFile(null, null);
        Files.copy(resourceAsStream, tempZipPath, StandardCopyOption.REPLACE_EXISTING);
        resourceAsStream.close();
        File zipFile = tempZipPath.toFile();
        Assert.assertNotNull(zipFile);
        Assert.assertTrue(zipFile.exists());

        // create a test user
        CoselmarUserTopiaDao coselmarUserDao = getServiceContext().getPersistenceContext().getCoselmarUserDao();
        CoselmarUser coselmarUser = coselmarUserDao.create();
        coselmarUser.setName("Peuplu");
        coselmarUser.setFirstname("Jean");
        getServiceContext().getPersistenceContext().commit();

        MassiveDocumentsImportResult importResult = documentsWebService.importFromZip(zipFile, coselmarUser);
        Assert.assertNotNull(importResult);
        Assert.assertFalse(importResult.getMissingFiles().isEmpty());
        Assert.assertEquals(1, importResult.getMissingFiles().size());
        Assert.assertEquals("Reunion-v0.3.pdf", importResult.getMissingFiles().get(0));
        Assert.assertFalse(importResult.getDocumentsWithoutAttachment().isEmpty());
        Assert.assertEquals(1, importResult.getDocumentsWithoutAttachment().size());
        Assert.assertEquals("Lutin", importResult.getDocumentsWithoutAttachment().get(0));
        // Should import have been rollback
        DocumentTopiaDao documentDao = getServiceContext().getPersistenceContext().getDocumentDao();
        List<Document> documents = documentDao.findAll();
        Assert.assertEquals(0, documents.size());
    }
}
