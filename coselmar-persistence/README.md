# coselmar-persistence

Toute la logique de persistence est réalisée dans ce module. Elle
s'appuie sur la librairie **Topia** pour générer les entités depuis le
modèle UML sous le fichier `src/main/xmi/coselmar-model.zargo` .
