---
-- #%L
-- Coselmar :: Persistence
-- %%
-- Copyright (C) 2014 - 2016 Ifremer, Code Lutin
-- %%
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
-- 
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/gpl-3.0.html>.
-- #L%
---

CREATE TABLE link (
    topiaId character varying(255) PRIMARY KEY,
    topiaVersion bigint NOT NULL,
    topiaCreateDate timestamp without time zone NOT NULL,
    url VARCHAR(255),
    name VARCHAR(255)
);

CREATE TABLE links_question(
    question VARCHAR(255) NOT NULL REFERENCES QUESTION(TOPIAID),
    links VARCHAR(255) NOT NULL REFERENCES LINK(TOPIAID)
);