package fr.ifremer.coselmar.config;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import fr.ifremer.coselmar.exceptions.CoselmarTechnicalException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.config.ApplicationConfig;
import org.nuiton.config.ArgumentsParserException;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class CoselmarServicesConfig {

    private static final Log log = LogFactory.getLog(CoselmarServicesConfig.class);

    protected ApplicationConfig applicationConfig;

    public CoselmarServicesConfig(String filename) {
        this(filename, null);
    }

    public CoselmarServicesConfig(String filename, Properties defaultValues) {
        applicationConfig = new ApplicationConfig();
        applicationConfig.loadDefaultOptions(CoselmarServicesConfigOption.values());
        applicationConfig.setAppName("coselmar");
        applicationConfig.setConfigFileName(filename);
        if (defaultValues != null) {
            for (Map.Entry<Object, Object> entry : defaultValues.entrySet()) {

                applicationConfig.setOption((String) entry.getKey(),
                                            (String) entry.getValue());
            }
        }
        try {
            applicationConfig.parse();
        } catch (ArgumentsParserException e) {
            throw new CoselmarTechnicalException(e);
        }
        if (log.isInfoEnabled()) {
            StringBuilder builder = new StringBuilder("Coselmar configuration:");
            builder.append("\nFilename: ").append(filename);
            List<CoselmarServicesConfigOption> options =
                Lists.newArrayList(CoselmarServicesConfigOption.values());
            for (CoselmarServicesConfigOption option : options) {
                builder.append(String.format("\n%1$-40s = %2$s",
                    option.getKey(),
                    applicationConfig.getOption(option)));
            }
            log.info(builder.toString());
        }
    }

    public Map<String, String> getTopiaProperties() {
        Map<String, String> topiaParameters = Maps.newHashMap();
        Properties properties = applicationConfig.getOptionStartsWith("hibernate");
        properties.putAll(applicationConfig.getOptionStartsWith("topia"));

        for (Object o : properties.keySet()) {
            String key = String.valueOf(o);
            String value = applicationConfig.getOption(String.class, key);
            topiaParameters.put(key, value);
        }
        return topiaParameters;
    }

    public File getDataDirectory() {
        return applicationConfig.getOptionAsFile(
            CoselmarServicesConfigOption.DATA_DIRECTORY.key);
    }

    public File getIndexDirectory() {
        File indexFile = applicationConfig.getOptionAsFile(
            CoselmarServicesConfigOption.INDEX_DIRECTORY.key);
        if (indexFile == null) {
            indexFile = applicationConfig.getOptionAsFile(CoselmarServicesConfigOption.DATA_DIRECTORY.key);
        }
        return indexFile;
    }

    /**
     * @return Le nom d'hôte du serveur SMTP.
     */
    public String getSmtpHost() {
        return applicationConfig.getOption(CoselmarServicesConfigOption.SMTP_HOST.key);
    }

    /**
     * @return Le port du serveur SMTP.
     */
    public int getSmtpPort() {
        return applicationConfig.getOptionAsInt(CoselmarServicesConfigOption.SMTP_PORT.key);
    }

    /**
     * @return L'adresse d'expéditeur pour les mails de notifications
     */
    public String getSmtpFrom() {
        return applicationConfig.getOption(CoselmarServicesConfigOption.SMTP_FROM.key);
    }

    public boolean isLogConfigurationProvided() {
        boolean logConfigurationProvided = StringUtils.isNotBlank(
            applicationConfig.getOption(CoselmarServicesConfigOption.LOG_CONFIGURATION_FILE.key));
        return logConfigurationProvided;
    }

    public File getLogConfigurationFile() {
        return applicationConfig.getOptionAsFile(
            CoselmarServicesConfigOption.LOG_CONFIGURATION_FILE.key);
    }

    public boolean isDevMode() {
        boolean isDevMode = applicationConfig.getOptionAsBoolean(
            CoselmarServicesConfigOption.DEV_MODE.key);
        return isDevMode;
    }

    public String getApplicationUrl() {
        return applicationConfig.getOption(CoselmarServicesConfigOption.APPLICATION_URL.key);
    }

    public String getWebSecurityKey() {
        return applicationConfig.getOption(CoselmarServicesConfigOption.WEB_SECURITY_KEY.key);
    }

    public String getEncryptionAlgorithm() {
        return "SHA-256";
    }

    public String getVersion() {
        return applicationConfig.getOption(CoselmarServicesConfigOption.APPLICATION_VERSION.key);
    }

    public boolean isPostgresqlDatabase() {
        String hibernateDriverClass = applicationConfig.getOption("hibernate.connection.driver_class");
        return hibernateDriverClass.toLowerCase().contains("postgresql");
    }
}
