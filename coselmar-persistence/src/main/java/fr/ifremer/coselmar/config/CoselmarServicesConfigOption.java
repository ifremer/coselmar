package fr.ifremer.coselmar.config;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.config.ConfigOptionDef;
import org.nuiton.i18n.I18n;

import java.io.File;

/**
 * Created by martel on 29/10/14.
 */
public enum CoselmarServicesConfigOption implements ConfigOptionDef {

    DATA_DIRECTORY(
        "coselmar.data.directory",
        I18n.n("coselmar.configuration.data.directory"),
        "${java.io.tmpdir}/coselmar",
        File.class),

    INDEX_DIRECTORY(
        "coselmar.index.directory",
        I18n.n("coselmar.configuration.index.directory"),
        null,
        File.class),

    SMTP_HOST(
        "coselmar.smtp.host",
        "Nom d'hôte du serveur SMTP",
        "", String.class),

    SMTP_PORT(
        "coselmar.smtp.port",
        "Le port du serveur SMTP",
        "25", Integer.class),

    SMTP_FROM(
        "coselmar.smtp.from",
        "L'adresse d'expéditeur pour les mails de notifications",
        "", String.class),

    LOG_CONFIGURATION_FILE(
        "coselmar.logConfigurationFile",
        "Path to the logs config file",
        null, String.class),

    DEV_MODE(
        "coselmar.devMode",
        "Mode développement, court-circuite l'envoi de mail",
        "true", Boolean.class),

    APPLICATION_URL(
        "coselmar.application.url",
        "URL de l'application Coselmar, utilisée dans l'envoi de mail",
        "", String.class),

    WEB_SECURITY_KEY(
        "coselmar.web.security.key",
        "Clef de sécurity permettant d'encoder les token d'authentication",
        "iamageek,r3477y", String.class),

    APPLICATION_VERSION(
        "coselmar.version",
        "Version de l'application",
        "", String.class),
    ;

    protected final String key;

    protected final String description;

    protected final Class<?> type;

    protected String defaultValue;

    private CoselmarServicesConfigOption(String key, String description, String defaultValue, Class<?> type) {
        this.key = key;
        this.description = description;
        this.defaultValue = defaultValue;
        this.type = type;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Class<?> getType() {
        return type;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }

    @Override
    public boolean isTransient() {
        return false;
    }

    @Override
    public boolean isFinal() {
        return false;
    }

    @Override
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public void setTransient(boolean isTransient){
        //Nothing to do
    }

    @Override
    public void setFinal(boolean isFinal) {
        //nothing to do
    }
}
