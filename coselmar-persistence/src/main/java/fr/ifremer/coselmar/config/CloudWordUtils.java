package fr.ifremer.coselmar.config;

/*
 * #%L
 * Coselmar :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2017 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Function;

import java.util.Arrays;
import java.util.List;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class CloudWordUtils {

    public static final int CLOUD_TAG_WORD_MIN_SIZE = 3;
    public static final List<String> MANUAL_EXCLUDED_TERMS_IN_CLOUD = Arrays.asList("http");

    public static final boolean isCloudableTerm(String term) {
        return term.length() > CLOUD_TAG_WORD_MIN_SIZE && !MANUAL_EXCLUDED_TERMS_IN_CLOUD.contains(term.toLowerCase());
    }

    public static final Function<String, String> SQLIFY_STRING = new Function<String, String>() {
        @Override
        public String apply(String input) {
            return "'" + input + "'";
        }
    };
}
