package fr.ifremer.coselmar.persistence.entity;

/*
 * #%L
 * Coselmar :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coselmar.persistence.DaoUtils;
import fr.ifremer.coselmar.persistence.SearchRequestBean;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CoselmarUserTopiaDao extends AbstractCoselmarUserTopiaDao<CoselmarUser> {

    /**
     * Retrieve all user that contains part of the given keywords in at least
     * one property.
     * For Example, a search witj keywords ['John', 'Doe'] will return users
     * that contains "John" in one of these properties and Doe in one these properties :
     * <ul>
     * <li>User with firstName John and mail containing doe</li>
     * <li>User with firstName Johnathan and name doe</li>
     * <li>User with mail john.doe@knowhere.uni</li>
     * </ul>
     *
     * @param onlyActive : to filter on only active users
     * @param keywords : List of String to search (if several, a "AND" operation
     *                 is done)
     *
     * @return Users thats contains in a property at least all given keywords
     *
     */
    public List<CoselmarUser> findAllLikeKeywords(List<String> keywords, boolean onlyActive) {

        StringBuilder hqlBuilder = new StringBuilder("FROM " + CoselmarUser.class.getName() + " CU");
        hqlBuilder.append(" WHERE 1=1 "); // Just because next clause will begin with operator

        Map<String, Object> args = new HashMap<>();

        // only active ?
        if (onlyActive) {
            String activeCondition = DaoUtils.andAttributeEquals("CU", CoselmarUser.PROPERTY_ACTIVE, args, true);
            hqlBuilder.append(activeCondition);
        }

        // Now lets start keywords on each text fields on User !
        if (keywords != null) {
            for (String keyword : keywords) {
                // a keyword is an AND clause with lot of OR clauses inside !
                hqlBuilder.append(" AND ( 1=0 "); // Same as previously : need to have an insignificant clause to add all OR after
                String orFirstname = DaoUtils.orAttributeLike("CU", CoselmarUser.PROPERTY_FIRSTNAME, args, keyword);
                hqlBuilder.append(orFirstname);

                // Name
                String orName = DaoUtils.orAttributeLike("CU", CoselmarUser.PROPERTY_NAME, args, keyword);
                hqlBuilder.append(orName);

                // Name
                String orMail = DaoUtils.orAttributeLike("CU", CoselmarUser.PROPERTY_MAIL, args, keyword);
                hqlBuilder.append(orMail);

                // Name
                String orOrganization = DaoUtils.orAttributeLike("CU", CoselmarUser.PROPERTY_ORGANIZATION, args, keyword);
                hqlBuilder.append(orOrganization);

                // Name
                String orQualification = DaoUtils.orAttributeLike("CU", CoselmarUser.PROPERTY_QUALIFICATION, args, keyword);
                hqlBuilder.append(orQualification);

                hqlBuilder.append(" ) "); // Close this keywords clause
            }
        }

        List<CoselmarUser> coselmarUsers = forHql(hqlBuilder.toString(), args).findAll();

        return coselmarUsers;
    }

    /**
     * Retrieve all user that are like the example. We search all
     * {@link fr.ifremer.coselmar.persistence.entity.CoselmarUser} that have for
     * each properties values like ones from Example
     *
     * For Example, a search with an example {firstName :'John', name: 'Doe'}
     * will return users all users with a firstName containing 'John' and name
     * containing 'Doe' :
     * <ul>
     * <li>User with firstName John and name Doe</li>
     * <li>User with firstName Johnathan and Name doe</li>
     * <li>User with firstName John and name Doenyon</li>
     * <li>...</li>
     * </ul>
     *
     * @param example   :   Prototype of User to find. We search all users that
     *                  can have properties like ones in example.
     *
     * @return Users thats contains in a property at least all given keywords
     *
     */
    public PaginationResult<CoselmarUser> findAllByExample(CoselmarUser example, boolean activeAndInactive, SearchRequestBean searchRequest) {

        StringBuilder hqlBuilder = new StringBuilder("FROM " + CoselmarUser.class.getName() + " CU");
        hqlBuilder.append(" WHERE 1=1 "); // Just because next clause will begin with operator

        Map<String, Object> args = new HashMap<>();

        // only active ?
        if (!activeAndInactive) {
            String activeCondition = DaoUtils.andAttributeEquals("CU", CoselmarUser.PROPERTY_ACTIVE, args, example.isActive());
            hqlBuilder.append(activeCondition);

        }

        // Search on FirstName ?
        if (StringUtils.isNotBlank(example.getFirstname())) {
            String firstNameCondition = DaoUtils.andAttributeLike("CU", CoselmarUser.PROPERTY_FIRSTNAME, args, example.getFirstname());
            hqlBuilder.append(firstNameCondition);

        }

        // Search on Name ?
        if (StringUtils.isNotBlank(example.getName())) {
            String nameCondition = DaoUtils.andAttributeLike("CU", CoselmarUser.PROPERTY_NAME, args, example.getName());
            hqlBuilder.append(nameCondition);

        }

        // Search on Mail ?
        if (StringUtils.isNotBlank(example.getMail())) {
            String mailCondition = DaoUtils.andAttributeLike("CU", CoselmarUser.PROPERTY_MAIL, args, example.getMail());
            hqlBuilder.append(mailCondition);

        }

        // Search on Organization ?
        if (StringUtils.isNotBlank(example.getOrganization())) {
            String organizationCondition = DaoUtils.andAttributeLike("CU", CoselmarUser.PROPERTY_ORGANIZATION, args, example.getOrganization());
            hqlBuilder.append(organizationCondition);

        }

        // Search on Qualification ?
        if (StringUtils.isNotBlank(example.getQualification())) {
            String qualificationCondition = DaoUtils.andAttributeLike("CU", CoselmarUser.PROPERTY_QUALIFICATION, args, example.getQualification());
            hqlBuilder.append(qualificationCondition);

        }

        // Search on Qualification ?
        if (example.getRole() != null) {
            String roleCondition = DaoUtils.andAttributeEquals("CU", CoselmarUser.PROPERTY_ROLE, args, example.getRole());
            hqlBuilder.append(roleCondition);

        }

        // Now lets start keywords on each text fields on User !
        List<String> keywords = searchRequest.getFullTextSearch();
        if (keywords != null) {
            for (String keyword : keywords) {
                // a keyword is an AND clause with lot of OR clauses inside !
                hqlBuilder.append(" AND ( 1=0 "); // Same as previously : need to have an insignificant clause to add all OR after
                String orFirstname = DaoUtils.orAttributeLike("CU", CoselmarUser.PROPERTY_FIRSTNAME, args, keyword);
                hqlBuilder.append(orFirstname);

                // Name
                String orName = DaoUtils.orAttributeLike("CU", CoselmarUser.PROPERTY_NAME, args, keyword);
                hqlBuilder.append(orName);

                // Name
                String orMail = DaoUtils.orAttributeLike("CU", CoselmarUser.PROPERTY_MAIL, args, keyword);
                hqlBuilder.append(orMail);

                // Name
                String orOrganization = DaoUtils.orAttributeLike("CU", CoselmarUser.PROPERTY_ORGANIZATION, args, keyword);
                hqlBuilder.append(orOrganization);

                // Name
                String orQualification = DaoUtils.orAttributeLike("CU", CoselmarUser.PROPERTY_QUALIFICATION, args, keyword);
                hqlBuilder.append(orQualification);

                hqlBuilder.append(" ) "); // Close this keywords clause
            }
        }

        PaginationParameter paginationParameter = PaginationParameter.of(searchRequest.getPage(), searchRequest.getLimit(), CoselmarUser.PROPERTY_MAIL, false);

        PaginationResult<CoselmarUser> coselmarUsers = forHql(hqlBuilder.toString(), args).findPage(paginationParameter);

        return coselmarUsers;
    }

} //CoselmarUserTopiaDao