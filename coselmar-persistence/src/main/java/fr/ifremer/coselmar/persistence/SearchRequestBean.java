package fr.ifremer.coselmar.persistence;

/*
 * #%L
 * Coselmar :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.io.Serializable;
import java.util.List;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class SearchRequestBean implements Serializable {

    private static final long serialVersionUID = -5556404267735629642L;

    public static final int DEFAULT_PAGE_SIZE = -1;
    public static final int DEFAULT_PAGE = 0;

    // Pagination Parameters
    protected int limit;
    protected int page;

    // Global onFields request parameters
    protected List<String> fullTextSearch;// if this is given, make fullText search for all given string

    public static final SearchRequestBean newDefaultSearchRequestBean() {
        SearchRequestBean searchRequestBean = new SearchRequestBean();
        searchRequestBean.setPage(DEFAULT_PAGE);
        searchRequestBean.setLimit(DEFAULT_PAGE_SIZE);
        return searchRequestBean;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        if (limit == null) {
            this.limit = DEFAULT_PAGE_SIZE;
        } else {
            this.limit = limit;
        }
    }

    public int getPage() {
        return page;
    }

    public void setPage(Integer page) {
        if (page == null) {
            this.page = DEFAULT_PAGE;
        } else {
            this.page = page;
        }
    }

    public List<String> getFullTextSearch() {
        return fullTextSearch;
    }

    public void setFullTextSearch(List<String> fullTextSearch) {
        this.fullTextSearch = fullTextSearch;
    }
}
