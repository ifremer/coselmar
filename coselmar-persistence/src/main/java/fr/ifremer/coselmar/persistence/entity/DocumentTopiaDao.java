package fr.ifremer.coselmar.persistence.entity;

/*
 * #%L
 * Coselmar :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coselmar.beans.DocumentSearchExample;
import fr.ifremer.coselmar.persistence.DaoUtils;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DocumentTopiaDao extends AbstractDocumentTopiaDao<Document> {

    public PaginationResult<Document> findPaginatedContainingAllKeywords(List<String> keywords, PaginationParameter page) {

        StringBuilder hqlBuilder = new StringBuilder("FROM " + Document.class.getName() + " D");
        Map<String, Object> args = new HashMap<>();

        if (keywords != null) {
            hqlBuilder.append(" WHERE ( 1 = 0 ");
            for (String keyword : keywords) {
                String nameClause = DaoUtils.orAttributeLike("D", Document.PROPERTY_NAME, args, keyword);
                String summaryClause = DaoUtils.orAttributeLike("D", Document.PROPERTY_SUMMARY, args, keyword);
                String authorsClause = DaoUtils.orAttributeLike("D", Document.PROPERTY_AUTHORS, args, keyword);
                String containsKeyword = DaoUtils.orAttributeContains("D", Document.PROPERTY_KEYWORDS, args, keyword);

                hqlBuilder.append(nameClause);
                hqlBuilder.append(summaryClause);
                hqlBuilder.append(authorsClause);
                hqlBuilder.append(containsKeyword);
            }
            hqlBuilder.append(" )");
        }

        String hql = hqlBuilder.toString();
        Long count = forHql(hql).count();
        List<Document> documents = forHql(hql, args).find(page);

        PaginationResult paginationResult = PaginationResult.of(documents, count, page);
        return paginationResult;
    }

    public List<Document> findAllFilterByUser(CoselmarUser currentUser, List<String> keywords) {

        StringBuilder hqlBuilder = new StringBuilder("SELECT DISTINCT(D) FROM " + Document.class.getName() + " D"
            + " LEFT OUTER JOIN D." + Document.PROPERTY_RESTRICTED_LIST + " CUG ");

        Map<String, Object> args = new HashMap<>();

        // can list all public document
        String privacyPublicCondition = DaoUtils.getQueryForAttributeEquals("D", Document.PROPERTY_PRIVACY, args, Privacy.PUBLIC, "");
        hqlBuilder.append(" WHERE ( " + privacyPublicCondition);

        // Can list his own document
        String ownerCondition = DaoUtils.orAttributeEquals("D", Document.PROPERTY_OWNER, args, currentUser);
        hqlBuilder.append(ownerCondition);

        // For limited access, check if user is in a restricted list
        String participantCondition = DaoUtils.orAttributeContains("CUG", CoselmarUserGroup.PROPERTY_MEMBERS, args, currentUser);
        hqlBuilder.append(participantCondition + ")");

        // Manage keywords search in : title, summary, authors and keywords

        if (keywords != null) {
            hqlBuilder.append(" AND ( 1 = 0 ");
            for (String keyword : keywords) {
                String nameClause = DaoUtils.orAttributeLike("D", Document.PROPERTY_NAME, args, keyword);
                String summaryClause = DaoUtils.orAttributeLike("D", Document.PROPERTY_SUMMARY, args, keyword);
                String authorsClause = DaoUtils.orAttributeLike("D", Document.PROPERTY_AUTHORS, args, keyword);
                String containsKeyword = DaoUtils.orAttributeContains("D", Document.PROPERTY_KEYWORDS, args, keyword);

                hqlBuilder.append(nameClause);
                hqlBuilder.append(summaryClause);
                hqlBuilder.append(authorsClause);
                hqlBuilder.append(containsKeyword);
            }
            hqlBuilder.append(" )");
        }


        List<Document> documents = forHql(hqlBuilder.toString(), args).findAll();

        return documents;
    }

    public PaginationResult<Document> findAllByExample(CoselmarUser userFilter, DocumentSearchExample searchExample) {

        StringBuilder hqlWithoutSelectBuilder = new StringBuilder(" FROM " + Document.class.getName() + " D"
            + " LEFT OUTER JOIN D." + Document.PROPERTY_RESTRICTED_LIST + " CUG "
            + " LEFT OUTER JOIN D." + Document.PROPERTY_OWNER + " DO ");
        hqlWithoutSelectBuilder.append(" WHERE 1=1 "); // Just because next clause will begin with operator

        Map<String, Object> args = new HashMap<>();

        // If there is a filter on User : list public, owned and ones he is on restricted list.
        if (userFilter != null) {
            // can list all public document
            String privacyPublicCondition = DaoUtils.getQueryForAttributeEquals("D", Document.PROPERTY_PRIVACY, args, Privacy.PUBLIC, "");
            hqlWithoutSelectBuilder.append(" AND ( " + privacyPublicCondition);

            // Can list his own document
            String ownerCondition = DaoUtils.orAttributeEquals("D", Document.PROPERTY_OWNER, args, userFilter);
            hqlWithoutSelectBuilder.append(ownerCondition);

            // For limited access, check if user is in a restricted list
            String participantCondition = DaoUtils.orAttributeContains("CUG", CoselmarUserGroup.PROPERTY_MEMBERS, args, userFilter);
            hqlWithoutSelectBuilder.append(participantCondition + ")");
        }


        // Manage example
        Document example = searchExample.getExample();
        if (example != null) {
            hqlWithoutSelectBuilder.append(" AND ( 1 = 1 ");

            if (StringUtils.isNotBlank(example.getName())) {
                String nameCondition = DaoUtils.andAttributeLike("D", Document.PROPERTY_NAME, args, example.getName());
                hqlWithoutSelectBuilder.append(nameCondition);
            }

            if (StringUtils.isNotBlank(example.getAuthors())) {
                String authorsCondition = DaoUtils.andAttributeLike("D", Document.PROPERTY_AUTHORS, args, example.getAuthors());
                hqlWithoutSelectBuilder.append(authorsCondition);
            }

            if (StringUtils.isNotBlank(example.getLicense())) {
                String licenseCondition = DaoUtils.andAttributeLike("D", Document.PROPERTY_LICENSE, args, example.getLicense());
                hqlWithoutSelectBuilder.append(licenseCondition);
            }

            if (StringUtils.isNotBlank(example.getType())) {
                String typeCondition = DaoUtils.andAttributeLike("D", Document.PROPERTY_TYPE, args, example.getType());
                hqlWithoutSelectBuilder.append(typeCondition);
            }

            if (example.getPrivacy() != null ) {
                String privacyCondition = DaoUtils.andAttributeEquals("D", Document.PROPERTY_PRIVACY, args, example.getPrivacy());
                hqlWithoutSelectBuilder.append(privacyCondition);
            }

            if (example.getKeywords() != null && !example.getKeywords().isEmpty()) {
                for (String keyword : example.getKeywords()) {
                    String keywordCondition = DaoUtils.andAttributeContains("D", Document.PROPERTY_KEYWORDS, args, keyword);
                    hqlWithoutSelectBuilder.append(keywordCondition);
                }
            }

            if (example.getOwner() != null) {
                String ownerCondition = DaoUtils.andAttributeEquals("D", Document.PROPERTY_OWNER, args, example.getOwner());
                hqlWithoutSelectBuilder.append(ownerCondition);
            }


            // Now try to find user with given name
            if (StringUtils.isNotBlank(searchExample.getOwnerName())) {

                String activeCondition = DaoUtils.andAttributeEquals("DO", CoselmarUser.PROPERTY_ACTIVE, args, true);
                hqlWithoutSelectBuilder.append(activeCondition);

                // try to find name in user#firstName or user#name!
                hqlWithoutSelectBuilder.append(" AND ( 1=0 "); // Same as previously : need to have an insignificant clause to add all OR after
                String orFirstname = DaoUtils.orAttributeLike("DO", CoselmarUser.PROPERTY_FIRSTNAME, args, searchExample.getOwnerName());
                hqlWithoutSelectBuilder.append(orFirstname);

                // Name
                String orName = DaoUtils.orAttributeLike("DO", CoselmarUser.PROPERTY_NAME, args, searchExample.getOwnerName());
                hqlWithoutSelectBuilder.append(orName);

                hqlWithoutSelectBuilder.append(" ) "); // Close this keywords clause
            }

            // Manage date range
            if (searchExample.getPublicationBeforeDate() != null) {
                String publicationDateConditionBefore = DaoUtils.andAttributeLesserOrEquals("D", Document.PROPERTY_PUBLICATION_DATE, args, searchExample.getPublicationBeforeDate());
                hqlWithoutSelectBuilder.append(publicationDateConditionBefore);
            }

            if (searchExample.getPublicationAfterDate() != null) {
                String publicationDateConditionAfter = DaoUtils.andAttributeGreaterOrEquals("D", Document.PROPERTY_PUBLICATION_DATE, args, searchExample.getPublicationAfterDate());
                hqlWithoutSelectBuilder.append(publicationDateConditionAfter);
            }

            if (searchExample.getDepositBeforeDate() != null) {
                String depositDateConditionBefore = DaoUtils.andAttributeLesserOrEquals("D", Document.PROPERTY_DEPOSIT_DATE, args, searchExample.getDepositBeforeDate());
                hqlWithoutSelectBuilder.append(depositDateConditionBefore);
            }

            if (searchExample.getDepositAfterDate() != null) {
                String depositDateConditionAfter = DaoUtils.andAttributeGreaterOrEquals("D", Document.PROPERTY_DEPOSIT_DATE, args, searchExample.getDepositAfterDate());
                hqlWithoutSelectBuilder.append(depositDateConditionAfter);
            }

            hqlWithoutSelectBuilder.append(" ) ");
        }


        // Manage keywords search in : title, summary, authors and keywords
        if (searchExample.getFullTextSearch() != null && !searchExample.getFullTextSearch().isEmpty()) {
            hqlWithoutSelectBuilder.append(" AND ( 1 = 0 ");
            for (String keyword : searchExample.getFullTextSearch()) {
                String nameClause = DaoUtils.orAttributeLike("D", Document.PROPERTY_NAME, args, keyword);
                String summaryClause = DaoUtils.orAttributeLike("D", Document.PROPERTY_SUMMARY, args, keyword);
                String authorsClause = DaoUtils.orAttributeLike("D", Document.PROPERTY_AUTHORS, args, keyword);
                String containsKeyword = DaoUtils.orAttributeContains("D", Document.PROPERTY_KEYWORDS, args, keyword);

                hqlWithoutSelectBuilder.append(nameClause);
                hqlWithoutSelectBuilder.append(summaryClause);
                hqlWithoutSelectBuilder.append(authorsClause);
                hqlWithoutSelectBuilder.append(containsKeyword);
            }
            hqlWithoutSelectBuilder.append(" )");
        }

        // Add the prefix for order clause
        searchExample.setOrderClause("D." + searchExample.getOrderClause());

        PaginationParameter paginationParameter = searchExample.getPaginationParameter();

        // Cannot use forHql().findPage due to topia bug with joins
        // count total elements
        StringBuilder hqlCountBuilder = new StringBuilder("SELECT count(D." + Document.PROPERTY_TOPIA_ID + ") ").append(hqlWithoutSelectBuilder);
        Long count = findUnique(hqlCountBuilder.toString(), args);
        // Get elements for wanted page
        StringBuilder hqlSelectBuilder = new StringBuilder("SELECT DISTINCT(D) ").append(hqlWithoutSelectBuilder);
        List<Document> documents = forHql(hqlSelectBuilder.toString(), args).find(paginationParameter);

        PaginationResult paginatedDocuments = PaginationResult.of(documents, count, paginationParameter);

        return paginatedDocuments;
    }

    public List<String> findCitationsByDocumentExample(CoselmarUser userFilter, DocumentSearchExample searchExample) {

        StringBuilder hqlBuilder = new StringBuilder("SELECT DISTINCT(D." + Document.PROPERTY_CITATION + ") FROM " + Document.class.getName() + " D"
            + " LEFT OUTER JOIN D." + Document.PROPERTY_RESTRICTED_LIST + " CUG "
            + " LEFT OUTER JOIN D." + Document.PROPERTY_OWNER + " DO ");
        hqlBuilder.append(" WHERE 1=1 "); // Just because next clause will begin with operator

        Map<String, Object> args = new HashMap<>();

        // If there is a filter on User : list public, owned and ones he is on restricted list.
        if (userFilter != null) {
            // can list all public document
            String privacyPublicCondition = DaoUtils.getQueryForAttributeEquals("D", Document.PROPERTY_PRIVACY, args, Privacy.PUBLIC, "");
            hqlBuilder.append(" AND ( " + privacyPublicCondition);

            // Can list his own document
            String ownerCondition = DaoUtils.orAttributeEquals("D", Document.PROPERTY_OWNER, args, userFilter);
            hqlBuilder.append(ownerCondition);

            // For limited access, check if user is in a restricted list
            String participantCondition = DaoUtils.orAttributeContains("CUG", CoselmarUserGroup.PROPERTY_MEMBERS, args, userFilter);
            hqlBuilder.append(participantCondition + ")");
        }


        // Manage example
        Document example = searchExample.getExample();
        if (example != null) {
            hqlBuilder.append(" AND ( 1 = 1 ");

            if (StringUtils.isNotBlank(example.getName())) {
                String nameCondition = DaoUtils.andAttributeLike("D", Document.PROPERTY_NAME, args, example.getName());
                hqlBuilder.append(nameCondition);
            }

            if (StringUtils.isNotBlank(example.getAuthors())) {
                String authorsCondition = DaoUtils.andAttributeLike("D", Document.PROPERTY_AUTHORS, args, example.getAuthors());
                hqlBuilder.append(authorsCondition);
            }

            if (StringUtils.isNotBlank(example.getLicense())) {
                String licenseCondition = DaoUtils.andAttributeLike("D", Document.PROPERTY_LICENSE, args, example.getLicense());
                hqlBuilder.append(licenseCondition);
            }

            if (StringUtils.isNotBlank(example.getType())) {
                String typeCondition = DaoUtils.andAttributeLike("D", Document.PROPERTY_TYPE, args, example.getType());
                hqlBuilder.append(typeCondition);
            }

            if (example.getPrivacy() != null ) {
                String privacyCondition = DaoUtils.andAttributeEquals("D", Document.PROPERTY_PRIVACY, args, example.getPrivacy());
                hqlBuilder.append(privacyCondition);
            }

            if (example.getKeywords() != null && !example.getKeywords().isEmpty()) {
                for (String keyword : example.getKeywords()) {
                    String keywordCondition = DaoUtils.andAttributeContains("D", Document.PROPERTY_KEYWORDS, args, keyword);
                    hqlBuilder.append(keywordCondition);
                }
            }

            if (example.getOwner() != null) {
                String ownerCondition = DaoUtils.andAttributeEquals("D", Document.PROPERTY_OWNER, args, example.getOwner());
                hqlBuilder.append(ownerCondition);
            }


            // Now try to find user with given name
            if (StringUtils.isNotBlank(searchExample.getOwnerName())) {

                String activeCondition = DaoUtils.andAttributeEquals("DO", CoselmarUser.PROPERTY_ACTIVE, args, true);
                hqlBuilder.append(activeCondition);

                // try to find name in user#firstName or user#name!
                hqlBuilder.append(" AND ( 1=0 "); // Same as previously : need to have an insignificant clause to add all OR after
                String orFirstname = DaoUtils.orAttributeLike("DO", CoselmarUser.PROPERTY_FIRSTNAME, args, searchExample.getOwnerName());
                hqlBuilder.append(orFirstname);

                // Name
                String orName = DaoUtils.orAttributeLike("DO", CoselmarUser.PROPERTY_NAME, args, searchExample.getOwnerName());
                hqlBuilder.append(orName);

                hqlBuilder.append(" ) "); // Close this keywords clause
            }

            // Manage date range
            if (searchExample.getPublicationBeforeDate() != null) {
                String publicationDateConditionBefore = DaoUtils.andAttributeLesserOrEquals("D", Document.PROPERTY_PUBLICATION_DATE, args, searchExample.getPublicationBeforeDate());
                hqlBuilder.append(publicationDateConditionBefore);
            }

            if (searchExample.getPublicationAfterDate() != null) {
                String publicationDateConditionAfter = DaoUtils.andAttributeGreaterOrEquals("D", Document.PROPERTY_PUBLICATION_DATE, args, searchExample.getPublicationAfterDate());
                hqlBuilder.append(publicationDateConditionAfter);
            }

            if (searchExample.getDepositBeforeDate() != null) {
                String depositDateConditionBefore = DaoUtils.andAttributeLesserOrEquals("D", Document.PROPERTY_DEPOSIT_DATE, args, searchExample.getDepositBeforeDate());
                hqlBuilder.append(depositDateConditionBefore);
            }

            if (searchExample.getDepositAfterDate() != null) {
                String depositDateConditionAfter = DaoUtils.andAttributeGreaterOrEquals("D", Document.PROPERTY_DEPOSIT_DATE, args, searchExample.getDepositAfterDate());
                hqlBuilder.append(depositDateConditionAfter);
            }

            hqlBuilder.append(" ) ");
        }


        // Manage keywords search in : title, summary, authors and keywords
        if (searchExample.getFullTextSearch() != null && !searchExample.getFullTextSearch().isEmpty()) {
            hqlBuilder.append(" AND ( 1 = 0 ");
            for (String keyword : searchExample.getFullTextSearch()) {
                String nameClause = DaoUtils.orAttributeLike("D", Document.PROPERTY_NAME, args, keyword);
                String summaryClause = DaoUtils.orAttributeLike("D", Document.PROPERTY_SUMMARY, args, keyword);
                String authorsClause = DaoUtils.orAttributeLike("D", Document.PROPERTY_AUTHORS, args, keyword);
                String containsKeyword = DaoUtils.orAttributeContains("D", Document.PROPERTY_KEYWORDS, args, keyword);

                hqlBuilder.append(nameClause);
                hqlBuilder.append(summaryClause);
                hqlBuilder.append(authorsClause);
                hqlBuilder.append(containsKeyword);
            }
            hqlBuilder.append(" )");
        }

        // Add the prefix for order clause
        searchExample.setOrderClause("D." + Document.PROPERTY_CITATION);

        PaginationParameter paginationParameter = searchExample.getPaginationParameter();

        List<String> citations = find(hqlBuilder.toString(), args, paginationParameter);

        return citations;
    }

    public List<String> findCitationsByDocumentFromKeywords(List<String> keywords) {

        StringBuilder hqlBuilder = new StringBuilder("SELECT DISTINCT(D." + Document.PROPERTY_CITATION + ") FROM " + Document.class.getName() + " D");
        Map<String, Object> args = new HashMap<>();

        if (keywords != null) {
            hqlBuilder.append(" WHERE ( 1 = 0 ");
            for (String keyword : keywords) {
                String nameClause = DaoUtils.orAttributeLike("D", Document.PROPERTY_NAME, args, keyword);
                String summaryClause = DaoUtils.orAttributeLike("D", Document.PROPERTY_SUMMARY, args, keyword);
                String authorsClause = DaoUtils.orAttributeLike("D", Document.PROPERTY_AUTHORS, args, keyword);
                String containsKeyword = DaoUtils.orAttributeContains("D", Document.PROPERTY_KEYWORDS, args, keyword);

                hqlBuilder.append(nameClause);
                hqlBuilder.append(summaryClause);
                hqlBuilder.append(authorsClause);
                hqlBuilder.append(containsKeyword);
            }
            hqlBuilder.append(" )");
        }

        String hql = hqlBuilder.toString();
        List<String> citations = findAll(hql, args);

        return citations;
    }

    public List<String> findCitationsByDocumentIds(List<String> documentIds) {

        StringBuilder hqlBuilder = new StringBuilder("SELECT DISTINCT(D." + Document.PROPERTY_CITATION + ") FROM " + Document.class.getName() + " D");
        Map<String, Object> args = new HashMap<>();

        if (documentIds != null && !documentIds.isEmpty()) {
            hqlBuilder.append(" WHERE ( 1 = 0 ");
            String documentIdsClause = DaoUtils.orAttributeIn("D", Document.PROPERTY_TOPIA_ID, args, documentIds);
            hqlBuilder.append(documentIdsClause);
            hqlBuilder.append(" )");
        }

        String hql = hqlBuilder.toString();
        List<String> citations = findAll(hql, args);

        return citations;
    }

    public List<String> findAllKeywords() {

        StringBuilder hqlBuilder =
            new StringBuilder("SELECT DISTINCT(keywords)"
                + " FROM " + Document.class.getName() + " D "
                + " INNER JOIN D." + Document.PROPERTY_KEYWORDS + " keywords ");

        List<String> values = findAll(hqlBuilder.toString());

        return values;
    }

    public List<String> findAllTypes() {

        StringBuilder hqlBuilder =
            new StringBuilder("SELECT DISTINCT(D. " + Document.PROPERTY_TYPE + ")"
                + " FROM " + Document.class.getName() + " D ");

        List<String> values = findAll(hqlBuilder.toString());

        return values;
    }

} //DocumentTopiaDao
