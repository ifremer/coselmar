package fr.ifremer.coselmar.persistence.entity;

/*
 * #%L
 * Coselmar :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import fr.ifremer.coselmar.beans.CloudWord;
import fr.ifremer.coselmar.beans.QuestionSearchExample;
import fr.ifremer.coselmar.config.CloudWordUtils;
import fr.ifremer.coselmar.persistence.DaoUtils;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.topia.persistence.TopiaQueryBuilderAddCriteriaOrRunQueryStep;
import org.nuiton.topia.persistence.support.TopiaSqlQuery;
import org.nuiton.util.pagination.PaginationParameter;
import org.nuiton.util.pagination.PaginationResult;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QuestionTopiaDao extends AbstractQuestionTopiaDao<Question> {

    public PaginationResult<Question> findForExpert(CoselmarUser expert, QuestionSearchExample questionSearchExample, PaginationParameter page) {

        StringBuilder hqlWithoutSelectBuilder = new StringBuilder(" FROM ").append(Question.class.getName()).append(" Q ")
            .append(" INNER JOIN Q.").append(Question.PROPERTY_PARTICIPANTS).append(" CUG ");

        Map<String, Object> args = new HashMap<>();

        String publicCondition = DaoUtils.getQueryForAttributeEquals("Q", Question.PROPERTY_PRIVACY, args, Privacy.PUBLIC, "");

        hqlWithoutSelectBuilder.append(" WHERE ( (").append(publicCondition).append(" ) ");

        String privateCondition = DaoUtils.getQueryForAttributeEquals("Q", Question.PROPERTY_PRIVACY, args, Privacy.PRIVATE, "");

        hqlWithoutSelectBuilder.append(" OR (").append(privateCondition);

        String participantCondition = DaoUtils.andAttributeContains("CUG", CoselmarUserGroup.PROPERTY_MEMBERS, args, expert);
        hqlWithoutSelectBuilder.append(participantCondition);

        String clientCondition = DaoUtils.andAttributeContains("Q", Question.PROPERTY_CLIENTS, args, expert);

        hqlWithoutSelectBuilder.append(clientCondition).append(") )");

        String finerHql = refineSearch(questionSearchExample, "Q", args);
        hqlWithoutSelectBuilder.append(" AND (").append(finerHql).append(")" );

        StringBuilder hqlBuilder = new StringBuilder("SELECT Q ").append(hqlWithoutSelectBuilder);

        PaginationResult<Question> questions;
        if (page != null) {
            long count = findUnique("SELECT count(Q." + Question.PROPERTY_TOPIA_ID + " ) " + hqlWithoutSelectBuilder.toString(), args);
            List<Question> questionList = forHql(hqlBuilder.toString(), args).find(page);
            questions = PaginationResult.of(questionList, count, page);
        } else {
            List<Question> allQuestions = forHql(hqlBuilder.toString(), args).findAll();
            questions = PaginationResult.of(allQuestions, allQuestions.size(), PaginationParameter.of(0, -1));
        }

        return questions;
    }

    public List<Question> findForExpert(CoselmarUser expert) {

        StringBuilder hqlBuilder = new StringBuilder("SELECT Q FROM " + Question.class.getName() + " Q "
            + " INNER JOIN Q." + Question.PROPERTY_PARTICIPANTS + " CUG ");

        Map<String, Object> args = new HashMap<>();

        String publicCondition = DaoUtils.getQueryForAttributeEquals("Q", Question.PROPERTY_PRIVACY, args, Privacy.PUBLIC, "");

        hqlBuilder.append(" WHERE ( (" + publicCondition + " ) ");

        String privateCondition = DaoUtils.getQueryForAttributeEquals("Q", Question.PROPERTY_PRIVACY, args, Privacy.PRIVATE, "");

        hqlBuilder.append(" OR (" + privateCondition);

        String participantCondition = DaoUtils.andAttributeContains("CUG", CoselmarUserGroup.PROPERTY_MEMBERS, args, expert);
        hqlBuilder.append(participantCondition);

        String clientCondition = DaoUtils.andAttributeContains("Q", Question.PROPERTY_CLIENTS, args, expert);

        hqlBuilder.append(clientCondition + ") )");

        List<Question> questions = forHql(hqlBuilder.toString(), args).findAll();

        return questions;
    }

    public PaginationResult<Question> findForExpert(CoselmarUser expert, List<String> topiaIds, PaginationParameter page) {

        StringBuilder hqlWithoutSelectBuilder = new StringBuilder(" FROM " + Question.class.getName() + " Q "
            + " INNER JOIN Q." + Question.PROPERTY_PARTICIPANTS + " CUG ");

        Map<String, Object> args = new HashMap<>();

        String publicCondition = DaoUtils.getQueryForAttributeEquals("Q", Question.PROPERTY_PRIVACY, args, Privacy.PUBLIC, "");

        hqlWithoutSelectBuilder.append(" WHERE ( (" + publicCondition + " ) ");

        String privateCondition = DaoUtils.getQueryForAttributeEquals("Q", Question.PROPERTY_PRIVACY, args, Privacy.PRIVATE, "");

        hqlWithoutSelectBuilder.append(" OR (" + privateCondition + " AND ( 0 = 1 ");

        String participantCondition = DaoUtils.orAttributeContains("CUG", CoselmarUserGroup.PROPERTY_MEMBERS, args, expert);
        hqlWithoutSelectBuilder.append(participantCondition);

        String clientCondition = DaoUtils.orAttributeContains("Q", Question.PROPERTY_CLIENTS, args, expert);

        hqlWithoutSelectBuilder.append(clientCondition + ") ) )");

        String topiaIdsCondition = DaoUtils.andAttributeIn("Q", Question.PROPERTY_TOPIA_ID, args, topiaIds);
        hqlWithoutSelectBuilder.append(topiaIdsCondition);

        StringBuilder hqlBuilder = new StringBuilder("SELECT Q ").append(hqlWithoutSelectBuilder);

        PaginationResult<Question> questions;
        if (page != null) {
            long count = findUnique("SELECT count(Q." + Question.PROPERTY_TOPIA_ID + " ) " + hqlWithoutSelectBuilder.toString(), args);
            List<Question> questionList = forHql(hqlBuilder.toString(), args).find(page);
            questions = PaginationResult.of(questionList, count, page);
        } else {
            List<Question> allQuestions = forHql(hqlBuilder.toString(), args).findAll();
            questions = PaginationResult.of(allQuestions, allQuestions.size(), PaginationParameter.of(0, -1));
        }

        return questions;
    }

    public PaginationResult<Question> findForClient(CoselmarUser client, QuestionSearchExample searchExample, PaginationParameter page) {

        StringBuilder hqlWithoutSelectBuilder = new StringBuilder(" FROM " + Question.class.getName() + " Q ");

        Map<String, Object> args = new HashMap<>();

        String clientCondition = DaoUtils.andAttributeContains("Q", Question.PROPERTY_CLIENTS, args, client);

        hqlWithoutSelectBuilder.append(" WHERE 1=1 " + clientCondition + " ");

        if (searchExample != null) {
            String finerHql = refineSearch(searchExample, "Q", args);
            hqlWithoutSelectBuilder.append(" AND (" + finerHql + ")" );
        }

        StringBuilder hqlBuilder = new StringBuilder("SELECT Q ").append(hqlWithoutSelectBuilder);

        PaginationResult<Question> questions;
        if (page != null) {
            long count = findUnique("SELECT count(Q." + Question.PROPERTY_TOPIA_ID + " ) " + hqlWithoutSelectBuilder.toString(), args);
            List<Question> questionList = forHql(hqlBuilder.toString(), args).find(page);
            questions = PaginationResult.of(questionList, count, page);
        } else {
            List<Question> allQuestions = forHql(hqlBuilder.toString(), args).findAll();
            questions = PaginationResult.of(allQuestions, allQuestions.size(), PaginationParameter.of(0, -1));
        }

        return questions;
    }

    public PaginationResult<Question> findForClient(CoselmarUser client, List<String> topiaIds, PaginationParameter page) {

        TopiaQueryBuilderAddCriteriaOrRunQueryStep<Question> queryBuilder = forTopiaIdIn(topiaIds);
        queryBuilder.addContains(Question.PROPERTY_CLIENTS, client);

        PaginationResult<Question> questions;
        if (page != null) {
            questions = queryBuilder.findPage(page);
        } else {
            List<Question> allQuestions = queryBuilder.findAll();
            questions = PaginationResult.of(allQuestions, allQuestions.size(), PaginationParameter.of(0, -1));
        }

        return questions;
    }

    public PaginationResult<Question> findWithSearchExample(QuestionSearchExample searchExample, PaginationParameter page) {

        StringBuilder hqlWithoutSelectBuilder = new StringBuilder(" FROM " + Question.class.getName() + " Q ");

        Map<String, Object> args = new HashMap<>();

        if (searchExample != null) {
            String finerHql = refineSearch(searchExample, "Q", args);
            hqlWithoutSelectBuilder.append(" WHERE (" + finerHql + ")" );
        }

        StringBuilder hqlBuilder = new StringBuilder("SELECT Q ").append(hqlWithoutSelectBuilder);

        PaginationResult<Question> questions;
        if (page != null) {
            long count = findUnique("SELECT count(Q." + Question.PROPERTY_TOPIA_ID + " ) " + hqlWithoutSelectBuilder.toString(), args);
            List<Question> questionList = forHql(hqlBuilder.toString(), args).find(page);
            questions = PaginationResult.of(questionList, count, page);
        } else {
            List<Question> allQuestions = forHql(hqlWithoutSelectBuilder.toString(), args).findAll();
            questions = PaginationResult.of(allQuestions, allQuestions.size(), PaginationParameter.of(0, -1));
        }

        return questions;
    }

    public List<String> findAllThemes() {

        StringBuilder hqlBuilder =
            new StringBuilder("SELECT DISTINCT(themes)"
                + " FROM " + Question.class.getName() + " Q "
                + " INNER JOIN Q." + Question.PROPERTY_THEME + " themes ");

        List<String> values = findAll(hqlBuilder.toString());

        return values;
    }

    public List<String> findAllTypes() {

        StringBuilder hqlBuilder =
            new StringBuilder("SELECT DISTINCT(Q. " + Question.PROPERTY_TYPE + ")"
                + " FROM " + Question.class.getName() + " Q ");

        List<String> values = findAll(hqlBuilder.toString());

        return values;
    }

    public List<CloudWord> findTopWords(String questionId) {

        forTopiaIdEquals(questionId).findAny();

        List<CloudWord> values = topiaSqlSupport.findMultipleResult(new QuestionTermStatSqlQuery(questionId));

        return values;
    }

    public String refineSearch(QuestionSearchExample searchExample, String alias, Map args) {

        StringBuilder finerHqlBuilder = new StringBuilder(" 1=1 ");

        Question example = searchExample.getExample();

        if (example != null) {

            Privacy privacy = example.getPrivacy();
            if (privacy != null) {
                String privacyHql = DaoUtils.andAttributeEquals(alias, Question.PROPERTY_PRIVACY, args, privacy);
                finerHqlBuilder.append(privacyHql);
            }

            Status status = example.getStatus();
            if (status != null) {
                String statusHql = DaoUtils.andAttributeEquals(alias, Question.PROPERTY_STATUS, args, status);
                finerHqlBuilder.append(statusHql);
            } else {
                String statusHql = DaoUtils.andAttributeNotEquals(alias, Question.PROPERTY_STATUS, args, Status.DELETED);
                finerHqlBuilder.append(statusHql);
            }

            String type = example.getType();
            if (StringUtils.isNotBlank(type)) {
                String statusHql = DaoUtils.andAttributeEquals(alias, Question.PROPERTY_TYPE, args, type);
                finerHqlBuilder.append(statusHql);
            }

            if (StringUtils.isNotBlank(example.getTitle())) {
                String titleHql = DaoUtils.andAttributeLike(alias, Question.PROPERTY_TITLE, args, example.getTitle());
                finerHqlBuilder.append(titleHql);
            }

            if (example.getTheme() != null && !example.getTheme().isEmpty()) {
                for (String keyword : example.getTheme()) {
                    String themesCondition = DaoUtils.andAttributeContains(alias, Question.PROPERTY_THEME, args, keyword);
                    finerHqlBuilder.append(themesCondition);
                }
            }

            // Manage date range
            if (searchExample.getSubmissionBeforeDate() != null) {
                String submissionDateConditionBefore = DaoUtils.andAttributeLesserOrEquals(alias, Question.PROPERTY_SUBMISSION_DATE, args, searchExample.getSubmissionBeforeDate());
                finerHqlBuilder.append(submissionDateConditionBefore);
            }

            if (searchExample.getSubmissionAfterDate() != null) {
                String submissionDateConditionAfter = DaoUtils.andAttributeGreaterOrEquals(alias, Question.PROPERTY_SUBMISSION_DATE, args, searchExample.getSubmissionAfterDate());
                finerHqlBuilder.append(submissionDateConditionAfter);
            }

            if (searchExample.getDeadlineBeforeDate() != null) {
                String deadlineConditionBefore = DaoUtils.andAttributeLesserOrEquals(alias, Question.PROPERTY_DEADLINE, args, searchExample.getDeadlineBeforeDate());
                finerHqlBuilder.append(deadlineConditionBefore);
            }

            if (searchExample.getDeadlineAfterDate() != null) {
                String deadlineDateConditionAfter = DaoUtils.andAttributeGreaterOrEquals(alias, Question.PROPERTY_DEADLINE, args, searchExample.getDeadlineAfterDate());
                finerHqlBuilder.append(deadlineDateConditionAfter);
            }

            // Manage wanted users
            if (searchExample.getParticipant() != null) {
                String participantCondition= DaoUtils.andAttributeContains(alias, Question.PROPERTY_PARTICIPANTS + "." + CoselmarUserGroup.PROPERTY_MEMBERS, args, searchExample.getParticipant());
                finerHqlBuilder.append(participantCondition);
            }

            if (example.isSupervisorsNotEmpty()) {
                String supervisorsCondition= DaoUtils.andAttributeContains(alias, Question.PROPERTY_SUPERVISORS, args, example.getSupervisors());
                finerHqlBuilder.append(supervisorsCondition);
            }

            if (example.isContributorsNotEmpty()) {
                String contributorCondition= DaoUtils.andAttributeContains(alias, Question.PROPERTY_CONTRIBUTORS, args, example.getContributors());
                finerHqlBuilder.append(contributorCondition);
            }

            if (example.isClientsNotEmpty()) {
                String clientCondition= DaoUtils.andAttributeContains(alias, Question.PROPERTY_CLIENTS, args, example.getClients());
                finerHqlBuilder.append(clientCondition);
            }

        }

        List<String> keywords = searchExample.getFullTextSearch();
        if (keywords != null && !keywords.isEmpty()) {
            // should be like : """ ( (1=1)
            // AND ( 1=0 OR title like keyword1 OR summary like keyword1 OR theme contains keyword1 )
            // AND ( 1=0 OR title like keyword2 OR summary like keyword2 OR theme contains keyword2 )
            // ) """
            StringBuilder keywordsBuilder = new StringBuilder(" ( ( 1 = 1 ) ");

            for (String keyword : keywords) {
                keywordsBuilder.append(" AND ( 1=0 ");
                keywordsBuilder.append(DaoUtils.orAttributeLike(alias, Question.PROPERTY_TITLE, args, keyword));
                keywordsBuilder.append(DaoUtils.orAttributeLike(alias, Question.PROPERTY_SUMMARY, args, keyword));
                keywordsBuilder.append(DaoUtils.orAttributeContains(alias, Question.PROPERTY_THEME, args, keyword));
                keywordsBuilder.append(" ) ");
            }

            keywordsBuilder.append(" ) ");
            String keywordHql = keywordsBuilder.toString();
            finerHqlBuilder.append(" AND " + keywordHql);
        }

        String finerHql = finerHqlBuilder.toString();
        return finerHql;
    }

    private static class QuestionTermStatSqlQuery extends TopiaSqlQuery<CloudWord> {

        private final String sql;

        private final String getSql(String questionId) {

            return  "SELECT word, nentry FROM ts_stat( ' " +
                   "  with documents as ( select d." + Document.PROPERTY_NAME + " as name, d." + Document.PROPERTY_SUMMARY + " as summary, d." + Document.PROPERTY_FILE_CONTENT + " as fileContent, (SELECT COALESCE(string_agg(dk." + Document.PROPERTY_KEYWORDS + ", '' ''),'''') from document_keywords dk where dk.owner = d.topiaid) as keywords " +
                   "                      from question k " +
                   "                      LEFT JOIN relateddocuments_relatedquestion RD ON RD.relatedquestion = k.topiaid " +
                   "                      LEFT JOIN closingdocuments_relatedquestion CD ON CD.relatedquestion = k.topiaid " +
                   "                      LEFT JOIN document d on d.topiaid = CD.closingdocuments OR d.topiaid = RD.relateddocuments " +
                   "                      WHERE k.topiaid = ''" + questionId + "'' ) " +
                   "  SELECT  to_tsvector(''public.simple_english_conf'', q." + Question.PROPERTY_TITLE + ") " +
                   "          || to_tsvector(''public.simple_english_conf'', q." + Question.PROPERTY_SUMMARY + ") " +
                   "          || to_tsvector(''public.simple_english_conf'', (SELECT COALESCE(string_agg(qt." + Question.PROPERTY_THEME + ", '' ''),'''') from question_theme qt where qt.owner = q.topiaid ) ) " +
                   "          || to_tsvector(''public.simple_english_conf'', (SELECT COALESCE(string_agg(name, '' ''),'''' ) FROM documents) ) " +
                   "          || to_tsvector(''public.simple_english_conf'', (SELECT COALESCE(string_agg(summary, '' ''),'''' ) FROM documents) ) " +
                   "          || to_tsvector(''public.simple_english_conf'', (SELECT COALESCE(string_agg(keywords, '' ''),'''' ) FROM documents) ) " +
                   "          || to_tsvector(''public.simple_english_conf'', (SELECT COALESCE(string_agg(fileContent, '' ''),'''' ) FROM documents)) " +
                   "  FROM question q where q.topiaid = ''" + questionId + "''  ') " +
                   "  WHERE char_length(word) > " + CloudWordUtils.CLOUD_TAG_WORD_MIN_SIZE +
                   "  AND word NOT IN ( " + Joiner.on(',').join(Lists.transform(CloudWordUtils.MANUAL_EXCLUDED_TERMS_IN_CLOUD, CloudWordUtils.SQLIFY_STRING)) + " ) " +
                   "  ORDER BY nentry DESC ";
        }

        QuestionTermStatSqlQuery(String questionId) {
            this.sql = getSql(questionId);
        }

        @Override
        public PreparedStatement prepareQuery(Connection connection) throws SQLException {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            return preparedStatement;
        }

        @Override
        public CloudWord prepareResult(ResultSet set) throws SQLException {

            CloudWord cloudWord = new CloudWord(set.getString(1), set.getLong(2));

            return cloudWord;

        }
    }
} //QuestionTopiaDao
