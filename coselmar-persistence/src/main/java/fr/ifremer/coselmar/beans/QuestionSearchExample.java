package fr.ifremer.coselmar.beans;

/*
 * #%L
 * Coselmar :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coselmar.persistence.entity.CoselmarUser;
import fr.ifremer.coselmar.persistence.entity.Question;

import java.util.Date;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class QuestionSearchExample extends SearchExample<Question> {

    private static final long serialVersionUID = -3318003570685481073L;

    public static final QuestionSearchExample newDefaultSearchExample() {
        QuestionSearchExample searchExample = new QuestionSearchExample();
        searchExample.setPage(DEFAULT_PAGE);
        searchExample.setLimit(DEFAULT_PAGE_SIZE);
        searchExample.setOrderClause(Question.PROPERTY_SUBMISSION_DATE);
        searchExample.setOrderDesc(true);
        return searchExample;
    }

    protected Date submissionAfterDate;

    protected Date submissionBeforeDate;

    protected Date deadlineAfterDate;

    protected Date deadlineBeforeDate;

    protected String orderClause;

    protected boolean orderDesc;

    protected CoselmarUser participant;

    public Date getSubmissionAfterDate() {
        return submissionAfterDate != null ? new Date(submissionAfterDate.getTime()) : null;
    }

    public void setSubmissionAfterDate(Date submissionAfterDate) {
        if (submissionAfterDate != null) {
            this.submissionAfterDate = new Date(submissionAfterDate.getTime());
        } else {
            this.submissionAfterDate = null;
        }
    }

    public Date getSubmissionBeforeDate() {
        return submissionBeforeDate != null ? new Date(submissionBeforeDate.getTime()) : null;
    }

    public void setSubmissionBeforeDate(Date submissionBeforeDate) {
        if (submissionBeforeDate != null) {
            this.submissionBeforeDate = new Date(submissionBeforeDate.getTime());
        } else {
            this.submissionBeforeDate = null;
        }
    }

    public Date getDeadlineAfterDate() {
        return deadlineAfterDate != null ? new Date(deadlineAfterDate.getTime()) : null;
    }

    public void setDeadlineAfterDate(Date deadlineAfterDate) {
        if (deadlineAfterDate != null) {
            this.deadlineAfterDate = new Date(deadlineAfterDate.getTime());
        } else {
            this.deadlineAfterDate = null;
        }
    }

    public Date getDeadlineBeforeDate() {
        return deadlineBeforeDate != null ? new Date(deadlineBeforeDate.getTime()) : null;
    }

    public void setDeadlineBeforeDate(Date deadlineBeforeDate) {
        if (deadlineBeforeDate != null) {
            this.deadlineBeforeDate = new Date(deadlineBeforeDate.getTime());
        } else {
            this.deadlineBeforeDate = null;
        }
    }

    @Override
    public String getOrderClause() {
        return orderClause;
    }

    public void setOrderClause(String orderClause) {
        this.orderClause = orderClause;
    }

    @Override
    public boolean isOrderDesc() {
        return orderDesc;
    }

    public void setOrderDesc(boolean orderDesc) {
        this.orderDesc = orderDesc;
    }

    public CoselmarUser getParticipant() {
        return participant;
    }

    public void setParticipant(CoselmarUser participant) {
        this.participant = participant;
    }
}
