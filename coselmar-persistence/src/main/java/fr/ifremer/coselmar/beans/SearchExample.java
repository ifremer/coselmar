package fr.ifremer.coselmar.beans;

/*
 * #%L
 * Coselmar :: Persistence
 * %%
 * Copyright (C) 2014 - 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.nuiton.topia.persistence.TopiaEntity;
import org.nuiton.util.pagination.PaginationParameter;

import java.io.Serializable;
import java.util.List;

/**
 * @author ymartel (martel@codelutin.com)
 */
public abstract class SearchExample<E extends TopiaEntity> implements Serializable {

    private static final long serialVersionUID = 4024861043438299258L;

    public static final int DEFAULT_PAGE_SIZE = -1;
    public static final int DEFAULT_PAGE = 0;

    public PaginationParameter getPaginationParameter() {

        PaginationParameter paginationParameter = null;
        if (this.limit != null) {
            int wantedPage = this.page != null ? this.page.intValue() : 0;
            paginationParameter = PaginationParameter.of(wantedPage, this.limit.intValue(), getOrderClause(), isOrderDesc());
        } else {
            paginationParameter = PaginationParameter.of(DEFAULT_PAGE, DEFAULT_PAGE_SIZE, getOrderClause(), isOrderDesc());
        }

        return paginationParameter;
    }

    protected E example;

    protected List<String> fullTextSearch;

    // Pagination Parameters
    protected Integer limit;
    protected Integer page;

    public E getExample() {
        return example;
    }

    public void setExample(E example) {
        this.example = example;
    }

    public List<String> getFullTextSearch() {
        return fullTextSearch;
    }

    public void setFullTextSearch(List<String> fullTextSearch) {
        this.fullTextSearch = fullTextSearch;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        if (limit == null) {
            this.limit = DEFAULT_PAGE_SIZE;
        } else {
            this.limit = limit;
        }
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        if (page == null) {
            this.page = DEFAULT_PAGE;
        } else {
            this.page = page;
        }
    }

    public abstract String getOrderClause();
    public abstract boolean isOrderDesc();
}
