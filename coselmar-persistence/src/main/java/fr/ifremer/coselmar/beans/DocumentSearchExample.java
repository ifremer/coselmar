package fr.ifremer.coselmar.beans;

/*
 * #%L
 * Coselmar :: Persistence
 * %%
 * Copyright (C) 2014 - 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coselmar.persistence.entity.Document;

import java.util.Date;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class DocumentSearchExample extends SearchExample<Document> {

    public static final DocumentSearchExample newDefaultSearchExample() {
        DocumentSearchExample searchExample = new DocumentSearchExample();
        searchExample.setPage(DEFAULT_PAGE);
        searchExample.setLimit(DEFAULT_PAGE_SIZE);
        searchExample.setOrderClause(Document.PROPERTY_DEPOSIT_DATE);
        searchExample.setOrderDesc(true);
        return searchExample;
    }

    protected Date publicationAfterDate;

    protected Date publicationBeforeDate;

    protected Date depositAfterDate;

    protected Date depositBeforeDate;

    protected String ownerName;

    protected String orderClause;

    protected boolean orderDesc;

    public Date getPublicationAfterDate() {
        return publicationAfterDate != null ? new Date(publicationAfterDate.getTime()): null;
    }

    public void setPublicationAfterDate(Date publicationAfterDate) {
        if (publicationAfterDate != null) {
            this.publicationAfterDate = new Date(publicationAfterDate.getTime());
        } else {
            this.publicationAfterDate = null;
        }
    }

    public Date getPublicationBeforeDate() {
        return publicationBeforeDate != null ? new Date(publicationBeforeDate.getTime()) : null;
    }

    public void setPublicationBeforeDate(Date publicationBeforeDate) {
        if (publicationBeforeDate != null) {
            this.publicationBeforeDate = new Date(publicationBeforeDate.getTime());
        } else {
            this.publicationBeforeDate = null;
        }
    }

    public Date getDepositAfterDate() {
        return depositAfterDate != null ? new Date(depositAfterDate.getTime()) : null;
    }

    public void setDepositAfterDate(Date depositAfterDate) {
        if (depositAfterDate != null) {
            this.depositAfterDate = new Date(depositAfterDate.getTime());
        } else {
            this.depositAfterDate = null;
        }
    }

    public Date getDepositBeforeDate() {
        return depositBeforeDate != null ? new Date(depositBeforeDate.getTime()) : null;
    }

    public void setDepositBeforeDate(Date depositBeforeDate) {
        if (depositBeforeDate != null) {
            this.depositBeforeDate = new Date(depositBeforeDate.getTime());
        } else {
            this.depositBeforeDate = null;
        }
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    @Override
    public String getOrderClause() {
        return orderClause;
    }

    public void setOrderClause(String orderClause) {
        this.orderClause = orderClause;
    }

    @Override
    public boolean isOrderDesc() {
        return orderDesc;
    }

    public void setOrderDesc(boolean orderDesc) {
        this.orderDesc = orderDesc;
    }
}
