package fr.ifremer;

/*
 * #%L
 * Coselmar :: Persistence
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 - 2015 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coselmar.persistence.CoselmarTopiaPersistenceContext;
import fr.ifremer.coselmar.persistence.entity.Document;
import fr.ifremer.coselmar.persistence.entity.DocumentTopiaDao;
import fr.ifremer.coselmar.persistence.entity.Privacy;
import fr.ifremer.coselmar.persistence.entity.Question;
import fr.ifremer.coselmar.persistence.entity.QuestionTopiaDao;
import fr.ifremer.coselmar.persistence.entity.Status;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author ymartel <martel@codelutin.com>
 */
public class DocumentDaoTest extends AbstractCoselmarDaoTest {

    @Test
    public void testQuestionDocumentDelete() throws Exception {

        CoselmarTopiaPersistenceContext persistenceContext = application.newPersistenceContext();
        DocumentTopiaDao documentDao = persistenceContext.getDocumentDao();

        // Create two basic document
        Document firstDocument = documentDao.create();
        firstDocument.setName("My Awesome Test");
        firstDocument.setPrivacy(Privacy.PUBLIC);
        firstDocument.setSummary("Oh really, a summary?");

        Document secondDocument = documentDao.create();
        secondDocument.setName("My Awesome Test");
        secondDocument.setPrivacy(Privacy.PUBLIC);
        secondDocument.setSummary("Oh really, a summary?");

        persistenceContext.commit();

        // Create a question with same persistence context
        Question firstQuestion = persistenceContext.getQuestionDao().create();
        firstQuestion.setTitle("Is this a question?");
        firstQuestion.setStatus(Status.OPEN);
        firstQuestion.setSummary("We have to ask some question");
        firstQuestion.setPrivacy(Privacy.PUBLIC);
        firstQuestion.addRelatedDocuments(firstDocument);

        persistenceContext.commit();

        // Now, try to delete the first document
        documentDao.delete(firstDocument);
        persistenceContext.commit(); // Here we have no error


        // Create an other question linking the document thanks to an other persistance context
        CoselmarTopiaPersistenceContext secondPersistenceContext = application.newPersistenceContext();
        QuestionTopiaDao questionDao = secondPersistenceContext.getQuestionDao();

        Question secondQuestion = questionDao.create();
        secondQuestion.setTitle("Is this a question?");
        secondQuestion.setStatus(Status.OPEN);
        secondQuestion.setSummary("We have to ask some question");
        secondQuestion.setPrivacy(Privacy.PUBLIC);
        secondQuestion.addRelatedDocuments(secondDocument);

        secondPersistenceContext.commit();

        // Now, try to delete the document
        documentDao.delete(secondDocument); // Here we have error
        persistenceContext.commit();

        secondPersistenceContext.getHibernateSupport().getHibernateSession().clear();

        Question reloadQuestion = questionDao.forTopiaIdEquals(secondQuestion.getTopiaId()).findAny();
        Assert.assertTrue(reloadQuestion.getRelatedDocuments().isEmpty());
    }

}
