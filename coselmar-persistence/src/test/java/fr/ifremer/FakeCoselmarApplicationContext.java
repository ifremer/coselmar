package fr.ifremer;

/*
 * #%L
 * Coselmar :: Rest Services
 * $Id:$
 * $HeadURL:$
 * %%
 * Copyright (C) 2014 Ifremer, Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import fr.ifremer.coselmar.persistence.CoselmarTopiaApplicationContext;
import fr.ifremer.coselmar.persistence.CoselmarTopiaPersistenceContext;
import org.apache.commons.logging.Log;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.TopiaConfigurationBuilder;

import java.util.LinkedList;
import java.util.List;

import static org.apache.commons.logging.LogFactory.getLog;


/**
 * @author ymartel <martel@codelutin.com>
 */
public class FakeCoselmarApplicationContext extends TestWatcher {

    private static Log log = getLog(FakeCoselmarApplicationContext.class);

    protected List<CoselmarTopiaPersistenceContext> openedTransactions = new LinkedList<>();

    protected CoselmarTopiaApplicationContext applicationContext;

    protected TopiaConfiguration topiaConfiguration;

    protected final String configurationPath;

    public FakeCoselmarApplicationContext(String configurationPath) {
        this.configurationPath = configurationPath;
    }

    @Override
    protected void starting(Description description) {

        TopiaConfigurationBuilder topiaConfigurationBuilder = new TopiaConfigurationBuilder();
        topiaConfiguration = topiaConfigurationBuilder.forTestDatabase(description.getTestClass(), description.getMethodName()).onlyCreateSchemaIfDatabaseIsEmpty().doNotValidateSchemaOnStartup().build();

        applicationContext = new CoselmarTopiaApplicationContext(topiaConfiguration);

    }

    @Override
    public void finished(Description description) {

        close();
    }

    public void close() {

        if (applicationContext != null && !applicationContext.isClosed()) {

            for (CoselmarTopiaPersistenceContext openedTransaction : openedTransactions) {

                if (log.isTraceEnabled()) {
                    log.trace("closing transaction " + openedTransaction);
                }

                if (!openedTransaction.isClosed()) {

                    openedTransaction.close();

                }

            }

            if (log.isTraceEnabled()) {
                log.trace("closing transaction " + applicationContext);
            }

            applicationContext.close();

        }
    }

    public CoselmarTopiaApplicationContext getTopiaApplicationContext() {
        return applicationContext;
    }

    public CoselmarTopiaPersistenceContext newPersistenceContext() {

        CoselmarTopiaPersistenceContext persistenceContext;

        persistenceContext = applicationContext.newPersistenceContext();

        if (log.isTraceEnabled()) {
            log.trace("opened transaction " + persistenceContext);
        }

        openedTransactions.add(persistenceContext);

        return persistenceContext;

    }

}
